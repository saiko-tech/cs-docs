64 CDynamicProp
 m_bAnimatedEveryTick: bool
 m_nEnablePhysics: uint8
 m_flDecalHealBloodRate: float32
 m_bUseHitboxesForRenderBox: bool
 m_flCreateTime: GameTime_t
 m_LightGroup: CUtlStringToken
 m_nCollisionFunctionMask: uint8
 m_usSolidFlags: uint8
 m_nObjectCulling: uint8
 CBodyComponent: CBodyComponent
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nBoolVariablesCount: int32
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_nOutsideWorld: uint16
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
 m_flNavIgnoreUntilTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_flFadeScale: float32
 m_flShadowStrength: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flElasticity: float32
 m_nInteractsExclude: uint64
 m_nSurroundType: SurroundingBoundsType_t
 m_flAnimTime: float32
 m_flSimulationTime: float32
 m_bClientSideRagdoll: bool
 m_fEffects: uint32
 m_bRenderToCubemaps: bool
 m_nAddDecal: int32
 m_vDecalForwardAxis: Vector
 m_MoveCollide: MoveCollide_t
 m_MoveType: MoveType_t
 m_nOwnerId: uint32
 m_nCollisionGroup: uint8
 m_vCapsuleCenter2: Vector
 m_vDecalPosition: Vector
 m_noGhostCollision: bool
 m_nInteractsWith: uint64
 m_vecSpecifiedSurroundingMins: Vector
 m_vCapsuleCenter1: Vector
 m_flCapsuleRadius: float32
 m_fadeMinDist: float32
 m_flDecalHealHeightRate: float32
 m_bvDisabledHitGroups: uint32[1]
 m_nSubclassID: CUtlStringToken
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_triggerBloat: uint8
 m_fadeMaxDist: float32
 m_nRenderFX: RenderFx_t
 m_nEntityId: uint32
 m_CollisionGroup: uint8
 m_iGlowTeam: int32
 m_nForceBone: int32
 m_bSimulatedEveryTick: bool
 m_bAnimGraphUpdateEnabled: bool
 m_bClientRagdoll: bool
 m_ubInterpolationFrame: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_iGlowType: int32
 m_bEligibleForScreenHighlight: bool
 m_nGlowRangeMin: int32
 m_bUseAnimGraph: bool
 m_iTeamNum: uint8
 m_nHierarchyId: uint16
 m_flGlowStartTime: float32
 m_flGlowBackfaceMult: float32
 m_vecForce: Vector
 m_clrRender: Color
 m_bFlashing: bool
 CRenderComponent: CRenderComponent
 m_flGlowTime: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_nInteractsAs: uint64
 m_vecMins: Vector
 m_vecMaxs: Vector
 m_nSolidType: SolidType_t
 m_nGlowRange: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_glowColorOverride: Color
 m_bInitiallyPopulateInterpHistory: bool
 m_bShouldAnimateDuringGameplayPause: bool

115 CInfoWorldLayer
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_worldName: CUtlSymbolLarge
 m_bWorldLayerVisible: bool
 m_flAnimTime: float32
 m_ubInterpolationFrame: uint8
 m_bSimulatedEveryTick: bool
 m_MoveType: MoveType_t
 m_nSubclassID: CUtlStringToken
 m_flCreateTime: GameTime_t
 m_iTeamNum: uint8
 m_flSimulationTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 CBodyComponent: CBodyComponent
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_layerName: CUtlSymbolLarge
 m_bEntitiesSpawned: bool
 m_MoveCollide: MoveCollide_t
 m_flElasticity: float32
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t

144 CPlantedC4
 m_nCollisionGroup: uint8
 m_glowColorOverride: Color
 m_bClientRagdoll: bool
 m_bSpotted: bool
 m_flAnimTime: float32
 m_flSimulationTime: float32
 m_flElasticity: float32
 m_vCapsuleCenter1: Vector
 m_nGlowRangeMin: int32
 m_bEligibleForScreenHighlight: bool
 m_bCannotBeDefused: bool
 m_iTeamNum: uint8
 m_nEntityId: uint32
 m_bAnimGraphUpdateEnabled: bool
 m_bHasExploded: bool
 m_bBombDefused: bool
 m_bvDisabledHitGroups: uint32[1]
 m_bInitiallyPopulateInterpHistory: bool
 CRenderComponent: CRenderComponent
 m_MoveCollide: MoveCollide_t
 m_nInteractsAs: uint64
 m_nGlowRange: int32
 m_nBombSite: int32
 m_nAddDecal: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_bClientSideRagdoll: bool
 m_fEffects: uint32
 m_nInteractsWith: uint64
 m_vecMins: Vector
 m_nEnablePhysics: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bShouldAnimateDuringGameplayPause: bool
 m_bBeingDefused: bool
 m_flDefuseLength: float32
 CBodyComponent: CBodyComponent
  m_nBoolVariablesCount: int32
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_cellY: uint16
  m_flScale: float32
  m_vecY: CNetworkedQuantizedFloat
  m_nNewSequenceParity: int32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_vecX: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_cellX: uint16
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_flPrevCycle: float32
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_flLastTeleportTime: float32
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nAnimLoopMode: AnimLoopMode_t
 m_LightGroup: CUtlStringToken
 m_nInteractsExclude: uint64
 m_nCollisionFunctionMask: uint8
 m_usSolidFlags: uint8
 m_vCapsuleCenter2: Vector
 m_bBombTicking: bool
 m_nObjectCulling: uint8
 m_hControlPanel: CHandle< CBaseEntity >
 m_flNavIgnoreUntilTime: GameTime_t
 m_nRenderFX: RenderFx_t
 m_flGlowStartTime: float32
 m_flDecalHealBloodRate: float32
 m_flDecalHealHeightRate: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_flTimerLength: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bSimulatedEveryTick: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSubclassID: CUtlStringToken
 m_flCapsuleRadius: float32
 m_iGlowType: int32
 m_nSourceSoundscapeHash: int32
 m_flDefuseCountDown: GameTime_t
 m_hBombDefuser: CHandle< CCSPlayerPawnBase >
 m_bSpottedByMask: uint32[2]
 m_ubInterpolationFrame: uint8
 m_bAnimatedEveryTick: bool
 m_nOwnerId: uint32
 m_flFadeScale: float32
 m_flShadowStrength: float32
 m_vDecalPosition: Vector
 m_flC4Blow: GameTime_t
 m_MoveType: MoveType_t
 m_bRenderToCubemaps: bool
 m_vecSpecifiedSurroundingMins: Vector
 m_flCreateTime: GameTime_t
 m_clrRender: Color
 m_nHierarchyId: uint16
 m_nSolidType: SolidType_t
 m_nSurroundType: SurroundingBoundsType_t
 m_nForceBone: int32
 m_vecForce: Vector
 m_nRenderMode: RenderMode_t
 m_vecMaxs: Vector
 m_triggerBloat: uint8
 m_iGlowTeam: int32
 m_bFlashing: bool
 m_flGlowTime: float32
 m_vDecalForwardAxis: Vector
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_CollisionGroup: uint8
 m_flGlowBackfaceMult: float32
 m_fadeMinDist: float32
 m_fadeMaxDist: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >

30 CC4
 m_nDropTick: GameTick_t
 m_flElasticity: float32
 m_nFallbackSeed: int32
 m_fadeMaxDist: float32
 m_flFadeScale: float32
 m_iItemIDLow: uint32
 m_weaponMode: CSWeaponMode
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_hOuter: CHandle< CBaseEntity >
 m_nForceBone: int32
 m_iReapplyProvisionParity: int32
 m_iAccountID: uint32
 m_iState: WeaponState_t
 m_nFireSequenceStartTimeChange: int32
 m_bIsHauledBack: bool
 m_flAnimTime: float32
 m_bClientSideRagdoll: bool
 m_fArmedTime: GameTime_t
 m_flCapsuleRadius: float32
 m_fAccuracyPenalty: float32
 m_iRecoilIndex: int32
 m_fLastShotTime: GameTime_t
 m_nCollisionGroup: uint8
 m_vCapsuleCenter2: Vector
 m_flPostponeFireReadyTime: GameTime_t
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_vCapsuleCenter1: Vector
 m_nEntityId: uint32
 m_flFallbackWear: float32
 m_flDroppedAtTime: GameTime_t
 m_triggerBloat: uint8
 m_bFlashing: bool
 m_bSilencerOn: bool
 m_flNextSecondaryAttackTickRatio: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_bBurstMode: bool
 m_ubInterpolationFrame: uint8
 m_bPlayerFireEventIsPrimary: bool
 m_iClip1: int32
 m_flCreateTime: GameTime_t
 m_nNextPrimaryAttackTick: GameTick_t
 m_iGlowTeam: int32
 m_bInitialized: bool
 m_bAnimatedEveryTick: bool
 m_nRenderMode: RenderMode_t
 m_nFallbackPaintKit: int32
 CBodyComponent: CBodyComponent
  m_angRotation: QAngle
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
 m_MoveCollide: MoveCollide_t
 m_vLookTargetPosition: Vector
 m_nHierarchyId: uint16
 m_CollisionGroup: uint8
 m_bAnimGraphUpdateEnabled: bool
 m_flFireSequenceStartTime: float32
 m_nObjectCulling: uint8
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_iEntityLevel: uint32
 m_iTeamNum: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_nEnablePhysics: uint8
 m_nAddDecal: int32
 m_vecForce: Vector
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsAs: uint64
 m_iOriginalTeamNumber: int32
 m_fEffects: uint32
 m_flShadowStrength: float32
 m_nSurroundType: SurroundingBoundsType_t
 m_bShowC4LED: bool
 m_bSpottedByMask: uint32[2]
 m_flNextPrimaryAttackTickRatio: float32
 m_nViewModelIndex: uint32
 m_bSimulatedEveryTick: bool
 m_LightGroup: CUtlStringToken
 m_vDecalPosition: Vector
 m_nFallbackStatTrak: int32
 m_bReloadVisuallyComplete: bool
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_bStartedArming: bool
 m_bvDisabledHitGroups: uint32[1]
 m_MoveType: MoveType_t
 m_vecMaxs: Vector
 m_vDecalForwardAxis: Vector
 m_OriginalOwnerXuidLow: uint32
 m_iClip2: int32
 m_clrRender: Color
 m_nSolidType: SolidType_t
 m_glowColorOverride: Color
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_nNextThinkTick: GameTick_t
 m_nSubclassID: CUtlStringToken
 m_usSolidFlags: uint8
 m_bIsPlantingViaUse: bool
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bBombPlacedAnimation: bool
 m_flGlowStartTime: float32
 m_flDecalHealBloodRate: float32
 m_flDecalHealHeightRate: float32
 CRenderComponent: CRenderComponent
 m_nCollisionFunctionMask: uint8
 m_vecMins: Vector
 m_vecSpecifiedSurroundingMins: Vector
 m_iGlowType: int32
 m_iItemIDHigh: uint32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_nInteractsWith: uint64
 m_nOwnerId: uint32
 m_nGlowRange: int32
 m_nGlowRangeMin: int32
 m_iItemDefinitionIndex: uint16
 m_szCustomName: char[161]
 m_OriginalOwnerXuidHigh: uint32
 m_flRecoilIndex: float32
 m_pReserveAmmo: int32[2]
 m_bClientRagdoll: bool
 m_iInventoryPosition: uint32
 m_bEligibleForScreenHighlight: bool
 m_fadeMinDist: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_iIronSightMode: int32
 m_bSpotted: bool
 m_nNextSecondaryAttackTick: GameTick_t
 m_flSimulationTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nInteractsExclude: uint64
 m_ProviderType: attributeprovidertypes_t
 m_iEntityQuality: int32
 m_iNumEmptyAttacks: int32
 m_nRenderFX: RenderFx_t
 m_bRenderToCubemaps: bool
 m_flGlowBackfaceMult: float32
 m_bInReload: bool
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flGlowTime: float32

104 CHEGrenade
 m_MoveCollide: MoveCollide_t
 m_weaponMode: CSWeaponMode
 m_bPinPulled: bool
 m_nNextPrimaryAttackTick: GameTick_t
 m_iInventoryPosition: uint32
 m_iOriginalTeamNumber: int32
 m_fLastShotTime: GameTime_t
 m_bRedraw: bool
 m_bClientSideRagdoll: bool
 m_triggerBloat: uint8
 m_iGlowType: int32
 m_vDecalForwardAxis: Vector
 m_iClip2: int32
 m_iTeamNum: uint8
 m_flDroppedAtTime: GameTime_t
 m_iEntityLevel: uint32
 m_nFallbackStatTrak: int32
 m_flNavIgnoreUntilTime: GameTime_t
 m_clrRender: Color
 m_nInteractsAs: uint64
 m_bClientRagdoll: bool
 m_iAccountID: uint32
 m_iRecoilIndex: int32
 CBodyComponent: CBodyComponent
  m_angRotation: QAngle
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_nNewSequenceParity: int32
  m_bClientSideAnimation: bool
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_hParent: CGameSceneNodeHandle
  m_nRandomSeedOffset: int32
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
 m_nHierarchyId: uint16
 m_fadeMaxDist: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_vecSpecifiedSurroundingMins: Vector
 m_nNextThinkTick: GameTick_t
 m_vCapsuleCenter2: Vector
 m_iGlowTeam: int32
 m_nFireSequenceStartTimeChange: int32
 m_flPostponeFireReadyTime: GameTime_t
 m_vLookTargetPosition: Vector
 m_bvDisabledHitGroups: uint32[1]
 m_nSubclassID: CUtlStringToken
 m_nEntityId: uint32
 m_flDecalHealBloodRate: float32
 m_flDecalHealHeightRate: float32
 m_bRenderToCubemaps: bool
 m_glowColorOverride: Color
 m_flFadeScale: float32
 m_eThrowStatus: EGrenadeThrowState
 m_ubInterpolationFrame: uint8
 m_ProviderType: attributeprovidertypes_t
 m_bJumpThrow: bool
 m_flElasticity: float32
 m_flShadowStrength: float32
 m_bInitialized: bool
 m_bIsHauledBack: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_iReapplyProvisionParity: int32
 m_iEntityQuality: int32
 m_flFallbackWear: float32
 m_OriginalOwnerXuidHigh: uint32
 m_bPlayerFireEventIsPrimary: bool
 m_bSilencerOn: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveType: MoveType_t
 m_bEligibleForScreenHighlight: bool
 m_flFireSequenceStartTime: float32
 m_vCapsuleCenter1: Vector
 m_fadeMinDist: float32
 m_flNextPrimaryAttackTickRatio: float32
 m_flSimulationTime: float32
 m_nCollisionFunctionMask: uint8
 m_nGlowRange: int32
 m_vecForce: Vector
 m_iItemIDHigh: uint32
 m_bInReload: bool
 m_flThrowStrengthApproach: float32
 m_vecMaxs: Vector
 m_usSolidFlags: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flCapsuleRadius: float32
 m_flCreateTime: GameTime_t
 m_nCollisionGroup: uint8
 m_nNextSecondaryAttackTick: GameTick_t
 m_LightGroup: CUtlStringToken
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_iNumEmptyAttacks: int32
 m_iIronSightMode: int32
 m_bIsHeldByPlayer: bool
 m_flAnimTime: float32
 m_nInteractsExclude: uint64
 m_vDecalPosition: Vector
 m_fAccuracyPenalty: float32
 m_nSurroundType: SurroundingBoundsType_t
 m_nForceBone: int32
 m_nDropTick: GameTick_t
 m_nViewModelIndex: uint32
 m_nRenderMode: RenderMode_t
 m_flThrowStrength: float32
 m_pReserveAmmo: int32[2]
 m_nSolidType: SolidType_t
 m_flGlowTime: float32
 m_nAddDecal: int32
 m_fThrowTime: GameTime_t
 m_bAnimGraphUpdateEnabled: bool
 m_nOwnerId: uint32
 m_nEnablePhysics: uint8
 m_bFlashing: bool
 m_flGlowBackfaceMult: float32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_szCustomName: char[161]
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nObjectCulling: uint8
 m_bInitiallyPopulateInterpHistory: bool
 m_flRecoilIndex: float32
 m_iItemIDLow: uint32
 m_nFallbackPaintKit: int32
 m_nFallbackSeed: int32
 m_iState: WeaponState_t
 m_CollisionGroup: uint8
 m_nGlowRangeMin: int32
 m_flGlowStartTime: float32
 m_iItemDefinitionIndex: uint16
 m_flNextSecondaryAttackTickRatio: float32
 m_iClip1: int32
 m_vecMins: Vector
 m_OriginalOwnerXuidLow: uint32
 CRenderComponent: CRenderComponent
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nRenderFX: RenderFx_t
 m_fDropTime: GameTime_t
 m_bSimulatedEveryTick: bool
 m_nInteractsWith: uint64
 m_bReloadVisuallyComplete: bool
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_hOuter: CHandle< CBaseEntity >
 m_bBurstMode: bool

213 CWeaponMAC10
 m_iItemDefinitionIndex: uint16
 m_iAccountID: uint32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_MoveCollide: MoveCollide_t
 m_nEnablePhysics: uint8
 m_flGlowBackfaceMult: float32
 CBodyComponent: CBodyComponent
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
 m_pReserveAmmo: int32[2]
 m_flFadeScale: float32
 m_hOuter: CHandle< CBaseEntity >
 m_flFireSequenceStartTime: float32
 m_bNeedsBoltAction: bool
 m_bRenderToCubemaps: bool
 m_iItemIDLow: uint32
 m_flPostponeFireReadyTime: GameTime_t
 m_nHierarchyId: uint16
 m_bClientRagdoll: bool
 m_bBurstMode: bool
 m_iBurstShotsRemaining: int32
 m_nSubclassID: CUtlStringToken
 m_ubInterpolationFrame: uint8
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_fEffects: uint32
 m_fadeMinDist: float32
 m_nObjectCulling: uint8
 m_nFireSequenceStartTimeChange: int32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_flNextPrimaryAttackTickRatio: float32
 m_nOwnerId: uint32
 m_nCollisionFunctionMask: uint8
 m_nFallbackStatTrak: int32
 m_nSolidType: SolidType_t
 m_CollisionGroup: uint8
 m_iInventoryPosition: uint32
 m_nDropTick: GameTick_t
 m_fLastShotTime: GameTime_t
 m_flElasticity: float32
 m_LightGroup: CUtlStringToken
 m_nEntityId: uint32
 CRenderComponent: CRenderComponent
 m_nCollisionGroup: uint8
 m_vCapsuleCenter2: Vector
 m_bSimulatedEveryTick: bool
 m_iGlowTeam: int32
 m_iState: WeaponState_t
 m_glowColorOverride: Color
 m_flGlowStartTime: float32
 m_iNumEmptyAttacks: int32
 m_OriginalOwnerXuidHigh: uint32
 m_flDroppedAtTime: GameTime_t
 m_bClientSideRagdoll: bool
 m_nInteractsAs: uint64
 m_nInteractsExclude: uint64
 m_nAddDecal: int32
 m_iEntityQuality: int32
 m_bvDisabledHitGroups: uint32[1]
 m_iClip2: int32
 m_iClip1: int32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flGlowTime: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_bSilencerOn: bool
 m_flShadowStrength: float32
 m_nViewModelIndex: uint32
 m_flFallbackWear: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_iEntityLevel: uint32
 m_bInReload: bool
 m_nForceBone: int32
 m_iReapplyProvisionParity: int32
 m_vCapsuleCenter1: Vector
 m_fadeMaxDist: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_nNextPrimaryAttackTick: GameTick_t
 m_nGlowRange: int32
 m_iRecoilIndex: int32
 m_iOriginalTeamNumber: int32
 m_nInteractsWith: uint64
 m_flRecoilIndex: float32
 m_weaponMode: CSWeaponMode
 m_iGlowType: int32
 m_vDecalPosition: Vector
 m_bInitialized: bool
 m_nGlowRangeMin: int32
 m_bIsHauledBack: bool
 m_flAnimTime: float32
 m_MoveType: MoveType_t
 m_vecSpecifiedSurroundingMins: Vector
 m_nFallbackSeed: int32
 m_fAccuracyPenalty: float32
 m_iIronSightMode: int32
 m_zoomLevel: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_usSolidFlags: uint8
 m_flCapsuleRadius: float32
 m_bPlayerFireEventIsPrimary: bool
 m_nNextSecondaryAttackTick: GameTick_t
 m_vecMins: Vector
 m_triggerBloat: uint8
 m_bFlashing: bool
 m_nRenderMode: RenderMode_t
 m_flDecalHealBloodRate: float32
 m_flSimulationTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flNavIgnoreUntilTime: GameTime_t
 m_OriginalOwnerXuidLow: uint32
 m_nRenderFX: RenderFx_t
 m_vecForce: Vector
 m_iItemIDHigh: uint32
 m_flDecalHealHeightRate: float32
 m_vLookTargetPosition: Vector
 m_vecMaxs: Vector
 m_bShouldAnimateDuringGameplayPause: bool
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
 m_nFallbackPaintKit: int32
 m_flNextSecondaryAttackTickRatio: float32
 m_flCreateTime: GameTime_t
 m_iTeamNum: uint8
 m_bAnimatedEveryTick: bool
 m_ProviderType: attributeprovidertypes_t
 m_szCustomName: char[161]
 m_bEligibleForScreenHighlight: bool
 m_vDecalForwardAxis: Vector
 m_bAnimGraphUpdateEnabled: bool
 m_nNextThinkTick: GameTick_t
 m_clrRender: Color
 m_nSurroundType: SurroundingBoundsType_t
 m_bReloadVisuallyComplete: bool

98 CFuncTrackTrain
 m_nRenderMode: RenderMode_t
 m_nInteractsWith: uint64
 m_flCapsuleRadius: float32
 m_flShadowStrength: float32
 m_flDecalHealBloodRate: float32
 m_clrRender: Color
 m_nHierarchyId: uint16
 m_flGlowBackfaceMult: float32
 m_flFadeScale: float32
 m_nEnablePhysics: uint8
 m_nGlowRange: int32
 m_flCreateTime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_bFlashing: bool
 CRenderComponent: CRenderComponent
 m_MoveType: MoveType_t
 m_triggerBloat: uint8
 m_vDecalForwardAxis: Vector
 m_flDecalHealHeightRate: float32
 m_nAddDecal: int32
 CBodyComponent: CBodyComponent
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
 m_nSolidType: SolidType_t
 m_vCapsuleCenter1: Vector
 m_nGlowRangeMin: int32
 m_iGlowType: int32
 m_glowColorOverride: Color
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bRenderToCubemaps: bool
 m_iGlowTeam: int32
 m_flGlowStartTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bAnimatedEveryTick: bool
 m_vecSpecifiedSurroundingMaxs: Vector
 m_CollisionGroup: uint8
 m_vCapsuleCenter2: Vector
 m_bvDisabledHitGroups: uint32[1]
 m_flAnimTime: float32
 m_nSubclassID: CUtlStringToken
 m_nRenderFX: RenderFx_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_flSimulationTime: float32
 m_iTeamNum: uint8
 m_nCollisionGroup: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_fadeMinDist: float32
 m_nOwnerId: uint32
 m_nCollisionFunctionMask: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_vecSpecifiedSurroundingMins: Vector
 m_flElasticity: float32
 m_LightGroup: CUtlStringToken
 m_nInteractsAs: uint64
 m_nInteractsExclude: uint64
 m_flGlowTime: float32
 m_bEligibleForScreenHighlight: bool
 m_fadeMaxDist: float32
 m_nObjectCulling: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bSimulatedEveryTick: bool
 m_vDecalPosition: Vector
 m_vecMaxs: Vector
 m_usSolidFlags: uint8
 m_MoveCollide: MoveCollide_t
 m_fEffects: uint32
 m_nEntityId: uint32
 m_vecMins: Vector

176 CSoundAreaEntitySphere
 m_bSimulatedEveryTick: bool
 CBodyComponent: CBodyComponent
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
 m_iTeamNum: uint8
 m_fEffects: uint32
 m_flElasticity: float32
 m_vPos: Vector
 m_flRadius: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_ubInterpolationFrame: uint8
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_bDisabled: bool
 m_flSimulationTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSubclassID: CUtlStringToken
 m_flCreateTime: GameTime_t
 m_iszSoundAreaType: CUtlSymbolLarge
 m_flAnimTime: float32
 m_MoveCollide: MoveCollide_t
 m_MoveType: MoveType_t
 m_hEffectEntity: CHandle< CBaseEntity >

219 CWeaponP250
 m_bAnimatedEveryTick: bool
 m_nRenderFX: RenderFx_t
 m_nInteractsWith: uint64
 m_flGlowStartTime: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_bClientRagdoll: bool
 m_bReloadVisuallyComplete: bool
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_vecMaxs: Vector
 m_flFireSequenceStartTime: float32
 m_fAccuracyPenalty: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flNavIgnoreUntilTime: GameTime_t
 m_nInteractsExclude: uint64
 m_vCapsuleCenter2: Vector
 m_flDecalHealBloodRate: float32
 m_szCustomName: char[161]
 m_iAccountID: uint32
 m_OriginalOwnerXuidLow: uint32
 m_CollisionGroup: uint8
 m_flNextSecondaryAttackTickRatio: float32
 m_vecSpecifiedSurroundingMins: Vector
 m_bBurstMode: bool
 m_nDropTick: GameTick_t
 m_bNeedsBoltAction: bool
 m_flAnimTime: float32
 m_nOwnerId: uint32
 m_flShadowStrength: float32
 m_ProviderType: attributeprovidertypes_t
 m_OriginalOwnerXuidHigh: uint32
 m_MoveCollide: MoveCollide_t
 m_nCollisionGroup: uint8
 m_vCapsuleCenter1: Vector
 m_flCapsuleRadius: float32
 m_fadeMinDist: float32
 m_iItemIDHigh: uint32
 m_flRecoilIndex: float32
 m_iBurstShotsRemaining: int32
 m_bClientSideRagdoll: bool
 m_flElasticity: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_bAnimGraphUpdateEnabled: bool
 CRenderComponent: CRenderComponent
 m_MoveType: MoveType_t
 m_nInteractsAs: uint64
 m_vLookTargetPosition: Vector
 m_bPlayerFireEventIsPrimary: bool
 m_iRecoilIndex: int32
 m_nViewModelIndex: uint32
 m_bRenderToCubemaps: bool
 m_nSurroundType: SurroundingBoundsType_t
 m_flFadeScale: float32
 m_nForceBone: int32
 m_bInitialized: bool
 m_iClip1: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_clrRender: Color
 m_nHierarchyId: uint16
 m_vDecalPosition: Vector
 m_iItemIDLow: uint32
 m_nNextPrimaryAttackTick: GameTick_t
 m_iEntityQuality: int32
 m_iInventoryPosition: uint32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_nCollisionFunctionMask: uint8
 m_glowColorOverride: Color
 m_vDecalForwardAxis: Vector
 m_flDecalHealHeightRate: float32
 m_hOuter: CHandle< CBaseEntity >
 m_nFallbackStatTrak: int32
 m_flSimulationTime: float32
 m_nEntityId: uint32
 m_triggerBloat: uint8
 m_bFlashing: bool
 m_nFallbackSeed: int32
 m_vecMins: Vector
 m_nGlowRangeMin: int32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_iReapplyProvisionParity: int32
 m_nSubclassID: CUtlStringToken
 m_vecSpecifiedSurroundingMaxs: Vector
 m_iItemDefinitionIndex: uint16
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_iTeamNum: uint8
 m_iState: WeaponState_t
 m_nFireSequenceStartTimeChange: int32
 m_nNextThinkTick: GameTick_t
 m_nNextSecondaryAttackTick: GameTick_t
 m_iClip2: int32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_fLastShotTime: GameTime_t
 m_iNumEmptyAttacks: int32
 m_nEnablePhysics: uint8
 m_nObjectCulling: uint8
 m_iGlowTeam: int32
 m_bSilencerOn: bool
 m_iGlowType: int32
 m_nGlowRange: int32
 m_nFallbackPaintKit: int32
 m_bIsHauledBack: bool
 m_bvDisabledHitGroups: uint32[1]
 CBodyComponent: CBodyComponent
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
 m_bEligibleForScreenHighlight: bool
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_pReserveAmmo: int32[2]
 m_bInReload: bool
 m_flNextPrimaryAttackTickRatio: float32
 m_usSolidFlags: uint8
 m_fadeMaxDist: float32
 m_iEntityLevel: uint32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
 m_weaponMode: CSWeaponMode
 m_nSolidType: SolidType_t
 m_vecForce: Vector
 m_ubInterpolationFrame: uint8
 m_bSimulatedEveryTick: bool
 m_nRenderMode: RenderMode_t
 m_nAddDecal: int32
 m_flDroppedAtTime: GameTime_t
 m_flPostponeFireReadyTime: GameTime_t
 m_zoomLevel: int32
 m_flCreateTime: GameTime_t
 m_LightGroup: CUtlStringToken
 m_flGlowBackfaceMult: float32
 m_flFallbackWear: float32
 m_iOriginalTeamNumber: int32
 m_flGlowTime: float32
 m_iIronSightMode: int32

146 CPlayerPing
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_hPingedEntity: CHandle< CBaseEntity >
 m_flSimulationTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_ubInterpolationFrame: uint8
 m_hPlayer: CHandle< CBaseEntity >
 m_bUrgent: bool
 m_flAnimTime: float32
 m_MoveType: MoveType_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_flElasticity: float32
 m_iType: int32
 m_szPlaceName: char[18]
 m_MoveCollide: MoveCollide_t
 m_nSubclassID: CUtlStringToken
 m_flCreateTime: GameTime_t
 m_iTeamNum: uint8
 CBodyComponent: CBodyComponent
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
 m_bSimulatedEveryTick: bool

202 CWeaponCSBase
 m_clrRender: Color
 m_nInteractsExclude: uint64
 m_flGlowBackfaceMult: float32
 m_iClip2: int32
 m_bClientSideRagdoll: bool
 m_bAnimatedEveryTick: bool
 m_usSolidFlags: uint8
 m_nNextSecondaryAttackTick: GameTick_t
 m_bEligibleForScreenHighlight: bool
 m_iItemIDLow: uint32
 m_nFireSequenceStartTimeChange: int32
 m_flPostponeFireReadyTime: GameTime_t
 m_nSubclassID: CUtlStringToken
 m_vLookTargetPosition: Vector
 m_weaponMode: CSWeaponMode
 m_iRecoilIndex: int32
 m_bvDisabledHitGroups: uint32[1]
 m_nSolidType: SolidType_t
 m_vCapsuleCenter2: Vector
 m_ubInterpolationFrame: uint8
 m_bSimulatedEveryTick: bool
 m_flCapsuleRadius: float32
 m_hOuter: CHandle< CBaseEntity >
 m_nFallbackPaintKit: int32
 m_flFireSequenceStartTime: float32
 m_flSimulationTime: float32
 m_nCollisionFunctionMask: uint8
 m_vCapsuleCenter1: Vector
 m_fLastShotTime: GameTime_t
 m_CollisionGroup: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flShadowStrength: float32
 m_iAccountID: uint32
 m_fAccuracyPenalty: float32
 m_flNextPrimaryAttackTickRatio: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_iEntityQuality: int32
 m_iGlowType: int32
 m_nFallbackStatTrak: int32
 m_nEntityId: uint32
 m_nGlowRangeMin: int32
 m_bIsHauledBack: bool
 m_pReserveAmmo: int32[2]
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecMaxs: Vector
 m_vecForce: Vector
 m_bReloadVisuallyComplete: bool
 CBodyComponent: CBodyComponent
  m_hParent: CGameSceneNodeHandle
  m_nRandomSeedOffset: int32
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_nNewSequenceParity: int32
  m_bClientSideAnimation: bool
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
 m_triggerBloat: uint8
 m_nFallbackSeed: int32
 m_nDropTick: GameTick_t
 m_nRenderMode: RenderMode_t
 m_nInteractsAs: uint64
 m_nGlowRange: int32
 m_bSilencerOn: bool
 m_nOwnerId: uint32
 m_bInitiallyPopulateInterpHistory: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flFadeScale: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bClientRagdoll: bool
 m_ProviderType: attributeprovidertypes_t
 m_flAnimTime: float32
 m_flGlowTime: float32
 m_iEntityLevel: uint32
 m_OriginalOwnerXuidHigh: uint32
 m_iTeamNum: uint8
 m_fadeMaxDist: float32
 m_bRenderToCubemaps: bool
 m_flCreateTime: GameTime_t
 m_nHierarchyId: uint16
 m_bFlashing: bool
 m_bAnimGraphUpdateEnabled: bool
 m_nViewModelIndex: uint32
 m_flNavIgnoreUntilTime: GameTime_t
 m_LightGroup: CUtlStringToken
 m_nInteractsWith: uint64
 m_fadeMinDist: float32
 m_iItemIDHigh: uint32
 m_nNextPrimaryAttackTick: GameTick_t
 m_flDecalHealBloodRate: float32
 m_iState: WeaponState_t
 m_bBurstMode: bool
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_fEffects: uint32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vecMins: Vector
 m_nObjectCulling: uint8
 m_iItemDefinitionIndex: uint16
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_iOriginalTeamNumber: int32
 m_vDecalPosition: Vector
 m_iNumEmptyAttacks: int32
 m_nAddDecal: int32
 m_flDecalHealHeightRate: float32
 m_iInventoryPosition: uint32
 m_flRecoilIndex: float32
 m_iIronSightMode: int32
 m_flElasticity: float32
 m_vDecalForwardAxis: Vector
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_OriginalOwnerXuidLow: uint32
 m_bInReload: bool
 m_vecSpecifiedSurroundingMins: Vector
 m_iGlowTeam: int32
 m_iReapplyProvisionParity: int32
 m_flNextSecondaryAttackTickRatio: float32
 m_flDroppedAtTime: GameTime_t
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveType: MoveType_t
 m_glowColorOverride: Color
 m_bInitialized: bool
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
 m_flFallbackWear: float32
 m_bPlayerFireEventIsPrimary: bool
 m_nSurroundType: SurroundingBoundsType_t
 m_flGlowStartTime: float32
 m_nForceBone: int32
 m_szCustomName: char[161]
 CRenderComponent: CRenderComponent
 m_nCollisionGroup: uint8
 m_nEnablePhysics: uint8
 m_iClip1: int32
 m_MoveCollide: MoveCollide_t
 m_nRenderFX: RenderFx_t
 m_nNextThinkTick: GameTick_t

13 CBasePlayerController
 m_flSimulationTime: float32
 m_fFlags: uint32
 m_iTeamNum: uint8
 m_iConnected: PlayerConnectedState
 m_vecX: CNetworkedQuantizedFloat
 m_flFriction: float32
 m_flGravityScale: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flCreateTime: GameTime_t
 m_bSimulatedEveryTick: bool
 m_iszPlayerName: char[128]
 m_vecZ: CNetworkedQuantizedFloat
 m_flTimeScale: float32
 m_nTickBase: uint32
 m_iDesiredFOV: uint32
 m_vecBaseVelocity: Vector
 m_hPawn: CHandle< CBasePlayerPawn >
 m_steamID: uint64
 m_nNextThinkTick: GameTick_t
 m_vecY: CNetworkedQuantizedFloat

139 CPhysMagnet
 m_vecSpecifiedSurroundingMins: Vector
 m_flGlowTime: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool
 m_vecMins: Vector
 m_flNavIgnoreUntilTime: GameTime_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_bShouldAnimateDuringGameplayPause: bool
 m_bClientRagdoll: bool
 m_flSimulationTime: float32
 CBodyComponent: CBodyComponent
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nBoolVariablesCount: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_flWeight: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flGlowBackfaceMult: float32
 m_vDecalPosition: Vector
 m_flDecalHealBloodRate: float32
 m_flDecalHealHeightRate: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nInteractsWith: uint64
 m_nHierarchyId: uint16
 m_bEligibleForScreenHighlight: bool
 m_nSurroundType: SurroundingBoundsType_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bFlashing: bool
 m_flGlowStartTime: float32
 m_nAddDecal: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nSubclassID: CUtlStringToken
 m_triggerBloat: uint8
 m_vCapsuleCenter1: Vector
 m_nEntityId: uint32
 m_nCollisionGroup: uint8
 m_vDecalForwardAxis: Vector
 CRenderComponent: CRenderComponent
 m_MoveType: MoveType_t
 m_flElasticity: float32
 m_clrRender: Color
 m_nEnablePhysics: uint8
 m_flFadeScale: float32
 m_nObjectCulling: uint8
 m_flCreateTime: GameTime_t
 m_nInteractsAs: uint64
 m_nInteractsExclude: uint64
 m_nForceBone: int32
 m_nGlowRangeMin: int32
 m_fEffects: uint32
 m_flCapsuleRadius: float32
 m_nGlowRange: int32
 m_usSolidFlags: uint8
 m_vecForce: Vector
 m_bvDisabledHitGroups: uint32[1]
 m_vCapsuleCenter2: Vector
 m_fadeMinDist: float32
 m_nRenderFX: RenderFx_t
 m_bRenderToCubemaps: bool
 m_vecMaxs: Vector
 m_nCollisionFunctionMask: uint8
 m_iGlowType: int32
 m_flShadowStrength: float32
 m_nRenderMode: RenderMode_t
 m_LightGroup: CUtlStringToken
 m_nOwnerId: uint32
 m_iTeamNum: uint8
 m_bAnimGraphUpdateEnabled: bool
 m_flAnimTime: float32
 m_MoveCollide: MoveCollide_t
 m_ubInterpolationFrame: uint8
 m_fadeMaxDist: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_bClientSideRagdoll: bool
 m_nSolidType: SolidType_t
 m_iGlowTeam: int32
 m_bSimulatedEveryTick: bool
 m_CollisionGroup: uint8
 m_glowColorOverride: Color

228 CWeaponUMP45
 m_iItemDefinitionIndex: uint16
 m_bInReload: bool
 m_nNextPrimaryAttackTick: GameTick_t
 m_flNextSecondaryAttackTickRatio: float32
 m_bAnimatedEveryTick: bool
 m_flFallbackWear: float32
 m_bSilencerOn: bool
 m_flShadowStrength: float32
 m_flFadeScale: float32
 m_vecMins: Vector
 m_nGlowRange: int32
 m_nViewModelIndex: uint32
 m_vCapsuleCenter1: Vector
 m_flCreateTime: GameTime_t
 m_nEntityId: uint32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_glowColorOverride: Color
 m_flDecalHealBloodRate: float32
 m_nFallbackSeed: int32
 m_bRenderToCubemaps: bool
 m_nInteractsWith: uint64
 m_iEntityQuality: int32
 m_iEntityLevel: uint32
 m_fEffects: uint32
 m_iBurstShotsRemaining: int32
 m_nInteractsAs: uint64
 m_bEligibleForScreenHighlight: bool
 m_iReapplyProvisionParity: int32
 m_bSimulatedEveryTick: bool
 m_usSolidFlags: uint8
 m_OriginalOwnerXuidLow: uint32
 m_nRenderFX: RenderFx_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nSolidType: SolidType_t
 m_iAccountID: uint32
 m_MoveCollide: MoveCollide_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_nFallbackPaintKit: int32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_ubInterpolationFrame: uint8
 m_iClip1: int32
 CBodyComponent: CBodyComponent
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
 m_nCollisionFunctionMask: uint8
 m_vCapsuleCenter2: Vector
 m_fadeMaxDist: float32
 m_bBurstMode: bool
 m_iOriginalTeamNumber: int32
 m_flSimulationTime: float32
 CRenderComponent: CRenderComponent
 m_bNeedsBoltAction: bool
 m_iGlowTeam: int32
 m_vecSpecifiedSurroundingMins: Vector
 m_iTeamNum: uint8
 m_triggerBloat: uint8
 m_nGlowRangeMin: int32
 m_iInventoryPosition: uint32
 m_bClientSideRagdoll: bool
 m_nDropTick: GameTick_t
 m_hOuter: CHandle< CBaseEntity >
 m_vDecalForwardAxis: Vector
 m_vecForce: Vector
 m_pReserveAmmo: int32[2]
 m_clrRender: Color
 m_nCollisionGroup: uint8
 m_flGlowTime: float32
 m_vDecalPosition: Vector
 m_flDroppedAtTime: GameTime_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_nObjectCulling: uint8
 m_flCapsuleRadius: float32
 m_nForceBone: int32
 m_bClientRagdoll: bool
 m_iItemIDLow: uint32
 m_nFallbackStatTrak: int32
 m_bPlayerFireEventIsPrimary: bool
 m_nHierarchyId: uint16
 m_vecMaxs: Vector
 m_iGlowType: int32
 m_bIsHauledBack: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bAnimGraphUpdateEnabled: bool
 m_flRecoilIndex: float32
 m_flGlowBackfaceMult: float32
 m_fadeMinDist: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_nNextThinkTick: GameTick_t
 m_nEnablePhysics: uint8
 m_nInteractsExclude: uint64
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nSurroundType: SurroundingBoundsType_t
 m_OriginalOwnerXuidHigh: uint32
 m_iRecoilIndex: int32
 m_flNextPrimaryAttackTickRatio: float32
 m_nRenderMode: RenderMode_t
 m_flGlowStartTime: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_bShouldAnimateDuringGameplayPause: bool
 m_flFireSequenceStartTime: float32
 m_MoveType: MoveType_t
 m_ProviderType: attributeprovidertypes_t
 m_szCustomName: char[161]
 m_weaponMode: CSWeaponMode
 m_flPostponeFireReadyTime: GameTime_t
 m_flAnimTime: float32
 m_bFlashing: bool
 m_vLookTargetPosition: Vector
 m_bInitialized: bool
 m_nFireSequenceStartTimeChange: int32
 m_bReloadVisuallyComplete: bool
 m_iIronSightMode: int32
 m_LightGroup: CUtlStringToken
 m_CollisionGroup: uint8
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_zoomLevel: int32
 m_bvDisabledHitGroups: uint32[1]
 m_nSubclassID: CUtlStringToken
 m_nAddDecal: int32
 m_iItemIDHigh: uint32
 m_nOwnerId: uint32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
 m_fLastShotTime: GameTime_t
 m_flElasticity: float32
 m_iState: WeaponState_t
 m_fAccuracyPenalty: float32
 m_iNumEmptyAttacks: int32
 m_nNextSecondaryAttackTick: GameTick_t
 m_iClip2: int32
 m_flDecalHealHeightRate: float32

220 CWeaponP90
 m_flNavIgnoreUntilTime: GameTime_t
 m_vecMins: Vector
 m_hOuter: CHandle< CBaseEntity >
 m_nFallbackSeed: int32
 m_bSilencerOn: bool
 m_iClip2: int32
 m_MoveType: MoveType_t
 m_nSubclassID: CUtlStringToken
 m_iGlowTeam: int32
 m_fAccuracyPenalty: float32
 m_fEffects: uint32
 m_flGlowBackfaceMult: float32
 m_fadeMinDist: float32
 m_nNextPrimaryAttackTick: GameTick_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecMaxs: Vector
 m_nNextThinkTick: GameTick_t
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nObjectCulling: uint8
 m_flElasticity: float32
 m_nGlowRange: int32
 m_nFireSequenceStartTimeChange: int32
 m_nViewModelIndex: uint32
 m_iClip1: int32
 m_iTeamNum: uint8
 m_nCollisionFunctionMask: uint8
 m_bEligibleForScreenHighlight: bool
 m_vDecalPosition: Vector
 m_flDecalHealHeightRate: float32
 m_nForceBone: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flCreateTime: GameTime_t
 m_usSolidFlags: uint8
 m_iItemIDLow: uint32
 m_flDroppedAtTime: GameTime_t
 m_bvDisabledHitGroups: uint32[1]
 m_iNumEmptyAttacks: int32
 m_nEnablePhysics: uint8
 m_flShadowStrength: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_bAnimGraphUpdateEnabled: bool
 m_iEntityLevel: uint32
 m_iRecoilIndex: int32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_nInteractsAs: uint64
 m_nSurroundType: SurroundingBoundsType_t
 m_vCapsuleCenter2: Vector
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_pReserveAmmo: int32[2]
 m_vDecalForwardAxis: Vector
 m_nFallbackPaintKit: int32
 m_bInReload: bool
 m_bRenderToCubemaps: bool
 m_vCapsuleCenter1: Vector
 m_iEntityQuality: int32
 m_bIsHauledBack: bool
 m_nAddDecal: int32
 m_iItemIDHigh: uint32
 m_flFallbackWear: float32
 m_flNextPrimaryAttackTickRatio: float32
 m_flSimulationTime: float32
 m_nCollisionGroup: uint8
 m_OriginalOwnerXuidHigh: uint32
 m_flRecoilIndex: float32
 m_flPostponeFireReadyTime: GameTime_t
 m_iBurstShotsRemaining: int32
 m_ubInterpolationFrame: uint8
 m_nRenderFX: RenderFx_t
 m_clrRender: Color
 m_iGlowType: int32
 m_iOriginalTeamNumber: int32
 m_fLastShotTime: GameTime_t
 m_OriginalOwnerXuidLow: uint32
 m_flNextSecondaryAttackTickRatio: float32
 m_MoveCollide: MoveCollide_t
 m_bAnimatedEveryTick: bool
 m_nNextSecondaryAttackTick: GameTick_t
 m_nOwnerId: uint32
 m_nHierarchyId: uint16
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vecForce: Vector
 m_iInventoryPosition: uint32
 m_bNeedsBoltAction: bool
 m_bInitiallyPopulateInterpHistory: bool
 CRenderComponent: CRenderComponent
 CBodyComponent: CBodyComponent
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
 m_bInitialized: bool
 m_nFallbackStatTrak: int32
 m_nInteractsWith: uint64
 m_flCapsuleRadius: float32
 m_vLookTargetPosition: Vector
 m_iState: WeaponState_t
 m_bReloadVisuallyComplete: bool
 m_nEntityId: uint32
 m_glowColorOverride: Color
 m_bClientSideRagdoll: bool
 m_bSimulatedEveryTick: bool
 m_bFlashing: bool
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nRenderMode: RenderMode_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_bClientRagdoll: bool
 m_szCustomName: char[161]
 m_flFireSequenceStartTime: float32
 m_bPlayerFireEventIsPrimary: bool
 m_flAnimTime: float32
 m_flGlowStartTime: float32
 m_weaponMode: CSWeaponMode
 m_bBurstMode: bool
 m_nDropTick: GameTick_t
 m_nSolidType: SolidType_t
 m_flGlowTime: float32
 m_flFadeScale: float32
 m_iAccountID: uint32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_nInteractsExclude: uint64
 m_iReapplyProvisionParity: int32
 m_triggerBloat: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_flDecalHealBloodRate: float32
 m_ProviderType: attributeprovidertypes_t
 m_iIronSightMode: int32
 m_zoomLevel: int32
 m_LightGroup: CUtlStringToken
 m_CollisionGroup: uint8
 m_nGlowRangeMin: int32
 m_fadeMaxDist: float32
 m_iItemDefinitionIndex: uint16

91 CFuncBrush
 m_nInteractsWith: uint64
 m_nCollisionFunctionMask: uint8
 m_CollisionGroup: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_iGlowType: int32
 m_vDecalPosition: Vector
 m_nSubclassID: CUtlStringToken
 m_bSimulatedEveryTick: bool
 m_nAddDecal: int32
 m_nGlowRangeMin: int32
 m_flGlowStartTime: float32
 m_bAnimatedEveryTick: bool
 m_vecSpecifiedSurroundingMins: Vector
 m_flGlowTime: float32
 m_flGlowBackfaceMult: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 CBodyComponent: CBodyComponent
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
 m_ubInterpolationFrame: uint8
 m_nHierarchyId: uint16
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nSolidType: SolidType_t
 m_nGlowRange: int32
 m_bEligibleForScreenHighlight: bool
 m_flShadowStrength: float32
 m_flDecalHealBloodRate: float32
 m_flAnimTime: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_usSolidFlags: uint8
 m_flDecalHealHeightRate: float32
 m_bvDisabledHitGroups: uint32[1]
 m_nInteractsAs: uint64
 m_nInteractsExclude: uint64
 m_iTeamNum: uint8
 m_nEntityId: uint32
 m_nCollisionGroup: uint8
 m_fadeMinDist: float32
 m_flSimulationTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveCollide: MoveCollide_t
 m_clrRender: Color
 m_bRenderToCubemaps: bool
 CRenderComponent: CRenderComponent
 m_fadeMaxDist: float32
 m_MoveType: MoveType_t
 m_nSurroundType: SurroundingBoundsType_t
 m_bFlashing: bool
 m_flFadeScale: float32
 m_flCapsuleRadius: float32
 m_glowColorOverride: Color
 m_nRenderFX: RenderFx_t
 m_vCapsuleCenter2: Vector
 m_flCreateTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_vecMins: Vector
 m_nObjectCulling: uint8
 m_vDecalForwardAxis: Vector
 m_fEffects: uint32
 m_flElasticity: float32
 m_vCapsuleCenter1: Vector
 m_triggerBloat: uint8
 m_nEnablePhysics: uint8
 m_LightGroup: CUtlStringToken
 m_vecMaxs: Vector
 m_iGlowTeam: int32
 m_flNavIgnoreUntilTime: GameTime_t
 m_nOwnerId: uint32

212 CWeaponM4A1
 m_nInteractsExclude: uint64
 m_nSolidType: SolidType_t
 m_vLookTargetPosition: Vector
 m_iEntityLevel: uint32
 m_flElasticity: float32
 m_iNumEmptyAttacks: int32
 m_MoveCollide: MoveCollide_t
 m_nHierarchyId: uint16
 m_nAddDecal: int32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_vecMaxs: Vector
 m_hOuter: CHandle< CBaseEntity >
 m_nDropTick: GameTick_t
 m_flDecalHealHeightRate: float32
 m_nFallbackSeed: int32
 m_iAccountID: uint32
 m_flFireSequenceStartTime: float32
 m_LightGroup: CUtlStringToken
 m_nCollisionGroup: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_fadeMinDist: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_MoveType: MoveType_t
 m_flGlowBackfaceMult: float32
 m_vDecalPosition: Vector
 m_vDecalForwardAxis: Vector
 m_bRenderToCubemaps: bool
 m_vCapsuleCenter2: Vector
 m_iGlowTeam: int32
 m_bBurstMode: bool
 m_nNextThinkTick: GameTick_t
 m_iClip1: int32
 m_ProviderType: attributeprovidertypes_t
 m_zoomLevel: int32
 m_iBurstShotsRemaining: int32
 m_bClientSideRagdoll: bool
 m_nSurroundType: SurroundingBoundsType_t
 m_bNeedsBoltAction: bool
 m_fEffects: uint32
 m_szCustomName: char[161]
 m_nObjectCulling: uint8
 m_OriginalOwnerXuidLow: uint32
 m_flRecoilIndex: float32
 m_flSimulationTime: float32
 m_flCapsuleRadius: float32
 m_flGlowTime: float32
 m_flShadowStrength: float32
 m_flCreateTime: GameTime_t
 m_flPostponeFireReadyTime: GameTime_t
 m_fLastShotTime: GameTime_t
 m_nGlowRange: int32
 m_nGlowRangeMin: int32
 m_bIsHauledBack: bool
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_flAnimTime: float32
 m_iTeamNum: uint8
 m_nRenderFX: RenderFx_t
 m_vCapsuleCenter1: Vector
 m_bClientRagdoll: bool
 m_bInReload: bool
 CRenderComponent: CRenderComponent
 m_iRecoilIndex: int32
 m_nInteractsWith: uint64
 m_glowColorOverride: Color
 m_flGlowStartTime: float32
 m_iInventoryPosition: uint32
 m_flFadeScale: float32
 m_iItemDefinitionIndex: uint16
 m_nRenderMode: RenderMode_t
 m_triggerBloat: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_iGlowType: int32
 m_fAccuracyPenalty: float32
 m_bvDisabledHitGroups: uint32[1]
 CBodyComponent: CBodyComponent
  m_angRotation: QAngle
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_nBoolVariablesCount: int32
  m_cellZ: uint16
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_nNewSequenceParity: int32
  m_bClientSideAnimation: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_hParent: CGameSceneNodeHandle
  m_nRandomSeedOffset: int32
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_iReapplyProvisionParity: int32
 m_iItemIDLow: uint32
 m_iIronSightMode: int32
 m_clrRender: Color
 m_fadeMaxDist: float32
 m_flDecalHealBloodRate: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_bAnimatedEveryTick: bool
 m_bAnimGraphUpdateEnabled: bool
 m_flDroppedAtTime: GameTime_t
 m_nEntityId: uint32
 m_nCollisionFunctionMask: uint8
 m_bPlayerFireEventIsPrimary: bool
 m_weaponMode: CSWeaponMode
 m_nOwnerId: uint32
 m_iEntityQuality: int32
 m_flNextSecondaryAttackTickRatio: float32
 m_usSolidFlags: uint8
 m_nFallbackPaintKit: int32
 m_bReloadVisuallyComplete: bool
 m_iClip2: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_CollisionGroup: uint8
 m_vecForce: Vector
 m_nForceBone: int32
 m_flFallbackWear: float32
 m_nNextSecondaryAttackTick: GameTick_t
 m_ubInterpolationFrame: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsAs: uint64
 m_nFireSequenceStartTimeChange: int32
 m_pReserveAmmo: int32[2]
 m_nSubclassID: CUtlStringToken
 m_bSimulatedEveryTick: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_iState: WeaponState_t
 m_nEnablePhysics: uint8
 m_nViewModelIndex: uint32
 m_bInitiallyPopulateInterpHistory: bool
 m_bInitialized: bool
 m_OriginalOwnerXuidHigh: uint32
 m_nNextPrimaryAttackTick: GameTick_t
 m_flNextPrimaryAttackTickRatio: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bFlashing: bool
 m_iItemIDHigh: uint32
 m_nFallbackStatTrak: int32
 m_vecMins: Vector
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
 m_bSilencerOn: bool
 m_iOriginalTeamNumber: int32
 m_bEligibleForScreenHighlight: bool

215 CWeaponMP7
 m_flAnimTime: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_flNextSecondaryAttackTickRatio: float32
 m_iClip1: int32
 m_nObjectCulling: uint8
 m_iRecoilIndex: int32
 m_iClip2: int32
 m_CollisionGroup: uint8
 m_nDropTick: GameTick_t
 m_nEntityId: uint32
 m_flFadeScale: float32
 m_vDecalPosition: Vector
 m_bClientRagdoll: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_vCapsuleCenter2: Vector
 m_iGlowTeam: int32
 m_pReserveAmmo: int32[2]
 m_nRenderMode: RenderMode_t
 m_nSolidType: SolidType_t
 m_nEnablePhysics: uint8
 m_bInitialized: bool
 m_bFlashing: bool
 m_iInventoryPosition: uint32
 m_nFireSequenceStartTimeChange: int32
 m_flSimulationTime: float32
 m_nInteractsAs: uint64
 m_vDecalForwardAxis: Vector
 m_fEffects: uint32
 m_nNextSecondaryAttackTick: GameTick_t
 m_nHierarchyId: uint16
 m_flGlowBackfaceMult: float32
 m_nViewModelIndex: uint32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flShadowStrength: float32
 m_hOuter: CHandle< CBaseEntity >
 CRenderComponent: CRenderComponent
 m_bClientSideRagdoll: bool
 m_vecSpecifiedSurroundingMins: Vector
 m_szCustomName: char[161]
 m_zoomLevel: int32
 m_bAnimatedEveryTick: bool
 m_LightGroup: CUtlStringToken
 m_bRenderToCubemaps: bool
 m_nFallbackSeed: int32
 m_iState: WeaponState_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bSimulatedEveryTick: bool
 m_usSolidFlags: uint8
 m_flGlowStartTime: float32
 m_iReapplyProvisionParity: int32
 m_iEntityLevel: uint32
 m_flPostponeFireReadyTime: GameTime_t
 m_flNextPrimaryAttackTickRatio: float32
 m_vLookTargetPosition: Vector
 m_MoveCollide: MoveCollide_t
 m_nSubclassID: CUtlStringToken
 m_fadeMaxDist: float32
 m_nAddDecal: int32
 m_bInitiallyPopulateInterpHistory: bool
 m_nForceBone: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_vecMaxs: Vector
 m_iAccountID: uint32
 m_iOriginalTeamNumber: int32
 m_ubInterpolationFrame: uint8
 m_nInteractsWith: uint64
 m_iEntityQuality: int32
 m_nInteractsExclude: uint64
 m_nOwnerId: uint32
 m_iGlowType: int32
 m_bSilencerOn: bool
 m_iIronSightMode: int32
 m_bNeedsBoltAction: bool
 m_nCollisionGroup: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_nFallbackPaintKit: int32
 m_bInReload: bool
 m_nNextPrimaryAttackTick: GameTick_t
 m_bEligibleForScreenHighlight: bool
 m_bShouldAnimateDuringGameplayPause: bool
 m_ProviderType: attributeprovidertypes_t
 m_nFallbackStatTrak: int32
 m_nNextThinkTick: GameTick_t
 m_clrRender: Color
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flFallbackWear: float32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_flCreateTime: GameTime_t
 m_iTeamNum: uint8
 m_OriginalOwnerXuidLow: uint32
 m_bvDisabledHitGroups: uint32[1]
 m_flNavIgnoreUntilTime: GameTime_t
 m_flGlowTime: float32
 m_fAccuracyPenalty: float32
 m_bBurstMode: bool
 m_bIsHauledBack: bool
 m_nCollisionFunctionMask: uint8
 m_flDecalHealHeightRate: float32
 m_iItemIDLow: uint32
 m_OriginalOwnerXuidHigh: uint32
 m_bPlayerFireEventIsPrimary: bool
 m_MoveType: MoveType_t
 m_vecMins: Vector
 m_vecSpecifiedSurroundingMaxs: Vector
 m_fLastShotTime: GameTime_t
 m_nGlowRangeMin: int32
 m_iBurstShotsRemaining: int32
 m_nRenderFX: RenderFx_t
 m_triggerBloat: uint8
 m_glowColorOverride: Color
 m_bAnimGraphUpdateEnabled: bool
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_flRecoilIndex: float32
 CBodyComponent: CBodyComponent
  m_hParent: CGameSceneNodeHandle
  m_nRandomSeedOffset: int32
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_nNewSequenceParity: int32
  m_bClientSideAnimation: bool
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
 m_flElasticity: float32
 m_nGlowRange: int32
 m_iNumEmptyAttacks: int32
 m_vCapsuleCenter1: Vector
 m_flDecalHealBloodRate: float32
 m_weaponMode: CSWeaponMode
 m_fadeMinDist: float32
 m_vecForce: Vector
 m_bReloadVisuallyComplete: bool
 m_flCapsuleRadius: float32
 m_iItemDefinitionIndex: uint16
 m_iItemIDHigh: uint32
 m_flFireSequenceStartTime: float32
 m_flDroppedAtTime: GameTime_t
 m_flTimeSilencerSwitchComplete: GameTime_t

217 CWeaponNegev
 m_vDecalPosition: Vector
 m_ubInterpolationFrame: uint8
 m_flShadowStrength: float32
 CRenderComponent: CRenderComponent
 m_bRenderToCubemaps: bool
 m_nGlowRange: int32
 m_vDecalForwardAxis: Vector
 m_bBurstMode: bool
 m_nCollisionGroup: uint8
 m_flNextPrimaryAttackTickRatio: float32
 m_flGlowBackfaceMult: float32
 m_nInteractsExclude: uint64
 m_bAnimGraphUpdateEnabled: bool
 m_iNumEmptyAttacks: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_fadeMaxDist: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_ProviderType: attributeprovidertypes_t
 m_LightGroup: CUtlStringToken
 m_nInteractsAs: uint64
 m_bInitialized: bool
 m_nFallbackStatTrak: int32
 m_nFireSequenceStartTimeChange: int32
 m_bPlayerFireEventIsPrimary: bool
 m_nSubclassID: CUtlStringToken
 m_nAddDecal: int32
 m_bvDisabledHitGroups: uint32[1]
 m_bEligibleForScreenHighlight: bool
 m_MoveCollide: MoveCollide_t
 m_vecMins: Vector
 m_glowColorOverride: Color
 m_fadeMinDist: float32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_iOriginalTeamNumber: int32
 m_flAnimTime: float32
 m_vecSpecifiedSurroundingMins: Vector
 m_fAccuracyPenalty: float32
 m_nDropTick: GameTick_t
 m_bAnimatedEveryTick: bool
 m_OriginalOwnerXuidHigh: uint32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_iItemDefinitionIndex: uint16
 m_iRecoilIndex: int32
 m_nForceBone: int32
 m_iGlowType: int32
 m_iGlowTeam: int32
 m_flDecalHealBloodRate: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_iEntityLevel: uint32
 m_fLastShotTime: GameTime_t
 m_flNextSecondaryAttackTickRatio: float32
 m_triggerBloat: uint8
 m_vecForce: Vector
 m_nFallbackPaintKit: int32
 m_nFallbackSeed: int32
 m_bReloadVisuallyComplete: bool
 m_flGlowStartTime: float32
 m_clrRender: Color
 m_flGlowTime: float32
 m_nObjectCulling: uint8
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_flSimulationTime: float32
 m_bSilencerOn: bool
 m_bNeedsBoltAction: bool
 m_nNextSecondaryAttackTick: GameTick_t
 m_nEntityId: uint32
 m_nHierarchyId: uint16
 m_nSurroundType: SurroundingBoundsType_t
 m_weaponMode: CSWeaponMode
 m_nNextThinkTick: GameTick_t
 m_nOwnerId: uint32
 m_nGlowRangeMin: int32
 m_iAccountID: uint32
 m_iTeamNum: uint8
 m_fEffects: uint32
 m_nRenderMode: RenderMode_t
 m_nCollisionFunctionMask: uint8
 m_flFadeScale: float32
 m_szCustomName: char[161]
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_vCapsuleCenter2: Vector
 m_zoomLevel: int32
 m_MoveType: MoveType_t
 m_nRenderFX: RenderFx_t
 m_bSimulatedEveryTick: bool
 m_bShouldAnimateDuringGameplayPause: bool
 m_vLookTargetPosition: Vector
 m_flElasticity: float32
 m_flCapsuleRadius: float32
 m_hOuter: CHandle< CBaseEntity >
 m_iItemIDHigh: uint32
 m_flFireSequenceStartTime: float32
 m_nNextPrimaryAttackTick: GameTick_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flFallbackWear: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_nInteractsWith: uint64
 m_usSolidFlags: uint8
 m_nSolidType: SolidType_t
 m_vCapsuleCenter1: Vector
 m_flDecalHealHeightRate: float32
 m_flPostponeFireReadyTime: GameTime_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_iEntityQuality: int32
 m_vecMaxs: Vector
 m_iItemIDLow: uint32
 m_bClientRagdoll: bool
 m_CollisionGroup: uint8
 m_iIronSightMode: int32
 CBodyComponent: CBodyComponent
  m_angRotation: QAngle
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_nBoolVariablesCount: int32
  m_cellZ: uint16
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_nNewSequenceParity: int32
  m_bClientSideAnimation: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_hParent: CGameSceneNodeHandle
  m_nRandomSeedOffset: int32
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
 m_iState: WeaponState_t
 m_flDroppedAtTime: GameTime_t
 m_iReapplyProvisionParity: int32
 m_bFlashing: bool
 m_iInventoryPosition: uint32
 m_bIsHauledBack: bool
 m_pReserveAmmo: int32[2]
 m_flCreateTime: GameTime_t
 m_OriginalOwnerXuidLow: uint32
 m_flRecoilIndex: float32
 m_bInReload: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_bClientSideRagdoll: bool
 m_nEnablePhysics: uint8
 m_iBurstShotsRemaining: int32
 m_iClip2: int32
 m_nViewModelIndex: uint32
 m_iClip1: int32

62 CDronegun
 m_fadeMaxDist: float32
 m_nAddDecal: int32
 m_bShouldAnimateDuringGameplayPause: bool
 CRenderComponent: CRenderComponent
 m_bSimulatedEveryTick: bool
 m_nCollisionFunctionMask: uint8
 m_vecMins: Vector
 m_nObjectCulling: uint8
 m_vecAttentionTarget: Vector
 m_vecTargetOffset: Vector
 m_bHasTarget: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nCollisionGroup: uint8
 m_iGlowTeam: int32
 m_vDecalForwardAxis: Vector
 m_bAnimatedEveryTick: bool
 m_vecMaxs: Vector
 m_bFlashing: bool
 m_fEffects: uint32
 m_iGlowType: int32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_vDecalPosition: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bClientRagdoll: bool
 m_vCapsuleCenter2: Vector
 m_nGlowRange: int32
 m_fadeMinDist: float32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flShadowStrength: float32
 m_nRenderMode: RenderMode_t
 m_bRenderToCubemaps: bool
 m_nHierarchyId: uint16
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nRenderFX: RenderFx_t
 m_nSurroundType: SurroundingBoundsType_t
 m_CollisionGroup: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_bAnimGraphUpdateEnabled: bool
 m_flCreateTime: GameTime_t
 m_nInteractsExclude: uint64
 m_triggerBloat: uint8
 m_nEnablePhysics: uint8
 m_flDecalHealBloodRate: float32
 m_vecForce: Vector
 CBodyComponent: CBodyComponent
  m_nBoolVariablesCount: int32
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_name: CUtlStringToken
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_nHitboxSet: uint8
  m_MeshGroupMask: uint64
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nOwnerId: uint32
 m_nForceBone: int32
 m_flGlowBackfaceMult: float32
 m_flFadeScale: float32
 m_flDecalHealHeightRate: float32
 m_flCapsuleRadius: float32
 m_bClientSideRagdoll: bool
 m_ubInterpolationFrame: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_vCapsuleCenter1: Vector
 m_flGlowStartTime: float32
 m_flSimulationTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSubclassID: CUtlStringToken
 m_bEligibleForScreenHighlight: bool
 m_MoveType: MoveType_t
 m_nInteractsAs: uint64
 m_flGlowTime: float32
 m_nSolidType: SolidType_t
 m_bInitiallyPopulateInterpHistory: bool
 m_flAnimTime: float32
 m_MoveCollide: MoveCollide_t
 m_clrRender: Color
 m_nInteractsWith: uint64
 m_nEntityId: uint32
 m_nGlowRangeMin: int32
 m_bvDisabledHitGroups: uint32[1]
 m_iTeamNum: uint8
 m_flElasticity: float32
 m_LightGroup: CUtlStringToken
 m_usSolidFlags: uint8
 m_glowColorOverride: Color

81 CEnvVolumetricFogController
 m_flStartScatterTime: GameTime_t
 m_flStartAnisotropy: float32
 m_bStartDisabled: bool
 CBodyComponent: CBodyComponent
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
 m_MoveType: MoveType_t
 m_nIndirectTextureDimZ: int32
 m_vBoxMaxs: Vector
 m_vBoxMins: Vector
 m_MoveCollide: MoveCollide_t
 m_bSimulatedEveryTick: bool
 m_bAnimatedEveryTick: bool
 m_flScattering: float32
 m_flSimulationTime: float32
 m_flStartDrawDistance: float32
 m_flDefaultDrawDistance: float32
 m_nForceRefreshCount: int32
 m_flCreateTime: GameTime_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_flFadeSpeed: float32
 m_nIndirectTextureDimX: int32
 m_flDefaultScattering: float32
 m_bIsMaster: bool
 m_nSubclassID: CUtlStringToken
 m_ubInterpolationFrame: uint8
 m_flAnisotropy: float32
 m_flFadeInEnd: float32
 m_nIndirectTextureDimY: int32
 m_flStartDrawDistanceTime: GameTime_t
 m_flElasticity: float32
 m_flDrawDistance: float32
 m_flFadeInStart: float32
 m_flIndirectStrength: float32
 m_flDefaultAnisotropy: float32
 m_bEnableIndirect: bool
 m_hFogIndirectTexture: CStrongHandle< InfoForResourceTypeCTextureBase >
 m_iTeamNum: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_bActive: bool
 m_flStartAnisoTime: GameTime_t
 m_flAnimTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flStartScattering: float32

161 CPropCounter
 m_nForceBone: int32
 m_nOwnerId: uint32
 m_nCollisionFunctionMask: uint8
 m_glowColorOverride: Color
 CRenderComponent: CRenderComponent
 m_flElasticity: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_bRenderToCubemaps: bool
 m_nSolidType: SolidType_t
 m_nGlowRange: int32
 m_nGlowRangeMin: int32
 m_fadeMaxDist: float32
 m_bAnimatedEveryTick: bool
 m_hEffectEntity: CHandle< CBaseEntity >
 m_CollisionGroup: uint8
 m_nEnablePhysics: uint8
 m_iGlowTeam: int32
 m_flGlowStartTime: float32
 m_flGlowBackfaceMult: float32
 m_nAddDecal: int32
 m_MoveType: MoveType_t
 m_bClientRagdoll: bool
 m_bvDisabledHitGroups: uint32[1]
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_flDecalHealHeightRate: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_ubInterpolationFrame: uint8
 m_bClientSideRagdoll: bool
 m_bSimulatedEveryTick: bool
 m_triggerBloat: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_bFlashing: bool
 m_flCreateTime: GameTime_t
 m_flGlowTime: float32
 m_clrRender: Color
 m_nInteractsAs: uint64
 m_vCapsuleCenter2: Vector
 m_iGlowType: int32
 m_flDecalHealBloodRate: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_bShouldAnimateDuringGameplayPause: bool
 m_bAnimGraphUpdateEnabled: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flCapsuleRadius: float32
 m_nSubclassID: CUtlStringToken
 m_flShadowStrength: float32
 m_nRenderMode: RenderMode_t
 m_nEntityId: uint32
 m_vCapsuleCenter1: Vector
 m_fadeMinDist: float32
 m_MoveCollide: MoveCollide_t
 m_usSolidFlags: uint8
 m_iTeamNum: uint8
 m_flSimulationTime: float32
 m_vecMins: Vector
 m_vecMaxs: Vector
 m_vDecalForwardAxis: Vector
 m_flAnimTime: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_LightGroup: CUtlStringToken
 m_nCollisionGroup: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bEligibleForScreenHighlight: bool
 m_vDecalPosition: Vector
 m_fEffects: uint32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nRenderFX: RenderFx_t
 m_nHierarchyId: uint16
 m_vecSpecifiedSurroundingMins: Vector
 m_nObjectCulling: uint8
 m_vecForce: Vector
 m_flDisplayValue: float32
 CBodyComponent: CBodyComponent
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nBoolVariablesCount: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
 m_nInteractsExclude: uint64
 m_flFadeScale: float32
 m_nInteractsWith: uint64

224 CWeaponShield
 m_flCapsuleRadius: float32
 m_vLookTargetPosition: Vector
 m_bInReload: bool
 m_iIronSightMode: int32
 m_iAccountID: uint32
 m_iInventoryPosition: uint32
 m_flCreateTime: GameTime_t
 m_flElasticity: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vCapsuleCenter2: Vector
 m_nGlowRangeMin: int32
 m_bAnimGraphUpdateEnabled: bool
 m_bSilencerOn: bool
 m_nNextThinkTick: GameTick_t
 m_iTeamNum: uint8
 m_nInteractsWith: uint64
 m_fadeMinDist: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_nViewModelIndex: uint32
 m_bFlashing: bool
 m_flNextPrimaryAttackTickRatio: float32
 m_fEffects: uint32
 m_vDecalPosition: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_iState: WeaponState_t
 m_bBurstMode: bool
 m_vDecalForwardAxis: Vector
 m_iItemIDLow: uint32
 m_flPostponeFireReadyTime: GameTime_t
 m_bNeedsBoltAction: bool
 m_vecSpecifiedSurroundingMins: Vector
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nObjectCulling: uint8
 m_szCustomName: char[161]
 m_flFallbackWear: float32
 m_bAnimatedEveryTick: bool
 m_nSolidType: SolidType_t
 m_bEligibleForScreenHighlight: bool
 m_flDisplayHealth: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_iClip1: int32
 m_nEntityId: uint32
 m_nCollisionFunctionMask: uint8
 m_flShadowStrength: float32
 m_iReapplyProvisionParity: int32
 m_bInitialized: bool
 m_flFireSequenceStartTime: float32
 m_bReloadVisuallyComplete: bool
 m_fLastShotTime: GameTime_t
 m_iOriginalTeamNumber: int32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 CBodyComponent: CBodyComponent
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nCollisionGroup: uint8
 m_usSolidFlags: uint8
 m_CollisionGroup: uint8
 m_flDecalHealHeightRate: float32
 m_iBurstShotsRemaining: int32
 CRenderComponent: CRenderComponent
 m_bClientSideRagdoll: bool
 m_nRenderFX: RenderFx_t
 m_vCapsuleCenter1: Vector
 m_ProviderType: attributeprovidertypes_t
 m_iRecoilIndex: int32
 m_flAnimTime: float32
 m_vecForce: Vector
 m_bPlayerFireEventIsPrimary: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_bClientRagdoll: bool
 m_fAccuracyPenalty: float32
 m_nFallbackSeed: int32
 m_flDroppedAtTime: GameTime_t
 m_nNextPrimaryAttackTick: GameTick_t
 m_nInteractsAs: uint64
 m_nInteractsExclude: uint64
 m_nDropTick: GameTick_t
 m_iClip2: int32
 m_MoveType: MoveType_t
 m_LightGroup: CUtlStringToken
 m_flGlowBackfaceMult: float32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
 m_flRecoilIndex: float32
 m_nEnablePhysics: uint8
 m_weaponMode: CSWeaponMode
 m_bSimulatedEveryTick: bool
 m_bvDisabledHitGroups: uint32[1]
 m_nSubclassID: CUtlStringToken
 m_flNavIgnoreUntilTime: GameTime_t
 m_flNextSecondaryAttackTickRatio: float32
 m_nHierarchyId: uint16
 m_triggerBloat: uint8
 m_iEntityQuality: int32
 m_OriginalOwnerXuidLow: uint32
 m_nFallbackPaintKit: int32
 m_pReserveAmmo: int32[2]
 m_vecMaxs: Vector
 m_flFadeScale: float32
 m_iGlowType: int32
 m_nGlowRange: int32
 m_flDecalHealBloodRate: float32
 m_OriginalOwnerXuidHigh: uint32
 m_nFireSequenceStartTimeChange: int32
 m_nNextSecondaryAttackTick: GameTick_t
 m_flGlowTime: float32
 m_nForceBone: int32
 m_ubInterpolationFrame: uint8
 m_bShouldAnimateDuringGameplayPause: bool
 m_iItemIDHigh: uint32
 m_clrRender: Color
 m_nOwnerId: uint32
 m_flGlowStartTime: float32
 m_fadeMaxDist: float32
 m_MoveCollide: MoveCollide_t
 m_nRenderMode: RenderMode_t
 m_iNumEmptyAttacks: int32
 m_zoomLevel: int32
 m_nSurroundType: SurroundingBoundsType_t
 m_hOuter: CHandle< CBaseEntity >
 m_hEffectEntity: CHandle< CBaseEntity >
 m_iItemDefinitionIndex: uint16
 m_nFallbackStatTrak: int32
 m_flSimulationTime: float32
 m_iGlowTeam: int32
 m_glowColorOverride: Color
 m_iEntityLevel: uint32
 m_nAddDecal: int32
 m_bRenderToCubemaps: bool
 m_vecMins: Vector
 m_bIsHauledBack: bool

112 CInfoMapRegion
 m_flCreateTime: GameTime_t
 m_iTeamNum: uint8
 m_flElasticity: float32
 m_flNavIgnoreUntilTime: GameTime_t
 CBodyComponent: CBodyComponent
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
 m_nSubclassID: CUtlStringToken
 m_szLocToken: char[128]
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bSimulatedEveryTick: bool
 m_flSimulationTime: float32
 m_MoveCollide: MoveCollide_t
 m_ubInterpolationFrame: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_flRadius: float32
 m_flAnimTime: float32
 m_MoveType: MoveType_t

160 CPredictedViewModel
 m_flAnimTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 CBodyComponent: CBodyComponent
  m_nBoolVariablesCount: int32
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_flLastTeleportTime: float32
  m_nResetEventsParity: int32
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_nRandomSeedOffset: int32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_hSequence: HSequence
  m_MeshGroupMask: uint64
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
 m_fEffects: uint32
 m_nViewModelIndex: uint32
 m_flSimulationTime: float32
 m_clrRender: Color
 m_nAnimationParity: uint32
 m_flAnimationStartTime: float32
 m_hWeapon: CHandle< CBasePlayerWeapon >
 m_hControlPanel: CHandle< CBaseEntity >

40 CCSGO_TeamSelectCharacterPosition
 m_xuid: uint64
 m_iItemDefinitionIndex: uint16
 m_iItemIDLow: uint32
 m_flSimulationTime: float32
 m_flElasticity: float32
 m_sWeaponName: CUtlString
 m_nSubclassID: CUtlStringToken
 m_nOrdinal: int32
 m_bInitialized: bool
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_iInventoryPosition: uint32
 m_ubInterpolationFrame: uint8
 m_iEntityLevel: uint32
 m_iItemIDHigh: uint32
 CBodyComponent: CBodyComponent
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
 m_bAnimatedEveryTick: bool
 m_nRandom: int32
 m_iAccountID: uint32
 m_szCustomName: char[161]
 m_MoveCollide: MoveCollide_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_iTeamNum: uint8
 m_bSimulatedEveryTick: bool
 m_nVariant: int32
 m_iEntityQuality: int32
 m_flAnimTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flCreateTime: GameTime_t
 m_fEffects: uint32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveType: MoveType_t

44 CCSGO_WingmanIntroCounterTerroristPosition
 CBodyComponent: CBodyComponent
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
 m_sWeaponName: CUtlString
 m_iEntityLevel: uint32
 m_iItemIDHigh: uint32
 m_flAnimTime: float32
 m_MoveCollide: MoveCollide_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flElasticity: float32
 m_iItemIDLow: uint32
 m_iAccountID: uint32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flCreateTime: GameTime_t
 m_iTeamNum: uint8
 m_nVariant: int32
 m_nRandom: int32
 m_xuid: uint64
 m_szCustomName: char[161]
 m_bAnimatedEveryTick: bool
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
 m_flSimulationTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSubclassID: CUtlStringToken
 m_fEffects: uint32
 m_iItemDefinitionIndex: uint16
 m_iEntityQuality: int32
 m_MoveType: MoveType_t
 m_ubInterpolationFrame: uint8
 m_bSimulatedEveryTick: bool
 m_nOrdinal: int32
 m_iInventoryPosition: uint32
 m_bInitialized: bool

77 CEnvParticleGlow
 m_ColorTint: Color
 m_flFreezeTransitionDuration: float32
 m_nStopType: int32
 m_flAlphaScale: float32
 m_bAnimateDuringGameplayPause: bool
 m_iServerControlPointAssignments: uint8[4]
 m_bNoFreeze: bool
 m_bFrozen: bool
 m_flPreSimTime: float32
 m_vServerControlPoints: Vector[4]
 m_hControlPointEnts: CHandle< CBaseEntity >[64]
 m_bNoSave: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_szSnapshotFileName: char[512]
 m_bActive: bool
 m_flRadiusScale: float32
 m_hTextureOverride: CStrongHandle< InfoForResourceTypeCTextureBase >
 m_flStartTime: GameTime_t
 m_bNoRamp: bool
 m_flSelfIllumScale: float32
 CBodyComponent: CBodyComponent
  m_cellX: uint16
  m_cellZ: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_iEffectIndex: CStrongHandle< InfoForResourceTypeIParticleSystemDefinition >

102 CGrassBurn
 m_flSimulationTime: float32
 m_flCreateTime: GameTime_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flAnimTime: float32
 CBodyComponent: CBodyComponent
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveCollide: MoveCollide_t
 m_flGrassBurnClearTime: float32
 m_nSubclassID: CUtlStringToken
 m_iTeamNum: uint8
 m_bSimulatedEveryTick: bool
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_MoveType: MoveType_t
 m_ubInterpolationFrame: uint8
 m_fEffects: uint32
 m_flElasticity: float32

143 CPhysPropWeaponUpgrade
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flElasticity: float32
 m_vecSpecifiedSurroundingMins: Vector
 m_flGlowStartTime: float32
 m_flShadowStrength: float32
 m_nObjectCulling: uint8
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_ubInterpolationFrame: uint8
 m_nGlowRangeMin: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_noGhostCollision: bool
 m_bvDisabledHitGroups: uint32[1]
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSubclassID: CUtlStringToken
 m_vCapsuleCenter1: Vector
 m_bEligibleForScreenHighlight: bool
 m_flGlowBackfaceMult: float32
 m_fEffects: uint32
 m_LightGroup: CUtlStringToken
 m_nInteractsWith: uint64
 m_nInteractsExclude: uint64
 m_flGlowTime: float32
 m_bAwake: bool
 m_bRenderToCubemaps: bool
 m_nOwnerId: uint32
 m_nCollisionFunctionMask: uint8
 m_vecMaxs: Vector
 CRenderComponent: CRenderComponent
 m_nEntityId: uint32
 m_nCollisionGroup: uint8
 m_triggerBloat: uint8
 m_bFlashing: bool
 m_bAnimGraphUpdateEnabled: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_fadeMinDist: float32
 m_nForceBone: int32
 CBodyComponent: CBodyComponent
  m_bUseParentRenderBounds: bool
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_name: CUtlStringToken
  m_flPrevCycle: float32
  m_nIdealMotionType: int8
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_flLastTeleportTime: float32
  m_angRotation: QAngle
  m_flScale: float32
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_cellY: uint16
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_hSequence: HSequence
  m_materialGroup: CUtlStringToken
  m_nHitboxSet: uint8
  m_flWeight: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_nBoolVariablesCount: int32
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_vecX: CNetworkedQuantizedFloat
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_bClientSideAnimation: bool
  m_flCycle: float32
  m_bClientClothCreationSuppressed: bool
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nOutsideWorld: uint16
  m_nResetEventsParity: int32
  m_vecY: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_MeshGroupMask: uint64
  m_nNewSequenceParity: int32
  m_nRandomSeedOffset: int32
 m_fadeMaxDist: float32
 m_vDecalForwardAxis: Vector
 m_iTeamNum: uint8
 m_bSimulatedEveryTick: bool
 m_nEnablePhysics: uint8
 m_flFadeScale: float32
 m_flDecalHealHeightRate: float32
 m_vecForce: Vector
 m_bAnimatedEveryTick: bool
 m_nRenderMode: RenderMode_t
 m_usSolidFlags: uint8
 m_glowColorOverride: Color
 m_vDecalPosition: Vector
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_bClientRagdoll: bool
 m_nRenderFX: RenderFx_t
 m_vecMins: Vector
 m_iGlowTeam: int32
 m_MoveCollide: MoveCollide_t
 m_clrRender: Color
 m_nSolidType: SolidType_t
 m_flDecalHealBloodRate: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_flCreateTime: GameTime_t
 m_nSurroundType: SurroundingBoundsType_t
 m_flSimulationTime: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nInteractsAs: uint64
 m_nGlowRange: int32
 m_MoveType: MoveType_t
 m_bClientSideRagdoll: bool
 m_spawnflags: uint32
 m_flNavIgnoreUntilTime: GameTime_t
 m_nHierarchyId: uint16
 m_CollisionGroup: uint8
 m_vCapsuleCenter2: Vector
 m_flCapsuleRadius: float32
 m_iGlowType: int32
 m_nAddDecal: int32
 m_bInitiallyPopulateInterpHistory: bool

169 CSensorGrenadeProjectile
 m_flElasticity: float32
 m_nInteractsAs: uint64
 m_vecMins: Vector
 m_iGlowTeam: int32
 m_bInitiallyPopulateInterpHistory: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_nCollisionFunctionMask: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vCapsuleCenter2: Vector
 m_vLookTargetPosition: Vector
 m_vecZ: CNetworkedQuantizedFloat
 m_nEntityId: uint32
 m_MoveType: MoveType_t
 m_fEffects: uint32
 m_nCollisionGroup: uint8
 m_nGlowRange: int32
 m_vecX: CNetworkedQuantizedFloat
 m_bAnimatedEveryTick: bool
 m_nInteractsWith: uint64
 m_vCapsuleCenter1: Vector
 m_bFlashing: bool
 m_flGlowTime: float32
 m_vecForce: Vector
 CRenderComponent: CRenderComponent
 m_vecMaxs: Vector
 m_vecSpecifiedSurroundingMins: Vector
 m_fadeMaxDist: float32
 m_nAddDecal: int32
 m_flDamage: float32
 m_nExplodeEffectIndex: CStrongHandle< InfoForResourceTypeIParticleSystemDefinition >
 m_vecY: CNetworkedQuantizedFloat
 m_bShouldAnimateDuringGameplayPause: bool
 m_nRenderMode: RenderMode_t
 m_LightGroup: CUtlStringToken
 m_nOwnerId: uint32
 m_nEnablePhysics: uint8
 m_fadeMinDist: float32
 m_vDecalPosition: Vector
 m_vDecalForwardAxis: Vector
 m_bClientSideRagdoll: bool
 m_nSolidType: SolidType_t
 m_iGlowType: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flGlowStartTime: float32
 m_hThrower: CHandle< CBaseEntity >
 CBodyComponent: CBodyComponent
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nBoolVariablesCount: int32
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flScale: float32
  m_bClientClothCreationSuppressed: bool
  m_cellY: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_nRandomSeedOffset: int32
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_bUseParentRenderBounds: bool
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_cellZ: uint16
  m_materialGroup: CUtlStringToken
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_bClientSideAnimation: bool
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flLastTeleportTime: float32
  m_angRotation: QAngle
  m_nOutsideWorld: uint16
  m_flWeight: CNetworkedQuantizedFloat
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_cellX: uint16
 m_flCreateTime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_bRenderToCubemaps: bool
 m_nGlowRangeMin: int32
 m_flFadeScale: float32
 m_nExplodeEffectTickBegin: int32
 m_bIsLive: bool
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bSimulatedEveryTick: bool
 m_nSurroundType: SurroundingBoundsType_t
 m_flCapsuleRadius: float32
 m_flGlowBackfaceMult: float32
 m_flShadowStrength: float32
 m_nObjectCulling: uint8
 m_DmgRadius: float32
 m_bvDisabledHitGroups: uint32[1]
 m_nSubclassID: CUtlStringToken
 m_bClientRagdoll: bool
 m_flDecalHealHeightRate: float32
 m_flSimulationTime: float32
 m_iTeamNum: uint8
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_usSolidFlags: uint8
 m_CollisionGroup: uint8
 m_glowColorOverride: Color
 m_flDecalHealBloodRate: float32
 m_vecExplodeEffectOrigin: Vector
 m_triggerBloat: uint8
 m_bAnimGraphUpdateEnabled: bool
 m_fFlags: uint32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_clrRender: Color
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_MoveCollide: MoveCollide_t
 m_nRenderFX: RenderFx_t
 m_nInteractsExclude: uint64
 m_nHierarchyId: uint16
 m_bEligibleForScreenHighlight: bool
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nForceBone: int32
 m_flDetonateTime: GameTime_t
 m_vInitialVelocity: Vector
 m_nBounces: int32

197 CWaterBullet
 m_bClientSideRagdoll: bool
 m_bSimulatedEveryTick: bool
 m_bFlashing: bool
 m_flGlowStartTime: float32
 m_bClientRagdoll: bool
 m_flAnimTime: float32
 m_bRenderToCubemaps: bool
 m_nEntityId: uint32
 m_nCollisionFunctionMask: uint8
 m_vecSpecifiedSurroundingMins: Vector
 CBodyComponent: CBodyComponent
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nBoolVariablesCount: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_flWeight: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
 m_nOwnerId: uint32
 m_CollisionGroup: uint8
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_LightGroup: CUtlStringToken
 m_vecSpecifiedSurroundingMaxs: Vector
 m_fadeMinDist: float32
 m_nObjectCulling: uint8
 m_vDecalPosition: Vector
 m_vecMins: Vector
 m_nCollisionGroup: uint8
 m_flCapsuleRadius: float32
 m_glowColorOverride: Color
 m_flShadowStrength: float32
 m_nInteractsAs: uint64
 m_iGlowType: int32
 m_vDecalForwardAxis: Vector
 m_nForceBone: int32
 m_bvDisabledHitGroups: uint32[1]
 m_nRenderFX: RenderFx_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_iGlowTeam: int32
 m_nGlowRange: int32
 m_nAddDecal: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_vecMaxs: Vector
 m_bEligibleForScreenHighlight: bool
 m_flFadeScale: float32
 CRenderComponent: CRenderComponent
 m_flGlowBackfaceMult: float32
 m_vecForce: Vector
 m_hEffectEntity: CHandle< CBaseEntity >
 m_usSolidFlags: uint8
 m_vCapsuleCenter1: Vector
 m_vCapsuleCenter2: Vector
 m_nInteractsExclude: uint64
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_MoveType: MoveType_t
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_nInteractsWith: uint64
 m_clrRender: Color
 m_flGlowTime: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_triggerBloat: uint8
 m_nEnablePhysics: uint8
 m_flDecalHealBloodRate: float32
 m_flSimulationTime: float32
 m_flCreateTime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_iTeamNum: uint8
 m_nHierarchyId: uint16
 m_bAnimGraphUpdateEnabled: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_flNavIgnoreUntilTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_nSolidType: SolidType_t
 m_nSurroundType: SurroundingBoundsType_t
 m_flDecalHealHeightRate: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_MoveCollide: MoveCollide_t
 m_nSubclassID: CUtlStringToken
 m_flElasticity: float32
 m_nGlowRangeMin: int32
 m_fadeMaxDist: float32

22 CBrBaseItem
 m_flGlowTime: float32
 m_flFadeScale: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_ubInterpolationFrame: uint8
 m_nInteractsWith: uint64
 m_nInteractsExclude: uint64
 m_nGlowRange: int32
 m_nGlowRangeMin: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flCreateTime: GameTime_t
 m_clrRender: Color
 m_vDecalForwardAxis: Vector
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecMaxs: Vector
 m_vecSpecifiedSurroundingMins: Vector
 m_nForceBone: int32
 m_MoveType: MoveType_t
 m_fEffects: uint32
 m_nRenderMode: RenderMode_t
 m_nSurroundType: SurroundingBoundsType_t
 m_vDecalPosition: Vector
 m_iTeamNum: uint8
 m_flElasticity: float32
 m_nAddDecal: int32
 m_bShouldAnimateDuringGameplayPause: bool
 m_flGlowBackfaceMult: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_bSimulatedEveryTick: bool
 m_LightGroup: CUtlStringToken
 m_nCollisionFunctionMask: uint8
 m_CollisionGroup: uint8
 m_vCapsuleCenter1: Vector
 m_bEligibleForScreenHighlight: bool
 m_nSubclassID: CUtlStringToken
 m_spawnflags: uint32
 m_vCapsuleCenter2: Vector
 m_fadeMaxDist: float32
 m_nObjectCulling: uint8
 m_bAwake: bool
 CBodyComponent: CBodyComponent
  m_nNewSequenceParity: int32
  m_nRandomSeedOffset: int32
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_name: CUtlStringToken
  m_bUseParentRenderBounds: bool
  m_nIdealMotionType: int8
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_flPrevCycle: float32
  m_angRotation: QAngle
  m_flLastTeleportTime: float32
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_cellY: uint16
  m_flScale: float32
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_cellZ: uint16
  m_hSequence: HSequence
  m_materialGroup: CUtlStringToken
  m_nHitboxSet: uint8
  m_flWeight: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nBoolVariablesCount: int32
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_vecX: CNetworkedQuantizedFloat
  m_bClientSideAnimation: bool
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_flCycle: float32
  m_bClientClothCreationSuppressed: bool
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nResetEventsParity: int32
  m_nOutsideWorld: uint16
  m_hierarchyAttachName: CUtlStringToken
  m_MeshGroupMask: uint64
  m_vecY: CNetworkedQuantizedFloat
 m_MoveCollide: MoveCollide_t
 m_bClientSideRagdoll: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_bRenderToCubemaps: bool
 m_noGhostCollision: bool
 m_nRenderFX: RenderFx_t
 m_bClientRagdoll: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_triggerBloat: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_iGlowType: int32
 m_flDecalHealHeightRate: float32
 CRenderComponent: CRenderComponent
 m_usSolidFlags: uint8
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_flSimulationTime: float32
 m_bAnimatedEveryTick: bool
 m_glowColorOverride: Color
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bAnimGraphUpdateEnabled: bool
 m_vecForce: Vector
 m_bvDisabledHitGroups: uint32[1]
 m_nEntityId: uint32
 m_nOwnerId: uint32
 m_vecMins: Vector
 m_flGlowStartTime: float32
 m_fadeMinDist: float32
 m_iGlowTeam: int32
 m_bFlashing: bool
 m_nInteractsAs: uint64
 m_nHierarchyId: uint16
 m_nCollisionGroup: uint8
 m_nSolidType: SolidType_t
 m_nEnablePhysics: uint8
 m_flCapsuleRadius: float32
 m_flShadowStrength: float32
 m_flDecalHealBloodRate: float32

66 CEconWearable
 m_OriginalOwnerXuidLow: uint32
 m_nSubclassID: CUtlStringToken
 m_vecForce: Vector
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
 m_MoveCollide: MoveCollide_t
 m_vDecalForwardAxis: Vector
 m_iEntityLevel: uint32
 m_LightGroup: CUtlStringToken
 m_nCollisionFunctionMask: uint8
 m_vecMaxs: Vector
 m_bFlashing: bool
 m_flAnimTime: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vDecalPosition: Vector
 m_usSolidFlags: uint8
 m_CollisionGroup: uint8
 m_nObjectCulling: uint8
 m_nEntityId: uint32
 m_iItemIDHigh: uint32
 m_nFallbackStatTrak: int32
 m_nRenderMode: RenderMode_t
 m_nInteractsExclude: uint64
 m_nInteractsWith: uint64
 m_bClientRagdoll: bool
 m_iGlowTeam: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flFallbackWear: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bAnimatedEveryTick: bool
 m_triggerBloat: uint8
 m_nHierarchyId: uint16
 m_glowColorOverride: Color
 m_flDecalHealHeightRate: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_iGlowType: int32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_fadeMinDist: float32
 m_bRenderToCubemaps: bool
 m_nSurroundType: SurroundingBoundsType_t
 m_flGlowBackfaceMult: float32
 m_iInventoryPosition: uint32
 m_OriginalOwnerXuidHigh: uint32
 m_MoveType: MoveType_t
 m_vLookTargetPosition: Vector
 m_nOwnerId: uint32
 m_vCapsuleCenter1: Vector
 m_bSimulatedEveryTick: bool
 m_flGlowStartTime: float32
 m_nFallbackSeed: int32
 m_bInitiallyPopulateInterpHistory: bool
 m_bAnimGraphUpdateEnabled: bool
 m_iAccountID: uint32
 m_flFadeScale: float32
 m_bEligibleForScreenHighlight: bool
 m_flCreateTime: GameTime_t
 m_nEnablePhysics: uint8
 m_nSolidType: SolidType_t
 m_flGlowTime: float32
 CBodyComponent: CBodyComponent
  m_flScale: float32
  m_vecY: CNetworkedQuantizedFloat
  m_nNewSequenceParity: int32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_vecX: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_cellX: uint16
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_flPrevCycle: float32
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_flLastTeleportTime: float32
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nAnimLoopMode: AnimLoopMode_t
  m_nBoolVariablesCount: int32
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_cellY: uint16
 m_nForceBone: int32
 m_iReapplyProvisionParity: int32
 m_hOuter: CHandle< CBaseEntity >
 m_bInitialized: bool
 m_clrRender: Color
 m_nCollisionGroup: uint8
 m_fEffects: uint32
 m_flElasticity: float32
 m_flDecalHealBloodRate: float32
 m_ProviderType: attributeprovidertypes_t
 m_ubInterpolationFrame: uint8
 m_nGlowRangeMin: int32
 m_blinktoggle: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_nRenderFX: RenderFx_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_flexWeight: CNetworkUtlVectorBase< float32 >
 m_flSimulationTime: float32
 m_iTeamNum: uint8
 m_nInteractsAs: uint64
 m_fadeMaxDist: float32
 m_iItemIDLow: uint32
 m_nFallbackPaintKit: int32
 m_bShouldAnimateDuringGameplayPause: bool
 m_bClientSideRagdoll: bool
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vCapsuleCenter2: Vector
 m_nAddDecal: int32
 m_szCustomName: char[161]
 m_flShadowStrength: float32
 m_vecSpecifiedSurroundingMins: Vector
 m_flCapsuleRadius: float32
 m_nGlowRange: int32
 m_iItemDefinitionIndex: uint16
 CRenderComponent: CRenderComponent
 m_vecMins: Vector
 m_iEntityQuality: int32
 m_bvDisabledHitGroups: uint32[1]

109 CInferno
 m_nGlowRange: int32
 m_bFlashing: bool
 m_flGlowTime: float32
 m_bRenderToCubemaps: bool
 m_CollisionGroup: uint8
 m_fireCount: int32
 CRenderComponent: CRenderComponent
 m_nInteractsAs: uint64
 m_flDecalHealBloodRate: float32
 m_nSolidType: SolidType_t
 m_flGlowBackfaceMult: float32
 m_nAddDecal: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_fireYDelta: int32[64]
 m_ubInterpolationFrame: uint8
 m_fEffects: uint32
 m_vCapsuleCenter2: Vector
 m_usSolidFlags: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_flCapsuleRadius: float32
 m_iGlowType: int32
 m_nGlowRangeMin: int32
 m_nObjectCulling: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_nEntityId: uint32
 m_bInPostEffectTime: bool
 m_vecMaxs: Vector
 m_vecSpecifiedSurroundingMins: Vector
 m_LightGroup: CUtlStringToken
 m_nInteractsWith: uint64
 m_fadeMinDist: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nRenderMode: RenderMode_t
 m_nRenderFX: RenderFx_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_triggerBloat: uint8
 m_nEnablePhysics: uint8
 m_vCapsuleCenter1: Vector
 m_MoveCollide: MoveCollide_t
 m_iTeamNum: uint8
 m_nFireEffectTickBegin: int32
 m_flGlowStartTime: float32
 m_fadeMaxDist: float32
 m_flSimulationTime: float32
 m_flElasticity: float32
 m_nCollisionFunctionMask: uint8
 m_vecMins: Vector
 m_flDecalHealHeightRate: float32
 CBodyComponent: CBodyComponent
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
 m_nSubclassID: CUtlStringToken
 m_BurnNormal: Vector[64]
 m_vDecalPosition: Vector
 m_bFireIsBurning: bool[64]
 m_nCollisionGroup: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flShadowStrength: float32
 m_fireParentYDelta: int32[64]
 m_bvDisabledHitGroups: uint32[1]
 m_MoveType: MoveType_t
 m_nHierarchyId: uint16
 m_flFadeScale: float32
 m_vDecalForwardAxis: Vector
 m_fireParentXDelta: int32[64]
 m_fireParentZDelta: int32[64]
 m_clrRender: Color
 m_nOwnerId: uint32
 m_glowColorOverride: Color
 m_fireZDelta: int32[64]
 m_nFireLifetime: float32
 m_bAnimatedEveryTick: bool
 m_nInteractsExclude: uint64
 m_flCreateTime: GameTime_t
 m_bSimulatedEveryTick: bool
 m_iGlowTeam: int32
 m_bEligibleForScreenHighlight: bool
 m_fireXDelta: int32[64]
 m_nInfernoType: int32
 m_flAnimTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32

134 CParticleSystem
 m_flPreSimTime: float32
 m_vServerControlPoints: Vector[4]
 m_iServerControlPointAssignments: uint8[4]
 m_hControlPointEnts: CHandle< CBaseEntity >[64]
 m_bNoSave: bool
 m_bFrozen: bool
 m_szSnapshotFileName: char[512]
 m_nStopType: int32
 m_bAnimateDuringGameplayPause: bool
 m_iEffectIndex: CStrongHandle< InfoForResourceTypeIParticleSystemDefinition >
 m_flStartTime: GameTime_t
 m_bNoRamp: bool
 CBodyComponent: CBodyComponent
  m_hParent: CGameSceneNodeHandle
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_vecX: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_cellX: uint16
  m_cellZ: uint16
 m_bNoFreeze: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_bActive: bool
 m_flFreezeTransitionDuration: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32

108 CIncendiaryGrenade
 m_flSimulationTime: float32
 m_bSimulatedEveryTick: bool
 m_clrRender: Color
 m_iItemDefinitionIndex: uint16
 m_bBurstMode: bool
 m_flThrowStrengthApproach: float32
 m_flAnimTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flDecalHealHeightRate: float32
 m_bvDisabledHitGroups: uint32[1]
 m_fEffects: uint32
 m_bRenderToCubemaps: bool
 m_nObjectCulling: uint8
 m_iReapplyProvisionParity: int32
 m_flRecoilIndex: float32
 m_MoveCollide: MoveCollide_t
 m_ProviderType: attributeprovidertypes_t
 m_bClientSideRagdoll: bool
 m_bAnimatedEveryTick: bool
 m_bAnimGraphUpdateEnabled: bool
 m_bClientRagdoll: bool
 m_nFallbackSeed: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_CollisionGroup: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_vCapsuleCenter2: Vector
 m_nFallbackStatTrak: int32
 m_bSilencerOn: bool
 CRenderComponent: CRenderComponent
 m_pReserveAmmo: int32[2]
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flFadeScale: float32
 m_nAddDecal: int32
 m_iEntityQuality: int32
 m_nFireSequenceStartTimeChange: int32
 m_iOriginalTeamNumber: int32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_MoveType: MoveType_t
 m_iInventoryPosition: uint32
 m_iRecoilIndex: int32
 m_flPostponeFireReadyTime: GameTime_t
 m_nDropTick: GameTick_t
 m_fLastShotTime: GameTime_t
 m_flCreateTime: GameTime_t
 m_nOwnerId: uint32
 m_nSolidType: SolidType_t
 m_fadeMaxDist: float32
 m_LightGroup: CUtlStringToken
 m_nInteractsExclude: uint64
 m_vecMaxs: Vector
 m_iGlowType: int32
 m_nCollisionGroup: uint8
 m_vCapsuleCenter1: Vector
 m_flCapsuleRadius: float32
 m_nGlowRange: int32
 m_flGlowTime: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_flFireSequenceStartTime: float32
 m_nRenderMode: RenderMode_t
 m_nEnablePhysics: uint8
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_fThrowTime: GameTime_t
 m_nHierarchyId: uint16
 m_iGlowTeam: int32
 m_vLookTargetPosition: Vector
 m_nNextPrimaryAttackTick: GameTick_t
 m_flGlowStartTime: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_bInitialized: bool
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_bPlayerFireEventIsPrimary: bool
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_bJumpThrow: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_glowColorOverride: Color
 m_flDecalHealBloodRate: float32
 m_flNextSecondaryAttackTickRatio: float32
 m_vecForce: Vector
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_triggerBloat: uint8
 m_bReloadVisuallyComplete: bool
 m_flDroppedAtTime: GameTime_t
 m_bIsHauledBack: bool
 m_nSubclassID: CUtlStringToken
 m_nEntityId: uint32
 m_bFlashing: bool
 m_vDecalPosition: Vector
 m_iItemIDHigh: uint32
 m_fAccuracyPenalty: float32
 m_bPinPulled: bool
 m_nRenderFX: RenderFx_t
 m_nInteractsAs: uint64
 m_OriginalOwnerXuidLow: uint32
 m_nFallbackPaintKit: int32
 m_bInReload: bool
 m_nGlowRangeMin: int32
 m_flShadowStrength: float32
 m_hOuter: CHandle< CBaseEntity >
 m_iAccountID: uint32
 m_iIronSightMode: int32
 m_bIsHeldByPlayer: bool
 m_usSolidFlags: uint8
 m_flNextPrimaryAttackTickRatio: float32
 m_nNextSecondaryAttackTick: GameTick_t
 m_ubInterpolationFrame: uint8
 m_weaponMode: CSWeaponMode
 CBodyComponent: CBodyComponent
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
 m_vecMins: Vector
 m_flGlowBackfaceMult: float32
 m_fadeMinDist: float32
 m_nNextThinkTick: GameTick_t
 m_iClip2: int32
 m_iTeamNum: uint8
 m_iEntityLevel: uint32
 m_iItemIDLow: uint32
 m_nCollisionFunctionMask: uint8
 m_szCustomName: char[161]
 m_iNumEmptyAttacks: int32
 m_eThrowStatus: EGrenadeThrowState
 m_flThrowStrength: float32
 m_flElasticity: float32
 m_bEligibleForScreenHighlight: bool
 m_iState: WeaponState_t
 m_fDropTime: GameTime_t
 m_iClip1: int32
 m_nInteractsWith: uint64
 m_nForceBone: int32
 m_flFallbackWear: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_OriginalOwnerXuidHigh: uint32
 m_bRedraw: bool
 m_nSurroundType: SurroundingBoundsType_t
 m_vDecalForwardAxis: Vector
 m_nViewModelIndex: uint32
 m_hOwnerEntity: CHandle< CBaseEntity >

138 CPhysicsPropMultiplayer
 m_flFadeScale: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_hOwner: CHandle< CBaseEntity >
  m_Transforms: CNetworkUtlVectorBase< CTransform >
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nOwnerId: uint32
 m_fadeMinDist: float32
 m_bEligibleForScreenHighlight: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_usSolidFlags: uint8
 m_iGlowTeam: int32
 m_bShouldAnimateDuringGameplayPause: bool
 m_nSubclassID: CUtlStringToken
 m_flNavIgnoreUntilTime: GameTime_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nCollisionFunctionMask: uint8
 m_flDecalHealBloodRate: float32
 m_vecForce: Vector
 m_nGlowRangeMin: int32
 m_nObjectCulling: uint8
 m_nAddDecal: int32
 m_ubInterpolationFrame: uint8
 m_fEffects: uint32
 m_clrRender: Color
 m_flDecalHealHeightRate: float32
 m_nForceBone: int32
 m_nRenderMode: RenderMode_t
 m_nInteractsAs: uint64
 m_vDecalPosition: Vector
 m_MoveCollide: MoveCollide_t
 m_flGlowTime: float32
 m_bAwake: bool
 CBodyComponent: CBodyComponent
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_bClientSideAnimation: bool
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_flCycle: float32
  m_bClientClothCreationSuppressed: bool
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nResetEventsParity: int32
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_MeshGroupMask: uint64
  m_nNewSequenceParity: int32
  m_nRandomSeedOffset: int32
  m_name: CUtlStringToken
  m_bUseParentRenderBounds: bool
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_flPrevCycle: float32
  m_nIdealMotionType: int8
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_angRotation: QAngle
  m_flLastTeleportTime: float32
  m_cellY: uint16
  m_flScale: float32
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hParent: CGameSceneNodeHandle
  m_hSequence: HSequence
  m_materialGroup: CUtlStringToken
  m_nHitboxSet: uint8
  m_flWeight: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nBoolVariablesCount: int32
 m_spawnflags: uint32
 m_vecMins: Vector
 m_bAnimatedEveryTick: bool
 m_CollisionGroup: uint8
 m_noGhostCollision: bool
 m_nSurroundType: SurroundingBoundsType_t
 m_flCapsuleRadius: float32
 m_bFlashing: bool
 m_flGlowBackfaceMult: float32
 m_bClientRagdoll: bool
 m_bRenderToCubemaps: bool
 m_nInteractsExclude: uint64
 m_vecMaxs: Vector
 m_bvDisabledHitGroups: uint32[1]
 m_nEnablePhysics: uint8
 m_fadeMaxDist: float32
 m_MoveType: MoveType_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nRenderFX: RenderFx_t
 m_flSimulationTime: float32
 m_vecSpecifiedSurroundingMins: Vector
 m_nGlowRange: int32
 m_nCollisionGroup: uint8
 m_bAnimGraphUpdateEnabled: bool
 m_bSimulatedEveryTick: bool
 m_nInteractsWith: uint64
 m_nHierarchyId: uint16
 m_flElasticity: float32
 m_nSolidType: SolidType_t
 m_triggerBloat: uint8
 m_iGlowType: int32
 m_flShadowStrength: float32
 m_vDecalForwardAxis: Vector
 CRenderComponent: CRenderComponent
 m_bClientSideRagdoll: bool
 m_LightGroup: CUtlStringToken
 m_nEntityId: uint32
 m_vCapsuleCenter1: Vector
 m_vCapsuleCenter2: Vector
 m_glowColorOverride: Color
 m_flGlowStartTime: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flCreateTime: GameTime_t
 m_iTeamNum: uint8
 m_bInitiallyPopulateInterpHistory: bool

162 CRagdollManager
 m_iCurrentMaxRagdollCount: int8

199 CWeaponAWP
 m_bInitialized: bool
 m_fLastShotTime: GameTime_t
 m_iClip2: int32
 m_bFlashing: bool
 m_flShadowStrength: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_flSimulationTime: float32
 m_vecMaxs: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bInReload: bool
 m_bAnimatedEveryTick: bool
 m_usSolidFlags: uint8
 m_flGlowStartTime: float32
 m_flRecoilIndex: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nSurroundType: SurroundingBoundsType_t
 m_iItemIDHigh: uint32
 m_ProviderType: attributeprovidertypes_t
 m_nFireSequenceStartTimeChange: int32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_nSubclassID: CUtlStringToken
 m_ubInterpolationFrame: uint8
 m_LightGroup: CUtlStringToken
 m_nEnablePhysics: uint8
 m_nGlowRangeMin: int32
 m_bShouldAnimateDuringGameplayPause: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveCollide: MoveCollide_t
 m_fEffects: uint32
 m_szCustomName: char[161]
 m_nFallbackStatTrak: int32
 m_nEntityId: uint32
 m_iGlowType: int32
 m_vDecalForwardAxis: Vector
 m_fadeMaxDist: float32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
 m_flNextSecondaryAttackTickRatio: float32
 CBodyComponent: CBodyComponent
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
 m_nGlowRange: int32
 m_bEligibleForScreenHighlight: bool
 m_nViewModelIndex: uint32
 m_vecSpecifiedSurroundingMins: Vector
 m_iRecoilIndex: int32
 m_flNextPrimaryAttackTickRatio: float32
 m_bRenderToCubemaps: bool
 m_iGlowTeam: int32
 m_pReserveAmmo: int32[2]
 m_nNextThinkTick: GameTick_t
 m_vCapsuleCenter2: Vector
 m_vLookTargetPosition: Vector
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_iInventoryPosition: uint32
 m_flFallbackWear: float32
 m_flDroppedAtTime: GameTime_t
 m_vecMins: Vector
 m_weaponMode: CSWeaponMode
 m_OriginalOwnerXuidHigh: uint32
 m_nNextPrimaryAttackTick: GameTick_t
 m_nRenderFX: RenderFx_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_flDecalHealHeightRate: float32
 m_vDecalPosition: Vector
 m_iState: WeaponState_t
 m_bPlayerFireEventIsPrimary: bool
 m_bClientSideRagdoll: bool
 m_fadeMinDist: float32
 m_flFadeScale: float32
 m_flPostponeFireReadyTime: GameTime_t
 m_bvDisabledHitGroups: uint32[1]
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flDecalHealBloodRate: float32
 m_fAccuracyPenalty: float32
 m_OriginalOwnerXuidLow: uint32
 m_nOwnerId: uint32
 m_nAddDecal: int32
 m_iReapplyProvisionParity: int32
 m_glowColorOverride: Color
 m_bSilencerOn: bool
 m_iIronSightMode: int32
 m_bClientRagdoll: bool
 m_hOuter: CHandle< CBaseEntity >
 m_nFallbackSeed: int32
 m_iClip1: int32
 m_flElasticity: float32
 m_bAnimGraphUpdateEnabled: bool
 m_nInteractsExclude: uint64
 m_CollisionGroup: uint8
 m_nObjectCulling: uint8
 m_nFallbackPaintKit: int32
 m_nDropTick: GameTick_t
 m_MoveType: MoveType_t
 m_bSimulatedEveryTick: bool
 m_clrRender: Color
 m_nInteractsWith: uint64
 m_vCapsuleCenter1: Vector
 m_flCapsuleRadius: float32
 m_iItemIDLow: uint32
 m_zoomLevel: int32
 m_iTeamNum: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_nInteractsAs: uint64
 m_iNumEmptyAttacks: int32
 m_iItemDefinitionIndex: uint16
 m_iEntityQuality: int32
 m_iAccountID: uint32
 m_bNeedsBoltAction: bool
 m_nNextSecondaryAttackTick: GameTick_t
 m_nCollisionGroup: uint8
 m_flFireSequenceStartTime: float32
 m_iOriginalTeamNumber: int32
 m_bIsHauledBack: bool
 m_flGlowTime: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_iEntityLevel: uint32
 m_nSolidType: SolidType_t
 m_iBurstShotsRemaining: int32
 m_vecForce: Vector
 m_nRenderMode: RenderMode_t
 m_nHierarchyId: uint16
 m_flGlowBackfaceMult: float32
 m_flAnimTime: float32
 m_nCollisionFunctionMask: uint8
 m_nForceBone: int32
 m_flCreateTime: GameTime_t
 m_bReloadVisuallyComplete: bool
 CRenderComponent: CRenderComponent
 m_triggerBloat: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bBurstMode: bool

53 CCSPropExplodingBarrel
 m_nInteractsWith: uint64
 m_vecSpecifiedSurroundingMins: Vector
 m_flGlowBackfaceMult: float32
 m_bSimulatedEveryTick: bool
 m_bClientSideRagdoll: bool
 m_clrRender: Color
 m_nSolidType: SolidType_t
 m_flGlowTime: float32
 m_bClientRagdoll: bool
 CBodyComponent: CBodyComponent
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_bClientSideAnimation: bool
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_flCycle: float32
  m_bClientClothCreationSuppressed: bool
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nResetEventsParity: int32
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_MeshGroupMask: uint64
  m_nNewSequenceParity: int32
  m_nRandomSeedOffset: int32
  m_name: CUtlStringToken
  m_bUseParentRenderBounds: bool
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_flPrevCycle: float32
  m_nIdealMotionType: int8
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_angRotation: QAngle
  m_flLastTeleportTime: float32
  m_cellY: uint16
  m_flScale: float32
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hParent: CGameSceneNodeHandle
  m_hSequence: HSequence
  m_materialGroup: CUtlStringToken
  m_nHitboxSet: uint8
  m_flWeight: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nBoolVariablesCount: int32
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nCollisionFunctionMask: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nGlowRange: int32
 m_flSimulationTime: float32
 m_LightGroup: CUtlStringToken
 m_flCreateTime: GameTime_t
 m_nRenderFX: RenderFx_t
 m_nCollisionGroup: uint8
 m_vecMins: Vector
 m_bAnimGraphUpdateEnabled: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bRenderToCubemaps: bool
 m_nInteractsAs: uint64
 m_nEntityId: uint32
 m_nEnablePhysics: uint8
 m_flCapsuleRadius: float32
 m_iGlowTeam: int32
 m_vDecalForwardAxis: Vector
 m_flNavIgnoreUntilTime: GameTime_t
 m_flDecalHealBloodRate: float32
 m_noGhostCollision: bool
 m_bFlashing: bool
 m_nSubclassID: CUtlStringToken
 m_ubInterpolationFrame: uint8
 m_bAnimatedEveryTick: bool
 m_vecMaxs: Vector
 m_CollisionGroup: uint8
 m_nGlowRangeMin: int32
 m_glowColorOverride: Color
 m_MoveType: MoveType_t
 m_nForceBone: int32
 m_bAwake: bool
 m_flGlowStartTime: float32
 m_nRenderMode: RenderMode_t
 m_nOwnerId: uint32
 m_bEligibleForScreenHighlight: bool
 m_vDecalPosition: Vector
 m_vecForce: Vector
 m_flElasticity: float32
 m_fEffects: uint32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nHierarchyId: uint16
 m_vCapsuleCenter1: Vector
 m_fadeMinDist: float32
 m_bvDisabledHitGroups: uint32[1]
 m_iTeamNum: uint8
 m_MoveCollide: MoveCollide_t
 m_nInteractsExclude: uint64
 m_fadeMaxDist: float32
 m_nAddDecal: int32
 m_bShouldAnimateDuringGameplayPause: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_usSolidFlags: uint8
 m_vCapsuleCenter2: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_spawnflags: uint32
 m_bInitiallyPopulateInterpHistory: bool
 m_nSurroundType: SurroundingBoundsType_t
 m_nObjectCulling: uint8
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_triggerBloat: uint8
 m_flFadeScale: float32
 m_flShadowStrength: float32
 m_flDecalHealHeightRate: float32
 CRenderComponent: CRenderComponent
 m_iGlowType: int32

96 CFuncMoveLinear
 m_flDecalHealBloodRate: float32
 m_flDecalHealHeightRate: float32
 m_nSurroundType: SurroundingBoundsType_t
 m_nCollisionFunctionMask: uint8
 m_usSolidFlags: uint8
 m_triggerBloat: uint8
 m_flCapsuleRadius: float32
 m_bFlashing: bool
 m_vDecalForwardAxis: Vector
 m_vecY: CNetworkedQuantizedFloat
 m_nRenderFX: RenderFx_t
 m_nRenderMode: RenderMode_t
 m_nEntityId: uint32
 m_flGlowTime: float32
 m_flGlowBackfaceMult: float32
 m_fadeMinDist: float32
 m_MoveType: MoveType_t
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nSolidType: SolidType_t
 m_CollisionGroup: uint8
 m_vCapsuleCenter2: Vector
 m_flAnimTime: float32
 m_iGlowType: int32
 m_nGlowRange: int32
 m_nGlowRangeMin: int32
 m_vDecalPosition: Vector
 m_bSimulatedEveryTick: bool
 m_vecSpecifiedSurroundingMins: Vector
 m_vecSpecifiedSurroundingMaxs: Vector
 m_glowColorOverride: Color
 m_flCreateTime: GameTime_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsExclude: uint64
 m_nEnablePhysics: uint8
 m_flFadeScale: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_vecX: CNetworkedQuantizedFloat
 m_MoveCollide: MoveCollide_t
 m_vecZ: CNetworkedQuantizedFloat
 m_nHierarchyId: uint16
 m_bRenderToCubemaps: bool
 m_flGlowStartTime: float32
 m_bEligibleForScreenHighlight: bool
 m_nAddDecal: int32
 m_bvDisabledHitGroups: uint32[1]
 m_iTeamNum: uint8
 m_nInteractsAs: uint64
 m_nCollisionGroup: uint8
 m_vecMaxs: Vector
 m_fFlags: uint32
 m_flElasticity: float32
 m_clrRender: Color
 m_nInteractsWith: uint64
 m_nOwnerId: uint32
 m_fadeMaxDist: float32
 m_nObjectCulling: uint8
 CBodyComponent: CBodyComponent
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_name: CUtlStringToken
 CRenderComponent: CRenderComponent
 m_vCapsuleCenter1: Vector
 m_flSimulationTime: float32
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_vecMins: Vector
 m_iGlowTeam: int32
 m_flShadowStrength: float32
 m_fEffects: uint32
 m_nSubclassID: CUtlStringToken
 m_ubInterpolationFrame: uint8
 m_LightGroup: CUtlStringToken
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32

218 CWeaponNOVA
 m_nInteractsExclude: uint64
 m_nGlowRangeMin: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_fAccuracyPenalty: float32
 m_flNextSecondaryAttackTickRatio: float32
 CRenderComponent: CRenderComponent
 m_bRenderToCubemaps: bool
 m_vecMaxs: Vector
 m_iEntityLevel: uint32
 m_iAccountID: uint32
 m_iState: WeaponState_t
 m_bInReload: bool
 m_nOwnerId: uint32
 m_flElasticity: float32
 m_fLastShotTime: GameTime_t
 m_nEntityId: uint32
 m_nCollisionFunctionMask: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_flGlowTime: float32
 m_bPlayerFireEventIsPrimary: bool
 m_flDroppedAtTime: GameTime_t
 m_bvDisabledHitGroups: uint32[1]
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_ubInterpolationFrame: uint8
 m_usSolidFlags: uint8
 m_iGlowType: int32
 m_iEntityQuality: int32
 m_flCreateTime: GameTime_t
 m_iGlowTeam: int32
 m_weaponMode: CSWeaponMode
 m_bIsHauledBack: bool
 m_vDecalForwardAxis: Vector
 m_bInitiallyPopulateInterpHistory: bool
 m_iNumEmptyAttacks: int32
 m_bReloadVisuallyComplete: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flDecalHealHeightRate: float32
 m_bAnimGraphUpdateEnabled: bool
 m_iItemIDHigh: uint32
 m_iItemIDLow: uint32
 m_nSurroundType: SurroundingBoundsType_t
 m_flGlowStartTime: float32
 m_nAddDecal: int32
 m_bShouldAnimateDuringGameplayPause: bool
 m_flPostponeFireReadyTime: GameTime_t
 m_nViewModelIndex: uint32
 m_clrRender: Color
 m_nGlowRange: int32
 m_hOuter: CHandle< CBaseEntity >
 m_iInventoryPosition: uint32
 m_flRecoilIndex: float32
 m_vCapsuleCenter2: Vector
 m_flFadeScale: float32
 m_MoveCollide: MoveCollide_t
 m_MoveType: MoveType_t
 m_nSubclassID: CUtlStringToken
 m_fEffects: uint32
 m_triggerBloat: uint8
 m_fadeMaxDist: float32
 m_nForceBone: int32
 m_iIronSightMode: int32
 m_nNextPrimaryAttackTick: GameTick_t
 m_iClip2: int32
 m_nEnablePhysics: uint8
 m_nObjectCulling: uint8
 m_flNextPrimaryAttackTickRatio: float32
 m_fadeMinDist: float32
 m_flShadowStrength: float32
 m_nDropTick: GameTick_t
 m_szCustomName: char[161]
 m_nNextSecondaryAttackTick: GameTick_t
 m_vecForce: Vector
 m_ProviderType: attributeprovidertypes_t
 m_iOriginalTeamNumber: int32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_flAnimTime: float32
 m_flSimulationTime: float32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_flFallbackWear: float32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_iClip1: int32
 m_flNavIgnoreUntilTime: GameTime_t
 m_nSolidType: SolidType_t
 m_glowColorOverride: Color
 m_bInitialized: bool
 m_nRenderFX: RenderFx_t
 m_OriginalOwnerXuidLow: uint32
 m_CollisionGroup: uint8
 m_vCapsuleCenter1: Vector
 m_flCapsuleRadius: float32
 m_OriginalOwnerXuidHigh: uint32
 m_nFallbackStatTrak: int32
 m_nRenderMode: RenderMode_t
 m_nHierarchyId: uint16
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_iItemDefinitionIndex: uint16
 m_nFallbackPaintKit: int32
 m_bClientSideRagdoll: bool
 m_bAnimatedEveryTick: bool
 m_vDecalPosition: Vector
 m_nCollisionGroup: uint8
 m_vecMins: Vector
 m_bFlashing: bool
 m_iRecoilIndex: int32
 m_pReserveAmmo: int32[2]
 CBodyComponent: CBodyComponent
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_nNewSequenceParity: int32
  m_bClientSideAnimation: bool
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_hParent: CGameSceneNodeHandle
  m_nRandomSeedOffset: int32
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
 m_nInteractsAs: uint64
 m_flFireSequenceStartTime: float32
 m_nFireSequenceStartTimeChange: int32
 m_bBurstMode: bool
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_LightGroup: CUtlStringToken
 m_flGlowBackfaceMult: float32
 m_iReapplyProvisionParity: int32
 m_nInteractsWith: uint64
 m_bClientRagdoll: bool
 m_vLookTargetPosition: Vector
 m_bSimulatedEveryTick: bool
 m_flDecalHealBloodRate: float32
 m_nFallbackSeed: int32
 m_nNextThinkTick: GameTick_t
 m_iTeamNum: uint8
 m_bEligibleForScreenHighlight: bool
 m_bSilencerOn: bool

69 CEnvCombinedLightProbeVolume
 m_nLightProbeSizeZ: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveType: MoveType_t
 m_flCreateTime: GameTime_t
 m_bCustomCubemapTexture: bool
 m_hLightProbeDirectLightShadowsTexture: CStrongHandle< InfoForResourceTypeCTextureBase >
 m_vBoxMins: Vector
 m_flEdgeFadeDist: float32
 m_bEnabled: bool
 m_nSubclassID: CUtlStringToken
 m_ubInterpolationFrame: uint8
 m_bAnimatedEveryTick: bool
 m_Color: Color
 m_hCubemapTexture: CStrongHandle< InfoForResourceTypeCTextureBase >
 m_bStartDisabled: bool
 m_nLightProbeAtlasZ: int32
 m_iTeamNum: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_hLightProbeDirectLightIndicesTexture: CStrongHandle< InfoForResourceTypeCTextureBase >
 m_nLightProbeAtlasY: int32
 m_flBrightness: float32
 m_bMoveable: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 CBodyComponent: CBodyComponent
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
 m_vBoxMaxs: Vector
 m_LightGroups: CUtlSymbolLarge
 m_nEnvCubeMapArrayIndex: int32
 m_nLightProbeSizeY: int32
 m_flSimulationTime: float32
 m_bSimulatedEveryTick: bool
 m_hLightProbeTexture: CStrongHandle< InfoForResourceTypeCTextureBase >
 m_nHandshake: int32
 m_nPriority: int32
 m_vEdgeFadeDists: Vector
 m_nLightProbeAtlasX: int32
 m_flAnimTime: float32
 m_MoveCollide: MoveCollide_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_flElasticity: float32
 m_hLightProbeDirectLightScalarsTexture: CStrongHandle< InfoForResourceTypeCTextureBase >
 m_nLightProbeSizeX: int32

72 CEnvCubemapFog
 m_nSubclassID: CUtlStringToken
 m_bHeightFogEnabled: bool
 m_flFogHeightWidth: float32
 m_flFogMaxOpacity: float32
 m_hFogCubemapTexture: CStrongHandle< InfoForResourceTypeCTextureBase >
 CBodyComponent: CBodyComponent
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
 m_ubInterpolationFrame: uint8
 m_iTeamNum: uint8
 m_flFogFalloffExponent: float32
 m_flLODBias: float32
 m_bStartDisabled: bool
 m_flSimulationTime: float32
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_flEndDistance: float32
 m_MoveType: MoveType_t
 m_flFogHeightStart: float32
 m_hSkyMaterial: CStrongHandle< InfoForResourceTypeIMaterial2 >
 m_flAnimTime: float32
 m_MoveCollide: MoveCollide_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flFogHeightEnd: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flNavIgnoreUntilTime: GameTime_t
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flCreateTime: GameTime_t
 m_flElasticity: float32
 m_bSimulatedEveryTick: bool
 m_flStartDistance: float32
 m_flFogHeightExponent: float32
 m_nCubemapSourceType: int32
 m_iszSkyEntity: CUtlSymbolLarge
 m_bActive: bool
 m_bHasHeightFogEnd: bool

137 CPhysicsProp
 m_flGlowStartTime: float32
 m_flFadeScale: float32
 m_ubInterpolationFrame: uint8
 m_nInteractsAs: uint64
 m_vecMaxs: Vector
 m_nEnablePhysics: uint8
 m_vCapsuleCenter1: Vector
 m_vDecalForwardAxis: Vector
 m_spawnflags: uint32
 m_nRenderMode: RenderMode_t
 m_fadeMinDist: float32
 m_fadeMaxDist: float32
 m_flDecalHealHeightRate: float32
 m_nForceBone: int32
 CRenderComponent: CRenderComponent
 m_nSubclassID: CUtlStringToken
 m_flGlowTime: float32
 m_nSolidType: SolidType_t
 m_CollisionGroup: uint8
 m_nObjectCulling: uint8
 m_vDecalPosition: Vector
 m_flNavIgnoreUntilTime: GameTime_t
 m_nOwnerId: uint32
 m_flGlowBackfaceMult: float32
 m_vecMins: Vector
 m_glowColorOverride: Color
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bAwake: bool
 CBodyComponent: CBodyComponent
  m_flCycle: float32
  m_bClientClothCreationSuppressed: bool
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nOutsideWorld: uint16
  m_nResetEventsParity: int32
  m_vecY: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_MeshGroupMask: uint64
  m_nNewSequenceParity: int32
  m_nRandomSeedOffset: int32
  m_bUseParentRenderBounds: bool
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_name: CUtlStringToken
  m_flPrevCycle: float32
  m_nIdealMotionType: int8
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_flLastTeleportTime: float32
  m_angRotation: QAngle
  m_flScale: float32
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_cellY: uint16
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_hSequence: HSequence
  m_materialGroup: CUtlStringToken
  m_nHitboxSet: uint8
  m_flWeight: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_nBoolVariablesCount: int32
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_vecX: CNetworkedQuantizedFloat
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_bClientSideAnimation: bool
 m_nCollisionFunctionMask: uint8
 m_usSolidFlags: uint8
 m_flShadowStrength: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_bvDisabledHitGroups: uint32[1]
 m_bClientSideRagdoll: bool
 m_nInteractsExclude: uint64
 m_flDecalHealBloodRate: float32
 m_noGhostCollision: bool
 m_flCreateTime: GameTime_t
 m_nInteractsWith: uint64
 m_nGlowRange: int32
 m_bAnimGraphUpdateEnabled: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_flCapsuleRadius: float32
 m_nCollisionGroup: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bFlashing: bool
 m_bClientRagdoll: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flElasticity: float32
 m_iGlowTeam: int32
 m_nGlowRangeMin: int32
 m_vecForce: Vector
 m_triggerBloat: uint8
 m_vCapsuleCenter2: Vector
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nEntityId: uint32
 m_bAnimatedEveryTick: bool
 m_nRenderFX: RenderFx_t
 m_nSurroundType: SurroundingBoundsType_t
 m_vecSpecifiedSurroundingMins: Vector
 m_iGlowType: int32
 m_nAddDecal: int32
 m_flSimulationTime: float32
 m_fEffects: uint32
 m_LightGroup: CUtlStringToken
 m_bRenderToCubemaps: bool
 m_bEligibleForScreenHighlight: bool
 m_bSimulatedEveryTick: bool
 m_clrRender: Color
 m_iTeamNum: uint8
 m_nHierarchyId: uint16
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_hOwner: CHandle< CBaseEntity >
  m_Transforms: CNetworkUtlVectorBase< CTransform >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveType: MoveType_t
 m_MoveCollide: MoveCollide_t
 m_bShouldAnimateDuringGameplayPause: bool

187 CSurvivalSpawnChopper
 m_nCollisionFunctionMask: uint8
 m_nGlowRange: int32
 m_glowColorOverride: Color
 m_flGlowTime: float32
 m_fadeMinDist: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 CBodyComponent: CBodyComponent
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nBoolVariablesCount: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
 m_flGlowBackfaceMult: float32
 m_nRenderFX: RenderFx_t
 m_flCreateTime: GameTime_t
 m_bClientSideRagdoll: bool
 m_iGlowType: int32
 m_flFadeScale: float32
 m_flShadowStrength: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_vecForce: Vector
 m_flAnimTime: float32
 m_CollisionGroup: uint8
 m_vCapsuleCenter1: Vector
 m_nGlowRangeMin: int32
 m_flGlowStartTime: float32
 m_bEligibleForScreenHighlight: bool
 m_nInteractsWith: uint64
 m_nSolidType: SolidType_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nHierarchyId: uint16
 m_nEntityId: uint32
 m_nEnablePhysics: uint8
 m_vCapsuleCenter2: Vector
 m_bFlashing: bool
 m_vDecalPosition: Vector
 m_iTeamNum: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_clrRender: Color
 m_bAnimatedEveryTick: bool
 m_iGlowTeam: int32
 m_flDecalHealHeightRate: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bSimulatedEveryTick: bool
 m_nOwnerId: uint32
 m_nCollisionGroup: uint8
 m_usSolidFlags: uint8
 m_nForceBone: int32
 m_bvDisabledHitGroups: uint32[1]
 m_ubInterpolationFrame: uint8
 m_vecMins: Vector
 m_bRenderToCubemaps: bool
 m_MoveType: MoveType_t
 m_fEffects: uint32
 m_vDecalForwardAxis: Vector
 m_bInitiallyPopulateInterpHistory: bool
 m_bAnimGraphUpdateEnabled: bool
 m_MoveCollide: MoveCollide_t
 m_vecSpecifiedSurroundingMins: Vector
 m_nAddDecal: int32
 m_flNavIgnoreUntilTime: GameTime_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsAs: uint64
 m_fadeMaxDist: float32
 m_flDecalHealBloodRate: float32
 m_bClientRagdoll: bool
 m_flElasticity: float32
 m_LightGroup: CUtlStringToken
 m_nSubclassID: CUtlStringToken
 m_nRenderMode: RenderMode_t
 m_vecMaxs: Vector
 m_triggerBloat: uint8
 m_nObjectCulling: uint8
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nInteractsExclude: uint64
 m_flCapsuleRadius: float32
 CRenderComponent: CRenderComponent
 m_flSimulationTime: float32

2 CBaseAnimGraph
 m_nCollisionGroup: uint8
 m_flFadeScale: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_ubInterpolationFrame: uint8
 m_vecMins: Vector
 m_vecMaxs: Vector
 m_nObjectCulling: uint8
 m_nRenderMode: RenderMode_t
 m_nSolidType: SolidType_t
 m_iGlowTeam: int32
 m_flDecalHealBloodRate: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_flCreateTime: GameTime_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecSpecifiedSurroundingMins: Vector
 m_vCapsuleCenter1: Vector
 m_vCapsuleCenter2: Vector
 m_flShadowStrength: float32
 m_vDecalPosition: Vector
 m_bvDisabledHitGroups: uint32[1]
 m_bClientSideRagdoll: bool
 m_bSimulatedEveryTick: bool
 m_bAnimatedEveryTick: bool
 m_vDecalForwardAxis: Vector
 m_flSimulationTime: float32
 m_bRenderToCubemaps: bool
 m_nInteractsAs: uint64
 m_fadeMaxDist: float32
 CBodyComponent: CBodyComponent
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_flLastTeleportTime: float32
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nBoolVariablesCount: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
 m_nEntityId: uint32
 m_flGlowStartTime: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nCollisionFunctionMask: uint8
 m_usSolidFlags: uint8
 m_triggerBloat: uint8
 m_nEnablePhysics: uint8
 m_flGlowBackfaceMult: float32
 m_nHierarchyId: uint16
 m_flElasticity: float32
 m_LightGroup: CUtlStringToken
 m_nSubclassID: CUtlStringToken
 m_MoveCollide: MoveCollide_t
 m_nOwnerId: uint32
 m_nGlowRange: int32
 m_bEligibleForScreenHighlight: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_clrRender: Color
 m_iGlowType: int32
 m_nGlowRangeMin: int32
 m_flGlowTime: float32
 m_fadeMinDist: float32
 m_nForceBone: int32
 m_MoveType: MoveType_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsWith: uint64
 m_nInteractsExclude: uint64
 m_nSurroundType: SurroundingBoundsType_t
 m_CollisionGroup: uint8
 m_glowColorOverride: Color
 m_bFlashing: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_bShouldAnimateDuringGameplayPause: bool
 m_nRenderFX: RenderFx_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nAddDecal: int32
 m_bAnimGraphUpdateEnabled: bool
 m_bClientRagdoll: bool
 m_flAnimTime: float32
 m_iTeamNum: uint8
 m_flCapsuleRadius: float32
 m_flDecalHealHeightRate: float32
 m_vecForce: Vector
 CRenderComponent: CRenderComponent
 m_fEffects: uint32

12 CBaseModelEntity
 CBodyComponent: CBodyComponent
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
 m_nInteractsAs: uint64
 m_nHierarchyId: uint16
 m_usSolidFlags: uint8
 m_vCapsuleCenter1: Vector
 m_flAnimTime: float32
 m_flSimulationTime: float32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nOwnerId: uint32
 m_flCapsuleRadius: float32
 m_fadeMinDist: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_iTeamNum: uint8
 m_nRenderFX: RenderFx_t
 m_bRenderToCubemaps: bool
 m_nEntityId: uint32
 m_fEffects: uint32
 m_LightGroup: CUtlStringToken
 m_triggerBloat: uint8
 m_vDecalForwardAxis: Vector
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_bvDisabledHitGroups: uint32[1]
 m_flGlowStartTime: float32
 m_flShadowStrength: float32
 m_flDecalHealBloodRate: float32
 m_MoveType: MoveType_t
 m_flElasticity: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsWith: uint64
 m_glowColorOverride: Color
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_vecMins: Vector
 m_flGlowTime: float32
 m_nObjectCulling: uint8
 m_bEligibleForScreenHighlight: bool
 m_MoveCollide: MoveCollide_t
 m_flCreateTime: GameTime_t
 m_bSimulatedEveryTick: bool
 m_clrRender: Color
 m_iGlowType: int32
 m_vecMaxs: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nEnablePhysics: uint8
 m_iGlowTeam: int32
 m_bFlashing: bool
 m_vDecalPosition: Vector
 m_vCapsuleCenter2: Vector
 m_flGlowBackfaceMult: float32
 m_nAddDecal: int32
 m_flDecalHealHeightRate: float32
 m_nGlowRange: int32
 CRenderComponent: CRenderComponent
 m_nSubclassID: CUtlStringToken
 m_ubInterpolationFrame: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nSolidType: SolidType_t
 m_CollisionGroup: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_nInteractsExclude: uint64
 m_nSurroundType: SurroundingBoundsType_t
 m_nGlowRangeMin: int32
 m_fadeMaxDist: float32
 m_flFadeScale: float32
 m_nRenderMode: RenderMode_t
 m_nCollisionGroup: uint8
 m_nCollisionFunctionMask: uint8

106 CHostageCarriableProp
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bSimulatedEveryTick: bool
 m_bInitiallyPopulateInterpHistory: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_flElasticity: float32
 m_nInteractsWith: uint64
 m_fadeMaxDist: float32
 m_nRenderMode: RenderMode_t
 m_triggerBloat: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flGlowBackfaceMult: float32
 m_fadeMinDist: float32
 m_flSimulationTime: float32
 m_bClientSideRagdoll: bool
 m_iTeamNum: uint8
 m_bvDisabledHitGroups: uint32[1]
 m_bShouldAnimateDuringGameplayPause: bool
 m_bClientRagdoll: bool
 m_fEffects: uint32
 m_bRenderToCubemaps: bool
 m_vCapsuleCenter1: Vector
 m_flGlowTime: float32
 m_flFadeScale: float32
 m_nObjectCulling: uint8
 m_flAnimTime: float32
 m_nHierarchyId: uint16
 m_vecSpecifiedSurroundingMins: Vector
 m_iGlowType: int32
 m_glowColorOverride: Color
 m_nForceBone: int32
 m_MoveType: MoveType_t
 m_nSolidType: SolidType_t
 m_iGlowTeam: int32
 m_nAddDecal: int32
 m_CollisionGroup: uint8
 m_nGlowRange: int32
 m_flShadowStrength: float32
 m_flCreateTime: GameTime_t
 m_MoveCollide: MoveCollide_t
 m_bAnimatedEveryTick: bool
 m_vDecalForwardAxis: Vector
 m_nEnablePhysics: uint8
 m_flCapsuleRadius: float32
 m_nGlowRangeMin: int32
 m_vDecalPosition: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nRenderFX: RenderFx_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vecMaxs: Vector
 m_vecMins: Vector
 m_flDecalHealBloodRate: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 CBodyComponent: CBodyComponent
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_nOutsideWorld: uint16
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nBoolVariablesCount: int32
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
 m_LightGroup: CUtlStringToken
 m_ubInterpolationFrame: uint8
 m_vCapsuleCenter2: Vector
 m_flDecalHealHeightRate: float32
 m_nSubclassID: CUtlStringToken
 m_nInteractsExclude: uint64
 m_nCollisionFunctionMask: uint8
 m_bFlashing: bool
 m_bEligibleForScreenHighlight: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_nOwnerId: uint32
 m_usSolidFlags: uint8
 m_nCollisionGroup: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_flGlowStartTime: float32
 m_bAnimGraphUpdateEnabled: bool
 m_vecForce: Vector
 m_clrRender: Color
 m_nInteractsAs: uint64
 m_nEntityId: uint32
 CRenderComponent: CRenderComponent

186 CSun
 m_bOn: bool
 m_flHazeScale: float32
 m_flHDRColorScale: float32
 m_flAlphaHdr: float32
 m_flFarZScale: float32
 m_clrRender: Color
 m_bmaxColor: bool
 m_flAlphaHaze: float32
 m_flSize: float32
 m_flAlphaScale: float32
 m_vDirection: Vector
 m_iszEffectName: CUtlSymbolLarge
 m_iszSSEffectName: CUtlSymbolLarge
 m_clrOverlay: Color
 m_flRotation: float32

75 CEnvGasCanister
 m_nEnablePhysics: uint8
 m_vCapsuleCenter2: Vector
 m_flGlowTime: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_flWorldEnterTime: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vecMins: Vector
 m_nInteractsWith: uint64
 m_vDecalPosition: Vector
 m_bAnimGraphUpdateEnabled: bool
 m_bLanded: bool
 m_vecDirection: Vector
 m_MoveType: MoveType_t
 m_bSimulatedEveryTick: bool
 m_LightGroup: CUtlStringToken
 m_flInitialZSpeed: float32
 m_vecStartAngles: QAngle
 m_bLaunchedFromWithinWorld: bool
 m_hEffectEntity: CHandle< CBaseEntity >
 m_iGlowTeam: int32
 m_bShouldAnimateDuringGameplayPause: bool
 m_iTeamNum: uint8
 m_bInitiallyPopulateInterpHistory: bool
 m_vecForce: Vector
 m_bInSkybox: bool
 m_nSubclassID: CUtlStringToken
 m_vecSpecifiedSurroundingMins: Vector
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nInteractsAs: uint64
 m_nEntityId: uint32
 m_nCollisionFunctionMask: uint8
 m_bFlashing: bool
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveCollide: MoveCollide_t
 m_flElasticity: float32
 m_bDoImpactEffects: bool
 m_flDecalHealBloodRate: float32
 m_vecSkyboxOrigin: Vector
 m_bClientSideRagdoll: bool
 m_nCollisionGroup: uint8
 m_usSolidFlags: uint8
 m_nAddDecal: int32
 m_flDecalHealHeightRate: float32
 m_flZAcceleration: float32
 CBodyComponent: CBodyComponent
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nOwnerOnlyBoolVariablesCount: int32
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_nBoolVariablesCount: int32
  m_nRandomSeedOffset: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
 m_flCapsuleRadius: float32
 m_nObjectCulling: uint8
 m_flFlightTime: float32
 m_nSolidType: SolidType_t
 m_nSurroundType: SurroundingBoundsType_t
 m_nGlowRangeMin: int32
 m_triggerBloat: uint8
 m_nMyZoneIndex: int32
 m_bvDisabledHitGroups: uint32[1]
 m_nHierarchyId: uint16
 m_iGlowType: int32
 m_flGlowStartTime: float32
 m_bEligibleForScreenHighlight: bool
 m_flLaunchTime: GameTime_t
 m_flCreateTime: GameTime_t
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_flHorizSpeed: float32
 m_nRenderFX: RenderFx_t
 m_nInteractsExclude: uint64
 m_glowColorOverride: Color
 m_flGlowBackfaceMult: float32
 m_flFadeScale: float32
 m_flSimulationTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_ubInterpolationFrame: uint8
 m_vecImpactPosition: Vector
 m_vecParabolaDirection: Vector
 m_hSkyboxCopy: CHandle< CBaseEntity >
 m_vecEnterWorldPosition: Vector
 m_flFlightSpeed: float32
 CRenderComponent: CRenderComponent
 m_flAnimTime: float32
 m_nRenderMode: RenderMode_t
 m_nGlowRange: int32
 m_bRenderToCubemaps: bool
 m_nOwnerId: uint32
 m_fadeMinDist: float32
 m_vecMaxs: Vector
 m_fadeMaxDist: float32
 m_flShadowStrength: float32
 m_vDecalForwardAxis: Vector
 m_nForceBone: int32
 m_bClientRagdoll: bool
 m_vecStartPosition: Vector
 m_clrRender: Color
 m_CollisionGroup: uint8
 m_vCapsuleCenter1: Vector
 m_flSkyboxScale: float32

119 CKnife
 m_flDecalHealBloodRate: float32
 m_fAccuracyPenalty: float32
 m_nNextPrimaryAttackTick: GameTick_t
 CBodyComponent: CBodyComponent
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_angRotation: QAngle
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
 m_flElasticity: float32
 m_vCapsuleCenter1: Vector
 m_iGlowType: int32
 m_iTeamNum: uint8
 m_iItemIDHigh: uint32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
 m_MoveType: MoveType_t
 m_bClientRagdoll: bool
 m_bInReload: bool
 m_flNextSecondaryAttackTickRatio: float32
 m_fEffects: uint32
 m_vecSpecifiedSurroundingMins: Vector
 m_nGlowRangeMin: int32
 m_ProviderType: attributeprovidertypes_t
 m_nDropTick: GameTick_t
 CRenderComponent: CRenderComponent
 m_nHierarchyId: uint16
 m_fadeMinDist: float32
 m_bSilencerOn: bool
 m_clrRender: Color
 m_iRecoilIndex: int32
 m_nRenderFX: RenderFx_t
 m_nSurroundType: SurroundingBoundsType_t
 m_glowColorOverride: Color
 m_pReserveAmmo: int32[2]
 m_iReapplyProvisionParity: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nCollisionGroup: uint8
 m_nCollisionFunctionMask: uint8
 m_bEligibleForScreenHighlight: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flPostponeFireReadyTime: GameTime_t
 m_vecForce: Vector
 m_ubInterpolationFrame: uint8
 m_bvDisabledHitGroups: uint32[1]
 m_flSimulationTime: float32
 m_flFallbackWear: float32
 m_flFireSequenceStartTime: float32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_nFallbackStatTrak: int32
 m_fLastShotTime: GameTime_t
 m_nEnablePhysics: uint8
 m_hOuter: CHandle< CBaseEntity >
 m_iEntityLevel: uint32
 m_OriginalOwnerXuidLow: uint32
 m_weaponMode: CSWeaponMode
 m_nGlowRange: int32
 m_nFallbackPaintKit: int32
 m_nFireSequenceStartTimeChange: int32
 m_bIsHauledBack: bool
 m_flGlowTime: float32
 m_nObjectCulling: uint8
 m_bInitialized: bool
 m_nNextSecondaryAttackTick: GameTick_t
 m_nViewModelIndex: uint32
 m_LightGroup: CUtlStringToken
 m_usSolidFlags: uint8
 m_nAddDecal: int32
 m_bBurstMode: bool
 m_szCustomName: char[161]
 m_nRenderMode: RenderMode_t
 m_flShadowStrength: float32
 m_vDecalForwardAxis: Vector
 m_vLookTargetPosition: Vector
 m_iOriginalTeamNumber: int32
 m_nEntityId: uint32
 m_bShouldAnimateDuringGameplayPause: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_flRecoilIndex: float32
 m_iState: WeaponState_t
 m_bPlayerFireEventIsPrimary: bool
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_iClip2: int32
 m_nInteractsWith: uint64
 m_flGlowStartTime: float32
 m_flGlowBackfaceMult: float32
 m_bAnimGraphUpdateEnabled: bool
 m_flCreateTime: GameTime_t
 m_vCapsuleCenter2: Vector
 m_nNextThinkTick: GameTick_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_iItemDefinitionIndex: uint16
 m_iAccountID: uint32
 m_flDroppedAtTime: GameTime_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_iEntityQuality: int32
 m_bReloadVisuallyComplete: bool
 m_nFallbackSeed: int32
 m_bClientSideRagdoll: bool
 m_bRenderToCubemaps: bool
 m_triggerBloat: uint8
 m_nForceBone: int32
 m_nSolidType: SolidType_t
 m_bFlashing: bool
 m_flFadeScale: float32
 m_flAnimTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bAnimatedEveryTick: bool
 m_nOwnerId: uint32
 m_iGlowTeam: int32
 m_OriginalOwnerXuidHigh: uint32
 m_flNextPrimaryAttackTickRatio: float32
 m_MoveCollide: MoveCollide_t
 m_nSubclassID: CUtlStringToken
 m_nInteractsAs: uint64
 m_vecMins: Vector
 m_CollisionGroup: uint8
 m_fadeMaxDist: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_iNumEmptyAttacks: int32
 m_iItemIDLow: uint32
 m_iInventoryPosition: uint32
 m_bSimulatedEveryTick: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_nInteractsExclude: uint64
 m_flDecalHealHeightRate: float32
 m_iIronSightMode: int32
 m_iClip1: int32
 m_vecMaxs: Vector
 m_flCapsuleRadius: float32
 m_vDecalPosition: Vector

122 CLightEntity
 m_nRenderMode: RenderMode_t
 m_nEntityId: uint32
 m_vDecalForwardAxis: Vector
 m_flNavIgnoreUntilTime: GameTime_t
 m_nRenderFX: RenderFx_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_triggerBloat: uint8
 m_nGlowRange: int32
 m_flElasticity: float32
 m_vCapsuleCenter2: Vector
 m_fadeMaxDist: float32
 m_nAddDecal: int32
 m_bvDisabledHitGroups: uint32[1]
 CRenderComponent: CRenderComponent
 m_bRenderToCubemaps: bool
 m_nGlowRangeMin: int32
 m_nObjectCulling: uint8
 m_ubInterpolationFrame: uint8
 m_bSimulatedEveryTick: bool
 m_usSolidFlags: uint8
 m_iGlowType: int32
 m_flGlowBackfaceMult: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_clrRender: Color
 m_vecSpecifiedSurroundingMins: Vector
 m_iGlowTeam: int32
 CBodyComponent: CBodyComponent
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vDecalPosition: Vector
 m_nSubclassID: CUtlStringToken
 m_LightGroup: CUtlStringToken
 m_nCollisionFunctionMask: uint8
 m_nEnablePhysics: uint8
 m_flGlowStartTime: float32
 CLightComponent: CLightComponent
  m_flRange: float32
  m_flCapsuleLength: float32
  m_flAttenuation0: float32
  m_bFlicker: bool
  m_vPrecomputedOBBAngles: QAngle
  m_bUseSecondaryColor: bool
  m_flOrthoLightHeight: float32
  m_flShadowCascadeDistance2: float32
  m_nShadowPriority: int32
  m_nBakedShadowIndex: int32
  m_flShadowFadeMinDist: float32
  m_flShadowFadeMaxDist: float32
  m_flFogContributionStength: float32
  m_Color: Color
  m_flOrthoLightWidth: float32
  m_flShadowCascadeDistance1: float32
  m_nShadowCascadeResolution3: int32
  m_flFadeMaxDist: float32
  m_bMixedShadows: bool
  m_flBrightnessScale: float32
  m_nShadowHeight: int32
  m_nStyle: int32
  m_flShadowCascadeDistance3: float32
  m_bUsesIndexedBakedLighting: bool
  m_Pattern: CUtlString
  m_flShadowCascadeDistanceFade: float32
  m_flPrecomputedMaxRange: float32
  m_nFogLightingMode: int32
  m_flSkyIntensity: float32
  m_flAttenuation2: float32
  m_nCastShadows: int32
  m_bRenderDiffuse: bool
  m_flShadowCascadeDistance0: float32
  m_nShadowCascadeResolution1: int32
  m_flMinRoughness: float32
  m_hLightCookie: CStrongHandle< InfoForResourceTypeCTextureBase >
  m_nShadowCascadeResolution2: int32
  m_nCascades: int32
  m_vPrecomputedOBBOrigin: Vector
  m_SkyAmbientBounce: Color
  m_flBrightness: float32
  m_flAttenuation1: float32
  m_bRenderToCubemaps: bool
  m_LightGroups: CUtlSymbolLarge
  m_SecondaryColor: Color
  m_flBrightnessMult: float32
  m_flLightStyleStartTime: GameTime_t
  m_vPrecomputedOBBExtent: Vector
  m_flFalloff: float32
  m_nRenderSpecular: int32
  m_nCascadeRenderStaticObjects: int32
  m_nIndirectLight: int32
  m_flFadeMinDist: float32
  m_flPhi: float32
  m_bRenderTransmissive: bool
  m_bEnabled: bool
  m_nShadowWidth: int32
  m_nShadowCascadeResolution0: int32
  m_flNearClipPlane: float32
  m_bPrecomputedFieldsValid: bool
  m_vPrecomputedBoundsMins: Vector
  m_SkyColor: Color
  m_flTheta: float32
  m_flShadowCascadeCrossFade: float32
  m_nDirectLight: int32
  m_vPrecomputedBoundsMaxs: Vector
 m_flDecalHealHeightRate: float32
 m_flAnimTime: float32
 m_nOwnerId: uint32
 m_nHierarchyId: uint16
 m_nSolidType: SolidType_t
 m_vCapsuleCenter1: Vector
 m_flCapsuleRadius: float32
 m_nSurroundType: SurroundingBoundsType_t
 m_fadeMinDist: float32
 m_flSimulationTime: float32
 m_MoveType: MoveType_t
 m_flCreateTime: GameTime_t
 m_nInteractsExclude: uint64
 m_nCollisionGroup: uint8
 m_vecMaxs: Vector
 m_flFadeScale: float32
 m_iTeamNum: uint8
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_glowColorOverride: Color
 m_flGlowTime: float32
 m_bEligibleForScreenHighlight: bool
 m_nInteractsAs: uint64
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bFlashing: bool
 m_flShadowStrength: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_MoveCollide: MoveCollide_t
 m_nInteractsWith: uint64
 m_vecMins: Vector
 m_CollisionGroup: uint8
 m_flDecalHealBloodRate: float32

27 CBreakableProp
 m_ubInterpolationFrame: uint8
 m_bClientRagdoll: bool
 m_bAnimatedEveryTick: bool
 m_iGlowTeam: int32
 m_nForceBone: int32
 m_flAnimTime: float32
 m_triggerBloat: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_nHierarchyId: uint16
 m_vCapsuleCenter2: Vector
 m_glowColorOverride: Color
 m_bFlashing: bool
 m_bShouldAnimateDuringGameplayPause: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flElasticity: float32
 m_nRenderFX: RenderFx_t
 m_CollisionGroup: uint8
 m_nEnablePhysics: uint8
 m_bEligibleForScreenHighlight: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_MoveCollide: MoveCollide_t
 CBodyComponent: CBodyComponent
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nBoolVariablesCount: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
 m_nInteractsExclude: uint64
 m_nSurroundType: SurroundingBoundsType_t
 m_nGlowRangeMin: int32
 m_nAddDecal: int32
 m_bInitiallyPopulateInterpHistory: bool
 m_flCreateTime: GameTime_t
 m_bSimulatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_nInteractsWith: uint64
 m_usSolidFlags: uint8
 m_iGlowType: int32
 m_flGlowBackfaceMult: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nCollisionGroup: uint8
 m_vecMins: Vector
 m_vCapsuleCenter1: Vector
 m_vDecalPosition: Vector
 m_bAnimGraphUpdateEnabled: bool
 m_vecForce: Vector
 m_fEffects: uint32
 m_clrRender: Color
 m_flGlowStartTime: float32
 m_flShadowStrength: float32
 m_vDecalForwardAxis: Vector
 CRenderComponent: CRenderComponent
 m_iTeamNum: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_bRenderToCubemaps: bool
 m_nCollisionFunctionMask: uint8
 m_nSolidType: SolidType_t
 m_fadeMaxDist: float32
 m_flDecalHealHeightRate: float32
 m_noGhostCollision: bool
 m_nOwnerId: uint32
 m_vecMaxs: Vector
 m_flDecalHealBloodRate: float32
 m_flCapsuleRadius: float32
 m_flSimulationTime: float32
 m_nSubclassID: CUtlStringToken
 m_nGlowRange: int32
 m_fadeMinDist: float32
 m_flFadeScale: float32
 m_nObjectCulling: uint8
 m_bvDisabledHitGroups: uint32[1]
 m_MoveType: MoveType_t
 m_nRenderMode: RenderMode_t
 m_nInteractsAs: uint64
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bClientSideRagdoll: bool
 m_LightGroup: CUtlStringToken
 m_nEntityId: uint32
 m_flGlowTime: float32

74 CEnvDetailController
 m_flFadeStartDist: float32
 m_flFadeEndDist: float32

99 CFuncWater
 m_ubInterpolationFrame: uint8
 m_iGlowType: int32
 m_fadeMinDist: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_LightGroup: CUtlStringToken
 m_vecMaxs: Vector
 m_nCollisionGroup: uint8
 m_flCapsuleRadius: float32
 m_bvDisabledHitGroups: uint32[1]
 CRenderComponent: CRenderComponent
 m_MoveCollide: MoveCollide_t
 m_nEntityId: uint32
 m_nOwnerId: uint32
 m_usSolidFlags: uint8
 m_flSimulationTime: float32
 m_nSubclassID: CUtlStringToken
 m_fEffects: uint32
 m_nRenderFX: RenderFx_t
 m_flGlowBackfaceMult: float32
 m_nObjectCulling: uint8
 m_nHierarchyId: uint16
 m_flFadeScale: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bSimulatedEveryTick: bool
 m_nInteractsExclude: uint64
 m_MoveType: MoveType_t
 m_triggerBloat: uint8
 m_fadeMaxDist: float32
 m_bFlashing: bool
 m_flElasticity: float32
 m_vCapsuleCenter2: Vector
 m_nGlowRangeMin: int32
 m_flAnimTime: float32
 m_bRenderToCubemaps: bool
 m_nInteractsWith: uint64
 m_nSolidType: SolidType_t
 m_nGlowRange: int32
 m_flGlowStartTime: float32
 m_nAddDecal: int32
 m_flDecalHealBloodRate: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flNavIgnoreUntilTime: GameTime_t
 m_nInteractsAs: uint64
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flDecalHealHeightRate: float32
 m_flCreateTime: GameTime_t
 m_nCollisionFunctionMask: uint8
 m_vDecalForwardAxis: Vector
 m_vDecalPosition: Vector
 m_clrRender: Color
 m_nSurroundType: SurroundingBoundsType_t
 m_glowColorOverride: Color
 m_iTeamNum: uint8
 m_vecSpecifiedSurroundingMins: Vector
 CBodyComponent: CBodyComponent
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
 m_flGlowTime: float32
 m_bEligibleForScreenHighlight: bool
 m_CollisionGroup: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vCapsuleCenter1: Vector
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nRenderMode: RenderMode_t
 m_vecMins: Vector
 m_flShadowStrength: float32
 m_bAnimatedEveryTick: bool
 m_nEnablePhysics: uint8
 m_iGlowTeam: int32

180 CSoundOpvarSetPathCornerEntity
 m_flCreateTime: GameTime_t
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSubclassID: CUtlStringToken
 m_MoveType: MoveType_t
 m_bSimulatedEveryTick: bool
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_iszStackName: CUtlSymbolLarge
 m_iOpvarIndex: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 CBodyComponent: CBodyComponent
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
 m_bUseAutoCompare: bool
 m_iszOperatorName: CUtlSymbolLarge
 m_ubInterpolationFrame: uint8
 m_iTeamNum: uint8
 m_MoveCollide: MoveCollide_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_flElasticity: float32
 m_iszOpvarName: CUtlSymbolLarge
 m_flAnimTime: float32
 m_flSimulationTime: float32

188 CTablet
 m_iClip1: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_vecNearestMetalCratePos: Vector
 m_flNextPrimaryAttackTickRatio: float32
 m_flNextSecondaryAttackTickRatio: float32
 m_nNextPrimaryAttackTick: GameTick_t
 m_nHierarchyId: uint16
 m_nObjectCulling: uint8
 m_iAccountID: uint32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_flUpgradeExpirationTime: GameTime_t[4]
 m_bInReload: bool
 m_MoveType: MoveType_t
 m_nRenderMode: RenderMode_t
 m_vCapsuleCenter1: Vector
 m_fadeMinDist: float32
 m_hOuter: CHandle< CBaseEntity >
 m_nCollisionFunctionMask: uint8
 m_nForceBone: int32
 m_szCustomName: char[161]
 m_flPostponeFireReadyTime: GameTime_t
 m_nNextSecondaryAttackTick: GameTick_t
 m_iNumEmptyAttacks: int32
 m_flCreateTime: GameTime_t
 m_vCapsuleCenter2: Vector
 m_vecForce: Vector
 m_iItemIDHigh: uint32
 m_nFallbackPaintKit: int32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_bInitiallyPopulateInterpHistory: bool
 m_nViewModelIndex: uint32
 m_bEligibleForScreenHighlight: bool
 m_flGlowBackfaceMult: float32
 m_nNextThinkTick: GameTick_t
 m_nSubclassID: CUtlStringToken
 m_iEntityQuality: int32
 m_bInitialized: bool
 m_vecLocalHexFlags: int32[42]
 m_iClip2: int32
 m_nCollisionGroup: uint8
 m_CollisionGroup: uint8
 m_bAnimGraphUpdateEnabled: bool
 m_iOriginalTeamNumber: int32
 m_flAnimTime: float32
 m_flFireSequenceStartTime: float32
 m_bIsHauledBack: bool
 m_bBurstMode: bool
 m_flShowMapTime: GameTime_t
 m_nRenderFX: RenderFx_t
 m_nOwnerId: uint32
 m_fadeMaxDist: float32
 m_flShadowStrength: float32
 m_ProviderType: attributeprovidertypes_t
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nInteractsAs: uint64
 m_nSurroundType: SurroundingBoundsType_t
 m_OriginalOwnerXuidLow: uint32
 m_nFallbackSeed: int32
 m_iInventoryPosition: uint32
 m_iState: WeaponState_t
 m_vecPlayerPositionHistory: Vector[24]
 m_nContractKillGridHighResIndex: int32
 CRenderComponent: CRenderComponent
 m_vecSpecifiedSurroundingMins: Vector
 m_bShouldAnimateDuringGameplayPause: bool
 m_bPlayerFireEventIsPrimary: bool
 m_flRecoilIndex: float32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_flFallbackWear: float32
 m_flScanProgress: float32
 m_iTeamNum: uint8
 m_vLookTargetPosition: Vector
 m_nDropTick: GameTick_t
 m_skinState: tablet_skin_state_t
 m_vecNotificationTimestamps: GameTime_t[8]
 m_fLastShotTime: GameTime_t
 m_LightGroup: CUtlStringToken
 m_nGlowRange: int32
 m_flGlowTime: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_weaponMode: CSWeaponMode
 m_flSimulationTime: float32
 m_flFadeScale: float32
 m_OriginalOwnerXuidHigh: uint32
 m_bReloadVisuallyComplete: bool
 m_bSimulatedEveryTick: bool
 m_bAnimatedEveryTick: bool
 m_nEnablePhysics: uint8
 m_flGlowStartTime: float32
 m_iReapplyProvisionParity: int32
 m_iItemIDLow: uint32
 m_nFallbackStatTrak: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nEntityId: uint32
 m_nSolidType: SolidType_t
 m_iGlowTeam: int32
 m_bFlashing: bool
 m_usSolidFlags: uint8
 m_iEntityLevel: uint32
 m_iRecoilIndex: int32
 m_nLastPurchaseIndex: int32
 m_bvDisabledHitGroups: uint32[1]
 m_ubInterpolationFrame: uint8
 m_fEffects: uint32
 m_flDroppedAtTime: GameTime_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_nInteractsExclude: uint64
 m_pReserveAmmo: int32[2]
 m_iItemDefinitionIndex: uint16
 m_vecNotificationIds: int32[8]
 m_glowColorOverride: Color
 m_fAccuracyPenalty: float32
 CBodyComponent: CBodyComponent
  m_angRotation: QAngle
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_nBoolVariablesCount: int32
  m_cellZ: uint16
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_nNewSequenceParity: int32
  m_bClientSideAnimation: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_hParent: CGameSceneNodeHandle
  m_nRandomSeedOffset: int32
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
 m_nInteractsWith: uint64
 m_vecMins: Vector
 m_vecMaxs: Vector
 m_iGlowType: int32
 m_bRenderToCubemaps: bool
 m_flDecalHealHeightRate: float32
 m_bSilencerOn: bool
 m_nContractKillGridIndex: int32
 m_bTabletReceptionIsBlocked: bool
 m_MoveCollide: MoveCollide_t
 m_bClientSideRagdoll: bool
 m_vDecalForwardAxis: Vector
 m_flDecalHealBloodRate: float32
 m_nFireSequenceStartTimeChange: int32
 m_nGlowRangeMin: int32
 m_vDecalPosition: Vector
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flElasticity: float32
 m_clrRender: Color
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flCapsuleRadius: float32
 m_triggerBloat: uint8
 m_nAddDecal: int32
 m_bClientRagdoll: bool
 m_iIronSightMode: int32
 m_flBootTime: GameTime_t

191 CTonemapController2
 m_flSimulationTime: float32
 m_iTeamNum: uint8
 m_MoveCollide: MoveCollide_t
 m_fEffects: uint32
 m_flElasticity: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_ubInterpolationFrame: uint8
 m_flAutoExposureMax: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool
 m_flExposureAdaptationSpeedUp: float32
 m_flTonemapEVSmoothingRange: float32
 CBodyComponent: CBodyComponent
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
 m_MoveType: MoveType_t
 m_flTonemapPercentBrightPixels: float32
 m_flCreateTime: GameTime_t
 m_flTonemapMinAvgLum: float32
 m_flExposureAdaptationSpeedDown: float32
 m_nSubclassID: CUtlStringToken
 m_flAutoExposureMin: float32
 m_flTonemapPercentTarget: float32
 m_flAnimTime: float32
 m_bSimulatedEveryTick: bool

35 CCSGameRulesProxy
 m_pGameRules: CCSGameRules*
  m_fWarmupPeriodEnd: GameTime_t
  m_timeUntilNextPhaseStarts: float32
  m_bMatchWaitingForResume: bool
  m_nGuardianModeSpecialWeaponNeeded: int32
  m_arrTournamentActiveCasterAccounts: uint32[4]
  m_SpawnTileState: ESurvivalSpawnTileState[224]
  m_flTabletHexSize: float32
  m_bTerroristTimeOutActive: bool
  m_MatchDevice: int32
  m_iMatchStats_PlayersAlive_T: int32[30]
  m_roundData_playerPositions: int32[64]
  m_bServerPaused: bool
  m_nTerroristTimeOuts: int32
  m_bIsQueuedMatchmaking: bool
  m_bPlayAllStepSoundsOnServer: bool
  m_bCTCantBuy: bool
  m_TeamRespawnWaveTimes: float32[32]
  m_nEndMatchMapVoteWinner: int32
  m_nPauseStartTick: int32
  m_bMapHasRescueZone: bool
  m_nGuardianModeSpecialKillsRemaining: int32
  m_eRoundWinReason: int32
  m_nRoundsPlayedThisPhase: int32
  m_bMatchAbortedDueToPlayerBan: bool
  m_bAnyHostageReached: bool
  m_bMapHasBombTarget: bool
  m_iNumGunGameProgressiveWeaponsT: int32
  m_numBestOfMaps: int32
  m_bBombPlanted: bool
  m_bTCantBuy: bool
  m_nOvertimePlaying: int32
  m_szMatchStatTxt: char[512]
  m_flCMMItemDropRevealStartTime: GameTime_t
  m_arrFeaturedGiftersAccounts: uint32[4]
  m_pGameModeRules: CCSGameModeRules*
  m_SurvivalGameRuleDecisionTypes: ESurvivalGameRuleDecision_t[16]
  m_nMatchEndCount: uint8
  m_nNextMapInMapgroup: int32
  m_nEndMatchMapGroupVoteOptions: int32[10]
  m_iPlayerSpawnHexIndices: int32[64]
  m_roundData_playerTeams: int32[64]
  m_bWarmupPeriod: bool
  m_totalRoundsPlayed: int32
  m_iHostagesRemaining: int32
  m_bIsValveDS: bool
  m_szTournamentEventStage: char[512]
  m_vMinimapMins: Vector
  m_vecPlayAreaMaxs: Vector
  m_SurvivalGameRuleDecisionValues: int32[16]
  m_flTabletHexOriginX: float32
  m_bTechnicalTimeOut: bool
  m_iRoundWinStatus: int32
  m_iSpectatorSlotCount: int32
  m_bHasMatchStarted: bool
  m_flCMMItemDropRevealEndTime: GameTime_t
  m_numGlobalGiftsGiven: uint32
  m_spawnStage: SpawnStage_t
  m_iRoundTime: int32
  m_bLogoMap: bool
  m_GGProgressiveWeaponOrderT: int32[60]
  m_iNumConsecutiveTerroristLoses: int32
  m_bTeamIntroPeriod: bool
  m_numGlobalGiftsPeriodSeconds: uint32
  m_arrFeaturedGiftersGifts: uint32[4]
  m_nEndMatchMapGroupVoteTypes: int32[10]
  m_iFirstSecondHalfRound: int32
  m_fRoundStartTime: GameTime_t
  m_nGuardianModeWaveNumber: int32
  m_iMatchStats_RoundResults: int32[30]
  m_vMinimapMaxs: Vector
  m_iNumConsecutiveCTLoses: int32
  m_nTotalPausedTicks: int32
  m_iNumGunGameProgressiveWeaponsCT: int32
  m_szTournamentEventName: char[512]
  m_MinimapVerticalSectionHeights: float32[8]
  m_bGamePaused: bool
  m_flSpawnSelectionTimeEndCurrentStage: float32
  m_flTabletHexOriginY: float32
  m_nCTTeamIntroVariant: int32
  m_nTTeamIntroVariant: int32
  m_fMatchStartTime: float32
  m_bGameRestart: bool
  m_iMatchStats_PlayersAlive_CT: int32[30]
  m_flSpawnSelectionTimeStartCurrentStage: float32
  m_nQueuedMatchmakingMode: int32
  m_bIsDroppingItems: bool
  m_bIsQuestEligible: bool
  m_flGuardianBuyUntilTime: GameTime_t
  m_flNextRespawnWave: GameTime_t[32]
  m_flSurvivalStartTime: float32
  m_flGameStartTime: float32
  m_GGProgressiveWeaponKillUpgradeOrderT: int32[60]
  m_bCTTimeOutActive: bool
  m_numGlobalGifters: uint32
  m_arrProhibitedItemIndices: uint16[100]
  m_roundData_playerXuids: uint64[64]
  m_nMatchSeed: int32
  m_gamePhase: int32
  m_nTournamentPredictionsPct: int32
  m_flSpawnSelectionTimeEndLastStage: float32
  m_bBlockersPresent: bool
  m_iBombSite: int32
  m_bFreezePeriod: bool
  m_nServerQuestID: int32
  m_vecPlayAreaMins: Vector
  m_fWarmupPeriodStart: GameTime_t
  m_flCTTimeOutRemaining: float32
  m_flRestartRoundTime: GameTime_t
  m_nHalloweenMaskListSeed: int32
  m_bBombDropped: bool
  m_bRoundInProgress: bool
  m_bMapHasBuyZone: bool
  m_GGProgressiveWeaponKillUpgradeOrderCT: int32[60]
  m_szTournamentPredictionsTxt: char[512]
  m_flTerroristTimeOutRemaining: float32
  m_nCTTimeOuts: int32
  m_GGProgressiveWeaponOrderCT: int32[60]

56 CDangerZone
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_vecDangerZoneOriginStartedAt: Vector
 m_flExtraRadiusStartTime: GameTime_t
 m_flExtraRadiusTotalLerpTime: float32
 m_iWave: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_ubInterpolationFrame: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_nDropOrder: int32
 m_MoveType: MoveType_t
 CBodyComponent: CBodyComponent
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
 m_MoveCollide: MoveCollide_t
 m_iTeamNum: uint8
 m_flSimulationTime: float32
 m_nSubclassID: CUtlStringToken
 m_flCreateTime: GameTime_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_flElasticity: float32
 m_bSimulatedEveryTick: bool
 m_bAnimatedEveryTick: bool
 m_flAnimTime: float32
 m_flExtraRadius: float32
 m_flBombLaunchTime: GameTime_t

84 CFireCrackerBlast
 m_clrRender: Color
 m_nCollisionGroup: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_fireCount: int32
 m_nFireEffectTickBegin: int32
 m_nRenderFX: RenderFx_t
 m_nRenderMode: RenderMode_t
 m_bEligibleForScreenHighlight: bool
 m_nInfernoType: int32
 m_MoveType: MoveType_t
 m_flShadowStrength: float32
 m_fireXDelta: int32[64]
 m_BurnNormal: Vector[64]
 m_nInteractsWith: uint64
 m_fEffects: uint32
 m_CollisionGroup: uint8
 m_nGlowRange: int32
 m_bFlashing: bool
 m_flGlowStartTime: float32
 m_bInPostEffectTime: bool
 m_flAnimTime: float32
 m_fireZDelta: int32[64]
 CRenderComponent: CRenderComponent
 m_flGlowBackfaceMult: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_bRenderToCubemaps: bool
 m_vCapsuleCenter1: Vector
 m_flDecalHealBloodRate: float32
 m_fireParentZDelta: int32[64]
 m_flCreateTime: GameTime_t
 m_nCollisionFunctionMask: uint8
 m_vecMins: Vector
 m_fadeMinDist: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_ubInterpolationFrame: uint8
 m_nInteractsAs: uint64
 m_triggerBloat: uint8
 m_flDecalHealHeightRate: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_MoveCollide: MoveCollide_t
 m_nSurroundType: SurroundingBoundsType_t
 m_iGlowType: int32
 m_bAnimatedEveryTick: bool
 m_bSimulatedEveryTick: bool
 m_nSolidType: SolidType_t
 m_nEnablePhysics: uint8
 m_nSubclassID: CUtlStringToken
 m_vecMaxs: Vector
 m_flGlowTime: float32
 m_fadeMaxDist: float32
 m_vDecalPosition: Vector
 m_fireYDelta: int32[64]
 m_nFireLifetime: float32
 m_iTeamNum: uint8
 m_nInteractsExclude: uint64
 m_nEntityId: uint32
 m_nHierarchyId: uint16
 m_vecSpecifiedSurroundingMins: Vector
 m_vCapsuleCenter2: Vector
 m_glowColorOverride: Color
 m_flFadeScale: float32
 CBodyComponent: CBodyComponent
  m_MeshGroupMask: uint64
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_materialGroup: CUtlStringToken
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
 m_nAddDecal: int32
 m_vDecalForwardAxis: Vector
 m_fireParentXDelta: int32[64]
 m_nObjectCulling: uint8
 m_fireParentYDelta: int32[64]
 m_flSimulationTime: float32
 m_usSolidFlags: uint8
 m_flCapsuleRadius: float32
 m_iGlowTeam: int32
 m_bFireIsBurning: bool[64]
 m_bvDisabledHitGroups: uint32[1]
 m_nOwnerId: uint32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_LightGroup: CUtlStringToken
 m_nGlowRangeMin: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flElasticity: float32

126 CLightSpotEntity
 m_flElasticity: float32
 m_glowColorOverride: Color
 m_flGlowBackfaceMult: float32
 m_nRenderFX: RenderFx_t
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_MoveCollide: MoveCollide_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_vecSpecifiedSurroundingMins: Vector
 m_bvDisabledHitGroups: uint32[1]
 CRenderComponent: CRenderComponent
 m_iTeamNum: uint8
 m_nEntityId: uint32
 m_flCapsuleRadius: float32
 m_nGlowRangeMin: int32
 m_flGlowTime: float32
 m_bEligibleForScreenHighlight: bool
 m_MoveType: MoveType_t
 m_flCreateTime: GameTime_t
 m_bAnimatedEveryTick: bool
 m_bRenderToCubemaps: bool
 m_usSolidFlags: uint8
 m_nSolidType: SolidType_t
 CLightComponent: CLightComponent
  m_flAttenuation0: float32
  m_bFlicker: bool
  m_vPrecomputedOBBAngles: QAngle
  m_bUseSecondaryColor: bool
  m_flOrthoLightHeight: float32
  m_flShadowCascadeDistance2: float32
  m_nShadowPriority: int32
  m_nBakedShadowIndex: int32
  m_flFadeMaxDist: float32
  m_flShadowFadeMinDist: float32
  m_flShadowFadeMaxDist: float32
  m_flFogContributionStength: float32
  m_Color: Color
  m_flOrthoLightWidth: float32
  m_flShadowCascadeDistance1: float32
  m_nShadowCascadeResolution3: int32
  m_bUsesIndexedBakedLighting: bool
  m_bMixedShadows: bool
  m_flBrightnessScale: float32
  m_nShadowHeight: int32
  m_nStyle: int32
  m_flShadowCascadeDistance3: float32
  m_Pattern: CUtlString
  m_flShadowCascadeDistanceFade: float32
  m_nShadowCascadeResolution1: int32
  m_flPrecomputedMaxRange: float32
  m_nFogLightingMode: int32
  m_flSkyIntensity: float32
  m_flAttenuation2: float32
  m_nCastShadows: int32
  m_bRenderDiffuse: bool
  m_flShadowCascadeDistance0: float32
  m_flMinRoughness: float32
  m_hLightCookie: CStrongHandle< InfoForResourceTypeCTextureBase >
  m_nShadowCascadeResolution2: int32
  m_nCascades: int32
  m_vPrecomputedOBBOrigin: Vector
  m_SkyAmbientBounce: Color
  m_flBrightness: float32
  m_flAttenuation1: float32
  m_bRenderToCubemaps: bool
  m_LightGroups: CUtlSymbolLarge
  m_SecondaryColor: Color
  m_flBrightnessMult: float32
  m_flLightStyleStartTime: GameTime_t
  m_flFadeMinDist: float32
  m_vPrecomputedOBBExtent: Vector
  m_flFalloff: float32
  m_nRenderSpecular: int32
  m_nCascadeRenderStaticObjects: int32
  m_nIndirectLight: int32
  m_flPhi: float32
  m_bRenderTransmissive: bool
  m_bEnabled: bool
  m_nShadowWidth: int32
  m_nShadowCascadeResolution0: int32
  m_flNearClipPlane: float32
  m_bPrecomputedFieldsValid: bool
  m_vPrecomputedBoundsMins: Vector
  m_SkyColor: Color
  m_flTheta: float32
  m_flShadowCascadeCrossFade: float32
  m_nDirectLight: int32
  m_vPrecomputedBoundsMaxs: Vector
  m_flRange: float32
  m_flCapsuleLength: float32
 m_nAddDecal: int32
 m_ubInterpolationFrame: uint8
 m_LightGroup: CUtlStringToken
 m_nCollisionFunctionMask: uint8
 m_vecMaxs: Vector
 m_vCapsuleCenter2: Vector
 m_flGlowStartTime: float32
 m_flDecalHealHeightRate: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bSimulatedEveryTick: bool
 m_vecMins: Vector
 m_triggerBloat: uint8
 CBodyComponent: CBodyComponent
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_name: CUtlStringToken
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_vecX: CNetworkedQuantizedFloat
 m_fadeMaxDist: float32
 m_flShadowStrength: float32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_iGlowType: int32
 m_flNavIgnoreUntilTime: GameTime_t
 m_clrRender: Color
 m_nSurroundType: SurroundingBoundsType_t
 m_CollisionGroup: uint8
 m_bFlashing: bool
 m_nObjectCulling: uint8
 m_flDecalHealBloodRate: float32
 m_flSimulationTime: float32
 m_nSubclassID: CUtlStringToken
 m_nCollisionGroup: uint8
 m_vCapsuleCenter1: Vector
 m_nInteractsExclude: uint64
 m_nGlowRange: int32
 m_flFadeScale: float32
 m_vDecalPosition: Vector
 m_flAnimTime: float32
 m_fadeMinDist: float32
 m_nRenderMode: RenderMode_t
 m_nInteractsAs: uint64
 m_iGlowTeam: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nEnablePhysics: uint8
 m_vDecalForwardAxis: Vector
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsWith: uint64
 m_nOwnerId: uint32
 m_nHierarchyId: uint16

33 CColorCorrectionVolume
 m_flSimulationTime: float32
 m_nInteractsWith: uint64
 m_flGlowStartTime: float32
 m_bEligibleForScreenHighlight: bool
 m_flFadeScale: float32
 m_flDecalHealHeightRate: float32
 m_bAnimatedEveryTick: bool
 m_nRenderMode: RenderMode_t
 m_nInteractsAs: uint64
 m_nSurroundType: SurroundingBoundsType_t
 m_bFlashing: bool
 m_FadeDuration: float32
 m_bRenderToCubemaps: bool
 m_usSolidFlags: uint8
 m_nEnablePhysics: uint8
 m_vCapsuleCenter1: Vector
 m_vDecalPosition: Vector
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nObjectCulling: uint8
 m_nSubclassID: CUtlStringToken
 m_flElasticity: float32
 m_nCollisionFunctionMask: uint8
 m_triggerBloat: uint8
 m_lookupFilename: char[512]
 m_flAnimTime: float32
 m_vecSpecifiedSurroundingMins: Vector
 m_vDecalForwardAxis: Vector
 m_MoveType: MoveType_t
 m_nGlowRangeMin: int32
 m_fadeMaxDist: float32
 m_bClientSidePredicted: bool
 m_ubInterpolationFrame: uint8
 m_nRenderFX: RenderFx_t
 m_nSolidType: SolidType_t
 m_iGlowType: int32
 m_fadeMinDist: float32
 m_vecMins: Vector
 m_flCapsuleRadius: float32
 m_nAddDecal: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flNavIgnoreUntilTime: GameTime_t
 m_clrRender: Color
 m_LightGroup: CUtlStringToken
 m_nInteractsExclude: uint64
 CRenderComponent: CRenderComponent
 m_bSimulatedEveryTick: bool
 m_glowColorOverride: Color
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nHierarchyId: uint16
 m_nCollisionGroup: uint8
 m_bEnabled: bool
 m_spawnflags: uint32
 m_nEntityId: uint32
 m_nOwnerId: uint32
 m_nGlowRange: int32
 m_Weight: float32
 m_flGlowTime: float32
 m_flGlowBackfaceMult: float32
 m_bDisabled: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveCollide: MoveCollide_t
 m_flCreateTime: GameTime_t
 m_CollisionGroup: uint8
 m_iGlowTeam: int32
 m_iTeamNum: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_MaxWeight: float32
 CBodyComponent: CBodyComponent
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_name: CUtlStringToken
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
 m_fEffects: uint32
 m_vecMaxs: Vector
 m_flDecalHealBloodRate: float32
 m_vCapsuleCenter2: Vector
 m_flShadowStrength: float32
 m_bvDisabledHitGroups: uint32[1]

155 CPointValueRemapper
 m_MoveCollide: MoveCollide_t
 m_flElasticity: float32
 m_bSimulatedEveryTick: bool
 m_flMaximumChangePerSecond: float32
 m_flMomentumModifier: float32
 m_flAnimTime: float32
 m_ubInterpolationFrame: uint8
 m_flSimulationTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nInputType: ValueRemapperInputType_t
 m_bAnimatedEveryTick: bool
 m_hRemapLineStart: CHandle< CBaseEntity >
 m_bRequiresUseKey: bool
 m_nHapticsType: ValueRemapperHapticsType_t
 m_flSnapValue: float32
 CBodyComponent: CBodyComponent
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
 m_flNavIgnoreUntilTime: GameTime_t
 m_hOutputEntities: CNetworkUtlVectorBase< CHandle< CBaseEntity > >
 m_nRatchetType: ValueRemapperRatchetType_t
 m_flInputOffset: float32
 m_hRemapLineEnd: CHandle< CBaseEntity >
 m_nMomentumType: ValueRemapperMomentumType_t
 m_nSubclassID: CUtlStringToken
 m_iTeamNum: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bDisabled: bool
 m_flDisengageDistance: float32
 m_flEngageDistance: float32
 m_nOutputType: ValueRemapperOutputType_t
 m_MoveType: MoveType_t
 m_flCreateTime: GameTime_t
 m_fEffects: uint32
 m_bUpdateOnClient: bool

6 CBaseCSGrenade
 m_fadeMinDist: float32
 m_vDecalForwardAxis: Vector
 m_flNextSecondaryAttackTickRatio: float32
 m_nInteractsAs: uint64
 m_nRenderFX: RenderFx_t
 m_vecMaxs: Vector
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSurroundType: SurroundingBoundsType_t
 m_vecSpecifiedSurroundingMins: Vector
 m_vLookTargetPosition: Vector
 m_iReapplyProvisionParity: int32
 m_flPostponeFireReadyTime: GameTime_t
 m_nInteractsExclude: uint64
 m_OriginalOwnerXuidHigh: uint32
 m_bJumpThrow: bool
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_bReloadVisuallyComplete: bool
 m_bIsHeldByPlayer: bool
 m_bPinPulled: bool
 m_iAccountID: uint32
 m_nAddDecal: int32
 m_vecForce: Vector
 m_nFallbackSeed: int32
 m_flFireSequenceStartTime: float32
 m_iOriginalTeamNumber: int32
 m_flThrowStrength: float32
 m_glowColorOverride: Color
 m_flDecalHealBloodRate: float32
 m_OriginalOwnerXuidLow: uint32
 m_nOwnerId: uint32
 m_nEnablePhysics: uint8
 m_nFallbackPaintKit: int32
 m_iState: WeaponState_t
 CRenderComponent: CRenderComponent
 m_usSolidFlags: uint8
 m_nGlowRange: int32
 m_iGlowTeam: int32
 m_bEligibleForScreenHighlight: bool
 m_nNextThinkTick: GameTick_t
 m_bFlashing: bool
 m_iNumEmptyAttacks: int32
 m_flShadowStrength: float32
 m_flElasticity: float32
 m_CollisionGroup: uint8
 m_bAnimGraphUpdateEnabled: bool
 m_nForceBone: int32
 m_nFallbackStatTrak: int32
 m_bPlayerFireEventIsPrimary: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_triggerBloat: uint8
 m_nGlowRangeMin: int32
 m_flGlowTime: float32
 m_flDecalHealHeightRate: float32
 m_iRecoilIndex: int32
 m_nInteractsWith: uint64
 m_iItemDefinitionIndex: uint16
 m_flCapsuleRadius: float32
 m_nEntityId: uint32
 m_vecMins: Vector
 m_ProviderType: attributeprovidertypes_t
 m_iEntityQuality: int32
 m_iItemIDHigh: uint32
 CBodyComponent: CBodyComponent
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
 m_nCollisionGroup: uint8
 m_vCapsuleCenter2: Vector
 m_fadeMaxDist: float32
 m_MoveCollide: MoveCollide_t
 m_clrRender: Color
 m_szCustomName: char[161]
 m_nFireSequenceStartTimeChange: int32
 m_fAccuracyPenalty: float32
 m_bBurstMode: bool
 m_bAnimatedEveryTick: bool
 m_bRedraw: bool
 m_nNextSecondaryAttackTick: GameTick_t
 m_MoveType: MoveType_t
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nSolidType: SolidType_t
 m_bInitialized: bool
 m_flAnimTime: float32
 m_fThrowTime: GameTime_t
 m_iClip2: int32
 m_bSimulatedEveryTick: bool
 m_flFadeScale: float32
 m_bInReload: bool
 m_nCollisionFunctionMask: uint8
 m_bIsHauledBack: bool
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_nSubclassID: CUtlStringToken
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bSilencerOn: bool
 m_iTeamNum: uint8
 m_nObjectCulling: uint8
 m_vDecalPosition: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bShouldAnimateDuringGameplayPause: bool
 m_iEntityLevel: uint32
 m_flFallbackWear: float32
 m_fLastShotTime: GameTime_t
 m_LightGroup: CUtlStringToken
 m_flNextPrimaryAttackTickRatio: float32
 m_nHierarchyId: uint16
 m_flGlowBackfaceMult: float32
 m_weaponMode: CSWeaponMode
 m_nViewModelIndex: uint32
 m_flCreateTime: GameTime_t
 m_nDropTick: GameTick_t
 m_bvDisabledHitGroups: uint32[1]
 m_bRenderToCubemaps: bool
 m_nRenderMode: RenderMode_t
 m_vCapsuleCenter1: Vector
 m_iGlowType: int32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_iItemIDLow: uint32
 m_eThrowStatus: EGrenadeThrowState
 m_ubInterpolationFrame: uint8
 m_hOuter: CHandle< CBaseEntity >
 m_flDroppedAtTime: GameTime_t
 m_iIronSightMode: int32
 m_fDropTime: GameTime_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_bInitiallyPopulateInterpHistory: bool
 m_flRecoilIndex: float32
 m_flThrowStrengthApproach: float32
 m_nNextPrimaryAttackTick: GameTick_t
 m_flSimulationTime: float32
 m_bClientRagdoll: bool
 m_bClientSideRagdoll: bool
 m_flGlowStartTime: float32
 m_iInventoryPosition: uint32
 m_pReserveAmmo: int32[2]
 m_iClip1: int32

86 CFish
 m_poolOrigin: Vector
 m_waterLevel: float32
 m_lifeState: uint8
 m_x: float32
 m_y: float32
 m_z: float32
 m_angle: float32

130 CMolotovGrenade
 m_bSimulatedEveryTick: bool
 m_vCapsuleCenter1: Vector
 m_flElasticity: float32
 m_CollisionGroup: uint8
 m_flShadowStrength: float32
 m_bReloadVisuallyComplete: bool
 m_flThrowStrengthApproach: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool
 m_bJumpThrow: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flCreateTime: GameTime_t
 m_nSurroundType: SurroundingBoundsType_t
 m_fadeMaxDist: float32
 m_bPlayerFireEventIsPrimary: bool
 m_fEffects: uint32
 m_nInteractsWith: uint64
 m_nForceBone: int32
 m_ProviderType: attributeprovidertypes_t
 m_OriginalOwnerXuidLow: uint32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nCollisionGroup: uint8
 m_bFlashing: bool
 m_bSilencerOn: bool
 m_fDropTime: GameTime_t
 m_MoveCollide: MoveCollide_t
 m_ubInterpolationFrame: uint8
 m_vecMins: Vector
 m_nGlowRange: int32
 m_flFallbackWear: float32
 m_vLookTargetPosition: Vector
 m_iItemDefinitionIndex: uint16
 m_iEntityQuality: int32
 m_nRenderMode: RenderMode_t
 m_nInteractsExclude: uint64
 m_flCapsuleRadius: float32
 m_flGlowTime: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_OriginalOwnerXuidHigh: uint32
 m_nFallbackStatTrak: int32
 m_fThrowTime: GameTime_t
 m_flThrowStrength: float32
 m_triggerBloat: uint8
 m_nAddDecal: int32
 m_szCustomName: char[161]
 m_fAccuracyPenalty: float32
 m_bInReload: bool
 m_iState: WeaponState_t
 m_nNextPrimaryAttackTick: GameTick_t
 m_nOwnerId: uint32
 m_flDecalHealHeightRate: float32
 m_nInteractsAs: uint64
 m_nEntityId: uint32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bRenderToCubemaps: bool
 m_glowColorOverride: Color
 m_flDecalHealBloodRate: float32
 m_nSolidType: SolidType_t
 m_iGlowTeam: int32
 m_iEntityLevel: uint32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_hOuter: CHandle< CBaseEntity >
 m_flNextPrimaryAttackTickRatio: float32
 CBodyComponent: CBodyComponent
  m_nBoolVariablesCount: int32
  m_cellZ: uint16
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_nNewSequenceParity: int32
  m_bClientSideAnimation: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_hParent: CGameSceneNodeHandle
  m_nRandomSeedOffset: int32
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_angRotation: QAngle
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flGlowStartTime: float32
 m_nObjectCulling: uint8
 m_iReapplyProvisionParity: int32
 m_iTeamNum: uint8
 m_bIsHeldByPlayer: bool
 m_nNextSecondaryAttackTick: GameTick_t
 m_bvDisabledHitGroups: uint32[1]
 CRenderComponent: CRenderComponent
 m_vDecalForwardAxis: Vector
 m_bAnimGraphUpdateEnabled: bool
 m_nFallbackSeed: int32
 m_flFireSequenceStartTime: float32
 m_flRecoilIndex: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_bBurstMode: bool
 m_flFadeScale: float32
 m_bClientRagdoll: bool
 m_nNextThinkTick: GameTick_t
 m_LightGroup: CUtlStringToken
 m_nCollisionFunctionMask: uint8
 m_fadeMinDist: float32
 m_flDroppedAtTime: GameTime_t
 m_nDropTick: GameTick_t
 m_iGlowType: int32
 m_vDecalPosition: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_iAccountID: uint32
 m_iInventoryPosition: uint32
 m_usSolidFlags: uint8
 m_bShouldAnimateDuringGameplayPause: bool
 m_bRedraw: bool
 m_vCapsuleCenter2: Vector
 m_flGlowBackfaceMult: float32
 m_flPostponeFireReadyTime: GameTime_t
 m_bIsHauledBack: bool
 m_fLastShotTime: GameTime_t
 m_iNumEmptyAttacks: int32
 m_eThrowStatus: EGrenadeThrowState
 m_flNextSecondaryAttackTickRatio: float32
 m_iClip1: int32
 m_bClientSideRagdoll: bool
 m_vecMaxs: Vector
 m_nGlowRangeMin: int32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_iItemIDLow: uint32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_nFireSequenceStartTimeChange: int32
 m_weaponMode: CSWeaponMode
 m_iClip2: int32
 m_flAnimTime: float32
 m_clrRender: Color
 m_nEnablePhysics: uint8
 m_pReserveAmmo: int32[2]
 m_flSimulationTime: float32
 m_nRenderFX: RenderFx_t
 m_nFallbackPaintKit: int32
 m_bEligibleForScreenHighlight: bool
 m_vecForce: Vector
 m_iItemIDHigh: uint32
 m_bInitialized: bool
 m_MoveType: MoveType_t
 m_nHierarchyId: uint16
 m_iIronSightMode: int32
 m_bPinPulled: bool
 m_nViewModelIndex: uint32
 m_nSubclassID: CUtlStringToken
 m_vecSpecifiedSurroundingMins: Vector
 m_bInitiallyPopulateInterpHistory: bool
 m_iRecoilIndex: int32
 m_iOriginalTeamNumber: int32

11 CBaseGrenade
 m_nEntityId: uint32
 m_flGlowBackfaceMult: float32
 m_nObjectCulling: uint8
 m_clrRender: Color
 m_nCollisionGroup: uint8
 m_nEnablePhysics: uint8
 m_iGlowTeam: int32
 m_DmgRadius: float32
 m_nRenderFX: RenderFx_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_triggerBloat: uint8
 m_CollisionGroup: uint8
 m_nForceBone: int32
 CRenderComponent: CRenderComponent
 m_vecZ: CNetworkedQuantizedFloat
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecSpecifiedSurroundingMins: Vector
 m_vCapsuleCenter2: Vector
 m_fadeMaxDist: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_bShouldAnimateDuringGameplayPause: bool
 m_bAnimGraphUpdateEnabled: bool
 m_flDamage: float32
 m_bRenderToCubemaps: bool
 m_hThrower: CHandle< CBaseEntity >
 m_vCapsuleCenter1: Vector
 m_flGlowTime: float32
 m_bClientRagdoll: bool
 m_MoveCollide: MoveCollide_t
 m_flShadowStrength: float32
 m_nInteractsExclude: uint64
 m_flDecalHealBloodRate: float32
 m_flElasticity: float32
 m_vecY: CNetworkedQuantizedFloat
 m_flSimulationTime: float32
 m_bClientSideRagdoll: bool
 m_bAnimatedEveryTick: bool
 m_nCollisionFunctionMask: uint8
 m_vecX: CNetworkedQuantizedFloat
 m_MoveType: MoveType_t
 m_nInteractsWith: uint64
 m_nOwnerId: uint32
 m_vecMins: Vector
 m_iGlowType: int32
 m_nGlowRange: int32
 m_flGlowStartTime: float32
 m_flDetonateTime: GameTime_t
 CBodyComponent: CBodyComponent
  m_cellZ: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_bUseParentRenderBounds: bool
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_hierarchyAttachName: CUtlStringToken
  m_materialGroup: CUtlStringToken
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_flLastTeleportTime: float32
  m_angRotation: QAngle
  m_nIdealMotionType: int8
  m_bClientSideAnimation: bool
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_flWeight: CNetworkedQuantizedFloat
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_name: CUtlStringToken
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nBoolVariablesCount: int32
  m_vecZ: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_cellY: uint16
  m_hParent: CGameSceneNodeHandle
  m_flScale: float32
  m_bClientClothCreationSuppressed: bool
  m_vecX: CNetworkedQuantizedFloat
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_nRandomSeedOffset: int32
 m_bvDisabledHitGroups: uint32[1]
 m_fEffects: uint32
 m_nHierarchyId: uint16
 m_usSolidFlags: uint8
 m_flFadeScale: float32
 m_vDecalPosition: Vector
 m_vLookTargetPosition: Vector
 m_fFlags: uint32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vecMaxs: Vector
 m_bEligibleForScreenHighlight: bool
 m_flDecalHealHeightRate: float32
 m_bSimulatedEveryTick: bool
 m_nSubclassID: CUtlStringToken
 m_ubInterpolationFrame: uint8
 m_iTeamNum: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vDecalForwardAxis: Vector
 m_vecForce: Vector
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_bIsLive: bool
 m_fadeMinDist: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_LightGroup: CUtlStringToken
 m_flCreateTime: GameTime_t
 m_nSolidType: SolidType_t
 m_bFlashing: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_hOwner: CHandle< CBaseEntity >
  m_Transforms: CNetworkUtlVectorBase< CTransform >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nInteractsAs: uint64
 m_flCapsuleRadius: float32
 m_nGlowRangeMin: int32
 m_glowColorOverride: Color
 m_nAddDecal: int32
 m_nRenderMode: RenderMode_t

123 CLightEnvironmentEntity
 m_vecMins: Vector
 m_vCapsuleCenter2: Vector
 m_flDecalHealBloodRate: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_bSimulatedEveryTick: bool
 m_nInteractsWith: uint64
 m_nEntityId: uint32
 m_nOwnerId: uint32
 m_bFlashing: bool
 CLightComponent: CLightComponent
  m_Pattern: CUtlString
  m_flShadowCascadeDistanceFade: float32
  m_flAttenuation2: float32
  m_nCastShadows: int32
  m_bRenderDiffuse: bool
  m_flShadowCascadeDistance0: float32
  m_nShadowCascadeResolution1: int32
  m_flPrecomputedMaxRange: float32
  m_nFogLightingMode: int32
  m_flSkyIntensity: float32
  m_flMinRoughness: float32
  m_hLightCookie: CStrongHandle< InfoForResourceTypeCTextureBase >
  m_nShadowCascadeResolution2: int32
  m_nCascades: int32
  m_vPrecomputedOBBOrigin: Vector
  m_SkyAmbientBounce: Color
  m_flBrightness: float32
  m_flAttenuation1: float32
  m_bRenderToCubemaps: bool
  m_LightGroups: CUtlSymbolLarge
  m_SecondaryColor: Color
  m_flBrightnessMult: float32
  m_flLightStyleStartTime: GameTime_t
  m_flFalloff: float32
  m_nRenderSpecular: int32
  m_nCascadeRenderStaticObjects: int32
  m_nIndirectLight: int32
  m_flFadeMinDist: float32
  m_vPrecomputedOBBExtent: Vector
  m_flPhi: float32
  m_bRenderTransmissive: bool
  m_bEnabled: bool
  m_nShadowWidth: int32
  m_nShadowCascadeResolution0: int32
  m_flNearClipPlane: float32
  m_bPrecomputedFieldsValid: bool
  m_vPrecomputedBoundsMins: Vector
  m_SkyColor: Color
  m_flTheta: float32
  m_flShadowCascadeCrossFade: float32
  m_nDirectLight: int32
  m_vPrecomputedBoundsMaxs: Vector
  m_flRange: float32
  m_flCapsuleLength: float32
  m_flAttenuation0: float32
  m_bFlicker: bool
  m_vPrecomputedOBBAngles: QAngle
  m_bUseSecondaryColor: bool
  m_flOrthoLightHeight: float32
  m_flShadowCascadeDistance2: float32
  m_nShadowPriority: int32
  m_nBakedShadowIndex: int32
  m_Color: Color
  m_flOrthoLightWidth: float32
  m_flShadowCascadeDistance1: float32
  m_nShadowCascadeResolution3: int32
  m_flFadeMaxDist: float32
  m_flShadowFadeMinDist: float32
  m_flShadowFadeMaxDist: float32
  m_flFogContributionStength: float32
  m_flBrightnessScale: float32
  m_nShadowHeight: int32
  m_nStyle: int32
  m_flShadowCascadeDistance3: float32
  m_bUsesIndexedBakedLighting: bool
  m_bMixedShadows: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_triggerBloat: uint8
 m_vCapsuleCenter1: Vector
 m_nRenderFX: RenderFx_t
 CBodyComponent: CBodyComponent
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_name: CUtlStringToken
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_vecX: CNetworkedQuantizedFloat
 m_nSurroundType: SurroundingBoundsType_t
 m_nGlowRangeMin: int32
 m_flGlowTime: float32
 m_flShadowStrength: float32
 m_vecMaxs: Vector
 m_iGlowTeam: int32
 m_glowColorOverride: Color
 m_fadeMinDist: float32
 m_vDecalPosition: Vector
 m_flAnimTime: float32
 m_bAnimatedEveryTick: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nEnablePhysics: uint8
 m_iTeamNum: uint8
 m_MoveCollide: MoveCollide_t
 m_flCreateTime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flCapsuleRadius: float32
 m_fadeMaxDist: float32
 m_flSimulationTime: float32
 m_vecSpecifiedSurroundingMins: Vector
 m_flGlowStartTime: float32
 m_nAddDecal: int32
 m_bvDisabledHitGroups: uint32[1]
 m_nSolidType: SolidType_t
 m_usSolidFlags: uint8
 m_nGlowRange: int32
 CRenderComponent: CRenderComponent
 m_nInteractsExclude: uint64
 m_flDecalHealHeightRate: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flFadeScale: float32
 m_nCollisionFunctionMask: uint8
 m_nInteractsAs: uint64
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nCollisionGroup: uint8
 m_nSubclassID: CUtlStringToken
 m_MoveType: MoveType_t
 m_nHierarchyId: uint16
 m_CollisionGroup: uint8
 m_iGlowType: int32
 m_nObjectCulling: uint8
 m_clrRender: Color
 m_flElasticity: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_LightGroup: CUtlStringToken
 m_bRenderToCubemaps: bool
 m_bEligibleForScreenHighlight: bool
 m_flGlowBackfaceMult: float32
 m_fEffects: uint32
 m_vDecalForwardAxis: Vector

183 CSpotlightEnd
 m_hEffectEntity: CHandle< CBaseEntity >
 m_LightGroup: CUtlStringToken
 m_vecSpecifiedSurroundingMins: Vector
 m_flDecalHealHeightRate: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_iTeamNum: uint8
 m_bvDisabledHitGroups: uint32[1]
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nObjectCulling: uint8
 m_nOwnerId: uint32
 m_triggerBloat: uint8
 m_iGlowType: int32
 m_bFlashing: bool
 CRenderComponent: CRenderComponent
 CBodyComponent: CBodyComponent
  m_materialGroup: CUtlStringToken
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_MeshGroupMask: uint64
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
 m_CollisionGroup: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vDecalForwardAxis: Vector
 m_nCollisionFunctionMask: uint8
 m_vecMaxs: Vector
 m_nSolidType: SolidType_t
 m_vCapsuleCenter1: Vector
 m_flGlowTime: float32
 m_flShadowStrength: float32
 m_nRenderFX: RenderFx_t
 m_bRenderToCubemaps: bool
 m_flDecalHealBloodRate: float32
 m_bAnimatedEveryTick: bool
 m_flElasticity: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_usSolidFlags: uint8
 m_flSimulationTime: float32
 m_MoveType: MoveType_t
 m_bSimulatedEveryTick: bool
 m_nInteractsExclude: uint64
 m_bEligibleForScreenHighlight: bool
 m_flGlowBackfaceMult: float32
 m_Radius: float32
 m_MoveCollide: MoveCollide_t
 m_fEffects: uint32
 m_nInteractsWith: uint64
 m_vDecalPosition: Vector
 m_flAnimTime: float32
 m_flCreateTime: GameTime_t
 m_nInteractsAs: uint64
 m_nEntityId: uint32
 m_nEnablePhysics: uint8
 m_glowColorOverride: Color
 m_flGlowStartTime: float32
 m_flLightScale: float32
 m_ubInterpolationFrame: uint8
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_flCapsuleRadius: float32
 m_nGlowRange: int32
 m_fadeMinDist: float32
 m_flFadeScale: float32
 m_nRenderMode: RenderMode_t
 m_vCapsuleCenter2: Vector
 m_fadeMaxDist: float32
 m_nSubclassID: CUtlStringToken
 m_nSurroundType: SurroundingBoundsType_t
 m_nAddDecal: int32
 m_nHierarchyId: uint16
 m_nGlowRangeMin: int32
 m_vecMins: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_iGlowTeam: int32
 m_clrRender: Color
 m_nCollisionGroup: uint8

159 CPrecipitationBlocker
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_clrRender: Color
 m_nSolidType: SolidType_t
 m_bSimulatedEveryTick: bool
 m_iGlowTeam: int32
 m_flFadeScale: float32
 m_flShadowStrength: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nHierarchyId: uint16
 m_flAnimTime: float32
 m_nGlowRangeMin: int32
 m_iTeamNum: uint8
 m_flGlowStartTime: float32
 m_flDecalHealBloodRate: float32
 m_MoveType: MoveType_t
 m_nEntityId: uint32
 m_nCollisionFunctionMask: uint8
 m_iGlowType: int32
 m_bFlashing: bool
 m_flGlowTime: float32
 m_nObjectCulling: uint8
 m_LightGroup: CUtlStringToken
 m_nInteractsAs: uint64
 m_nInteractsWith: uint64
 m_nCollisionGroup: uint8
 m_vCapsuleCenter1: Vector
 m_nGlowRange: int32
 m_fadeMaxDist: float32
 m_fEffects: uint32
 m_flElasticity: float32
 m_nInteractsExclude: uint64
 m_nSubclassID: CUtlStringToken
 m_flCreateTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_vecMaxs: Vector
 m_CollisionGroup: uint8
 m_bvDisabledHitGroups: uint32[1]
 CBodyComponent: CBodyComponent
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_name: CUtlStringToken
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_vecX: CNetworkedQuantizedFloat
 m_bAnimatedEveryTick: bool
 m_bRenderToCubemaps: bool
 m_flCapsuleRadius: float32
 m_fadeMinDist: float32
 m_nAddDecal: int32
 m_vDecalForwardAxis: Vector
 m_usSolidFlags: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_ubInterpolationFrame: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_bEligibleForScreenHighlight: bool
 m_flGlowBackfaceMult: float32
 m_vDecalPosition: Vector
 m_flNavIgnoreUntilTime: GameTime_t
 m_nRenderFX: RenderFx_t
 m_nOwnerId: uint32
 m_triggerBloat: uint8
 m_nEnablePhysics: uint8
 m_vCapsuleCenter2: Vector
 CRenderComponent: CRenderComponent
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vecMins: Vector
 m_nSurroundType: SurroundingBoundsType_t
 m_glowColorOverride: Color
 m_flDecalHealHeightRate: float32
 m_flSimulationTime: float32
 m_MoveCollide: MoveCollide_t

208 CWeaponGalilAR
 m_bAnimatedEveryTick: bool
 m_flPostponeFireReadyTime: GameTime_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flFadeScale: float32
 m_bAnimGraphUpdateEnabled: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_iRecoilIndex: int32
 m_nCollisionGroup: uint8
 m_usSolidFlags: uint8
 m_flNextSecondaryAttackTickRatio: float32
 m_flCapsuleRadius: float32
 m_iGlowTeam: int32
 m_fadeMaxDist: float32
 m_bClientRagdoll: bool
 m_nFallbackPaintKit: int32
 m_iState: WeaponState_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_vecForce: Vector
 m_nFallbackSeed: int32
 m_fLastShotTime: GameTime_t
 m_nNextSecondaryAttackTick: GameTick_t
 m_nHierarchyId: uint16
 m_nGlowRange: int32
 m_bFlashing: bool
 m_bEligibleForScreenHighlight: bool
 m_nAddDecal: int32
 m_nCollisionFunctionMask: uint8
 m_flGlowTime: float32
 m_nNextPrimaryAttackTick: GameTick_t
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bInReload: bool
 m_iClip2: int32
 m_MoveCollide: MoveCollide_t
 m_nSubclassID: CUtlStringToken
 m_iInventoryPosition: uint32
 m_bIsHauledBack: bool
 m_fAccuracyPenalty: float32
 m_iTeamNum: uint8
 m_nRenderMode: RenderMode_t
 m_vecMaxs: Vector
 m_triggerBloat: uint8
 m_flGlowBackfaceMult: float32
 m_iReapplyProvisionParity: int32
 m_vCapsuleCenter1: Vector
 m_ProviderType: attributeprovidertypes_t
 m_nViewModelIndex: uint32
 m_iItemIDLow: uint32
 m_iAccountID: uint32
 m_flFireSequenceStartTime: float32
 m_flCreateTime: GameTime_t
 m_flTimeSilencerSwitchComplete: GameTime_t
 CRenderComponent: CRenderComponent
 m_LightGroup: CUtlStringToken
 m_nFireSequenceStartTimeChange: int32
 m_bPlayerFireEventIsPrimary: bool
 m_nDropTick: GameTick_t
 m_bvDisabledHitGroups: uint32[1]
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 CBodyComponent: CBodyComponent
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
 m_nEnablePhysics: uint8
 m_vDecalPosition: Vector
 m_nForceBone: int32
 m_OriginalOwnerXuidLow: uint32
 m_iOriginalTeamNumber: int32
 m_nEntityId: uint32
 m_vecMins: Vector
 m_OriginalOwnerXuidHigh: uint32
 m_nInteractsAs: uint64
 m_iGlowType: int32
 m_nGlowRangeMin: int32
 m_nFallbackStatTrak: int32
 m_bReloadVisuallyComplete: bool
 m_flDroppedAtTime: GameTime_t
 m_iEntityLevel: uint32
 m_flRecoilIndex: float32
 m_MoveType: MoveType_t
 m_fadeMinDist: float32
 m_flNextPrimaryAttackTickRatio: float32
 m_szCustomName: char[161]
 m_bSilencerOn: bool
 m_ubInterpolationFrame: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_nSurroundType: SurroundingBoundsType_t
 m_iItemDefinitionIndex: uint16
 m_bInitialized: bool
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_nSolidType: SolidType_t
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flDecalHealBloodRate: float32
 m_iClip1: int32
 m_flGlowStartTime: float32
 m_vLookTargetPosition: Vector
 m_clrRender: Color
 m_iEntityQuality: int32
 m_iItemIDHigh: uint32
 m_bBurstMode: bool
 m_pReserveAmmo: int32[2]
 m_nRenderFX: RenderFx_t
 m_nObjectCulling: uint8
 m_flDecalHealHeightRate: float32
 m_iBurstShotsRemaining: int32
 m_nNextThinkTick: GameTick_t
 m_bSimulatedEveryTick: bool
 m_CollisionGroup: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_hOuter: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_iIronSightMode: int32
 m_bNeedsBoltAction: bool
 m_flShadowStrength: float32
 m_vDecalForwardAxis: Vector
 m_flSimulationTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flElasticity: float32
 m_nInteractsExclude: uint64
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vCapsuleCenter2: Vector
 m_bInitiallyPopulateInterpHistory: bool
 m_bShouldAnimateDuringGameplayPause: bool
 m_flFallbackWear: float32
 m_weaponMode: CSWeaponMode
 m_zoomLevel: int32
 m_nOwnerId: uint32
 m_iNumEmptyAttacks: int32
 m_flAnimTime: float32
 m_bClientSideRagdoll: bool
 m_bRenderToCubemaps: bool
 m_nInteractsWith: uint64
 m_glowColorOverride: Color

16 CBasePropDoor
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flNavIgnoreUntilTime: GameTime_t
 m_nCollisionFunctionMask: uint8
 m_nEnablePhysics: uint8
 m_flShadowStrength: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_MoveType: MoveType_t
 m_iTeamNum: uint8
 m_nInteractsExclude: uint64
 m_nCollisionGroup: uint8
 m_iGlowType: int32
 m_nAddDecal: int32
 m_vecForce: Vector
 m_ubInterpolationFrame: uint8
 m_clrRender: Color
 m_nSolidType: SolidType_t
 m_bUseHitboxesForRenderBox: bool
 m_closedAngles: QAngle
 m_bSimulatedEveryTick: bool
 m_nOwnerId: uint32
 m_usSolidFlags: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flDecalHealBloodRate: float32
 m_noGhostCollision: bool
 m_closedPosition: Vector
 m_nHierarchyId: uint16
 m_vecMaxs: Vector
 m_vCapsuleCenter2: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bAnimGraphUpdateEnabled: bool
 CRenderComponent: CRenderComponent
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_bRenderToCubemaps: bool
 m_fadeMaxDist: float32
 m_bvDisabledHitGroups: uint32[1]
 m_spawnflags: uint32
 m_fEffects: uint32
 m_nInteractsWith: uint64
 m_bUseAnimGraph: bool
 m_nInteractsAs: uint64
 m_flCreateTime: GameTime_t
 m_bClientSideRagdoll: bool
 m_fadeMinDist: float32
 m_triggerBloat: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_vecMins: Vector
 m_flGlowStartTime: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_bClientRagdoll: bool
 m_nSubclassID: CUtlStringToken
 m_flElasticity: float32
 m_nEntityId: uint32
 m_nGlowRange: int32
 m_bFlashing: bool
 m_vDecalPosition: Vector
 m_vDecalForwardAxis: Vector
 m_MoveCollide: MoveCollide_t
 m_nRenderFX: RenderFx_t
 m_nRenderMode: RenderMode_t
 m_flFadeScale: float32
 m_flSimulationTime: float32
 m_bAnimatedEveryTick: bool
 m_vCapsuleCenter1: Vector
 m_flGlowTime: float32
 m_nObjectCulling: uint8
 m_nForceBone: int32
 CBodyComponent: CBodyComponent
  m_nRandomSeedOffset: int32
  m_name: CUtlStringToken
  m_bUseParentRenderBounds: bool
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_flPrevCycle: float32
  m_nIdealMotionType: int8
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_angRotation: QAngle
  m_flLastTeleportTime: float32
  m_cellY: uint16
  m_flScale: float32
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hParent: CGameSceneNodeHandle
  m_hSequence: HSequence
  m_materialGroup: CUtlStringToken
  m_nHitboxSet: uint8
  m_flWeight: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nBoolVariablesCount: int32
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_bClientSideAnimation: bool
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_flCycle: float32
  m_bClientClothCreationSuppressed: bool
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nResetEventsParity: int32
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_MeshGroupMask: uint64
  m_nNewSequenceParity: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nGlowRangeMin: int32
 m_flGlowBackfaceMult: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_eDoorState: DoorState_t
 m_bLocked: bool
 m_hMaster: CHandle< CBasePropDoor >
 m_LightGroup: CUtlStringToken
 m_flCapsuleRadius: float32
 m_iGlowTeam: int32
 m_flDecalHealHeightRate: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_CollisionGroup: uint8
 m_bEligibleForScreenHighlight: bool
 m_vecSpecifiedSurroundingMins: Vector
 m_glowColorOverride: Color

78 CEnvProjectedTexture
 m_flQuadraticAttenuation: float32
 m_nOwnerId: uint32
 m_bEnableShadows: bool
 m_bLightOnlyTarget: bool
 m_bLightWorld: bool
 m_bVolumetric: bool
 m_ubInterpolationFrame: uint8
 m_nInteractsExclude: uint64
 m_bFlipHorizontal: bool
 m_hTargetEntity: CHandle< CBaseEntity >
 m_flBrightnessScale: float32
 m_flColorTransitionTime: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_nRenderFX: RenderFx_t
 m_glowColorOverride: Color
 m_flGlowBackfaceMult: float32
 m_flVolumetricIntensity: float32
 m_triggerBloat: uint8
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flIntensity: float32
 m_fEffects: uint32
 m_LightGroup: CUtlStringToken
 m_vecMins: Vector
 m_flShadowStrength: float32
 m_flDecalHealHeightRate: float32
 m_clrRender: Color
 m_nHierarchyId: uint16
 m_bvDisabledHitGroups: uint32[1]
 m_flFadeScale: float32
 m_vDecalPosition: Vector
 m_nEnablePhysics: uint8
 m_flNoiseStrength: float32
 m_vCapsuleCenter1: Vector
 m_flNearZ: float32
 CRenderComponent: CRenderComponent
 m_SpotlightTextureName: char[512]
 CBodyComponent: CBodyComponent
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
 m_nSolidType: SolidType_t
 m_flGlowStartTime: float32
 m_nAddDecal: int32
 m_nNumPlanes: uint32
 m_MoveCollide: MoveCollide_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flFlashlightTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveType: MoveType_t
 m_vCapsuleCenter2: Vector
 m_bState: bool
 m_flLinearAttenuation: float32
 m_flCapsuleRadius: float32
 m_nSubclassID: CUtlStringToken
 m_nCollisionGroup: uint8
 m_bFlashing: bool
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nCollisionFunctionMask: uint8
 m_vecMaxs: Vector
 m_bSimulatedEveryTick: bool
 m_flAmbient: float32
 m_flFarZ: float32
 m_bEligibleForScreenHighlight: bool
 m_flPlaneOffset: float32
 m_bCameraSpace: bool
 m_iTeamNum: uint8
 m_nSpotlightTextureFrame: int32
 m_nObjectCulling: uint8
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsAs: uint64
 m_CollisionGroup: uint8
 m_iGlowType: int32
 m_flElasticity: float32
 m_flLightFOV: float32
 m_flAnimTime: float32
 m_nInteractsWith: uint64
 m_nEntityId: uint32
 m_fadeMaxDist: float32
 m_bAlwaysUpdate: bool
 m_iGlowTeam: int32
 m_nGlowRangeMin: int32
 m_fadeMinDist: float32
 m_flProjectionSize: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flGlowTime: float32
 m_flDecalHealBloodRate: float32
 m_vDecalForwardAxis: Vector
 m_flSimulationTime: float32
 m_bAnimatedEveryTick: bool
 m_nSurroundType: SurroundingBoundsType_t
 m_nGlowRange: int32
 m_LightColor: Color
 m_flCreateTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_usSolidFlags: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_bRenderToCubemaps: bool
 m_bSimpleProjection: bool
 m_nShadowQuality: uint32
 m_flRotation: float32

85 CFireSmoke
 m_flSimulationTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flCreateTime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nFlameModelIndex: int32
 m_flAnimTime: float32
 m_bSimulatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_flScale: float32
 m_nFlameFromAboveModelIndex: int32
 m_MoveType: MoveType_t
 m_nSubclassID: CUtlStringToken
 m_fEffects: uint32
 m_flStartScale: float32
 m_flScaleTime: float32
 m_nFlags: uint32
 m_hOwnerEntity: CHandle< CBaseEntity >
 CBodyComponent: CBodyComponent
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
 m_MoveCollide: MoveCollide_t
 m_iTeamNum: uint8
 m_flElasticity: float32
 m_bAnimatedEveryTick: bool

156 CPointWorldText
 m_flShadowStrength: float32
 m_flDecalHealHeightRate: float32
 m_vCapsuleCenter2: Vector
 m_nRenderMode: RenderMode_t
 m_nInteractsExclude: uint64
 m_nCollisionGroup: uint8
 m_nCollisionFunctionMask: uint8
 m_vCapsuleCenter1: Vector
 m_fadeMaxDist: float32
 m_bFullbright: bool
 m_iTeamNum: uint8
 m_nSubclassID: CUtlStringToken
 m_nGlowRangeMin: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_LightGroup: CUtlStringToken
 m_vecMins: Vector
 m_nSurroundType: SurroundingBoundsType_t
 m_nAddDecal: int32
 m_MoveType: MoveType_t
 m_nReorientMode: PointWorldTextReorientMode_t
 m_nOwnerId: uint32
 m_flGlowStartTime: float32
 m_fadeMinDist: float32
 m_vDecalForwardAxis: Vector
 m_nJustifyHorizontal: PointWorldTextJustifyHorizontal_t
 m_bvDisabledHitGroups: uint32[1]
 m_bRenderToCubemaps: bool
 m_flSimulationTime: float32
 m_bSimulatedEveryTick: bool
 m_bFlashing: bool
 m_flGlowBackfaceMult: float32
 m_FontName: char[64]
 m_flAnimTime: float32
 m_flElasticity: float32
 m_nRenderFX: RenderFx_t
 m_iGlowTeam: int32
 m_flGlowTime: float32
 m_bEligibleForScreenHighlight: bool
 m_flDepthOffset: float32
 m_MoveCollide: MoveCollide_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_vecMaxs: Vector
 m_nSolidType: SolidType_t
 m_nEnablePhysics: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 CBodyComponent: CBodyComponent
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
 m_flFadeScale: float32
 m_vDecalPosition: Vector
 m_flDecalHealBloodRate: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flFontSize: float32
 m_nInteractsAs: uint64
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nHierarchyId: uint16
 m_CollisionGroup: uint8
 m_iGlowType: int32
 m_glowColorOverride: Color
 m_flCreateTime: GameTime_t
 m_flWorldUnitsPerPx: float32
 m_ubInterpolationFrame: uint8
 m_nGlowRange: int32
 m_nEntityId: uint32
 m_bEnabled: bool
 m_nInteractsWith: uint64
 m_clrRender: Color
 m_nObjectCulling: uint8
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_triggerBloat: uint8
 m_flCapsuleRadius: float32
 m_messageText: char[512]
 CRenderComponent: CRenderComponent
 m_usSolidFlags: uint8
 m_Color: Color
 m_nJustifyVertical: PointWorldTextJustifyVertical_t
 m_vecSpecifiedSurroundingMins: Vector

154 CPointEntity
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveCollide: MoveCollide_t
 m_nSubclassID: CUtlStringToken
 m_fEffects: uint32
 m_bSimulatedEveryTick: bool
 m_flSimulationTime: float32
 CBodyComponent: CBodyComponent
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
 m_MoveType: MoveType_t
 m_iTeamNum: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flAnimTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flElasticity: float32
 m_flCreateTime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t

51 CCSPlayerPawnBase
 m_iGunGameProgressiveWeaponIndex: int32
 m_nSurvivalTeam: int32
 m_aimPunchTickFraction: float32
 m_vCapsuleCenter1: Vector
 m_nGlowRangeMin: int32
 m_pMovementServices: CPlayer_MovementServices*
  m_nLadderSurfacePropIndex: int32
  m_flJumpUntil: float32
  m_flOffsetTickStashedSpeed: float32
  m_flCrouchTransitionStartTime: GameTime_t
  m_bDesiresDuck: bool
  m_flFallVelocity: float32
  m_nDuckJumpTimeMsecs: uint32
  m_nDuckTimeMsecs: uint32
  m_flMaxFallVelocity: float32
  m_nCrouchState: uint32
  m_bDucked: bool
  m_nJumpTimeMsecs: uint32
  m_flMaxspeed: float32
  m_flDuckAmount: float32
  m_flDuckSpeed: float32
  m_flOffsetTickCompleteTime: float32
  m_bInCrouch: bool
  m_bDuckOverride: bool
  m_nToggleButtonDownMask: uint64
  m_arrForceSubtickMoveWhen: float32[4]
  m_nButtonDownMaskPrev: uint64
  m_bDucking: bool
  m_bInDuckJump: bool
  m_flLastDuckTime: float32
  m_vecLadderNormal: Vector
  m_flJumpVel: float32
  m_bOldJumpPressed: bool
  m_fStashGrenadeParameterWhen: GameTime_t
 m_nRenderFX: RenderFx_t
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bAnimatedEveryTick: bool
 m_bInBuyZone: bool
 enable: bool
 m_flDecalHealHeightRate: float32
 m_pCameraServices: CPlayer_CameraServices*
  m_hCtrl: CHandle< CFogController >
  m_vecCsViewPunchAngle: QAngle
  m_flCsViewPunchAngleTickRatio: float32
  m_hColorCorrectionCtrl: CHandle< CColorCorrection >
  m_hTonemapController: CHandle< CTonemapController2 >
  localSound: Vector[8]
  soundscapeEntityListIndex: int32
  m_flFOVRate: float32
  m_nCsViewPunchAngleTick: GameTick_t
  m_hZoomOwner: CHandle< CBaseEntity >
  localBits: uint8
  m_iFOVStart: uint32
  m_flFOVTime: GameTime_t
  soundscapeIndex: int32
  soundEventHash: uint32
  m_PostProcessingVolumes: CNetworkUtlVectorBase< CHandle< CPostProcessingVolume > >
  m_hViewEntity: CHandle< CBaseEntity >
  m_iFOV: uint32
 m_nHitBodyPart: int32
 m_aimPunchAngleVel: QAngle
 m_nCollisionGroup: uint8
 m_flHitHeading: float32
 m_iNumGunGameKillsWithCurrentWeapon: int32
 m_aimPunchAngle: QAngle
 lerptime: GameTime_t
 m_flGravityScale: float32
 m_flEmitSoundTime: GameTime_t
 m_flLowerBodyYawTarget: float32
 m_bStrafing: bool
 m_LightGroup: CUtlStringToken
 m_flFlashDuration: float32
 m_flGlowTime: float32
 m_bInHostageRescueZone: bool
 startLerpTo: float32
 m_nInteractsWith: uint64
 m_nEnablePhysics: uint8
 m_bIsScoped: bool
 m_nCollisionFunctionMask: uint8
 m_usSolidFlags: uint8
 m_nDeathCamMusic: int32
 m_iBlockingUseActionInProgress: CSPlayerBlockingUseAction_t
 m_ArmorValue: int32
 m_bNightVisionOn: bool
 m_bMadeFinalGunGameProgressiveKill: bool
 m_bIsSpawnRappelling: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_pActionTrackingServices: CCSPlayer_ActionTrackingServices*
  m_bIsRescuing: bool
  m_weaponPurchases: CUtlVectorEmbeddedNetworkVar< WeaponPurchaseCount_t >
   m_nItemDefIndex: uint16
   m_nCount: uint16
 m_bCanMoveDuringFreezePeriod: bool
 m_ubInterpolationFrame: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_bResumeZoom: bool
 m_bHideTargetID: bool
 m_bIsWalking: bool
 m_unRoundStartEquipmentValue: uint16
 m_nSurvivalTeamNumber: int32
 m_iTeamNum: uint8
 m_bFlashing: bool
 m_bIsGrabbingHostage: bool
 m_flProgressBarStartTime: float32
 m_unFreezetimeEndEquipmentValue: uint16
 m_iHideHUD: uint32
 m_thirdPersonHeading: QAngle
 m_triggerBloat: uint8
 m_vecBaseVelocity: Vector
 m_hEffectEntity: CHandle< CBaseEntity >
 m_hMyWearables: CNetworkUtlVectorBase< CHandle< CEconWearable > >
 m_vCapsuleCenter2: Vector
 m_flVelocityModifier: float32
 m_szLastPlaceName: char[18]
 m_nWorldGroupID: WorldGroupId_t
 m_flSlopeDropHeight: float32
 m_flElasticity: float32
 locallightscale: float32
 m_pViewModelServices: CCSPlayer_ViewModelServices*
  m_hViewModel: CHandle< CBaseViewModel >[3]
 m_bSpottedByMask: uint32[2]
 scale: int16
 m_vecMaxs: Vector
 m_iProgressBarDuration: int32
 m_flTimeScale: float32
 m_pWaterServices: CPlayer_WaterServices*
 endLerpTo: float32
 scattering: float32
 origin: Vector
 HDRColorScale: float32
 m_CollisionGroup: uint8
 m_vDecalForwardAxis: Vector
 m_nForceBone: int32
 m_bIsDefusing: bool
 m_iNumGunGameTRKillPoints: int32
 m_angEyeAngles: QAngle
 m_iHealth: int32
 m_isCurrentGunGameTeamLeader: bool
 m_bKilledByTaser: bool
 m_flFieldOfView: float32
 m_pObserverServices: CPlayer_ObserverServices*
  m_iObserverMode: uint8
  m_hObserverTarget: CHandle< CBaseEntity >
 m_iDirection: int32
 m_fEffects: uint32
 m_vecSpecifiedSurroundingMins: Vector
 m_vDecalPosition: Vector
 m_iSecondaryAddon: int32
 m_MoveType: MoveType_t
 m_clrRender: Color
 m_pWeaponServices: CPlayer_WeaponServices*
  m_bIsLookingAtWeapon: bool
  m_bIsHoldingLookAtWeapon: bool
  m_hLastWeapon: CHandle< CBasePlayerWeapon >
  m_flNextAttack: GameTime_t
  m_hMyWeapons: CNetworkUtlVectorBase< CHandle< CBasePlayerWeapon > >
  m_hActiveWeapon: CHandle< CBasePlayerWeapon >
  m_iAmmo: uint16[32]
 colorPrimaryLerpTo: Color
 farz: float32
 blend: bool
 m_iGlowType: int32
 m_bEligibleForScreenHighlight: bool
 m_bGunGameImmunity: bool
 m_iMoveState: int32
 m_nLastKillerIndex: CEntityIndex
 m_nLastConcurrentKilled: int32
 m_bNoReflectionFog: bool
 m_fadeMaxDist: float32
 m_flDeathTime: GameTime_t
 m_flDetectedByEnemySensorTime: GameTime_t
 m_iPrimaryAddon: int32
 skyboxFogFactorLerpTo: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_unTotalRoundDamageDealt: uint32
 m_flGuardianTooFarDistFrac: float32
 colorPrimary: Color
 m_vHeadConstraintOffset: Vector
 m_bClientRagdoll: bool
 m_nSubclassID: CUtlStringToken
 m_nWhichBombZone: int32
 m_bHud_RadarHidden: bool
 m_flSimulationTime: float32
 m_flSlopeDropOffset: float32
 m_nHeavyAssaultSuitCooldownRemaining: int32
 m_flFlashMaxAlpha: float32
 end: float32
 m_nAddDecal: int32
 m_hController: CHandle< CBasePlayerController >
 m_bKilledByHeadshot: bool
 duration: float32
 m_vecY: CNetworkedQuantizedFloat
 m_bHasFemaleVoice: bool
 m_flHealthShotBoostExpirationTime: float32
 m_iMaxHealth: int32
 m_flShadowStrength: float32
 m_fImmuneToGunGameDamageTime: GameTime_t
 m_vecSpawnRappellingRopeOrigin: Vector
 m_flFriction: float32
 m_nHierarchyId: uint16
 m_pUseServices: CPlayer_UseServices*
 m_bInitiallyPopulateInterpHistory: bool
 start: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_flGlowStartTime: float32
 m_bClientSideRagdoll: bool
 m_bRenderToCubemaps: bool
 m_passiveItems: bool[4]
 m_vecZ: CNetworkedQuantizedFloat
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_lifeState: uint8
 m_unCurrentEquipmentValue: uint16
 dirPrimary: Vector
 m_vecMins: Vector
 m_iGlowTeam: int32
 m_nOwnerId: uint32
 m_bvDisabledHitGroups: uint32[1]
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bHud_MiniScoreHidden: bool
 m_flStamina: float32
 m_nNextThinkTick: GameTick_t
 m_blinktoggle: bool
 m_bHasNightVision: bool
 CRenderComponent: CRenderComponent
 maxdensity: float32
 m_isCurrentGunGameLeader: bool
 m_hSurvivalAssassinationTarget: CHandle< CCSPlayerPawnBase >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_hOriginalController: CHandle< CCSPlayerController >
 m_iAddonBits: int32
 bClip3DSkyBoxNearToWorldFar: bool
 colorSecondary: Color
 m_flDecalHealBloodRate: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_bSpotted: bool
 m_bWaitForNoAttack: bool
 m_iShotsFired: int32
 maxdensityLerpTo: float32
 m_nSolidType: SolidType_t
 m_flCapsuleRadius: float32
 m_vecForce: Vector
 blendtobackground: float32
 m_glowColorOverride: Color
 m_fadeMinDist: float32
 m_flWaterLevel: float32
 m_fFlags: uint32
 m_flGlowBackfaceMult: float32
 m_MoveCollide: MoveCollide_t
 m_vecX: CNetworkedQuantizedFloat
 m_flCreateTime: GameTime_t
 m_bInNoDefuseArea: bool
 skyboxFogFactor: float32
 exponent: float32
 m_nGlowRange: int32
 m_nObjectCulling: uint8
 m_nInteractsAs: uint64
 m_aimPunchTickBase: int32
 m_flTimeOfLastInjury: float32
 m_bInBombZone: bool
 m_vecPlayerPatchEconIndices: uint32[5]
 m_nRenderMode: RenderMode_t
 m_nEntityId: uint32
 m_vLookTargetPosition: Vector
 m_ServerViewAngleChanges: CUtlVectorEmbeddedNetworkVar< ViewAngleServerChange_t >
  nType: FixAngleSet_t
  qAngle: QAngle
  nIndex: uint32
 CBodyComponent: CBodyComponent
  m_nNewSequenceParity: int32
  m_flCycle: float32
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nIdealMotionType: int8
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nBoolVariablesCount: int32
  m_cellX: uint16
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_cellZ: uint16
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_flLastTeleportTime: float32
  m_vecX: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_name: CUtlStringToken
  m_bUseParentRenderBounds: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_flPrevCycle: float32
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_hParent: CGameSceneNodeHandle
  m_nResetEventsParity: int32
  m_nAnimLoopMode: AnimLoopMode_t
  m_vecY: CNetworkedQuantizedFloat
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_nRandomSeedOffset: int32
  m_cellY: uint16
  m_nOwnerOnlyBoolVariablesCount: int32
  m_bClientClothCreationSuppressed: bool
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nOutsideWorld: uint16
  m_hierarchyAttachName: CUtlStringToken
  m_flWeight: CNetworkedQuantizedFloat
 m_flFadeScale: float32
 m_bAnimGraphUpdateEnabled: bool
 flClip3DSkyBoxNearToWorldFarOffset: float32
 m_hGroundEntity: CHandle< CBaseEntity >
 m_bSimulatedEveryTick: bool
 m_nInteractsExclude: uint64
 m_bHasMovedSinceSpawn: bool
 colorSecondaryLerpTo: Color
 m_nRelativeDirectionOfLastInjury: RelativeDamagedDirection_t
 m_fMolotovDamageTime: float32
 m_iPlayerState: CSPlayerState

61 CDrone
 m_bShouldAnimateDuringGameplayPause: bool
 m_flElasticity: float32
 m_nInteractsExclude: uint64
 m_nEntityId: uint32
 m_nGlowRange: int32
 m_flShadowStrength: float32
 m_vDecalPosition: Vector
 m_bAnimGraphUpdateEnabled: bool
 m_iTeamNum: uint8
 m_vCapsuleCenter1: Vector
 m_vCapsuleCenter2: Vector
 m_bFlashing: bool
 m_nObjectCulling: uint8
 m_flDecalHealBloodRate: float32
 m_flSimulationTime: float32
 CBodyComponent: CBodyComponent
  m_nNewSequenceParity: int32
  m_nRandomSeedOffset: int32
  m_name: CUtlStringToken
  m_bUseParentRenderBounds: bool
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_flPrevCycle: float32
  m_nIdealMotionType: int8
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_angRotation: QAngle
  m_flLastTeleportTime: float32
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_cellY: uint16
  m_flScale: float32
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_flWeight: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hParent: CGameSceneNodeHandle
  m_hSequence: HSequence
  m_materialGroup: CUtlStringToken
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nBoolVariablesCount: int32
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bClientSideAnimation: bool
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_flCycle: float32
  m_bClientClothCreationSuppressed: bool
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nResetEventsParity: int32
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_MeshGroupMask: uint64
 m_nForceBone: int32
 m_triggerBloat: uint8
 m_flDecalHealHeightRate: float32
 m_bPilotTakeoverAllowed: bool
 CRenderComponent: CRenderComponent
 m_hMoveToThisEntity: CHandle< CBaseEntity >
 m_bvDisabledHitGroups: uint32[1]
 m_MoveCollide: MoveCollide_t
 m_ubInterpolationFrame: uint8
 m_usSolidFlags: uint8
 m_nSolidType: SolidType_t
 m_nAddDecal: int32
 m_bAwake: bool
 m_nOwnerId: uint32
 m_vecMins: Vector
 m_vecSpecifiedSurroundingMaxs: Vector
 m_clrRender: Color
 m_iGlowType: int32
 m_glowColorOverride: Color
 m_flGlowTime: float32
 m_fadeMaxDist: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bClientSideRagdoll: bool
 m_CollisionGroup: uint8
 m_vDecalForwardAxis: Vector
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_nCollisionGroup: uint8
 m_bEligibleForScreenHighlight: bool
 m_bInitiallyPopulateInterpHistory: bool
 m_noGhostCollision: bool
 m_nSubclassID: CUtlStringToken
 m_bAnimatedEveryTick: bool
 m_vecForce: Vector
 m_hCurrentPilot: CHandle< CBaseEntity >
 m_vecTagPositions: Vector[24]
 m_LightGroup: CUtlStringToken
 m_bRenderToCubemaps: bool
 m_flCapsuleRadius: float32
 m_flFadeScale: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bClientRagdoll: bool
 m_flCreateTime: GameTime_t
 m_spawnflags: uint32
 m_nInteractsAs: uint64
 m_vecSpecifiedSurroundingMins: Vector
 m_iGlowTeam: int32
 m_flGlowBackfaceMult: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_nHierarchyId: uint16
 m_flGlowStartTime: float32
 m_fadeMinDist: float32
 m_hDeliveryCargo: CHandle< CBaseEntity >
 m_hRecentCargo: CHandle< CBaseEntity >
 m_hPotentialCargo: CHandle< CBaseEntity >
 m_MoveType: MoveType_t
 m_bSimulatedEveryTick: bool
 m_nRenderFX: RenderFx_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nCollisionFunctionMask: uint8
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_nRenderMode: RenderMode_t
 m_nInteractsWith: uint64
 m_vecMaxs: Vector
 m_nEnablePhysics: uint8
 m_nGlowRangeMin: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nSurroundType: SurroundingBoundsType_t
 m_vecTagIncrements: int32[24]

83 CEnvWind
 m_iWindSeed: uint32
 m_windRadius: int32
 m_flMinGustDelay: float32
 m_iMinWind: uint16
 m_flGustDuration: float32
 m_flInitialWindSpeed: float32
 m_iMaxWind: uint16
 m_iGustDirChange: uint16
 m_location: Vector
 m_iInitialWindDir: uint16
 m_flStartTime: GameTime_t
 m_iMinGust: uint16
 m_iMaxGust: uint16
 m_flMaxGustDelay: float32

4 CBaseClientUIEntity
 m_bRenderToCubemaps: bool
 m_nSurroundType: SurroundingBoundsType_t
 m_vCapsuleCenter2: Vector
 m_flShadowStrength: float32
 m_flAnimTime: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_ubInterpolationFrame: uint8
 m_bAnimatedEveryTick: bool
 m_nEnablePhysics: uint8
 m_vCapsuleCenter1: Vector
 m_flDecalHealBloodRate: float32
 CBodyComponent: CBodyComponent
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
 m_MoveCollide: MoveCollide_t
 m_nEntityId: uint32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bEligibleForScreenHighlight: bool
 m_fadeMaxDist: float32
 m_nSubclassID: CUtlStringToken
 m_fEffects: uint32
 m_flNavIgnoreUntilTime: GameTime_t
 m_nCollisionGroup: uint8
 m_glowColorOverride: Color
 m_vDecalPosition: Vector
 m_flCreateTime: GameTime_t
 m_iTeamNum: uint8
 m_vecMaxs: Vector
 m_flCapsuleRadius: float32
 m_flGlowBackfaceMult: float32
 m_bEnabled: bool
 m_PanelID: CUtlSymbolLarge
 m_nRenderMode: RenderMode_t
 m_clrRender: Color
 m_flGlowStartTime: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nGlowRange: int32
 m_usSolidFlags: uint8
 m_vDecalForwardAxis: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_DialogXMLName: CUtlSymbolLarge
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flElasticity: float32
 m_bFlashing: bool
 m_CollisionGroup: uint8
 m_nInteractsAs: uint64
 m_nOwnerId: uint32
 m_bvDisabledHitGroups: uint32[1]
 m_flGlowTime: float32
 m_flFadeScale: float32
 m_LightGroup: CUtlStringToken
 m_nInteractsExclude: uint64
 CRenderComponent: CRenderComponent
 m_flSimulationTime: float32
 m_nRenderFX: RenderFx_t
 m_triggerBloat: uint8
 m_nObjectCulling: uint8
 m_flDecalHealHeightRate: float32
 m_PanelClassName: CUtlSymbolLarge
 m_nCollisionFunctionMask: uint8
 m_vecMins: Vector
 m_iGlowTeam: int32
 m_fadeMinDist: float32
 m_nAddDecal: int32
 m_vecSpecifiedSurroundingMins: Vector
 m_iGlowType: int32
 m_bSimulatedEveryTick: bool
 m_nInteractsWith: uint64
 m_nHierarchyId: uint16
 m_nSolidType: SolidType_t
 m_nGlowRangeMin: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_MoveType: MoveType_t

31 CChicken
 m_nRenderMode: RenderMode_t
 m_bRenderToCubemaps: bool
 m_vecSpecifiedSurroundingMins: Vector
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_iGlowType: int32
 m_glowColorOverride: Color
 m_fadeMaxDist: float32
 m_bAnimGraphUpdateEnabled: bool
 m_iInventoryPosition: uint32
 m_bSimulatedEveryTick: bool
 m_vDecalPosition: Vector
 m_bUseHitboxesForRenderBox: bool
 m_iReapplyProvisionParity: int32
 m_ProviderType: attributeprovidertypes_t
 m_iItemDefinitionIndex: uint16
 CRenderComponent: CRenderComponent
 m_nHierarchyId: uint16
 m_vecMaxs: Vector
 m_flCapsuleRadius: float32
 m_vDecalForwardAxis: Vector
 m_bInitiallyPopulateInterpHistory: bool
 m_iEntityQuality: int32
 m_iItemIDHigh: uint32
 m_iItemIDLow: uint32
 m_szCustomName: char[161]
 m_bvDisabledHitGroups: uint32[1]
 CBodyComponent: CBodyComponent
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nBoolVariablesCount: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nInteractsWith: uint64
 m_nInteractsExclude: uint64
 m_nOwnerId: uint32
 m_nSolidType: SolidType_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nGlowRangeMin: int32
 m_bEligibleForScreenHighlight: bool
 m_flDecalHealBloodRate: float32
 m_hOuter: CHandle< CBaseEntity >
 m_usSolidFlags: uint8
 m_nEnablePhysics: uint8
 m_nAddDecal: int32
 m_bInitialized: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_ubInterpolationFrame: uint8
 m_nCollisionFunctionMask: uint8
 m_OriginalOwnerXuidHigh: uint32
 m_jumpedThisFrame: bool
 m_leader: CHandle< CCSPlayerPawnBase >
 m_flAnimTime: float32
 m_fEffects: uint32
 m_clrRender: Color
 m_nSurroundType: SurroundingBoundsType_t
 m_iGlowTeam: int32
 m_flShadowStrength: float32
 m_bClientRagdoll: bool
 m_MoveCollide: MoveCollide_t
 m_iTeamNum: uint8
 m_nInteractsAs: uint64
 m_bFlashing: bool
 m_flGlowTime: float32
 m_vecForce: Vector
 m_iEntityLevel: uint32
 m_MoveType: MoveType_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flNavIgnoreUntilTime: GameTime_t
 m_nCollisionGroup: uint8
 m_fadeMinDist: float32
 m_nObjectCulling: uint8
 m_iAccountID: uint32
 m_bAnimatedEveryTick: bool
 m_nRenderFX: RenderFx_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_LightGroup: CUtlStringToken
 m_nEntityId: uint32
 m_triggerBloat: uint8
 m_nForceBone: int32
 m_flGlowBackfaceMult: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bUseAnimGraph: bool
 m_bClientSideRagdoll: bool
 m_CollisionGroup: uint8
 m_vCapsuleCenter1: Vector
 m_flFadeScale: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_nSubclassID: CUtlStringToken
 m_flElasticity: float32
 m_vecMins: Vector
 m_vCapsuleCenter2: Vector
 m_nGlowRange: int32
 m_flDecalHealHeightRate: float32
 m_flGlowStartTime: float32
 m_OriginalOwnerXuidLow: uint32
 m_flSimulationTime: float32
 m_flCreateTime: GameTime_t
 m_noGhostCollision: bool

17 CBaseToggle
 m_ubInterpolationFrame: uint8
 m_iTeamNum: uint8
 m_vCapsuleCenter2: Vector
 m_flDecalHealHeightRate: float32
 CRenderComponent: CRenderComponent
 m_nSolidType: SolidType_t
 m_nGlowRangeMin: int32
 m_flElasticity: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_nInteractsWith: uint64
 m_nSubclassID: CUtlStringToken
 m_nCollisionFunctionMask: uint8
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bRenderToCubemaps: bool
 m_nInteractsExclude: uint64
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nGlowRange: int32
 m_bvDisabledHitGroups: uint32[1]
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_MoveType: MoveType_t
 m_bAnimatedEveryTick: bool
 m_vecMins: Vector
 m_bEligibleForScreenHighlight: bool
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_flGlowTime: float32
 m_flFadeScale: float32
 m_nObjectCulling: uint8
 m_triggerBloat: uint8
 m_iGlowTeam: int32
 CBodyComponent: CBodyComponent
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
 m_bSimulatedEveryTick: bool
 m_flGlowBackfaceMult: float32
 m_fadeMaxDist: float32
 m_flSimulationTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nRenderFX: RenderFx_t
 m_nOwnerId: uint32
 m_vecSpecifiedSurroundingMins: Vector
 m_flGlowStartTime: float32
 m_flAnimTime: float32
 m_MoveCollide: MoveCollide_t
 m_CollisionGroup: uint8
 m_fEffects: uint32
 m_nAddDecal: int32
 m_vDecalPosition: Vector
 m_clrRender: Color
 m_nInteractsAs: uint64
 m_nHierarchyId: uint16
 m_usSolidFlags: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_flCapsuleRadius: float32
 m_flShadowStrength: float32
 m_glowColorOverride: Color
 m_flCreateTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_LightGroup: CUtlStringToken
 m_nCollisionGroup: uint8
 m_nEnablePhysics: uint8
 m_vCapsuleCenter1: Vector
 m_iGlowType: int32
 m_bFlashing: bool
 m_fadeMinDist: float32
 m_vDecalForwardAxis: Vector
 m_nEntityId: uint32
 m_vecMaxs: Vector
 m_flDecalHealBloodRate: float32

67 CEntityDissolve
 m_bAnimatedEveryTick: bool
 m_nInteractsAs: uint64
 m_nSolidType: SolidType_t
 m_triggerBloat: uint8
 m_glowColorOverride: Color
 m_flFadeOutModelStart: float32
 m_ubInterpolationFrame: uint8
 m_nGlowRange: int32
 m_fadeMinDist: float32
 m_nObjectCulling: uint8
 m_flFadeScale: float32
 m_flFadeInStart: float32
 m_nDissolveType: EntityDisolveType_t
 m_flElasticity: float32
 m_nHierarchyId: uint16
 m_usSolidFlags: uint8
 m_vDecalPosition: Vector
 m_flAnimTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_fEffects: uint32
 m_clrRender: Color
 m_flDecalHealBloodRate: float32
 m_flFadeOutModelLength: float32
 m_nRenderMode: RenderMode_t
 m_nCollisionGroup: uint8
 m_vDecalForwardAxis: Vector
 m_bSimulatedEveryTick: bool
 m_bRenderToCubemaps: bool
 m_vCapsuleCenter2: Vector
 m_bEligibleForScreenHighlight: bool
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nMagnitude: uint32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flCreateTime: GameTime_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_nInteractsExclude: uint64
 m_nSurroundType: SurroundingBoundsType_t
 m_iGlowTeam: int32
 m_bvDisabledHitGroups: uint32[1]
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nEntityId: uint32
 m_flCapsuleRadius: float32
 m_flGlowTime: float32
 m_flStartTime: GameTime_t
 m_vDissolverOrigin: Vector
 CBodyComponent: CBodyComponent
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_name: CUtlStringToken
 m_nSubclassID: CUtlStringToken
 m_iTeamNum: uint8
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_LightGroup: CUtlStringToken
 m_nOwnerId: uint32
 m_vecSpecifiedSurroundingMins: Vector
 m_nGlowRangeMin: int32
 m_flFadeOutStart: float32
 m_vecMins: Vector
 m_CollisionGroup: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bFlashing: bool
 m_vecMaxs: Vector
 m_fadeMaxDist: float32
 m_flSimulationTime: float32
 m_nEnablePhysics: uint8
 m_vCapsuleCenter1: Vector
 m_iGlowType: int32
 m_flGlowStartTime: float32
 m_flFadeInLength: float32
 m_nInteractsWith: uint64
 m_flShadowStrength: float32
 m_nAddDecal: int32
 m_flDecalHealHeightRate: float32
 m_nRenderFX: RenderFx_t
 m_nCollisionFunctionMask: uint8
 m_flGlowBackfaceMult: float32
 m_flFadeOutLength: float32
 m_MoveCollide: MoveCollide_t
 m_MoveType: MoveType_t
 CRenderComponent: CRenderComponent

158 CPrecipitation
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nSubclassID: CUtlStringToken
 m_flNavIgnoreUntilTime: GameTime_t
 m_LightGroup: CUtlStringToken
 m_fadeMaxDist: float32
 m_nObjectCulling: uint8
 m_MoveCollide: MoveCollide_t
 m_MoveType: MoveType_t
 m_bRenderToCubemaps: bool
 m_nCollisionGroup: uint8
 m_vecMins: Vector
 m_nSolidType: SolidType_t
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_fEffects: uint32
 m_nOwnerId: uint32
 m_nEnablePhysics: uint8
 m_nGlowRange: int32
 m_flShadowStrength: float32
 m_flSimulationTime: float32
 m_iTeamNum: uint8
 m_bvDisabledHitGroups: uint32[1]
 m_nInteractsWith: uint64
 m_flDecalHealHeightRate: float32
 m_vCapsuleCenter2: Vector
 m_iGlowTeam: int32
 m_vDecalPosition: Vector
 m_vDecalForwardAxis: Vector
 m_clrRender: Color
 m_vecSpecifiedSurroundingMins: Vector
 m_vCapsuleCenter1: Vector
 m_flFadeScale: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsAs: uint64
 m_fadeMinDist: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flGlowTime: float32
 m_bDisabled: bool
 m_hEffectEntity: CHandle< CBaseEntity >
 m_triggerBloat: uint8
 m_flDecalHealBloodRate: float32
 m_bAnimatedEveryTick: bool
 m_nInteractsExclude: uint64
 m_nRenderFX: RenderFx_t
 m_nEntityId: uint32
 m_bEligibleForScreenHighlight: bool
 m_ubInterpolationFrame: uint8
 m_nRenderMode: RenderMode_t
 m_iGlowType: int32
 m_bFlashing: bool
 m_bClientSidePredicted: bool
 m_flElasticity: float32
 m_vecMaxs: Vector
 m_CollisionGroup: uint8
 CBodyComponent: CBodyComponent
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
 m_nCollisionFunctionMask: uint8
 m_spawnflags: uint32
 m_flCapsuleRadius: float32
 m_nGlowRangeMin: int32
 CRenderComponent: CRenderComponent
 m_flAnimTime: float32
 m_flCreateTime: GameTime_t
 m_usSolidFlags: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_flGlowStartTime: float32
 m_flGlowBackfaceMult: float32
 m_nAddDecal: int32
 m_bSimulatedEveryTick: bool
 m_nHierarchyId: uint16
 m_vecSpecifiedSurroundingMaxs: Vector
 m_glowColorOverride: Color

14 CBasePlayerPawn
 maxdensity: float32
 m_bInitiallyPopulateInterpHistory: bool
 flClip3DSkyBoxNearToWorldFarOffset: float32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flGlowTime: float32
 m_bClientRagdoll: bool
 colorPrimaryLerpTo: Color
 m_flGravityScale: float32
 m_MoveCollide: MoveCollide_t
 m_bClientSideRagdoll: bool
 m_nAddDecal: int32
 CRenderComponent: CRenderComponent
 m_fadeMaxDist: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSubclassID: CUtlStringToken
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_flDeathTime: GameTime_t
 m_bvDisabledHitGroups: uint32[1]
 m_lifeState: uint8
 m_nRenderFX: RenderFx_t
 start: float32
 exponent: float32
 m_nGlowRange: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flFriction: float32
 m_vecSpecifiedSurroundingMins: Vector
 m_nNextThinkTick: GameTick_t
 m_fFlags: uint32
 m_usSolidFlags: uint8
 m_CollisionGroup: uint8
 m_glowColorOverride: Color
 m_flGlowStartTime: float32
 m_LightGroup: CUtlStringToken
 m_nCollisionGroup: uint8
 m_blinktoggle: bool
 HDRColorScale: float32
 m_nGlowRangeMin: int32
 m_vDecalPosition: Vector
 m_vecForce: Vector
 m_bNoReflectionFog: bool
 m_iMaxHealth: int32
 m_nSolidType: SolidType_t
 m_hMyWearables: CNetworkUtlVectorBase< CHandle< CEconWearable > >
 m_nSurroundType: SurroundingBoundsType_t
 m_bFlashing: bool
 m_bAnimatedEveryTick: bool
 m_vecX: CNetworkedQuantizedFloat
 m_nEntityId: uint32
 m_vLookTargetPosition: Vector
 duration: float32
 m_nWorldGroupID: WorldGroupId_t
 m_bSimulatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 endLerpTo: float32
 m_flFadeScale: float32
 m_hController: CHandle< CBasePlayerController >
 m_flTimeScale: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_vecBaseVelocity: Vector
 m_iGlowTeam: int32
 m_bAnimGraphUpdateEnabled: bool
 m_nForceBone: int32
 m_pCameraServices: CPlayer_CameraServices*
  soundEventHash: uint32
  m_flCsViewPunchAngleTickRatio: float32
  localSound: Vector[8]
  m_hColorCorrectionCtrl: CHandle< CColorCorrection >
  soundscapeIndex: int32
  m_hCtrl: CHandle< CFogController >
  m_vecCsViewPunchAngle: QAngle
  m_nCsViewPunchAngleTick: GameTick_t
  localBits: uint8
  soundscapeEntityListIndex: int32
  m_PostProcessingVolumes: CNetworkUtlVectorBase< CHandle< CPostProcessingVolume > >
  m_hViewEntity: CHandle< CBaseEntity >
  m_hTonemapController: CHandle< CTonemapController2 >
 farz: float32
 enable: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_iHealth: int32
 skyboxFogFactor: float32
 dirPrimary: Vector
 m_vCapsuleCenter2: Vector
 m_flCapsuleRadius: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecMins: Vector
 colorSecondaryLerpTo: Color
 m_MoveType: MoveType_t
 m_iTeamNum: uint8
 m_ServerViewAngleChanges: CUtlVectorEmbeddedNetworkVar< ViewAngleServerChange_t >
  nType: FixAngleSet_t
  qAngle: QAngle
  nIndex: uint32
 m_flGlowBackfaceMult: float32
 lerptime: GameTime_t
 m_fadeMinDist: float32
 m_flFieldOfView: float32
 skyboxFogFactorLerpTo: float32
 blendtobackground: float32
 m_vecY: CNetworkedQuantizedFloat
 m_vecZ: CNetworkedQuantizedFloat
 m_hGroundEntity: CHandle< CBaseEntity >
 m_nInteractsWith: uint64
 end: float32
 m_flWaterLevel: float32
 scale: int16
 colorPrimary: Color
 m_flAnimTime: float32
 colorSecondary: Color
 m_ubInterpolationFrame: uint8
 bClip3DSkyBoxNearToWorldFar: bool
 m_flDecalHealHeightRate: float32
 m_bRenderToCubemaps: bool
 m_nHierarchyId: uint16
 m_nInteractsExclude: uint64
 m_vCapsuleCenter1: Vector
 m_iGlowType: int32
 m_flexWeight: CNetworkUtlVectorBase< float32 >
 startLerpTo: float32
 scattering: float32
 m_fEffects: uint32
 m_flElasticity: float32
 locallightscale: float32
 blend: bool
 m_flSimulationTime: float32
 m_clrRender: Color
 m_iHideHUD: uint32
 origin: Vector
 m_nEnablePhysics: uint8
 m_flDecalHealBloodRate: float32
 m_triggerBloat: uint8
 m_bShouldAnimateDuringGameplayPause: bool
 maxdensityLerpTo: float32
 m_nRenderMode: RenderMode_t
 m_nInteractsAs: uint64
 m_nOwnerId: uint32
 CBodyComponent: CBodyComponent
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_flScale: float32
  m_flCycle: float32
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bIsAnimationEnabled: bool
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellZ: uint16
  m_hSequence: HSequence
  m_hierarchyAttachName: CUtlStringToken
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_bUseParentRenderBounds: bool
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_flPrevCycle: float32
  m_materialGroup: CUtlStringToken
  m_bClientSideAnimation: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_MeshGroupMask: uint64
  m_nBoolVariablesCount: int32
  m_hParent: CGameSceneNodeHandle
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flLastTeleportTime: float32
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_angRotation: QAngle
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nOutsideWorld: uint16
  m_nResetEventsParity: int32
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nRandomSeedOffset: int32
  m_vecX: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_nNewSequenceParity: int32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
 m_flCreateTime: GameTime_t
 m_bEligibleForScreenHighlight: bool
 m_flShadowStrength: float32
 m_nObjectCulling: uint8
 m_vDecalForwardAxis: Vector
 m_nCollisionFunctionMask: uint8
 m_vecMaxs: Vector

223 CWeaponSG556
 m_iEntityLevel: uint32
 m_bInReload: bool
 m_flElasticity: float32
 m_vecMins: Vector
 m_flRecoilIndex: float32
 m_nNextThinkTick: GameTick_t
 m_OriginalOwnerXuidHigh: uint32
 m_flFallbackWear: float32
 m_iClip2: int32
 m_pReserveAmmo: int32[2]
 m_usSolidFlags: uint8
 m_iGlowType: int32
 m_flGlowStartTime: float32
 m_flGlowBackfaceMult: float32
 m_nGlowRange: int32
 m_nAddDecal: int32
 m_vDecalForwardAxis: Vector
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_hOwner: CHandle< CBaseEntity >
  m_Transforms: CNetworkUtlVectorBase< CTransform >
 m_MoveType: MoveType_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nDropTick: GameTick_t
 m_LightGroup: CUtlStringToken
 m_nSurroundType: SurroundingBoundsType_t
 m_iEntityQuality: int32
 m_bPlayerFireEventIsPrimary: bool
 m_nInteractsExclude: uint64
 m_bFlashing: bool
 m_flDecalHealHeightRate: float32
 m_vLookTargetPosition: Vector
 m_nRenderFX: RenderFx_t
 m_nOwnerId: uint32
 CRenderComponent: CRenderComponent
 m_iClip1: int32
 m_nFallbackSeed: int32
 m_glowColorOverride: Color
 m_bInitiallyPopulateInterpHistory: bool
 m_bIsHauledBack: bool
 m_fLastShotTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_vCapsuleCenter1: Vector
 m_flFadeScale: float32
 m_flPostponeFireReadyTime: GameTime_t
 m_flFireSequenceStartTime: float32
 CBodyComponent: CBodyComponent
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_nNewSequenceParity: int32
  m_bClientSideAnimation: bool
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_hParent: CGameSceneNodeHandle
  m_nRandomSeedOffset: int32
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
 m_nCollisionFunctionMask: uint8
 m_nSolidType: SolidType_t
 m_iItemIDLow: uint32
 m_vecForce: Vector
 m_nFallbackPaintKit: int32
 m_flNextSecondaryAttackTickRatio: float32
 m_flDroppedAtTime: GameTime_t
 m_vecSpecifiedSurroundingMins: Vector
 m_iGlowTeam: int32
 m_iItemIDHigh: uint32
 m_OriginalOwnerXuidLow: uint32
 m_bReloadVisuallyComplete: bool
 m_flSimulationTime: float32
 m_flCreateTime: GameTime_t
 m_bClientSideRagdoll: bool
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bShouldAnimateDuringGameplayPause: bool
 m_fAccuracyPenalty: float32
 m_flNextPrimaryAttackTickRatio: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_bRenderToCubemaps: bool
 m_vecMaxs: Vector
 m_nGlowRangeMin: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nFallbackStatTrak: int32
 m_iRecoilIndex: int32
 m_iNumEmptyAttacks: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSubclassID: CUtlStringToken
 m_CollisionGroup: uint8
 m_flCapsuleRadius: float32
 m_nNextPrimaryAttackTick: GameTick_t
 m_bNeedsBoltAction: bool
 m_ubInterpolationFrame: uint8
 m_iAccountID: uint32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_iBurstShotsRemaining: int32
 m_nInteractsAs: uint64
 m_nEnablePhysics: uint8
 m_iReapplyProvisionParity: int32
 m_iTeamNum: uint8
 m_nCollisionGroup: uint8
 m_flShadowStrength: float32
 m_bSilencerOn: bool
 m_flAnimTime: float32
 m_vDecalPosition: Vector
 m_flDecalHealBloodRate: float32
 m_iItemDefinitionIndex: uint16
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_nInteractsWith: uint64
 m_hOuter: CHandle< CBaseEntity >
 m_ProviderType: attributeprovidertypes_t
 m_nHierarchyId: uint16
 m_flGlowTime: float32
 m_fadeMinDist: float32
 m_iOriginalTeamNumber: int32
 m_nEntityId: uint32
 m_bAnimGraphUpdateEnabled: bool
 m_bClientRagdoll: bool
 m_nNextSecondaryAttackTick: GameTick_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_bInitialized: bool
 m_weaponMode: CSWeaponMode
 m_bvDisabledHitGroups: uint32[1]
 m_bSimulatedEveryTick: bool
 m_nFireSequenceStartTimeChange: int32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_iInventoryPosition: uint32
 m_szCustomName: char[161]
 m_iState: WeaponState_t
 m_MoveCollide: MoveCollide_t
 m_vCapsuleCenter2: Vector
 m_fadeMaxDist: float32
 m_nForceBone: int32
 m_bAnimatedEveryTick: bool
 m_bEligibleForScreenHighlight: bool
 m_nViewModelIndex: uint32
 m_iIronSightMode: int32
 m_zoomLevel: int32
 m_clrRender: Color
 m_triggerBloat: uint8
 m_nObjectCulling: uint8
 m_bBurstMode: bool

229 CWeaponXM1014
 m_bClientRagdoll: bool
 m_iEntityQuality: int32
 m_ubInterpolationFrame: uint8
 m_bSimulatedEveryTick: bool
 m_vCapsuleCenter2: Vector
 m_nFallbackStatTrak: int32
 m_bInReload: bool
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_MoveType: MoveType_t
 m_iItemDefinitionIndex: uint16
 m_bSilencerOn: bool
 m_flFadeScale: float32
 m_vDecalForwardAxis: Vector
 m_bPlayerFireEventIsPrimary: bool
 CBodyComponent: CBodyComponent
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
 m_nEntityId: uint32
 m_ProviderType: attributeprovidertypes_t
 m_fAccuracyPenalty: float32
 m_hOuter: CHandle< CBaseEntity >
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_iOriginalTeamNumber: int32
 m_nEnablePhysics: uint8
 m_bInitialized: bool
 m_nViewModelIndex: uint32
 m_nCollisionFunctionMask: uint8
 m_CollisionGroup: uint8
 m_iEntityLevel: uint32
 m_bReloadVisuallyComplete: bool
 m_nSubclassID: CUtlStringToken
 m_nCollisionGroup: uint8
 m_nAddDecal: int32
 m_iGlowTeam: int32
 m_flShadowStrength: float32
 m_nFireSequenceStartTimeChange: int32
 m_iRecoilIndex: int32
 m_bBurstMode: bool
 m_pReserveAmmo: int32[2]
 m_nOwnerId: uint32
 m_nFallbackPaintKit: int32
 m_flAnimTime: float32
 m_nInteractsAs: uint64
 m_bEligibleForScreenHighlight: bool
 m_szCustomName: char[161]
 m_iNumEmptyAttacks: int32
 m_vecMins: Vector
 m_OriginalOwnerXuidHigh: uint32
 m_nRenderFX: RenderFx_t
 m_clrRender: Color
 m_nInteractsExclude: uint64
 m_bRenderToCubemaps: bool
 m_bShouldAnimateDuringGameplayPause: bool
 m_vLookTargetPosition: Vector
 m_iReapplyProvisionParity: int32
 m_nNextSecondaryAttackTick: GameTick_t
 m_iClip1: int32
 m_iTeamNum: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_vecMaxs: Vector
 m_nSurroundType: SurroundingBoundsType_t
 m_vecSpecifiedSurroundingMaxs: Vector
 CRenderComponent: CRenderComponent
 m_flElasticity: float32
 m_triggerBloat: uint8
 m_vCapsuleCenter1: Vector
 m_flCapsuleRadius: float32
 m_fadeMinDist: float32
 m_bClientSideRagdoll: bool
 m_fadeMaxDist: float32
 m_flFallbackWear: float32
 m_iState: WeaponState_t
 m_flNextSecondaryAttackTickRatio: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nHierarchyId: uint16
 m_flDecalHealBloodRate: float32
 m_OriginalOwnerXuidLow: uint32
 m_iIronSightMode: int32
 m_nNextPrimaryAttackTick: GameTick_t
 m_flNextPrimaryAttackTickRatio: float32
 m_flGlowTime: float32
 m_nObjectCulling: uint8
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
 m_flDroppedAtTime: GameTime_t
 m_nGlowRange: int32
 m_iItemIDLow: uint32
 m_nFallbackSeed: int32
 m_nNextThinkTick: GameTick_t
 m_iItemIDHigh: uint32
 m_flRecoilIndex: float32
 m_bIsHauledBack: bool
 m_flSimulationTime: float32
 m_flDecalHealHeightRate: float32
 m_bAnimGraphUpdateEnabled: bool
 m_vecForce: Vector
 m_nForceBone: int32
 m_iAccountID: uint32
 m_flPostponeFireReadyTime: GameTime_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_flGlowStartTime: float32
 m_bvDisabledHitGroups: uint32[1]
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nInteractsWith: uint64
 m_bFlashing: bool
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flFireSequenceStartTime: float32
 m_weaponMode: CSWeaponMode
 m_nDropTick: GameTick_t
 m_bAnimatedEveryTick: bool
 m_vecSpecifiedSurroundingMins: Vector
 m_MoveCollide: MoveCollide_t
 m_fEffects: uint32
 m_usSolidFlags: uint8
 m_bInitiallyPopulateInterpHistory: bool
 m_nRenderMode: RenderMode_t
 m_LightGroup: CUtlStringToken
 m_iInventoryPosition: uint32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flCreateTime: GameTime_t
 m_flGlowBackfaceMult: float32
 m_vDecalPosition: Vector
 m_glowColorOverride: Color
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_fLastShotTime: GameTime_t
 m_nSolidType: SolidType_t
 m_iGlowType: int32
 m_nGlowRangeMin: int32
 m_iClip2: int32

226 CWeaponTaser
 m_nCollisionGroup: uint8
 m_bShouldAnimateDuringGameplayPause: bool
 m_fAccuracyPenalty: float32
 m_iNumEmptyAttacks: int32
 m_bFlashing: bool
 m_bInitiallyPopulateInterpHistory: bool
 m_zoomLevel: int32
 m_nNextPrimaryAttackTick: GameTick_t
 m_bPlayerFireEventIsPrimary: bool
 m_iBurstShotsRemaining: int32
 m_iClip1: int32
 m_flCreateTime: GameTime_t
 m_LightGroup: CUtlStringToken
 m_vCapsuleCenter1: Vector
 m_flDecalHealBloodRate: float32
 m_iReapplyProvisionParity: int32
 m_nNextSecondaryAttackTick: GameTick_t
 m_pReserveAmmo: int32[2]
 m_nViewModelIndex: uint32
 m_iItemDefinitionIndex: uint16
 m_OriginalOwnerXuidHigh: uint32
 m_flRecoilIndex: float32
 m_flFallbackWear: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nInteractsAs: uint64
 m_flCapsuleRadius: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_bClientRagdoll: bool
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_nCollisionFunctionMask: uint8
 m_triggerBloat: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nFallbackSeed: int32
 m_bNeedsBoltAction: bool
 m_flElasticity: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_iItemIDHigh: uint32
 m_iOriginalTeamNumber: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nInteractsExclude: uint64
 m_nEntityId: uint32
 m_iGlowTeam: int32
 m_bEligibleForScreenHighlight: bool
 m_bInReload: bool
 m_clrRender: Color
 m_nOwnerId: uint32
 m_nNextThinkTick: GameTick_t
 m_nHierarchyId: uint16
 m_usSolidFlags: uint8
 m_nEnablePhysics: uint8
 m_iGlowType: int32
 m_iEntityLevel: uint32
 m_iRecoilIndex: int32
 m_nInteractsWith: uint64
 m_vecMins: Vector
 m_MoveType: MoveType_t
 m_bAnimatedEveryTick: bool
 m_nGlowRange: int32
 m_nGlowRangeMin: int32
 m_ProviderType: attributeprovidertypes_t
 m_flDroppedAtTime: GameTime_t
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nFallbackStatTrak: int32
 m_MoveCollide: MoveCollide_t
 m_nSolidType: SolidType_t
 m_flGlowStartTime: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_hOuter: CHandle< CBaseEntity >
 m_iItemIDLow: uint32
 m_iIronSightMode: int32
 m_flAnimTime: float32
 m_fadeMaxDist: float32
 m_vLookTargetPosition: Vector
 m_OriginalOwnerXuidLow: uint32
 m_bIsHauledBack: bool
 m_flNextSecondaryAttackTickRatio: float32
 m_flNextPrimaryAttackTickRatio: float32
 m_bClientSideRagdoll: bool
 m_ubInterpolationFrame: uint8
 m_fadeMinDist: float32
 m_flFadeScale: float32
 m_iInventoryPosition: uint32
 m_flPostponeFireReadyTime: GameTime_t
 m_CollisionGroup: uint8
 m_nForceBone: int32
 m_szCustomName: char[161]
 m_iClip2: int32
 m_nRenderMode: RenderMode_t
 m_nSurroundType: SurroundingBoundsType_t
 m_vCapsuleCenter2: Vector
 m_nDropTick: GameTick_t
 m_iTeamNum: uint8
 m_flFireSequenceStartTime: float32
 m_bSimulatedEveryTick: bool
 m_nObjectCulling: uint8
 m_bInitialized: bool
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_bReloadVisuallyComplete: bool
 m_flGlowBackfaceMult: float32
 m_nAddDecal: int32
 m_vDecalPosition: Vector
 m_vDecalForwardAxis: Vector
 m_flDecalHealHeightRate: float32
 m_iAccountID: uint32
 m_flSimulationTime: float32
 m_iState: WeaponState_t
 m_bBurstMode: bool
 m_bvDisabledHitGroups: uint32[1]
 m_fEffects: uint32
 m_bRenderToCubemaps: bool
 m_vecMaxs: Vector
 m_nFallbackPaintKit: int32
 m_nFireSequenceStartTimeChange: int32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_fFireTime: GameTime_t
 m_nRenderFX: RenderFx_t
 m_vecSpecifiedSurroundingMins: Vector
 m_glowColorOverride: Color
 m_iEntityQuality: int32
 m_flNavIgnoreUntilTime: GameTime_t
 m_bAnimGraphUpdateEnabled: bool
 m_vecForce: Vector
 m_weaponMode: CSWeaponMode
 m_flShadowStrength: float32
 m_fLastShotTime: GameTime_t
 CBodyComponent: CBodyComponent
  m_angRotation: QAngle
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
 m_nSubclassID: CUtlStringToken
 m_flGlowTime: float32
 m_bSilencerOn: bool
 CRenderComponent: CRenderComponent

23 CBRC4Target
 m_vecMaxs: Vector
 m_vCapsuleCenter1: Vector
 m_flFadeScale: float32
 m_flShadowStrength: float32
 m_MoveCollide: MoveCollide_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nHierarchyId: uint16
 m_nCollisionFunctionMask: uint8
 m_bBrokenOpen: bool
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_flGlowBackfaceMult: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_iTeamNum: uint8
 m_vCapsuleCenter2: Vector
 m_nGlowRangeMin: int32
 m_flRadius: float32
 m_bvDisabledHitGroups: uint32[1]
 m_vecMins: Vector
 m_flCapsuleRadius: float32
 m_nGlowRange: int32
 m_flSimulationTime: float32
 m_nSubclassID: CUtlStringToken
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nOwnerId: uint32
 CBodyComponent: CBodyComponent
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_name: CUtlStringToken
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_nHitboxSet: uint8
  m_MeshGroupMask: uint64
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nBoolVariablesCount: int32
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
 m_flCreateTime: GameTime_t
 m_nEnablePhysics: uint8
 m_glowColorOverride: Color
 m_fadeMaxDist: float32
 m_nObjectCulling: uint8
 m_ubInterpolationFrame: uint8
 m_nRenderFX: RenderFx_t
 m_nEntityId: uint32
 m_usSolidFlags: uint8
 m_vDecalForwardAxis: Vector
 m_flDecalHealHeightRate: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_vecForce: Vector
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_bClientSideRagdoll: bool
 m_bSimulatedEveryTick: bool
 m_LightGroup: CUtlStringToken
 m_bAnimGraphUpdateEnabled: bool
 m_CollisionGroup: uint8
 m_fadeMinDist: float32
 m_vDecalPosition: Vector
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flGlowTime: float32
 m_flDecalHealBloodRate: float32
 m_bRenderToCubemaps: bool
 m_nSurroundType: SurroundingBoundsType_t
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveType: MoveType_t
 m_flElasticity: float32
 m_nRenderMode: RenderMode_t
 m_nSolidType: SolidType_t
 m_nAddDecal: int32
 m_bInitiallyPopulateInterpHistory: bool
 m_iGlowTeam: int32
 m_bFlashing: bool
 m_flGlowStartTime: float32
 CRenderComponent: CRenderComponent
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nInteractsAs: uint64
 m_nInteractsWith: uint64
 m_nCollisionGroup: uint8
 m_bEligibleForScreenHighlight: bool
 m_bClientRagdoll: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_triggerBloat: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_iGlowType: int32
 m_flAnimTime: float32
 m_clrRender: Color
 m_nInteractsExclude: uint64
 m_nForceBone: int32

111 CInfoLadderDismount
 m_nSubclassID: CUtlStringToken
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flElasticity: float32
 m_bAnimatedEveryTick: bool
 m_fEffects: uint32
 m_flNavIgnoreUntilTime: GameTime_t
 m_flAnimTime: float32
 m_flSimulationTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 CBodyComponent: CBodyComponent
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
 m_ubInterpolationFrame: uint8
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveCollide: MoveCollide_t
 m_MoveType: MoveType_t
 m_flCreateTime: GameTime_t
 m_iTeamNum: uint8
 m_bSimulatedEveryTick: bool

165 CRectLight
 m_flDecalHealHeightRate: float32
 m_nBounceLight: int32
 m_iGlowType: int32
 m_flBounceScale: float32
 m_fEffects: uint32
 m_flNavIgnoreUntilTime: GameTime_t
 m_flShape: float32
 m_vPrecomputedOBBOrigin: Vector
 m_vecMins: Vector
 m_bFlashing: bool
 m_flColorTemperature: float32
 m_nSubclassID: CUtlStringToken
 m_LightStyleTargets: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_vSizeParams: Vector
 m_flSoftX: float32
 m_nHierarchyId: uint16
 m_flGlowBackfaceMult: float32
 m_nColorMode: int32
 m_flFadeSizeEnd: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_flSkirtNear: float32
 m_nShadowPriority: int32
 m_hLightCookie: CStrongHandle< InfoForResourceTypeCTextureBase >
 m_flGlowStartTime: float32
 m_fadeMinDist: float32
 m_flDecalHealBloodRate: float32
 m_vCapsuleCenter2: Vector
 m_flCapsuleRadius: float32
 m_iGlowTeam: int32
 m_bvDisabledHitGroups: uint32[1]
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nInteractsAs: uint64
 m_LightStyleEvents: CNetworkUtlVectorBase< CUtlString >
 m_nFogShadows: int32
 m_flShadowFadeSizeEnd: float32
 CBodyComponent: CBodyComponent
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
 m_iTeamNum: uint8
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nBakedShadowIndex: int32
 m_LightStyleString: CUtlString
 CRenderComponent: CRenderComponent
 m_flCreateTime: GameTime_t
 m_clrRender: Color
 m_nGlowRange: int32
 m_nSurroundType: SurroundingBoundsType_t
 m_flFadeScale: float32
 m_flFadeSizeStart: float32
 m_flGlowTime: float32
 m_flRange: float32
 m_nShadowMapSize: int32
 m_flFogScale: float32
 m_flFogStrength: float32
 m_LightGroup: CUtlStringToken
 m_nInteractsWith: uint64
 m_nSolidType: SolidType_t
 m_vDecalForwardAxis: Vector
 m_flShadowFadeSizeStart: float32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nLuminaireShape: int32
 m_flLightStyleStartTime: GameTime_t
 m_nEnablePhysics: uint8
 m_nAddDecal: int32
 m_flBrightnessScale: float32
 m_flSoftY: float32
 m_bSimulatedEveryTick: bool
 m_vecMaxs: Vector
 m_CollisionGroup: uint8
 m_vAlternateColor: Vector
 m_fAlternateColorBrightness: float32
 m_vPrecomputedBoundsMaxs: Vector
 m_MoveCollide: MoveCollide_t
 m_nObjectCulling: uint8
 m_nBakeSpecularToCubemaps: int32
 m_flMinRoughness: float32
 m_MoveType: MoveType_t
 m_bAnimatedEveryTick: bool
 m_vDecalPosition: Vector
 m_nEntityId: uint32
 m_nDirectLight: int32
 m_vPrecomputedBoundsMins: Vector
 m_vecSpecifiedSurroundingMins: Vector
 m_ubInterpolationFrame: uint8
 m_nRenderMode: RenderMode_t
 m_nRenderFX: RenderFx_t
 m_nInteractsExclude: uint64
 m_flBrightness: float32
 m_nCastShadows: int32
 m_vShear: Vector
 m_vPrecomputedOBBAngles: QAngle
 m_nOwnerId: uint32
 m_triggerBloat: uint8
 m_glowColorOverride: Color
 m_flLuminaireSize: float32
 m_flLuminaireAnisotropy: float32
 m_flSkirt: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nCollisionFunctionMask: uint8
 m_fadeMaxDist: float32
 m_flShadowStrength: float32
 m_bPrecomputedFieldsValid: bool
 m_vPrecomputedOBBExtent: Vector
 m_nGlowRangeMin: int32
 m_bEligibleForScreenHighlight: bool
 m_Color: Color
 m_flElasticity: float32
 m_usSolidFlags: uint8
 m_vCapsuleCenter1: Vector
 m_flAnimTime: float32
 m_bRenderToCubemaps: bool
 m_QueuedLightStyleStrings: CNetworkUtlVectorBase< CUtlString >
 m_bShowLight: bool
 m_flSimulationTime: float32
 m_nCollisionGroup: uint8
 m_bContactShadow: bool
 m_nFog: int32
 m_bEnabled: bool
 m_vBakeSpecularToCubemapsSize: Vector

178 CSoundOpvarSetOBBEntity
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool
 m_flAnimTime: float32
 m_flSimulationTime: float32
 CBodyComponent: CBodyComponent
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
 m_MoveType: MoveType_t
 m_nSubclassID: CUtlStringToken
 m_flCreateTime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_iTeamNum: uint8
 m_fEffects: uint32
 m_flNavIgnoreUntilTime: GameTime_t
 m_MoveCollide: MoveCollide_t
 m_flElasticity: float32
 m_iszStackName: CUtlSymbolLarge
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bSimulatedEveryTick: bool
 m_iszOperatorName: CUtlSymbolLarge
 m_iszOpvarName: CUtlSymbolLarge
 m_iOpvarIndex: int32
 m_bUseAutoCompare: bool

76 CEnvLightProbeVolume
 m_flAnimTime: float32
 m_bSimulatedEveryTick: bool
 m_hLightProbeDirectLightScalarsTexture: CStrongHandle< InfoForResourceTypeCTextureBase >
 m_vBoxMaxs: Vector
 m_bEnabled: bool
 m_flSimulationTime: float32
 m_nSubclassID: CUtlStringToken
 m_flElasticity: float32
 m_hLightProbeTexture: CStrongHandle< InfoForResourceTypeCTextureBase >
 m_LightGroups: CUtlSymbolLarge
 m_bMoveable: bool
 m_nLightProbeAtlasZ: int32
 m_fEffects: uint32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_MoveType: MoveType_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flNavIgnoreUntilTime: GameTime_t
 m_hLightProbeDirectLightIndicesTexture: CStrongHandle< InfoForResourceTypeCTextureBase >
 m_bStartDisabled: bool
 m_nLightProbeAtlasY: int32
 m_bAnimatedEveryTick: bool
 m_nLightProbeSizeY: int32
 CBodyComponent: CBodyComponent
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
 m_MoveCollide: MoveCollide_t
 m_iTeamNum: uint8
 m_hLightProbeDirectLightShadowsTexture: CStrongHandle< InfoForResourceTypeCTextureBase >
 m_vBoxMins: Vector
 m_nHandshake: int32
 m_nLightProbeSizeZ: int32
 m_nLightProbeAtlasX: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flCreateTime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_nPriority: int32
 m_nLightProbeSizeX: int32

88 CFlashbang
 m_glowColorOverride: Color
 m_flNextPrimaryAttackTickRatio: float32
 m_ubInterpolationFrame: uint8
 m_iNumEmptyAttacks: int32
 m_bJumpThrow: bool
 m_nOwnerId: uint32
 m_vLookTargetPosition: Vector
 m_szCustomName: char[161]
 m_bAnimGraphUpdateEnabled: bool
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_bIsHeldByPlayer: bool
 m_bvDisabledHitGroups: uint32[1]
 m_nViewModelIndex: uint32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nCollisionGroup: uint8
 m_flFireSequenceStartTime: float32
 m_triggerBloat: uint8
 m_nGlowRangeMin: int32
 m_flGlowStartTime: float32
 m_vecForce: Vector
 m_nNextSecondaryAttackTick: GameTick_t
 m_flSimulationTime: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool
 m_hOuter: CHandle< CBaseEntity >
 m_weaponMode: CSWeaponMode
 m_nSubclassID: CUtlStringToken
 m_nInteractsWith: uint64
 m_vecSpecifiedSurroundingMins: Vector
 m_iItemIDLow: uint32
 m_flFallbackWear: float32
 m_bBurstMode: bool
 m_nEntityId: uint32
 m_flGlowBackfaceMult: float32
 m_flDecalHealHeightRate: float32
 m_nDropTick: GameTick_t
 m_nHierarchyId: uint16
 m_vCapsuleCenter1: Vector
 m_bReloadVisuallyComplete: bool
 m_nObjectCulling: uint8
 m_iIronSightMode: int32
 m_bRedraw: bool
 m_bClientRagdoll: bool
 m_iReapplyProvisionParity: int32
 m_iInventoryPosition: uint32
 m_nFallbackSeed: int32
 m_vCapsuleCenter2: Vector
 m_iGlowTeam: int32
 m_flShadowStrength: float32
 m_flRecoilIndex: float32
 m_eThrowStatus: EGrenadeThrowState
 CRenderComponent: CRenderComponent
 m_iTeamNum: uint8
 m_bSimulatedEveryTick: bool
 m_flDecalHealBloodRate: float32
 m_flCapsuleRadius: float32
 m_fadeMinDist: float32
 m_nFallbackPaintKit: int32
 m_bInReload: bool
 m_flThrowStrengthApproach: float32
 m_flAnimTime: float32
 m_flElasticity: float32
 m_clrRender: Color
 m_bInitiallyPopulateInterpHistory: bool
 m_nForceBone: int32
 m_pReserveAmmo: int32[2]
 m_MoveCollide: MoveCollide_t
 m_MoveType: MoveType_t
 m_nAddDecal: int32
 m_fAccuracyPenalty: float32
 m_fThrowTime: GameTime_t
 m_iClip2: int32
 m_fEffects: uint32
 m_nRenderFX: RenderFx_t
 m_iOriginalTeamNumber: int32
 m_nNextPrimaryAttackTick: GameTick_t
 m_bEligibleForScreenHighlight: bool
 m_bShouldAnimateDuringGameplayPause: bool
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_flNavIgnoreUntilTime: GameTime_t
 m_nEnablePhysics: uint8
 m_flFadeScale: float32
 m_usSolidFlags: uint8
 m_bFlashing: bool
 m_fDropTime: GameTime_t
 m_vDecalPosition: Vector
 m_iItemIDHigh: uint32
 m_OriginalOwnerXuidHigh: uint32
 m_nFallbackStatTrak: int32
 m_bSilencerOn: bool
 m_iClip1: int32
 m_nSolidType: SolidType_t
 m_nSurroundType: SurroundingBoundsType_t
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_bPinPulled: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nCollisionFunctionMask: uint8
 m_vecMaxs: Vector
 m_bRenderToCubemaps: bool
 m_nInteractsExclude: uint64
 m_iItemDefinitionIndex: uint16
 m_bInitialized: bool
 m_OriginalOwnerXuidLow: uint32
 CBodyComponent: CBodyComponent
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_nNewSequenceParity: int32
  m_bClientSideAnimation: bool
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_hParent: CGameSceneNodeHandle
  m_nRandomSeedOffset: int32
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bClientSideRagdoll: bool
 m_flPostponeFireReadyTime: GameTime_t
 m_fLastShotTime: GameTime_t
 m_ProviderType: attributeprovidertypes_t
 m_iGlowType: int32
 m_nGlowRange: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_iAccountID: uint32
 m_flDroppedAtTime: GameTime_t
 m_bIsHauledBack: bool
 m_nRenderMode: RenderMode_t
 m_LightGroup: CUtlStringToken
 m_CollisionGroup: uint8
 m_vecMins: Vector
 m_flThrowStrength: float32
 m_flNextSecondaryAttackTickRatio: float32
 m_flCreateTime: GameTime_t
 m_nInteractsAs: uint64
 m_vDecalForwardAxis: Vector
 m_iEntityLevel: uint32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
 m_iRecoilIndex: int32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_iState: WeaponState_t
 m_nFireSequenceStartTimeChange: int32
 m_bPlayerFireEventIsPrimary: bool
 m_nNextThinkTick: GameTick_t
 m_flGlowTime: float32
 m_fadeMaxDist: float32
 m_iEntityQuality: int32

110 CInfoInstructorHintHostageRescueZone
 m_flSimulationTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveCollide: MoveCollide_t
 m_flCreateTime: GameTime_t
 m_iTeamNum: uint8
 m_fEffects: uint32
 m_bSimulatedEveryTick: bool
 m_flAnimTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 CBodyComponent: CBodyComponent
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
 m_MoveType: MoveType_t
 m_nSubclassID: CUtlStringToken
 m_ubInterpolationFrame: uint8
 m_flElasticity: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t

204 CWeaponElite
 m_bSimulatedEveryTick: bool
 m_clrRender: Color
 m_nHierarchyId: uint16
 m_vLookTargetPosition: Vector
 m_iRecoilIndex: int32
 m_bBurstMode: bool
 m_nViewModelIndex: uint32
 m_bEligibleForScreenHighlight: bool
 m_fAccuracyPenalty: float32
 m_MoveCollide: MoveCollide_t
 m_LightGroup: CUtlStringToken
 m_nGlowRange: int32
 m_flGlowStartTime: float32
 m_nFallbackStatTrak: int32
 m_weaponMode: CSWeaponMode
 m_ubInterpolationFrame: uint8
 m_nInteractsAs: uint64
 m_vecMins: Vector
 m_vecSpecifiedSurroundingMins: Vector
 m_fadeMinDist: float32
 m_OriginalOwnerXuidHigh: uint32
 m_bSilencerOn: bool
 m_bIsHauledBack: bool
 m_nDropTick: GameTick_t
 m_nEnablePhysics: uint8
 m_bReloadVisuallyComplete: bool
 m_fLastShotTime: GameTime_t
 m_iBurstShotsRemaining: int32
 m_iClip2: int32
 m_bRenderToCubemaps: bool
 m_nSolidType: SolidType_t
 m_CollisionGroup: uint8
 m_vDecalPosition: Vector
 m_nForceBone: int32
 m_bPlayerFireEventIsPrimary: bool
 m_nAddDecal: int32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
 m_flNextSecondaryAttackTickRatio: float32
 m_glowColorOverride: Color
 m_iNumEmptyAttacks: int32
 m_nNextSecondaryAttackTick: GameTick_t
 m_pReserveAmmo: int32[2]
 m_MoveType: MoveType_t
 m_nInteractsWith: uint64
 m_hOuter: CHandle< CBaseEntity >
 m_nFallbackPaintKit: int32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nCollisionGroup: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_nObjectCulling: uint8
 m_bShouldAnimateDuringGameplayPause: bool
 m_nFireSequenceStartTimeChange: int32
 m_nSubclassID: CUtlStringToken
 m_flElasticity: float32
 m_nRenderFX: RenderFx_t
 m_nOwnerId: uint32
 m_vCapsuleCenter1: Vector
 m_iInventoryPosition: uint32
 m_flFallbackWear: float32
 m_flSimulationTime: float32
 m_vecForce: Vector
 m_iEntityLevel: uint32
 m_iState: WeaponState_t
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_iReapplyProvisionParity: int32
 m_szCustomName: char[161]
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_flAnimTime: float32
 m_nRenderMode: RenderMode_t
 m_usSolidFlags: uint8
 m_triggerBloat: uint8
 m_bFlashing: bool
 m_bNeedsBoltAction: bool
 m_bAnimatedEveryTick: bool
 m_iGlowType: int32
 m_fadeMaxDist: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bInReload: bool
 m_bvDisabledHitGroups: uint32[1]
 m_flCreateTime: GameTime_t
 m_ProviderType: attributeprovidertypes_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_iItemIDLow: uint32
 CRenderComponent: CRenderComponent
 m_flNextPrimaryAttackTickRatio: float32
 m_zoomLevel: int32
 m_iClip1: int32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_OriginalOwnerXuidLow: uint32
 m_bClientSideRagdoll: bool
 m_vecMaxs: Vector
 m_flGlowBackfaceMult: float32
 m_vDecalForwardAxis: Vector
 m_flGlowTime: float32
 m_flFireSequenceStartTime: float32
 m_flDroppedAtTime: GameTime_t
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_iIronSightMode: int32
 m_flNavIgnoreUntilTime: GameTime_t
 m_nCollisionFunctionMask: uint8
 m_vCapsuleCenter2: Vector
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_bClientRagdoll: bool
 m_iAccountID: uint32
 m_iOriginalTeamNumber: int32
 m_iTeamNum: uint8
 m_bInitialized: bool
 CBodyComponent: CBodyComponent
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_nNewSequenceParity: int32
  m_bClientSideAnimation: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_hParent: CGameSceneNodeHandle
  m_nRandomSeedOffset: int32
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_angRotation: QAngle
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_nBoolVariablesCount: int32
  m_cellZ: uint16
 m_nInteractsExclude: uint64
 m_flFadeScale: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_nGlowRangeMin: int32
 m_nNextThinkTick: GameTick_t
 m_flShadowStrength: float32
 m_flDecalHealBloodRate: float32
 m_iItemIDHigh: uint32
 m_flPostponeFireReadyTime: GameTime_t
 m_iGlowTeam: int32
 m_bAnimGraphUpdateEnabled: bool
 m_nFallbackSeed: int32
 m_flRecoilIndex: float32
 m_nEntityId: uint32
 m_iItemDefinitionIndex: uint16
 m_iEntityQuality: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flDecalHealHeightRate: float32
 m_nNextPrimaryAttackTick: GameTick_t
 m_flCapsuleRadius: float32

149 CPointCamera
 m_flCreateTime: GameTime_t
 m_bAnimatedEveryTick: bool
 m_FogColor: Color
 m_fBrightness: float32
 m_flZNear: float32
 m_nSubclassID: CUtlStringToken
 m_MoveType: MoveType_t
 m_flFogStart: float32
 m_bActive: bool
 m_flDofTiltToGround: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flElasticity: float32
 m_Resolution: float32
 m_flSimulationTime: float32
 m_fEffects: uint32
 m_bSimulatedEveryTick: bool
 m_bUseScreenAspectRatio: bool
 m_flAspectRatio: float32
 m_bNoSky: bool
 m_bDofEnabled: bool
 m_flDofNearBlurry: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flDofNearCrisp: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bFogEnable: bool
 m_flFogMaxDensity: float32
 m_flZFar: float32
 m_MoveCollide: MoveCollide_t
 CBodyComponent: CBodyComponent
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
 m_FOV: float32
 m_flAnimTime: float32
 m_iTeamNum: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_flFogEnd: float32
 m_flDofFarCrisp: float32
 m_flDofFarBlurry: float32
 m_ubInterpolationFrame: uint8

153 CPointCommentaryNode
 m_nGlowRange: int32
 m_flFadeScale: float32
 m_MoveCollide: MoveCollide_t
 m_flCreateTime: GameTime_t
 m_bClientSideRagdoll: bool
 m_ubInterpolationFrame: uint8
 m_iTeamNum: uint8
 m_triggerBloat: uint8
 m_flDecalHealBloodRate: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_bActive: bool
 m_iszTitle: CUtlSymbolLarge
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nCollisionFunctionMask: uint8
 m_vCapsuleCenter1: Vector
 m_iNodeNumberMax: int32
 m_nEnablePhysics: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vCapsuleCenter2: Vector
 CRenderComponent: CRenderComponent
 CBodyComponent: CBodyComponent
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nOwnerOnlyBoolVariablesCount: int32
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_nBoolVariablesCount: int32
  m_nRandomSeedOffset: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
 m_nRenderMode: RenderMode_t
 m_usSolidFlags: uint8
 m_nRenderFX: RenderFx_t
 m_LightGroup: CUtlStringToken
 m_bAnimGraphUpdateEnabled: bool
 m_nForceBone: int32
 m_bListenedTo: bool
 m_nOwnerId: uint32
 m_vecSpecifiedSurroundingMins: Vector
 m_nObjectCulling: uint8
 m_nAddDecal: int32
 m_flStartTime: GameTime_t
 m_bvDisabledHitGroups: uint32[1]
 m_nSubclassID: CUtlStringToken
 m_clrRender: Color
 m_glowColorOverride: Color
 m_flStartTimeInCommentary: float32
 m_bRenderToCubemaps: bool
 m_nInteractsAs: uint64
 m_vecMaxs: Vector
 m_flCapsuleRadius: float32
 m_iGlowTeam: int32
 m_flGlowTime: float32
 m_flDecalHealHeightRate: float32
 m_bClientRagdoll: bool
 m_hViewPosition: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_nInteractsWith: uint64
 m_bEligibleForScreenHighlight: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_fadeMinDist: float32
 m_iszSpeakers: CUtlSymbolLarge
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flElasticity: float32
 m_nInteractsExclude: uint64
 m_flShadowStrength: float32
 m_iNodeNumber: int32
 m_nHierarchyId: uint16
 m_CollisionGroup: uint8
 m_bFlashing: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_iszCommentaryFile: CUtlSymbolLarge
 m_nEntityId: uint32
 m_iGlowType: int32
 m_nGlowRangeMin: int32
 m_fadeMaxDist: float32
 m_vDecalForwardAxis: Vector
 m_bShouldAnimateDuringGameplayPause: bool
 m_vecForce: Vector
 m_flAnimTime: float32
 m_MoveType: MoveType_t
 m_bSimulatedEveryTick: bool
 m_nCollisionGroup: uint8
 m_flGlowBackfaceMult: float32
 m_vDecalPosition: Vector
 m_nSurroundType: SurroundingBoundsType_t
 m_flGlowStartTime: float32
 m_flSimulationTime: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vecMins: Vector
 m_nSolidType: SolidType_t
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >

195 CTripWireFireProjectile
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecMaxs: Vector
 m_flFadeScale: float32
 m_vLookTargetPosition: Vector
 m_flGlowBackfaceMult: float32
 m_fadeMaxDist: float32
 m_vecForce: Vector
 m_ubInterpolationFrame: uint8
 m_clrRender: Color
 m_vecMins: Vector
 m_vCapsuleCenter1: Vector
 m_flCapsuleRadius: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nCollisionFunctionMask: uint8
 m_triggerBloat: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_glowColorOverride: Color
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_bClientRagdoll: bool
 m_vecTripWireEndPositions: Vector[10]
 CRenderComponent: CRenderComponent
 CBodyComponent: CBodyComponent
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_nRandomSeedOffset: int32
  m_vecX: CNetworkedQuantizedFloat
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_cellZ: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_bUseParentRenderBounds: bool
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_hierarchyAttachName: CUtlStringToken
  m_materialGroup: CUtlStringToken
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flLastTeleportTime: float32
  m_angRotation: QAngle
  m_nIdealMotionType: int8
  m_bClientSideAnimation: bool
  m_nOutsideWorld: uint16
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_cellX: uint16
  m_flWeight: CNetworkedQuantizedFloat
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nBoolVariablesCount: int32
  m_name: CUtlStringToken
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_vecZ: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_bClientClothCreationSuppressed: bool
  m_cellY: uint16
  m_hParent: CGameSceneNodeHandle
  m_flScale: float32
 m_nSubclassID: CUtlStringToken
 m_DmgRadius: float32
 m_vecX: CNetworkedQuantizedFloat
 m_fFlags: uint32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nGlowRange: int32
 m_vecY: CNetworkedQuantizedFloat
 m_bShouldAnimateDuringGameplayPause: bool
 m_bTripWireEndPositionsUsed: bool[10]
 m_bvDisabledHitGroups: uint32[1]
 m_nRenderMode: RenderMode_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_iGlowTeam: int32
 m_usSolidFlags: uint8
 m_flGlowStartTime: float32
 m_fadeMinDist: float32
 m_bAnimatedEveryTick: bool
 m_nRenderFX: RenderFx_t
 m_nEntityId: uint32
 m_nHierarchyId: uint16
 m_nCollisionGroup: uint8
 m_flShadowStrength: float32
 m_vDecalPosition: Vector
 m_flDamage: float32
 m_nGlowRangeMin: int32
 m_bIsLive: bool
 m_flAttachTime: GameTime_t
 m_bClientSideRagdoll: bool
 m_LightGroup: CUtlStringToken
 m_nInteractsWith: uint64
 m_nInteractsExclude: uint64
 m_iGlowType: int32
 m_bInitiallyPopulateInterpHistory: bool
 m_nForceBone: int32
 m_flElasticity: float32
 m_bSimulatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_CollisionGroup: uint8
 m_nEnablePhysics: uint8
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bRenderToCubemaps: bool
 m_nOwnerId: uint32
 m_vCapsuleCenter2: Vector
 m_vDecalForwardAxis: Vector
 m_flDecalHealHeightRate: float32
 m_fEffects: uint32
 m_nSolidType: SolidType_t
 m_bFlashing: bool
 m_flGlowTime: float32
 m_flDecalHealBloodRate: float32
 m_MoveCollide: MoveCollide_t
 m_vecSpecifiedSurroundingMins: Vector
 m_hThrower: CHandle< CBaseEntity >
 m_vecZ: CNetworkedQuantizedFloat
 m_flSimulationTime: float32
 m_MoveType: MoveType_t
 m_flCreateTime: GameTime_t
 m_bEligibleForScreenHighlight: bool
 m_nAddDecal: int32
 m_iTeamNum: uint8
 m_nInteractsAs: uint64
 m_nObjectCulling: uint8
 m_bAnimGraphUpdateEnabled: bool
 m_flDetonateTime: GameTime_t

38 CCSGO_TeamIntroTerroristPosition
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_iEntityLevel: uint32
 m_flSimulationTime: float32
 m_nSubclassID: CUtlStringToken
 m_iAccountID: uint32
 m_iInventoryPosition: uint32
 m_szCustomName: char[161]
 m_flAnimTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flElasticity: float32
 m_nRandom: int32
 m_xuid: uint64
 CBodyComponent: CBodyComponent
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
 m_MoveType: MoveType_t
 m_iTeamNum: uint8
 m_nOrdinal: int32
 m_iEntityQuality: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_ubInterpolationFrame: uint8
 m_bSimulatedEveryTick: bool
 m_sWeaponName: CUtlString
 m_iItemIDHigh: uint32
 m_iItemIDLow: uint32
 m_bInitialized: bool
 m_MoveCollide: MoveCollide_t
 m_flCreateTime: GameTime_t
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_nVariant: int32
 m_iItemDefinitionIndex: uint16

48 CCSObserverPawn
 exponent: float32
 m_flElasticity: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nDeathCamMusic: int32
 m_unFreezetimeEndEquipmentValue: uint16
 m_thirdPersonHeading: QAngle
 m_passiveItems: bool[4]
 m_isCurrentGunGameTeamLeader: bool
 m_aimPunchAngle: QAngle
 m_bClientSideRagdoll: bool
 m_ArmorValue: int32
 m_iGlowType: int32
 m_flFieldOfView: float32
 m_flFlashMaxAlpha: float32
 lerptime: GameTime_t
 m_nRenderFX: RenderFx_t
 m_MoveCollide: MoveCollide_t
 m_bRenderToCubemaps: bool
 m_flShadowStrength: float32
 m_aimPunchTickFraction: float32
 m_bSpottedByMask: uint32[2]
 m_fImmuneToGunGameDamageTime: GameTime_t
 m_iProgressBarDuration: int32
 m_bEligibleForScreenHighlight: bool
 m_fadeMaxDist: float32
 m_nObjectCulling: uint8
 m_pObserverServices: CPlayer_ObserverServices*
  m_iObserverMode: uint8
  m_hObserverTarget: CHandle< CBaseEntity >
 m_iHealth: int32
 m_flCreateTime: GameTime_t
 m_usSolidFlags: uint8
 m_flGlowStartTime: float32
 m_nHeavyAssaultSuitCooldownRemaining: int32
 m_iDirection: int32
 m_nSolidType: SolidType_t
 m_vDecalForwardAxis: Vector
 scale: int16
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flHealthShotBoostExpirationTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nGlowRangeMin: int32
 m_pWaterServices: CPlayer_WaterServices*
 m_iGunGameProgressiveWeaponIndex: int32
 m_vecSpecifiedSurroundingMins: Vector
 m_flGlowTime: float32
 m_flWaterLevel: float32
 HDRColorScale: float32
 m_LightGroup: CUtlStringToken
 m_vCapsuleCenter1: Vector
 m_vLookTargetPosition: Vector
 m_vecSpawnRappellingRopeOrigin: Vector
 m_bGunGameImmunity: bool
 m_flGuardianTooFarDistFrac: float32
 m_bvDisabledHitGroups: uint32[1]
 m_iHideHUD: uint32
 m_triggerBloat: uint8
 m_nEnablePhysics: uint8
 m_bClientRagdoll: bool
 m_pCameraServices: CPlayer_CameraServices*
  m_nCsViewPunchAngleTick: GameTick_t
  m_hZoomOwner: CHandle< CBaseEntity >
  localBits: uint8
  m_PostProcessingVolumes: CNetworkUtlVectorBase< CHandle< CPostProcessingVolume > >
  m_iFOVStart: uint32
  m_flFOVTime: GameTime_t
  soundscapeIndex: int32
  soundEventHash: uint32
  m_hViewEntity: CHandle< CBaseEntity >
  m_iFOV: uint32
  localSound: Vector[8]
  soundscapeEntityListIndex: int32
  m_flFOVRate: float32
  m_hCtrl: CHandle< CFogController >
  m_vecCsViewPunchAngle: QAngle
  m_flCsViewPunchAngleTickRatio: float32
  m_hColorCorrectionCtrl: CHandle< CColorCorrection >
  m_hTonemapController: CHandle< CTonemapController2 >
 flClip3DSkyBoxNearToWorldFarOffset: float32
 m_fadeMinDist: float32
 m_unTotalRoundDamageDealt: uint32
 m_flDecalHealBloodRate: float32
 m_bStrafing: bool
 m_aimPunchAngleVel: QAngle
 m_flSlopeDropHeight: float32
 blend: bool
 m_nSurroundType: SurroundingBoundsType_t
 m_hOriginalController: CHandle< CCSPlayerController >
 m_vecZ: CNetworkedQuantizedFloat
 start: float32
 m_fEffects: uint32
 CRenderComponent: CRenderComponent
 scattering: float32
 m_iAddonBits: int32
 m_nWorldGroupID: WorldGroupId_t
 m_nSubclassID: CUtlStringToken
 m_hMyWearables: CNetworkUtlVectorBase< CHandle< CEconWearable > >
 m_flStamina: float32
 m_flFlashDuration: float32
 m_clrRender: Color
 m_vCapsuleCenter2: Vector
 m_iGlowTeam: int32
 m_nInteractsWith: uint64
 m_vecX: CNetworkedQuantizedFloat
 m_CollisionGroup: uint8
 m_iPrimaryAddon: int32
 m_nOwnerId: uint32
 m_bNightVisionOn: bool
 colorPrimary: Color
 m_iMaxHealth: int32
 m_nSurvivalTeam: int32
 m_hSurvivalAssassinationTarget: CHandle< CCSPlayerPawnBase >
 m_vecY: CNetworkedQuantizedFloat
 m_flCapsuleRadius: float32
 m_nWhichBombZone: int32
 m_nHitBodyPart: int32
 m_bHideTargetID: bool
 m_bSimulatedEveryTick: bool
 m_nHierarchyId: uint16
 m_flFadeScale: float32
 dirPrimary: Vector
 m_pUseServices: CPlayer_UseServices*
 m_hController: CHandle< CBasePlayerController >
 m_bIsGrabbingHostage: bool
 m_unCurrentEquipmentValue: uint16
 m_flVelocityModifier: float32
 m_nEntityId: uint32
 m_bCanMoveDuringFreezePeriod: bool
 bClip3DSkyBoxNearToWorldFar: bool
 m_ServerViewAngleChanges: CUtlVectorEmbeddedNetworkVar< ViewAngleServerChange_t >
  nType: FixAngleSet_t
  qAngle: QAngle
  nIndex: uint32
 m_hGroundEntity: CHandle< CBaseEntity >
 m_flNavIgnoreUntilTime: GameTime_t
 m_flGlowBackfaceMult: float32
 m_vecForce: Vector
 m_flGravityScale: float32
 maxdensity: float32
 enable: bool
 m_bIsDefusing: bool
 m_bWaitForNoAttack: bool
 m_vecPlayerPatchEconIndices: uint32[5]
 m_szLastPlaceName: char[18]
 m_bNoReflectionFog: bool
 m_lifeState: uint8
 m_vecMins: Vector
 m_bInBombZone: bool
 skyboxFogFactorLerpTo: float32
 m_bHasNightVision: bool
 m_iShotsFired: int32
 skyboxFogFactor: float32
 m_nGlowRange: int32
 m_blinktoggle: bool
 m_flDeathTime: GameTime_t
 m_nSurvivalTeamNumber: int32
 end: float32
 m_ubInterpolationFrame: uint8
 m_iTeamNum: uint8
 m_vecMaxs: Vector
 colorSecondaryLerpTo: Color
 m_fFlags: uint32
 m_flLowerBodyYawTarget: float32
 m_bMadeFinalGunGameProgressiveKill: bool
 m_isCurrentGunGameLeader: bool
 m_nCollisionGroup: uint8
 m_bHasFemaleVoice: bool
 m_flSlopeDropOffset: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bKilledByTaser: bool
 m_vecBaseVelocity: Vector
 m_bShouldAnimateDuringGameplayPause: bool
 m_bResumeZoom: bool
 m_iSecondaryAddon: int32
 blendtobackground: float32
 m_bFlashing: bool
 m_pViewModelServices: CCSPlayer_ViewModelServices*
  m_hViewModel: CHandle< CBaseViewModel >[3]
 m_bHud_MiniScoreHidden: bool
 m_flTimeOfLastInjury: float32
 m_nRelativeDirectionOfLastInjury: RelativeDamagedDirection_t
 m_bKilledByHeadshot: bool
 m_bIsWalking: bool
 m_bInNoDefuseArea: bool
 colorPrimaryLerpTo: Color
 m_pActionTrackingServices: CCSPlayer_ActionTrackingServices*
  m_weaponPurchases: CUtlVectorEmbeddedNetworkVar< WeaponPurchaseCount_t >
   m_nItemDefIndex: uint16
   m_nCount: uint16
  m_bIsRescuing: bool
 m_bSpotted: bool
 maxdensityLerpTo: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bHud_RadarHidden: bool
 colorSecondary: Color
 m_nInteractsAs: uint64
 m_nCollisionFunctionMask: uint8
 m_bInitiallyPopulateInterpHistory: bool
 m_aimPunchTickBase: int32
 m_nLastConcurrentKilled: int32
 endLerpTo: float32
 m_vHeadConstraintOffset: Vector
 m_nInteractsExclude: uint64
 m_iNumGunGameTRKillPoints: int32
 m_fMolotovDamageTime: float32
 m_vDecalPosition: Vector
 m_nLastKillerIndex: CEntityIndex
 m_angEyeAngles: QAngle
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_hOwner: CHandle< CBaseEntity >
  m_Transforms: CNetworkUtlVectorBase< CTransform >
 m_flHitHeading: float32
 CBodyComponent: CBodyComponent
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_bUseParentRenderBounds: bool
  m_nResetEventsParity: int32
  m_flPrevCycle: float32
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_hParent: CGameSceneNodeHandle
  m_nAnimLoopMode: AnimLoopMode_t
  m_vecY: CNetworkedQuantizedFloat
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_nRandomSeedOffset: int32
  m_nOwnerOnlyBoolVariablesCount: int32
  m_bClientClothCreationSuppressed: bool
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_flWeight: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nOutsideWorld: uint16
  m_hierarchyAttachName: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_nNewSequenceParity: int32
  m_flCycle: float32
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_flScale: float32
  m_nIdealMotionType: int8
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nBoolVariablesCount: int32
  m_cellX: uint16
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_cellZ: uint16
  m_angRotation: QAngle
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_flLastTeleportTime: float32
  m_vecX: CNetworkedQuantizedFloat
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_name: CUtlStringToken
 m_glowColorOverride: Color
 m_iPlayerState: CSPlayerState
 m_flEmitSoundTime: GameTime_t
 m_vecSpecifiedSurroundingMaxs: Vector
 startLerpTo: float32
 locallightscale: float32
 m_flTimeScale: float32
 m_bAnimatedEveryTick: bool
 m_nAddDecal: int32
 m_bHasMovedSinceSpawn: bool
 m_unRoundStartEquipmentValue: uint16
 m_bInBuyZone: bool
 origin: Vector
 m_pWeaponServices: CPlayer_WeaponServices*
  m_iAmmo: uint16[32]
  m_bIsLookingAtWeapon: bool
  m_bIsHoldingLookAtWeapon: bool
  m_hLastWeapon: CHandle< CBasePlayerWeapon >
  m_flNextAttack: GameTime_t
  m_hMyWeapons: CNetworkUtlVectorBase< CHandle< CBasePlayerWeapon > >
  m_hActiveWeapon: CHandle< CBasePlayerWeapon >
 m_pMovementServices: CPlayer_MovementServices*
  m_flDuckAmount: float32
  m_flDuckSpeed: float32
  m_flOffsetTickCompleteTime: float32
  m_bInCrouch: bool
  m_flMaxspeed: float32
  m_nToggleButtonDownMask: uint64
  m_bDuckOverride: bool
  m_nButtonDownMaskPrev: uint64
  m_bDucking: bool
  m_bInDuckJump: bool
  m_flLastDuckTime: float32
  m_arrForceSubtickMoveWhen: float32[4]
  m_flJumpVel: float32
  m_vecLadderNormal: Vector
  m_fStashGrenadeParameterWhen: GameTime_t
  m_bOldJumpPressed: bool
  m_flJumpUntil: float32
  m_flOffsetTickStashedSpeed: float32
  m_flCrouchTransitionStartTime: GameTime_t
  m_nLadderSurfacePropIndex: int32
  m_flFallVelocity: float32
  m_nDuckJumpTimeMsecs: uint32
  m_bDesiresDuck: bool
  m_flMaxFallVelocity: float32
  m_nCrouchState: uint32
  m_bDucked: bool
  m_nJumpTimeMsecs: uint32
  m_nDuckTimeMsecs: uint32
 m_flDetectedByEnemySensorTime: GameTime_t
 m_nNextThinkTick: GameTick_t
 m_flDecalHealHeightRate: float32
 m_flFriction: float32
 m_iBlockingUseActionInProgress: CSPlayerBlockingUseAction_t
 m_flProgressBarStartTime: float32
 farz: float32
 m_bIsScoped: bool
 m_iMoveState: int32
 m_bInHostageRescueZone: bool
 duration: float32
 m_flSimulationTime: float32
 m_MoveType: MoveType_t
 m_bAnimGraphUpdateEnabled: bool
 m_nForceBone: int32
 m_nRenderMode: RenderMode_t
 m_iNumGunGameKillsWithCurrentWeapon: int32
 m_bIsSpawnRappelling: bool

93 CFuncElectrifiedVolume
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_LightGroup: CUtlStringToken
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_MoveCollide: MoveCollide_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_iGlowType: int32
 m_usSolidFlags: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_flGlowBackfaceMult: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveType: MoveType_t
 m_nHierarchyId: uint16
 m_vecMaxs: Vector
 m_flGlowStartTime: float32
 m_flSimulationTime: float32
 m_flElasticity: float32
 m_clrRender: Color
 m_CollisionGroup: uint8
 m_flDecalHealBloodRate: float32
 m_bvDisabledHitGroups: uint32[1]
 m_flNavIgnoreUntilTime: GameTime_t
 m_bRenderToCubemaps: bool
 m_nSurroundType: SurroundingBoundsType_t
 m_flGlowTime: float32
 m_ubInterpolationFrame: uint8
 m_nEntityId: uint32
 m_glowColorOverride: Color
 m_vCapsuleCenter2: Vector
 m_nGlowRangeMin: int32
 m_bFlashing: bool
 m_flFadeScale: float32
 m_flDecalHealHeightRate: float32
 CBodyComponent: CBodyComponent
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
 m_iTeamNum: uint8
 m_nSolidType: SolidType_t
 m_nEnablePhysics: uint8
 m_bSimulatedEveryTick: bool
 m_nRenderFX: RenderFx_t
 m_nOwnerId: uint32
 m_vecMins: Vector
 m_EffectName: CUtlSymbolLarge
 m_bAnimatedEveryTick: bool
 m_nInteractsAs: uint64
 m_nInteractsExclude: uint64
 m_nCollisionFunctionMask: uint8
 m_nAddDecal: int32
 m_nRenderMode: RenderMode_t
 m_vCapsuleCenter1: Vector
 m_flCapsuleRadius: float32
 m_bEligibleForScreenHighlight: bool
 m_triggerBloat: uint8
 m_flAnimTime: float32
 m_fEffects: uint32
 m_nCollisionGroup: uint8
 m_nSubclassID: CUtlStringToken
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flShadowStrength: float32
 m_vDecalForwardAxis: Vector
 CRenderComponent: CRenderComponent
 m_nInteractsWith: uint64
 m_iGlowTeam: int32
 m_fadeMaxDist: float32
 m_nObjectCulling: uint8
 m_flCreateTime: GameTime_t
 m_nGlowRange: int32
 m_fadeMinDist: float32
 m_vDecalPosition: Vector

128 CMelee
 m_flFireSequenceStartTime: float32
 m_bBurstMode: bool
 m_iClip1: int32
 m_bClientSideRagdoll: bool
 m_fEffects: uint32
 m_flElasticity: float32
 m_bRenderToCubemaps: bool
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_nNextSecondaryAttackTick: GameTick_t
 m_bReloadVisuallyComplete: bool
 m_nEnablePhysics: uint8
 m_vCapsuleCenter2: Vector
 m_fadeMinDist: float32
 m_vLookTargetPosition: Vector
 m_iEntityLevel: uint32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsWith: uint64
 m_vecMins: Vector
 m_bShouldAnimateDuringGameplayPause: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_nObjectCulling: uint8
 m_nFallbackPaintKit: int32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_pReserveAmmo: int32[2]
 m_nGlowRangeMin: int32
 m_nFallbackSeed: int32
 m_flGlowStartTime: float32
 m_nViewModelIndex: uint32
 m_vecMaxs: Vector
 m_flGlowTime: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flFallbackWear: float32
 m_bSimulatedEveryTick: bool
 m_usSolidFlags: uint8
 m_flCapsuleRadius: float32
 m_iNumEmptyAttacks: int32
 m_nNextThinkTick: GameTick_t
 m_vecSpecifiedSurroundingMins: Vector
 m_vCapsuleCenter1: Vector
 m_nGlowRange: int32
 m_nForceBone: int32
 m_bIsHauledBack: bool
 m_nFireSequenceStartTimeChange: int32
 m_iOriginalTeamNumber: int32
 m_flNextSecondaryAttackTickRatio: float32
 m_nAddDecal: int32
 m_nDropTick: GameTick_t
 m_bSilencerOn: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_nOwnerId: uint32
 m_iGlowTeam: int32
 m_ProviderType: attributeprovidertypes_t
 m_iItemIDLow: uint32
 m_szCustomName: char[161]
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_vDecalForwardAxis: Vector
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool
 m_bClientRagdoll: bool
 m_hOuter: CHandle< CBaseEntity >
 m_iIronSightMode: int32
 m_bInReload: bool
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_iTeamNum: uint8
 m_clrRender: Color
 m_nHierarchyId: uint16
 m_OriginalOwnerXuidHigh: uint32
 m_iState: WeaponState_t
 m_flPostponeFireReadyTime: GameTime_t
 CRenderComponent: CRenderComponent
 m_nInteractsExclude: uint64
 m_nSolidType: SolidType_t
 m_vDecalPosition: Vector
 m_flDecalHealBloodRate: float32
 m_fAccuracyPenalty: float32
 m_iClip2: int32
 m_nEntityId: uint32
 m_iGlowType: int32
 m_glowColorOverride: Color
 m_flGlowBackfaceMult: float32
 m_iAccountID: uint32
 m_nInteractsAs: uint64
 m_iItemDefinitionIndex: uint16
 m_iInventoryPosition: uint32
 m_OriginalOwnerXuidLow: uint32
 m_fLastShotTime: GameTime_t
 m_nNextPrimaryAttackTick: GameTick_t
 m_iRecoilIndex: int32
 m_flRecoilIndex: float32
 m_flAnimTime: float32
 m_nSurroundType: SurroundingBoundsType_t
 m_fadeMaxDist: float32
 m_bAnimGraphUpdateEnabled: bool
 m_vecForce: Vector
 m_iReapplyProvisionParity: int32
 m_flDroppedAtTime: GameTime_t
 m_MoveType: MoveType_t
 m_flCreateTime: GameTime_t
 m_nCollisionGroup: uint8
 m_triggerBloat: uint8
 m_flFadeScale: float32
 m_weaponMode: CSWeaponMode
 CBodyComponent: CBodyComponent
  m_angRotation: QAngle
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
 m_LightGroup: CUtlStringToken
 m_bInitiallyPopulateInterpHistory: bool
 m_flSimulationTime: float32
 m_iEntityQuality: int32
 m_MoveCollide: MoveCollide_t
 m_ubInterpolationFrame: uint8
 m_flShadowStrength: float32
 m_bFlashing: bool
 m_bvDisabledHitGroups: uint32[1]
 m_flDecalHealHeightRate: float32
 m_nFallbackStatTrak: int32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flThrowAt: GameTime_t
 m_iItemIDHigh: uint32
 m_bPlayerFireEventIsPrimary: bool
 m_nSubclassID: CUtlStringToken
 m_nRenderMode: RenderMode_t
 m_nRenderFX: RenderFx_t
 m_nCollisionFunctionMask: uint8
 m_CollisionGroup: uint8
 m_bEligibleForScreenHighlight: bool
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bInitialized: bool
 m_flNextPrimaryAttackTickRatio: float32

59 CDecoyGrenade
 m_OriginalOwnerXuidLow: uint32
 m_nDropTick: GameTick_t
 m_flNextPrimaryAttackTickRatio: float32
 m_bAnimatedEveryTick: bool
 m_iEntityQuality: int32
 m_fAccuracyPenalty: float32
 m_fLastShotTime: GameTime_t
 m_bvDisabledHitGroups: uint32[1]
 m_flPostponeFireReadyTime: GameTime_t
 m_iTeamNum: uint8
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_flGlowBackfaceMult: float32
 m_flFadeScale: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_vDecalPosition: Vector
 m_nSubclassID: CUtlStringToken
 m_clrRender: Color
 m_glowColorOverride: Color
 m_flShadowStrength: float32
 m_nAddDecal: int32
 m_bReloadVisuallyComplete: bool
 m_vecSpecifiedSurroundingMins: Vector
 m_nGlowRangeMin: int32
 m_bInitiallyPopulateInterpHistory: bool
 m_iItemDefinitionIndex: uint16
 m_szCustomName: char[161]
 m_nCollisionFunctionMask: uint8
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nInteractsWith: uint64
 m_nEntityId: uint32
 m_LightGroup: CUtlStringToken
 m_flCapsuleRadius: float32
 m_iItemIDHigh: uint32
 m_bBurstMode: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_iGlowTeam: int32
 m_bAnimGraphUpdateEnabled: bool
 m_nFallbackSeed: int32
 m_flFireSequenceStartTime: float32
 m_bClientSideRagdoll: bool
 m_flGlowTime: float32
 m_bInReload: bool
 m_iNumEmptyAttacks: int32
 m_nViewModelIndex: uint32
 m_flSimulationTime: float32
 m_fEffects: uint32
 m_nOwnerId: uint32
 m_vDecalForwardAxis: Vector
 m_iReapplyProvisionParity: int32
 m_nInteractsExclude: uint64
 m_pReserveAmmo: int32[2]
 m_hEffectEntity: CHandle< CBaseEntity >
 m_triggerBloat: uint8
 m_CollisionGroup: uint8
 m_hOuter: CHandle< CBaseEntity >
 m_flDroppedAtTime: GameTime_t
 m_bShouldAnimateDuringGameplayPause: bool
 m_vLookTargetPosition: Vector
 m_eThrowStatus: EGrenadeThrowState
 m_ubInterpolationFrame: uint8
 m_flElasticity: float32
 m_iIronSightMode: int32
 m_iClip1: int32
 m_flFallbackWear: float32
 m_fThrowTime: GameTime_t
 m_fadeMaxDist: float32
 m_bJumpThrow: bool
 m_nNextSecondaryAttackTick: GameTick_t
 m_usSolidFlags: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_iItemIDLow: uint32
 m_bPinPulled: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_nInteractsAs: uint64
 m_vecMaxs: Vector
 m_fadeMinDist: float32
 m_bRedraw: bool
 m_nNextThinkTick: GameTick_t
 m_bRenderToCubemaps: bool
 m_vecForce: Vector
 m_iEntityLevel: uint32
 m_OriginalOwnerXuidHigh: uint32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_MoveType: MoveType_t
 m_nCollisionGroup: uint8
 m_flDecalHealBloodRate: float32
 m_iOriginalTeamNumber: int32
 m_MoveCollide: MoveCollide_t
 m_nEnablePhysics: uint8
 m_vCapsuleCenter1: Vector
 m_flThrowStrength: float32
 m_flNextSecondaryAttackTickRatio: float32
 m_nSolidType: SolidType_t
 m_bSilencerOn: bool
 CRenderComponent: CRenderComponent
 m_iClip2: int32
 m_bEligibleForScreenHighlight: bool
 m_ProviderType: attributeprovidertypes_t
 m_iInventoryPosition: uint32
 m_nFireSequenceStartTimeChange: int32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_vecMins: Vector
 m_nForceBone: int32
 m_iAccountID: uint32
 m_iRecoilIndex: int32
 m_flRecoilIndex: float32
 m_nGlowRange: int32
 m_bIsHeldByPlayer: bool
 m_iGlowType: int32
 m_flDecalHealHeightRate: float32
 m_iState: WeaponState_t
 m_fDropTime: GameTime_t
 m_nFallbackStatTrak: int32
 m_bPlayerFireEventIsPrimary: bool
 m_weaponMode: CSWeaponMode
 m_flCreateTime: GameTime_t
 m_bSimulatedEveryTick: bool
 m_nRenderFX: RenderFx_t
 m_nSurroundType: SurroundingBoundsType_t
 m_nObjectCulling: uint8
 m_bIsHauledBack: bool
 m_nFallbackPaintKit: int32
 m_flThrowStrengthApproach: float32
 m_nNextPrimaryAttackTick: GameTick_t
 CBodyComponent: CBodyComponent
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_hParent: CGameSceneNodeHandle
  m_nRandomSeedOffset: int32
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_nNewSequenceParity: int32
  m_bClientSideAnimation: bool
 m_nRenderMode: RenderMode_t
 m_nHierarchyId: uint16
 m_bClientRagdoll: bool
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_vCapsuleCenter2: Vector
 m_flGlowStartTime: float32
 m_bInitialized: bool
 m_flAnimTime: float32
 m_bFlashing: bool

89 CFogController
 scattering: float32
 HDRColorScale: float32
 colorPrimaryLerpTo: Color
 farz: float32
 skyboxFogFactorLerpTo: float32
 duration: float32
 colorPrimary: Color
 exponent: float32
 colorSecondaryLerpTo: Color
 skyboxFogFactor: float32
 startLerpTo: float32
 maxdensityLerpTo: float32
 dirPrimary: Vector
 maxdensity: float32
 enable: bool
 blend: bool
 m_bNoReflectionFog: bool
 blendtobackground: float32
 locallightscale: float32
 colorSecondary: Color
 start: float32
 end: float32
 endLerpTo: float32
 lerptime: GameTime_t

196 CVoteController
 m_flSimulationTime: float32
 m_nSubclassID: CUtlStringToken
 m_iTeamNum: uint8
 m_bAnimatedEveryTick: bool
 m_iOnlyTeamToVote: int32
 m_nPotentialVotes: int32
 CBodyComponent: CBodyComponent
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
 m_MoveType: MoveType_t
 m_flCreateTime: GameTime_t
 m_flElasticity: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_iActiveIssueIndex: int32
 m_bIsYesNoVote: bool
 m_flAnimTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_ubInterpolationFrame: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_MoveCollide: MoveCollide_t
 m_fEffects: uint32
 m_bSimulatedEveryTick: bool
 m_nVoteOptionCount: int32[5]

148 CPlayerVisibility
 m_MoveCollide: MoveCollide_t
 m_nSubclassID: CUtlStringToken
 m_flCreateTime: GameTime_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bSimulatedEveryTick: bool
 m_flFogMaxDensityMultiplier: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flElasticity: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_flVisibilityStrength: float32
 m_flFogDistanceMultiplier: float32
 m_bStartDisabled: bool
 m_flSimulationTime: float32
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_flAnimTime: float32
 CBodyComponent: CBodyComponent
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveType: MoveType_t
 m_ubInterpolationFrame: uint8
 m_iTeamNum: uint8
 m_flFadeTime: float32
 m_bIsEnabled: bool

182 CSoundOpvarSetPointEntity
 CBodyComponent: CBodyComponent
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
 m_nSubclassID: CUtlStringToken
 m_flCreateTime: GameTime_t
 m_bAnimatedEveryTick: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_bSimulatedEveryTick: bool
 m_iszStackName: CUtlSymbolLarge
 m_iszOperatorName: CUtlSymbolLarge
 m_flAnimTime: float32
 m_flSimulationTime: float32
 m_ubInterpolationFrame: uint8
 m_iTeamNum: uint8
 m_flElasticity: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_iszOpvarName: CUtlSymbolLarge
 m_bUseAutoCompare: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveCollide: MoveCollide_t
 m_MoveType: MoveType_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_iOpvarIndex: int32

28 CBumpMine
 m_fEffects: uint32
 m_usSolidFlags: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_pReserveAmmo: int32[2]
 m_nRenderMode: RenderMode_t
 m_triggerBloat: uint8
 m_flGlowStartTime: float32
 m_vDecalPosition: Vector
 m_iItemIDHigh: uint32
 m_fAccuracyPenalty: float32
 m_flAnimTime: float32
 m_nRenderFX: RenderFx_t
 m_nOwnerId: uint32
 m_vLookTargetPosition: Vector
 m_flSimulationTime: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_iReapplyProvisionParity: int32
 CRenderComponent: CRenderComponent
 m_bFlashing: bool
 m_bPlayerFireEventIsPrimary: bool
 m_iNumEmptyAttacks: int32
 m_MoveType: MoveType_t
 m_flCreateTime: GameTime_t
 m_nCollisionGroup: uint8
 m_iOriginalTeamNumber: int32
 m_iClip1: int32
 m_LightGroup: CUtlStringToken
 m_flGlowBackfaceMult: float32
 m_vecForce: Vector
 m_OriginalOwnerXuidHigh: uint32
 m_bvDisabledHitGroups: uint32[1]
 m_vecMins: Vector
 m_glowColorOverride: Color
 m_bAnimatedEveryTick: bool
 m_clrRender: Color
 m_flGlowTime: float32
 m_flDecalHealHeightRate: float32
 m_bAnimGraphUpdateEnabled: bool
 m_nCollisionFunctionMask: uint8
 m_nSolidType: SolidType_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_hOuter: CHandle< CBaseEntity >
 m_ProviderType: attributeprovidertypes_t
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_iTeamNum: uint8
 m_vDecalForwardAxis: Vector
 m_iClip2: int32
 m_MoveCollide: MoveCollide_t
 m_iState: WeaponState_t
 m_nDropTick: GameTick_t
 m_iEntityLevel: uint32
 m_OriginalOwnerXuidLow: uint32
 m_flFallbackWear: float32
 m_nViewModelIndex: uint32
 CBodyComponent: CBodyComponent
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_angRotation: QAngle
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
 m_nInteractsAs: uint64
 m_nAddDecal: int32
 m_iIronSightMode: int32
 m_flNextSecondaryAttackTickRatio: float32
 m_nInteractsWith: uint64
 m_nHierarchyId: uint16
 m_vecSpecifiedSurroundingMins: Vector
 m_vCapsuleCenter2: Vector
 m_flFadeScale: float32
 m_nObjectCulling: uint8
 m_iAccountID: uint32
 m_bBurstMode: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_ubInterpolationFrame: uint8
 m_fadeMinDist: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_nEntityId: uint32
 m_nSubclassID: CUtlStringToken
 m_nInteractsExclude: uint64
 m_flDecalHealBloodRate: float32
 m_flPostponeFireReadyTime: GameTime_t
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_vCapsuleCenter1: Vector
 m_nGlowRange: int32
 m_bSimulatedEveryTick: bool
 m_iEntityQuality: int32
 m_szCustomName: char[161]
 m_weaponMode: CSWeaponMode
 m_nNextPrimaryAttackTick: GameTick_t
 m_vecMaxs: Vector
 m_nGlowRangeMin: int32
 m_bSilencerOn: bool
 m_hEffectEntity: CHandle< CBaseEntity >
 m_iGlowTeam: int32
 m_flDroppedAtTime: GameTime_t
 m_flNextPrimaryAttackTickRatio: float32
 m_flElasticity: float32
 m_nFallbackPaintKit: int32
 m_nFireSequenceStartTimeChange: int32
 m_bClientRagdoll: bool
 m_nNextSecondaryAttackTick: GameTick_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_flShadowStrength: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_fadeMaxDist: float32
 m_nFallbackSeed: int32
 m_bClientSideRagdoll: bool
 m_bRenderToCubemaps: bool
 m_iGlowType: int32
 m_bShouldAnimateDuringGameplayPause: bool
 m_iInventoryPosition: uint32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_iItemDefinitionIndex: uint16
 m_bInReload: bool
 m_bIsHauledBack: bool
 m_fLastShotTime: GameTime_t
 m_nNextThinkTick: GameTick_t
 m_flCapsuleRadius: float32
 m_bEligibleForScreenHighlight: bool
 m_nForceBone: int32
 m_nFallbackStatTrak: int32
 m_iRecoilIndex: int32
 m_flRecoilIndex: float32
 m_nEnablePhysics: uint8
 m_bInitialized: bool
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_flFireSequenceStartTime: float32
 m_bReloadVisuallyComplete: bool
 m_CollisionGroup: uint8
 m_bInitiallyPopulateInterpHistory: bool
 m_iItemIDLow: uint32

58 CDEagle
 CRenderComponent: CRenderComponent
 m_nCollisionFunctionMask: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_iItemIDHigh: uint32
 m_iTeamNum: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_bInitiallyPopulateInterpHistory: bool
 m_nObjectCulling: uint8
 m_flDecalHealHeightRate: float32
 m_MoveType: MoveType_t
 m_fEffects: uint32
 m_nRenderFX: RenderFx_t
 m_nViewModelIndex: uint32
 m_nHierarchyId: uint16
 m_vecMaxs: Vector
 m_fAccuracyPenalty: float32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flGlowStartTime: float32
 m_flRecoilIndex: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nCollisionGroup: uint8
 m_nEnablePhysics: uint8
 m_flNextSecondaryAttackTickRatio: float32
 m_vecMins: Vector
 m_nSolidType: SolidType_t
 m_iRecoilIndex: int32
 m_nInteractsWith: uint64
 m_bAnimGraphUpdateEnabled: bool
 m_iAccountID: uint32
 m_nForceBone: int32
 m_OriginalOwnerXuidHigh: uint32
 m_flNextPrimaryAttackTickRatio: float32
 m_nNextSecondaryAttackTick: GameTick_t
 m_vCapsuleCenter1: Vector
 m_vecForce: Vector
 m_szCustomName: char[161]
 m_flFadeScale: float32
 m_nEntityId: uint32
 m_triggerBloat: uint8
 m_nGlowRangeMin: int32
 m_iReapplyProvisionParity: int32
 m_flFireSequenceStartTime: float32
 m_iIronSightMode: int32
 m_ubInterpolationFrame: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flDecalHealBloodRate: float32
 m_flDroppedAtTime: GameTime_t
 m_iNumEmptyAttacks: int32
 m_bvDisabledHitGroups: uint32[1]
 m_nInteractsAs: uint64
 m_vLookTargetPosition: Vector
 m_ProviderType: attributeprovidertypes_t
 m_iOriginalTeamNumber: int32
 m_LightGroup: CUtlStringToken
 m_flGlowBackfaceMult: float32
 m_bClientRagdoll: bool
 m_nSubclassID: CUtlStringToken
 m_bFlashing: bool
 m_iState: WeaponState_t
 m_flShadowStrength: float32
 m_iEntityLevel: uint32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_iBurstShotsRemaining: int32
 m_flElasticity: float32
 m_bRenderToCubemaps: bool
 m_nOwnerId: uint32
 m_iInventoryPosition: uint32
 m_nFallbackSeed: int32
 CBodyComponent: CBodyComponent
  m_angRotation: QAngle
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
 m_bClientSideRagdoll: bool
 m_flGlowTime: float32
 m_MoveCollide: MoveCollide_t
 m_flCreateTime: GameTime_t
 m_zoomLevel: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nFallbackPaintKit: int32
 m_nDropTick: GameTick_t
 m_nRenderMode: RenderMode_t
 m_bNeedsBoltAction: bool
 m_iClip2: int32
 m_flAnimTime: float32
 m_vDecalPosition: Vector
 m_OriginalOwnerXuidLow: uint32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_bSimulatedEveryTick: bool
 m_nGlowRange: int32
 m_nAddDecal: int32
 m_bInReload: bool
 m_iClip1: int32
 m_iGlowType: int32
 m_fadeMaxDist: float32
 m_nNextThinkTick: GameTick_t
 m_bShouldAnimateDuringGameplayPause: bool
 m_nFireSequenceStartTimeChange: int32
 m_bSilencerOn: bool
 m_flCapsuleRadius: float32
 m_flPostponeFireReadyTime: GameTime_t
 m_clrRender: Color
 m_bInitialized: bool
 m_bIsHauledBack: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_hOuter: CHandle< CBaseEntity >
 m_flSimulationTime: float32
 m_vCapsuleCenter2: Vector
 m_glowColorOverride: Color
 m_bAnimatedEveryTick: bool
 m_nInteractsExclude: uint64
 m_fadeMinDist: float32
 m_iEntityQuality: int32
 m_bBurstMode: bool
 m_bReloadVisuallyComplete: bool
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_fLastShotTime: GameTime_t
 m_usSolidFlags: uint8
 m_bEligibleForScreenHighlight: bool
 m_vDecalForwardAxis: Vector
 m_nNextPrimaryAttackTick: GameTick_t
 m_nFallbackStatTrak: int32
 m_weaponMode: CSWeaponMode
 m_pReserveAmmo: int32[2]
 m_CollisionGroup: uint8
 m_iGlowTeam: int32
 m_flFallbackWear: float32
 m_nSurroundType: SurroundingBoundsType_t
 m_iItemIDLow: uint32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_iItemDefinitionIndex: uint16
 m_bPlayerFireEventIsPrimary: bool

207 CWeaponG3SG1
 m_bEligibleForScreenHighlight: bool
 m_nAddDecal: int32
 m_hOuter: CHandle< CBaseEntity >
 m_bInitialized: bool
 m_iRecoilIndex: int32
 m_nNextPrimaryAttackTick: GameTick_t
 m_flFallbackWear: float32
 m_bvDisabledHitGroups: uint32[1]
 m_vecMins: Vector
 m_iGlowType: int32
 m_bInitiallyPopulateInterpHistory: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_iEntityQuality: int32
 m_iItemIDLow: uint32
 m_nNextSecondaryAttackTick: GameTick_t
 m_iClip2: int32
 m_nCollisionGroup: uint8
 m_nCollisionFunctionMask: uint8
 m_nGlowRangeMin: int32
 m_flFadeScale: float32
 m_vDecalPosition: Vector
 m_nFallbackPaintKit: int32
 m_bShouldAnimateDuringGameplayPause: bool
 m_ProviderType: attributeprovidertypes_t
 m_iState: WeaponState_t
 m_pReserveAmmo: int32[2]
 m_nInteractsWith: uint64
 m_nHierarchyId: uint16
 m_flGlowTime: float32
 m_flFireSequenceStartTime: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecSpecifiedSurroundingMins: Vector
 m_glowColorOverride: Color
 m_flPostponeFireReadyTime: GameTime_t
 m_nEntityId: uint32
 m_ubInterpolationFrame: uint8
 m_nFireSequenceStartTimeChange: int32
 m_iBurstShotsRemaining: int32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
 m_bBurstMode: bool
 m_flCreateTime: GameTime_t
 m_iTeamNum: uint8
 m_usSolidFlags: uint8
 m_bFlashing: bool
 m_flGlowBackfaceMult: float32
 m_flDecalHealHeightRate: float32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_nFallbackSeed: int32
 m_CollisionGroup: uint8
 m_iAccountID: uint32
 m_nNextThinkTick: GameTick_t
 m_bRenderToCubemaps: bool
 m_flDecalHealBloodRate: float32
 m_flRecoilIndex: float32
 m_nEnablePhysics: uint8
 m_iItemDefinitionIndex: uint16
 m_flDroppedAtTime: GameTime_t
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flShadowStrength: float32
 m_iItemIDHigh: uint32
 m_OriginalOwnerXuidLow: uint32
 m_bNeedsBoltAction: bool
 m_nSubclassID: CUtlStringToken
 m_bReloadVisuallyComplete: bool
 m_iNumEmptyAttacks: int32
 m_flNextSecondaryAttackTickRatio: float32
 m_flAnimTime: float32
 m_nRenderFX: RenderFx_t
 m_LightGroup: CUtlStringToken
 m_bIsHauledBack: bool
 m_nDropTick: GameTick_t
 m_flSimulationTime: float32
 m_MoveCollide: MoveCollide_t
 m_vCapsuleCenter1: Vector
 m_flGlowStartTime: float32
 m_vecForce: Vector
 m_bClientRagdoll: bool
 m_vLookTargetPosition: Vector
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_bAnimatedEveryTick: bool
 m_nRenderMode: RenderMode_t
 m_nInteractsExclude: uint64
 m_nSolidType: SolidType_t
 m_flCapsuleRadius: float32
 m_vDecalForwardAxis: Vector
 m_bAnimGraphUpdateEnabled: bool
 m_nForceBone: int32
 m_iOriginalTeamNumber: int32
 m_iIronSightMode: int32
 CRenderComponent: CRenderComponent
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vecMaxs: Vector
 m_szCustomName: char[161]
 m_OriginalOwnerXuidHigh: uint32
 m_flElasticity: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_zoomLevel: int32
 m_flNextPrimaryAttackTickRatio: float32
 m_bClientSideRagdoll: bool
 m_vecSpecifiedSurroundingMaxs: Vector
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveType: MoveType_t
 m_vCapsuleCenter2: Vector
 m_iClip1: int32
 m_iGlowTeam: int32
 m_fadeMinDist: float32
 m_bSimulatedEveryTick: bool
 m_triggerBloat: uint8
 m_iReapplyProvisionParity: int32
 m_weaponMode: CSWeaponMode
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nInteractsAs: uint64
 m_nSurroundType: SurroundingBoundsType_t
 m_nOwnerId: uint32
 m_iInventoryPosition: uint32
 m_fLastShotTime: GameTime_t
 m_nViewModelIndex: uint32
 m_clrRender: Color
 m_iEntityLevel: uint32
 m_nFallbackStatTrak: int32
 m_bPlayerFireEventIsPrimary: bool
 m_bInReload: bool
 m_bSilencerOn: bool
 CBodyComponent: CBodyComponent
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
 m_fEffects: uint32
 m_nGlowRange: int32
 m_fadeMaxDist: float32
 m_nObjectCulling: uint8
 m_fAccuracyPenalty: float32

166 CRopeKeyframe
 m_iRopeMaterialModelIndex: CStrongHandle< InfoForResourceTypeIMaterial2 >
 m_fLockedPoints: uint8
 m_nSegments: uint8
 m_bConstrainBetweenEndpoints: bool
 m_nChangeCount: uint8
 m_flScrollSpeed: float32
 m_hEndPoint: CHandle< CBaseEntity >
 m_TextureScale: float32
 m_iStartAttachment: AttachmentHandle_t
 m_iEndAttachment: AttachmentHandle_t
 m_hStartPoint: CHandle< CBaseEntity >
 CBodyComponent: CBodyComponent
  m_cellY: uint16
  m_cellZ: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
 m_RopeFlags: uint16
 m_Slack: int16
 m_Width: float32
 m_Subdiv: uint8
 m_RopeLength: int16

206 CWeaponFiveSeven
 m_nSubclassID: CUtlStringToken
 m_bRenderToCubemaps: bool
 m_fAccuracyPenalty: float32
 m_iBurstShotsRemaining: int32
 m_nViewModelIndex: uint32
 m_ProviderType: attributeprovidertypes_t
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
 m_iState: WeaponState_t
 m_nSurroundType: SurroundingBoundsType_t
 m_flDecalHealBloodRate: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_nNextSecondaryAttackTick: GameTick_t
 m_iClip2: int32
 m_nInteractsAs: uint64
 m_nInteractsWith: uint64
 m_vDecalPosition: Vector
 m_vecForce: Vector
 m_bInReload: bool
 m_fLastShotTime: GameTime_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_iTeamNum: uint8
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_hOwner: CHandle< CBaseEntity >
  m_Transforms: CNetworkUtlVectorBase< CTransform >
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nRenderMode: RenderMode_t
 m_LightGroup: CUtlStringToken
 m_nOwnerId: uint32
 m_flPostponeFireReadyTime: GameTime_t
 m_hOuter: CHandle< CBaseEntity >
 m_weaponMode: CSWeaponMode
 m_flRecoilIndex: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vCapsuleCenter2: Vector
 m_OriginalOwnerXuidLow: uint32
 m_flFireSequenceStartTime: float32
 m_iRecoilIndex: int32
 m_flFadeScale: float32
 m_fEffects: uint32
 m_bEligibleForScreenHighlight: bool
 m_clrRender: Color
 m_iGlowType: int32
 m_flGlowStartTime: float32
 m_nNextThinkTick: GameTick_t
 m_flElasticity: float32
 m_nHierarchyId: uint16
 m_iGlowTeam: int32
 m_flGlowTime: float32
 m_bClientRagdoll: bool
 m_nDropTick: GameTick_t
 m_nCollisionGroup: uint8
 m_flCapsuleRadius: float32
 m_iItemDefinitionIndex: uint16
 m_pReserveAmmo: int32[2]
 m_flCreateTime: GameTime_t
 m_bAnimatedEveryTick: bool
 m_nCollisionFunctionMask: uint8
 m_glowColorOverride: Color
 m_vDecalForwardAxis: Vector
 m_flDroppedAtTime: GameTime_t
 m_bSilencerOn: bool
 m_flSimulationTime: float32
 m_bClientSideRagdoll: bool
 m_iAccountID: uint32
 m_nFallbackSeed: int32
 m_bBurstMode: bool
 m_bIsHauledBack: bool
 m_iItemIDHigh: uint32
 m_iIronSightMode: int32
 m_iClip1: int32
 CBodyComponent: CBodyComponent
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
 m_nRenderFX: RenderFx_t
 m_vecMins: Vector
 m_usSolidFlags: uint8
 m_fadeMinDist: float32
 m_iNumEmptyAttacks: int32
 m_nGlowRange: int32
 m_bInitiallyPopulateInterpHistory: bool
 m_bAnimGraphUpdateEnabled: bool
 m_nForceBone: int32
 m_iReapplyProvisionParity: int32
 m_iInventoryPosition: uint32
 m_flFallbackWear: float32
 m_bvDisabledHitGroups: uint32[1]
 m_nFallbackPaintKit: int32
 m_iOriginalTeamNumber: int32
 m_ubInterpolationFrame: uint8
 m_nInteractsExclude: uint64
 m_triggerBloat: uint8
 m_vCapsuleCenter1: Vector
 m_nGlowRangeMin: int32
 m_nAddDecal: int32
 CRenderComponent: CRenderComponent
 m_vecMaxs: Vector
 m_iEntityQuality: int32
 m_nFallbackStatTrak: int32
 m_zoomLevel: int32
 m_flShadowStrength: float32
 m_MoveCollide: MoveCollide_t
 m_vecSpecifiedSurroundingMins: Vector
 m_nObjectCulling: uint8
 m_vLookTargetPosition: Vector
 m_flGlowBackfaceMult: float32
 m_iItemIDLow: uint32
 m_bNeedsBoltAction: bool
 m_nNextPrimaryAttackTick: GameTick_t
 m_fadeMaxDist: float32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_nEntityId: uint32
 m_bReloadVisuallyComplete: bool
 m_flNextSecondaryAttackTickRatio: float32
 m_flAnimTime: float32
 m_nEnablePhysics: uint8
 m_bFlashing: bool
 m_iEntityLevel: uint32
 m_OriginalOwnerXuidHigh: uint32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_flDecalHealHeightRate: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_szCustomName: char[161]
 m_bPlayerFireEventIsPrimary: bool
 m_MoveType: MoveType_t
 m_nSolidType: SolidType_t
 m_CollisionGroup: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bInitialized: bool
 m_flNextPrimaryAttackTickRatio: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bSimulatedEveryTick: bool
 m_nFireSequenceStartTimeChange: int32

9 CBaseEntity
 m_MoveType: MoveType_t
 m_flCreateTime: GameTime_t
 m_bAnimatedEveryTick: bool
 m_nSubclassID: CUtlStringToken
 m_iTeamNum: uint8
 m_bSimulatedEveryTick: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_ubInterpolationFrame: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveCollide: MoveCollide_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_flElasticity: float32
 m_flAnimTime: float32
 m_flSimulationTime: float32
 CBodyComponent: CBodyComponent
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken

52 CCSPlayerResource
 m_iHostageEntityIDs: CEntityIndex[12]
 m_bombsiteCenterB: Vector
 m_hostageRescueX: int32[4]
 m_bEndMatchNextMapAllVoted: bool
 m_bHostageAlive: bool[12]
 m_isHostageFollowingSomeone: bool[12]
 m_bombsiteCenterA: Vector
 m_hostageRescueY: int32[4]
 m_hostageRescueZ: int32[4]

133 CParadropChopper
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveType: MoveType_t
 m_nOwnerId: uint32
 m_nGlowRange: int32
 m_fEffects: uint32
 m_nHierarchyId: uint16
 m_vecMaxs: Vector
 m_nGlowRangeMin: int32
 m_vDecalPosition: Vector
 m_MoveCollide: MoveCollide_t
 m_nInteractsExclude: uint64
 m_vCapsuleCenter2: Vector
 m_flFadeScale: float32
 m_flSimulationTime: float32
 m_bSimulatedEveryTick: bool
 m_LightGroup: CUtlStringToken
 m_nSolidType: SolidType_t
 m_usSolidFlags: uint8
 m_CollisionGroup: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_iGlowTeam: int32
 m_glowColorOverride: Color
 m_fadeMaxDist: float32
 m_nAddDecal: int32
 m_flDecalHealHeightRate: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bAnimGraphUpdateEnabled: bool
 m_flCreateTime: GameTime_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flElasticity: float32
 m_nObjectCulling: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_flGlowStartTime: float32
 m_nForceBone: int32
 m_flAnimTime: float32
 m_nInteractsAs: uint64
 m_nInteractsWith: uint64
 m_nEntityId: uint32
 m_triggerBloat: uint8
 m_nEnablePhysics: uint8
 m_iGlowType: int32
 m_vDecalForwardAxis: Vector
 m_iTeamNum: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_vecMins: Vector
 m_bShouldAnimateDuringGameplayPause: bool
 CBodyComponent: CBodyComponent
  m_hParent: CGameSceneNodeHandle
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_flLastTeleportTime: float32
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nAnimLoopMode: AnimLoopMode_t
  m_nBoolVariablesCount: int32
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_cellY: uint16
  m_flScale: float32
  m_vecY: CNetworkedQuantizedFloat
  m_nNewSequenceParity: int32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_vecX: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_cellX: uint16
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_flPrevCycle: float32
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
 m_ubInterpolationFrame: uint8
 m_nCollisionGroup: uint8
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_bvDisabledHitGroups: uint32[1]
 m_nRenderFX: RenderFx_t
 m_vCapsuleCenter1: Vector
 m_flCapsuleRadius: float32
 m_bFlashing: bool
 m_flShadowStrength: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_hCallingPlayer: CHandle< CBaseEntity >
 m_bClientSideRagdoll: bool
 m_bAnimatedEveryTick: bool
 m_bRenderToCubemaps: bool
 m_bEligibleForScreenHighlight: bool
 m_flGlowBackfaceMult: float32
 m_vecForce: Vector
 m_bClientRagdoll: bool
 m_nSubclassID: CUtlStringToken
 m_clrRender: Color
 m_nRenderMode: RenderMode_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flGlowTime: float32
 m_nCollisionFunctionMask: uint8
 m_fadeMinDist: float32
 m_flDecalHealBloodRate: float32
 CRenderComponent: CRenderComponent

170 CShatterGlassShardPhysics
 m_bShouldAnimateDuringGameplayPause: bool
 m_vecPanelSize: Vector2D
 m_hOwnerEntity: CHandle< CBaseEntity >
 CBodyComponent: CBodyComponent
  m_bClientSideAnimation: bool
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nBoolVariablesCount: int32
  m_flPrevCycle: float32
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_vecZ: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nResetEventsParity: int32
  m_MeshGroupMask: uint64
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_hierarchyAttachName: CUtlStringToken
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flCycle: float32
  m_hSequence: HSequence
  m_name: CUtlStringToken
  m_nHitboxSet: uint8
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_vecX: CNetworkedQuantizedFloat
  m_flScale: float32
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_angRotation: QAngle
  m_nNewSequenceParity: int32
  m_nRandomSeedOffset: int32
  m_flLastTeleportTime: float32
  m_nOutsideWorld: uint16
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_hParent: CGameSceneNodeHandle
  m_flWeight: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_cellZ: uint16
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
 m_flNavIgnoreUntilTime: GameTime_t
 m_vecMins: Vector
 m_vecMaxs: Vector
 m_CollisionGroup: uint8
 m_bvDisabledHitGroups: uint32[1]
 m_glowColorOverride: Color
 m_vecForce: Vector
 m_bHasParent: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_nEntityId: uint32
 m_nCollisionGroup: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_flGlowTime: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_ShatterPanelMode: ShatterPanelMode
 m_vecPanelVertices: CNetworkUtlVectorBase< Vector2D >
 m_spawnflags: uint32
 m_nRenderFX: RenderFx_t
 m_nInteractsWith: uint64
 m_vecSpecifiedSurroundingMaxs: Vector
 m_iTeamNum: uint8
 m_clrRender: Color
 m_vecStressPositionB: Vector2D
 m_flGlowBackfaceMult: float32
 m_flDecalHealBloodRate: float32
 m_noGhostCollision: bool
 m_hMaterial: CStrongHandle< InfoForResourceTypeIMaterial2 >
 m_bClientSideRagdoll: bool
 m_LightGroup: CUtlStringToken
 m_nCollisionFunctionMask: uint8
 m_nSolidType: SolidType_t
 m_solid: ShardSolid_t
 m_vDecalPosition: Vector
 m_bAwake: bool
 m_flGlassHalfThickness: float32
 m_MoveCollide: MoveCollide_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nHierarchyId: uint16
 m_nAddDecal: int32
 m_MoveType: MoveType_t
 m_bEligibleForScreenHighlight: bool
 m_fadeMaxDist: float32
 m_SurfacePropStringToken: CUtlStringToken
 m_vCapsuleCenter1: Vector
 m_flCapsuleRadius: float32
 m_iGlowTeam: int32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_flSimulationTime: float32
 m_bAnimatedEveryTick: bool
 m_nEnablePhysics: uint8
 m_flDecalHealHeightRate: float32
 m_flFadeScale: float32
 m_bClientRagdoll: bool
 CRenderComponent: CRenderComponent
 m_nSubclassID: CUtlStringToken
 m_usSolidFlags: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_flGlowStartTime: float32
 m_bParentFrozen: bool
 m_triggerBloat: uint8
 m_flShadowStrength: float32
 m_nObjectCulling: uint8
 m_vDecalForwardAxis: Vector
 m_vCapsuleCenter2: Vector
 m_iGlowType: int32
 m_nModelID: int32
 m_fadeMinDist: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_ubInterpolationFrame: uint8
 m_fEffects: uint32
 m_nOwnerId: uint32
 m_nGlowRangeMin: int32
 m_bAnimGraphUpdateEnabled: bool
 m_nForceBone: int32
 m_vecStressPositionA: Vector2D
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bSimulatedEveryTick: bool
 m_nInteractsAs: uint64
 m_nGlowRange: int32
 m_nInteractsExclude: uint64
 m_bFlashing: bool
 m_flCreateTime: GameTime_t
 m_flElasticity: float32
 m_nRenderMode: RenderMode_t
 m_bRenderToCubemaps: bool

73 CEnvDecal
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flElasticity: float32
 m_CollisionGroup: uint8
 m_bFlashing: bool
 m_flGlowBackfaceMult: float32
 m_nObjectCulling: uint8
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_MoveCollide: MoveCollide_t
 m_hDecalMaterial: CStrongHandle< InfoForResourceTypeIMaterial2 >
 m_bRenderToCubemaps: bool
 m_usSolidFlags: uint8
 m_flGlowTime: float32
 m_vDecalPosition: Vector
 m_bProjectOnCharacters: bool
 m_flAnimTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flDecalHealBloodRate: float32
 m_iGlowTeam: int32
 m_nAddDecal: int32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsWith: uint64
 m_vecSpecifiedSurroundingMins: Vector
 m_vCapsuleCenter1: Vector
 m_fEffects: uint32
 m_nRenderFX: RenderFx_t
 m_glowColorOverride: Color
 m_flGlowStartTime: float32
 m_flFadeScale: float32
 m_bSimulatedEveryTick: bool
 m_LightGroup: CUtlStringToken
 m_nGlowRange: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 CRenderComponent: CRenderComponent
 m_MoveType: MoveType_t
 m_clrRender: Color
 m_vDecalForwardAxis: Vector
 m_bProjectOnWater: bool
 m_bvDisabledHitGroups: uint32[1]
 CBodyComponent: CBodyComponent
  m_materialGroup: CUtlStringToken
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_MeshGroupMask: uint64
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
 m_nOwnerId: uint32
 m_flCreateTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_flHeight: float32
 m_nRenderOrder: uint32
 m_nInteractsAs: uint64
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vCapsuleCenter2: Vector
 m_bEligibleForScreenHighlight: bool
 m_flSimulationTime: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_nGlowRangeMin: int32
 m_flDecalHealHeightRate: float32
 m_flWidth: float32
 m_iTeamNum: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_flDepth: float32
 m_flCapsuleRadius: float32
 m_flShadowStrength: float32
 m_nCollisionGroup: uint8
 m_vecMaxs: Vector
 m_vecMins: Vector
 m_nSolidType: SolidType_t
 m_triggerBloat: uint8
 m_fadeMinDist: float32
 m_bProjectOnWorld: bool
 m_flDepthSortBias: float32
 m_nSubclassID: CUtlStringToken
 m_bAnimatedEveryTick: bool
 m_fadeMaxDist: float32
 m_ubInterpolationFrame: uint8
 m_nCollisionFunctionMask: uint8
 m_nHierarchyId: uint16
 m_nEnablePhysics: uint8
 m_iGlowType: int32
 m_nInteractsExclude: uint64
 m_nEntityId: uint32

147 CPlayerSprayDecal
 m_flSimulationTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bRenderToCubemaps: bool
 m_nEntityId: uint32
 m_nSolidType: SolidType_t
 m_vCapsuleCenter2: Vector
 m_glowColorOverride: Color
 m_vecEndPos: Vector
 m_nGlowRange: int32
 m_nGlowRangeMin: int32
 m_flShadowStrength: float32
 m_vecStart: Vector
 m_nSubclassID: CUtlStringToken
 m_vecSpecifiedSurroundingMaxs: Vector
 m_fadeMinDist: float32
 m_nObjectCulling: uint8
 m_nAddDecal: int32
 m_vecNormal: Vector
 m_nHierarchyId: uint16
 m_vecMaxs: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 CRenderComponent: CRenderComponent
 m_iTeamNum: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nRenderFX: RenderFx_t
 m_vecMins: Vector
 m_nInteractsWith: uint64
 m_nCollisionFunctionMask: uint8
 m_triggerBloat: uint8
 m_vDecalPosition: Vector
 m_nHitbox: int32
 m_MoveCollide: MoveCollide_t
 m_flCreateTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_unAccountID: uint32
 m_flElasticity: float32
 m_nSurroundType: SurroundingBoundsType_t
 m_flGlowTime: float32
 m_vDecalForwardAxis: Vector
 m_flCreationTime: float32
 m_nTintID: int32
 m_flAnimTime: float32
 m_bFlashing: bool
 m_rtGcTime: uint32
 m_MoveType: MoveType_t
 m_fEffects: uint32
 m_CollisionGroup: uint8
 m_vCapsuleCenter1: Vector
 m_bEligibleForScreenHighlight: bool
 m_unTraceID: uint32
 m_hOwnerEntity: CHandle< CBaseEntity >
 CBodyComponent: CBodyComponent
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
 m_bAnimatedEveryTick: bool
 m_flCapsuleRadius: float32
 m_flGlowStartTime: float32
 m_fadeMaxDist: float32
 m_clrRender: Color
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nCollisionGroup: uint8
 m_usSolidFlags: uint8
 m_flGlowBackfaceMult: float32
 m_nUniqueID: int32
 m_vecLeft: Vector
 m_bSimulatedEveryTick: bool
 m_vecSpecifiedSurroundingMins: Vector
 m_iGlowTeam: int32
 m_flFadeScale: float32
 m_flDecalHealBloodRate: float32
 m_nInteractsAs: uint64
 m_nOwnerId: uint32
 m_nEnablePhysics: uint8
 m_nEntity: int32
 m_ubInterpolationFrame: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_LightGroup: CUtlStringToken
 m_flDecalHealHeightRate: float32
 m_nInteractsExclude: uint64
 m_iGlowType: int32
 m_nPlayer: int32
 m_nVersion: uint8
 m_ubSignature: uint8[128]
 m_bvDisabledHitGroups: uint32[1]

167 CSceneEntity
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flNavIgnoreUntilTime: GameTime_t
 m_bMultiplayer: bool
 m_bAutogenerated: bool
 m_flAnimTime: float32
 m_flCreateTime: GameTime_t
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveCollide: MoveCollide_t
 m_iTeamNum: uint8
 m_fEffects: uint32
 m_flElasticity: float32
 m_bAnimatedEveryTick: bool
 m_flSimulationTime: float32
 CBodyComponent: CBodyComponent
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
 m_bPaused: bool
 m_flForceClientTime: float32
 m_ubInterpolationFrame: uint8
 m_bIsPlayingBack: bool
 m_hActorList: CNetworkUtlVectorBase< CHandle< CBaseFlex > >
 m_nSceneStringIndex: uint16
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_MoveType: MoveType_t
 m_nSubclassID: CUtlStringToken
 m_bSimulatedEveryTick: bool

201 CWeaponBizon
 m_nRenderMode: RenderMode_t
 m_nInteractsExclude: uint64
 m_flNavIgnoreUntilTime: GameTime_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsWith: uint64
 m_hOuter: CHandle< CBaseEntity >
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nFallbackStatTrak: int32
 m_iTeamNum: uint8
 m_vLookTargetPosition: Vector
 m_iIronSightMode: int32
 m_flNextPrimaryAttackTickRatio: float32
 m_iClip2: int32
 m_flCreateTime: GameTime_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bEligibleForScreenHighlight: bool
 m_bBurstMode: bool
 m_nNextPrimaryAttackTick: GameTick_t
 m_flElasticity: float32
 m_nRenderFX: RenderFx_t
 m_bPlayerFireEventIsPrimary: bool
 CBodyComponent: CBodyComponent
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_nNewSequenceParity: int32
  m_bClientSideAnimation: bool
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_hParent: CGameSceneNodeHandle
  m_nRandomSeedOffset: int32
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
 m_MoveCollide: MoveCollide_t
 m_iGlowTeam: int32
 m_nDropTick: GameTick_t
 m_fadeMaxDist: float32
 m_CollisionGroup: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_OriginalOwnerXuidHigh: uint32
 m_nFallbackSeed: int32
 m_nNextThinkTick: GameTick_t
 m_MoveType: MoveType_t
 m_ubInterpolationFrame: uint8
 m_nOwnerId: uint32
 m_flFadeScale: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bAnimGraphUpdateEnabled: bool
 m_iState: WeaponState_t
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_bSimulatedEveryTick: bool
 m_nForceBone: int32
 m_nFireSequenceStartTimeChange: int32
 m_vDecalForwardAxis: Vector
 m_flNextSecondaryAttackTickRatio: float32
 m_nCollisionGroup: uint8
 m_flCapsuleRadius: float32
 m_iEntityLevel: uint32
 m_OriginalOwnerXuidLow: uint32
 m_bClientSideRagdoll: bool
 m_triggerBloat: uint8
 m_flGlowTime: float32
 m_bInReload: bool
 m_fLastShotTime: GameTime_t
 m_pReserveAmmo: int32[2]
 CRenderComponent: CRenderComponent
 m_fEffects: uint32
 m_vecMaxs: Vector
 m_usSolidFlags: uint8
 m_vCapsuleCenter1: Vector
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_bIsHauledBack: bool
 m_zoomLevel: int32
 m_glowColorOverride: Color
 m_fadeMinDist: float32
 m_flDecalHealHeightRate: float32
 m_ProviderType: attributeprovidertypes_t
 m_iAccountID: uint32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
 m_nFallbackPaintKit: int32
 m_nSurroundType: SurroundingBoundsType_t
 m_nGlowRangeMin: int32
 m_bFlashing: bool
 m_flFireSequenceStartTime: float32
 m_flPostponeFireReadyTime: GameTime_t
 m_bReloadVisuallyComplete: bool
 m_nViewModelIndex: uint32
 m_nSolidType: SolidType_t
 m_vecForce: Vector
 m_clrRender: Color
 m_vecSpecifiedSurroundingMaxs: Vector
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bAnimatedEveryTick: bool
 m_iItemDefinitionIndex: uint16
 m_nNextSecondaryAttackTick: GameTick_t
 m_LightGroup: CUtlStringToken
 m_nEnablePhysics: uint8
 m_vCapsuleCenter2: Vector
 m_nObjectCulling: uint8
 m_nAddDecal: int32
 m_flDecalHealBloodRate: float32
 m_iNumEmptyAttacks: int32
 m_nCollisionFunctionMask: uint8
 m_nGlowRange: int32
 m_iItemIDHigh: uint32
 m_iItemIDLow: uint32
 m_weaponMode: CSWeaponMode
 m_flDroppedAtTime: GameTime_t
 m_bNeedsBoltAction: bool
 m_nEntityId: uint32
 m_nHierarchyId: uint16
 m_iGlowType: int32
 m_bInitiallyPopulateInterpHistory: bool
 m_bInitialized: bool
 m_iRecoilIndex: int32
 m_flSimulationTime: float32
 m_bClientRagdoll: bool
 m_iInventoryPosition: uint32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_flShadowStrength: float32
 m_vDecalPosition: Vector
 m_iEntityQuality: int32
 m_iOriginalTeamNumber: int32
 m_iBurstShotsRemaining: int32
 m_flGlowBackfaceMult: float32
 m_szCustomName: char[161]
 m_bSilencerOn: bool
 m_iClip1: int32
 m_bRenderToCubemaps: bool
 m_flGlowStartTime: float32
 m_iReapplyProvisionParity: int32
 m_flFallbackWear: float32
 m_fAccuracyPenalty: float32
 m_flAnimTime: float32
 m_nInteractsAs: uint64
 m_vecMins: Vector
 m_nSubclassID: CUtlStringToken
 m_bShouldAnimateDuringGameplayPause: bool
 m_bvDisabledHitGroups: uint32[1]
 m_flRecoilIndex: float32

49 CCSPlayerController
 m_iCompTeammateColor: int32
 m_iCompetitiveRankingPredicted_Tie: int32
 m_nDisconnectionTick: int32
 m_flTimeScale: float32
 m_hObserverPawn: CHandle< CCSObserverPawn >
 m_iScore: int32
 m_fFlags: uint32
 m_pInGameMoneyServices: CCSPlayerController_InGameMoneyServices*
  m_iStartAccount: int32
  m_iTotalCashSpent: int32
  m_iCashSpentThisRound: int32
  m_iAccount: int32
 m_pInventoryServices: CCSPlayerController_InventoryServices*
  m_unMusicID: uint16
  m_rank: MedalRank_t[6]
  m_nPersonaDataPublicLevel: int32
  m_nPersonaDataPublicCommendsLeader: int32
  m_nPersonaDataPublicCommendsTeacher: int32
  m_nPersonaDataPublicCommendsFriendly: int32
  m_vecTerroristLoadoutCache: CUtlVectorEmbeddedNetworkVar< CEconItemView >
   m_iItemDefinitionIndex: uint16
   m_iItemIDHigh: uint32
   m_iAccountID: uint32
   m_Attributes: CUtlVector< CEconItemAttribute >
    m_iRawValue32: float32
    m_flInitialValue: float32
    m_nRefundableCurrency: int32
    m_bSetBonus: bool
    m_iAttributeDefinitionIndex: uint16
   m_szCustomName: char[161]
   m_iEntityQuality: int32
   m_iEntityLevel: uint32
   m_iItemIDLow: uint32
   m_iInventoryPosition: uint32
   m_bInitialized: bool
  m_vecCounterTerroristLoadoutCache: CUtlVectorEmbeddedNetworkVar< CEconItemView >
   m_bInitialized: bool
   m_iEntityQuality: int32
   m_iEntityLevel: uint32
   m_iItemIDLow: uint32
   m_iInventoryPosition: uint32
   m_szCustomName: char[161]
   m_iItemDefinitionIndex: uint16
   m_iItemIDHigh: uint32
   m_iAccountID: uint32
   m_Attributes: CUtlVector< CEconItemAttribute >
    m_flInitialValue: float32
    m_nRefundableCurrency: int32
    m_bSetBonus: bool
    m_iAttributeDefinitionIndex: uint16
    m_iRawValue32: float32
 m_iCompetitiveRankType: int8
 m_bHasControlledBotThisRound: bool
 m_bSimulatedEveryTick: bool
 m_bHasCommunicationAbuseMute: bool
 m_nEndMatchNextMapVote: int32
 m_bCanControlObservedBot: bool
 m_iPawnArmor: int32
 m_szCrosshairCodes: CUtlSymbolLarge
 m_vecKills: CNetworkUtlVectorBase< EKillTypes_t >
 m_nPlayerDominated: uint64
 m_iCompetitiveRankingPredicted_Loss: int32
 m_iPing: uint32
 m_flForceTeamTime: GameTime_t
 m_iCompetitiveRanking: int32
 m_iPawnHealth: uint32
 m_iPawnLifetimeEnd: int32
 m_flGravityScale: float32
 m_hPawn: CHandle< CBasePlayerPawn >
 m_pDamageServices: CCSPlayerController_DamageServices*
  m_nSendUpdate: int32
  m_DamageList: CUtlVectorEmbeddedNetworkVar< CDamageRecord >
   m_DamagerXuid: uint64
   m_RecipientXuid: uint64
   m_iDamage: int32
   m_iLastBulletUpdate: int32
   m_bIsOtherEnemy: bool
   m_PlayerDamager: CHandle< CCSPlayerPawnBase >
   m_PlayerRecipient: CHandle< CCSPlayerPawnBase >
   m_szPlayerDamagerName: CUtlString
   m_szPlayerRecipientName: CUtlString
   m_hPlayerControllerDamager: CHandle< CCSPlayerController >
   m_iNumHits: int32
   m_hPlayerControllerRecipient: CHandle< CCSPlayerController >
   m_iActualHealthRemoved: int32
   m_killType: EKillTypes_t
 m_unPlayerTvControlFlags: uint32
 m_vecBaseVelocity: Vector
 m_flFriction: float32
 m_flCreateTime: GameTime_t
 m_iszPlayerName: char[128]
 m_iPendingTeamNum: uint8
 m_hOriginalControllerOfCurrentPawn: CHandle< CCSPlayerController >
 m_iMVPs: int32
 m_iTeamNum: uint8
 m_szClan: CUtlSymbolLarge
 m_unActiveQuestId: uint16
 m_iPawnBotDifficulty: int32
 m_nNextThinkTick: GameTick_t
 m_iConnected: PlayerConnectedState
 m_iCoachingTeam: int32
 m_iCompetitiveRankingPredicted_Win: int32
 m_vecY: CNetworkedQuantizedFloat
 m_vecZ: CNetworkedQuantizedFloat
 m_nPlayerDominatingMe: uint64
 m_nQuestProgressReason: QuestProgress::Reason
 m_hPlayerPawn: CHandle< CCSPlayerPawn >
 m_bPawnHasDefuser: bool
 m_iPawnGunGameLevel: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_steamID: uint64
 m_iCompetitiveWins: int32
 m_bPawnIsAlive: bool
 m_iPawnLifetimeStart: int32
 m_pActionTrackingServices: CCSPlayerController_ActionTrackingServices*
  m_perRoundStats: CUtlVectorEmbeddedNetworkVar< CSPerRoundStats_t >
   m_iKillReward: int32
   m_iLiveTime: int32
   m_iDamage: int32
   m_iEquipmentValue: int32
   m_iMoneySaved: int32
   m_iDeaths: int32
   m_iAssists: int32
   m_iEnemiesFlashed: int32
   m_iCashEarned: int32
   m_iKills: int32
   m_iHeadShotKills: int32
   m_iObjective: int32
   m_iUtilityDamage: int32
  m_iEnemy4Ks: int32
  m_iObjective: int32
  m_iUtilityDamage: int32
  m_iEnemiesFlashed: int32
  m_iKillReward: int32
  m_iKills: int32
  m_iDeaths: int32
  m_iCashEarned: int32
  m_iEnemy5Ks: int32
  m_iNumRoundKills: int32
  m_iDamage: int32
  m_iMoneySaved: int32
  m_iAssists: int32
  m_iLiveTime: int32
  m_iHeadShotKills: int32
  m_iEnemy3Ks: int32
  m_iNumRoundKillsHeadshots: int32
  m_iEquipmentValue: int32
 m_iDesiredFOV: uint32
 m_bControllingBot: bool
 m_bPawnHasHelmet: bool
 m_vecX: CNetworkedQuantizedFloat
 m_flSimulationTime: float32
 m_nTickBase: uint32
 m_bEverPlayedOnTeam: bool
 m_nPawnCharacterDefIndex: uint16

70 CEnvCubemap
 m_ubInterpolationFrame: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nPriority: int32
 m_iTeamNum: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_LightGroups: CUtlSymbolLarge
 m_bMoveable: bool
 m_bStartDisabled: bool
 m_bIndoorCubeMap: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 CBodyComponent: CBodyComponent
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
 m_bCustomCubemapTexture: bool
 m_vBoxProjectMins: Vector
 m_bAnimatedEveryTick: bool
 m_flDiffuseScale: float32
 m_bEnabled: bool
 m_MoveType: MoveType_t
 m_flElasticity: float32
 m_flEdgeFadeDist: float32
 m_vEdgeFadeDists: Vector
 m_vBoxProjectMaxs: Vector
 m_bDefaultSpecEnvMap: bool
 m_bCopyDiffuseFromDefaultCubemap: bool
 m_flSimulationTime: float32
 m_MoveCollide: MoveCollide_t
 m_bSimulatedEveryTick: bool
 m_flInfluenceRadius: float32
 m_fEffects: uint32
 m_hCubemapTexture: CStrongHandle< InfoForResourceTypeCTextureBase >
 m_nHandshake: int32
 m_nEnvCubeMapArrayIndex: int32
 m_flAnimTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSubclassID: CUtlStringToken
 m_flCreateTime: GameTime_t
 m_bDefaultEnvMap: bool

177 CSoundOpvarSetAABBEntity
 m_flAnimTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_MoveType: MoveType_t
 m_flElasticity: float32
 m_bUseAutoCompare: bool
 m_iOpvarIndex: int32
 m_MoveCollide: MoveCollide_t
 m_nSubclassID: CUtlStringToken
 m_iTeamNum: uint8
 m_fEffects: uint32
 m_flNavIgnoreUntilTime: GameTime_t
 m_iszOperatorName: CUtlSymbolLarge
 CBodyComponent: CBodyComponent
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
 m_hEffectEntity: CHandle< CBaseEntity >
 m_iszStackName: CUtlSymbolLarge
 m_iszOpvarName: CUtlSymbolLarge
 m_flSimulationTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flCreateTime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_bSimulatedEveryTick: bool
 m_bAnimatedEveryTick: bool

181 CSoundOpvarSetPointBase
 m_fEffects: uint32
 m_flElasticity: float32
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_iszOpvarName: CUtlSymbolLarge
 m_flAnimTime: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_MoveCollide: MoveCollide_t
 m_flCreateTime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_bUseAutoCompare: bool
 m_flSimulationTime: float32
 CBodyComponent: CBodyComponent
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
 m_iTeamNum: uint8
 m_bSimulatedEveryTick: bool
 m_iszOperatorName: CUtlSymbolLarge
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveType: MoveType_t
 m_iszStackName: CUtlSymbolLarge
 m_iOpvarIndex: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nSubclassID: CUtlStringToken

173 CSmokeGrenadeProjectile
 m_bAnimatedEveryTick: bool
 m_nSolidType: SolidType_t
 m_vDecalPosition: Vector
 m_nForceBone: int32
 m_nSmokeEffectTickBegin: int32
 m_flElasticity: float32
 m_vecMaxs: Vector
 m_nObjectCulling: uint8
 m_bShouldAnimateDuringGameplayPause: bool
 m_DmgRadius: float32
 m_vecX: CNetworkedQuantizedFloat
 m_nCollisionFunctionMask: uint8
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_bIsLive: bool
 m_flDetonateTime: GameTime_t
 m_flDamage: float32
 m_VoxelFrameData: CUtlVector< uint8 >
 m_flDecalHealBloodRate: float32
 m_nEnablePhysics: uint8
 m_hThrower: CHandle< CBaseEntity >
 m_nBounces: int32
 m_vecMins: Vector
 m_nGlowRange: int32
 CBodyComponent: CBodyComponent
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_vecZ: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_bClientClothCreationSuppressed: bool
  m_cellY: uint16
  m_hParent: CGameSceneNodeHandle
  m_flScale: float32
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_nRandomSeedOffset: int32
  m_vecX: CNetworkedQuantizedFloat
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_cellZ: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_bUseParentRenderBounds: bool
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_hierarchyAttachName: CUtlStringToken
  m_materialGroup: CUtlStringToken
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flLastTeleportTime: float32
  m_angRotation: QAngle
  m_nIdealMotionType: int8
  m_bClientSideAnimation: bool
  m_nOutsideWorld: uint16
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_cellX: uint16
  m_flWeight: CNetworkedQuantizedFloat
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nBoolVariablesCount: int32
  m_name: CUtlStringToken
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
 m_nRenderFX: RenderFx_t
 m_bRenderToCubemaps: bool
 m_nEntityId: uint32
 m_iGlowType: int32
 m_vDecalForwardAxis: Vector
 m_bInitiallyPopulateInterpHistory: bool
 m_fFlags: uint32
 m_nHierarchyId: uint16
 m_vecSpecifiedSurroundingMins: Vector
 m_flGlowStartTime: float32
 m_flGlowBackfaceMult: float32
 m_vLookTargetPosition: Vector
 m_vInitialVelocity: Vector
 m_vecExplodeEffectOrigin: Vector
 m_bSimulatedEveryTick: bool
 CRenderComponent: CRenderComponent
 m_clrRender: Color
 m_nInteractsWith: uint64
 m_vSmokeColor: Vector
 m_vSmokeDetonationPos: Vector
 m_MoveType: MoveType_t
 m_bClientSideRagdoll: bool
 m_fEffects: uint32
 m_flFadeScale: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_ubInterpolationFrame: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_vCapsuleCenter1: Vector
 m_vCapsuleCenter2: Vector
 m_flGlowTime: float32
 m_flCreateTime: GameTime_t
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_iTeamNum: uint8
 m_nInteractsAs: uint64
 m_nOwnerId: uint32
 m_triggerBloat: uint8
 m_nExplodeEffectTickBegin: int32
 m_flSimulationTime: float32
 m_nRenderMode: RenderMode_t
 m_LightGroup: CUtlStringToken
 m_CollisionGroup: uint8
 m_glowColorOverride: Color
 m_fadeMinDist: float32
 m_fadeMaxDist: float32
 m_nAddDecal: int32
 m_nSubclassID: CUtlStringToken
 m_vecZ: CNetworkedQuantizedFloat
 m_bDidSmokeEffect: bool
 m_nCollisionGroup: uint8
 m_nGlowRangeMin: int32
 m_bEligibleForScreenHighlight: bool
 m_flDecalHealHeightRate: float32
 m_bvDisabledHitGroups: uint32[1]
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_iGlowTeam: int32
 m_flShadowStrength: float32
 m_vecForce: Vector
 m_bClientRagdoll: bool
 m_nInteractsExclude: uint64
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bFlashing: bool
 m_nExplodeEffectIndex: CStrongHandle< InfoForResourceTypeIParticleSystemDefinition >
 m_nRandomSeed: int32
 m_nSurroundType: SurroundingBoundsType_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_usSolidFlags: uint8
 m_flCapsuleRadius: float32
 m_bAnimGraphUpdateEnabled: bool
 m_vecY: CNetworkedQuantizedFloat
 m_MoveCollide: MoveCollide_t

221 CWeaponSawedoff
 m_bBurstMode: bool
 m_flDroppedAtTime: GameTime_t
 m_bSimulatedEveryTick: bool
 m_nEntityId: uint32
 m_vCapsuleCenter1: Vector
 m_vDecalForwardAxis: Vector
 m_bAnimGraphUpdateEnabled: bool
 m_flCapsuleRadius: float32
 m_nAddDecal: int32
 m_iAccountID: uint32
 m_bvDisabledHitGroups: uint32[1]
 m_iClip2: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_OriginalOwnerXuidHigh: uint32
 m_nFireSequenceStartTimeChange: int32
 m_flFadeScale: float32
 m_fAccuracyPenalty: float32
 m_iOriginalTeamNumber: int32
 m_usSolidFlags: uint8
 m_nGlowRangeMin: int32
 m_bFlashing: bool
 CRenderComponent: CRenderComponent
 m_flNextSecondaryAttackTickRatio: float32
 m_MoveCollide: MoveCollide_t
 m_CollisionGroup: uint8
 m_szCustomName: char[161]
 m_nFallbackStatTrak: int32
 m_iIronSightMode: int32
 m_bIsHauledBack: bool
 m_vecMaxs: Vector
 m_nSurroundType: SurroundingBoundsType_t
 m_bEligibleForScreenHighlight: bool
 m_fadeMinDist: float32
 m_flDecalHealHeightRate: float32
 m_iReapplyProvisionParity: int32
 m_nFallbackSeed: int32
 m_ProviderType: attributeprovidertypes_t
 m_nDropTick: GameTick_t
 m_flShadowStrength: float32
 m_iItemIDLow: uint32
 m_flFallbackWear: float32
 m_bPlayerFireEventIsPrimary: bool
 m_nViewModelIndex: uint32
 m_flGlowStartTime: float32
 m_flGlowBackfaceMult: float32
 m_iItemDefinitionIndex: uint16
 m_weaponMode: CSWeaponMode
 m_ubInterpolationFrame: uint8
 m_flElasticity: float32
 m_clrRender: Color
 m_flDecalHealBloodRate: float32
 m_iInventoryPosition: uint32
 m_nNextPrimaryAttackTick: GameTick_t
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flNavIgnoreUntilTime: GameTime_t
 m_nEnablePhysics: uint8
 m_vLookTargetPosition: Vector
 m_bSilencerOn: bool
 m_iGlowType: int32
 m_nObjectCulling: uint8
 m_flFireSequenceStartTime: float32
 m_flPostponeFireReadyTime: GameTime_t
 m_bReloadVisuallyComplete: bool
 m_fEffects: uint32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_bClientRagdoll: bool
 m_OriginalOwnerXuidLow: uint32
 m_bInReload: bool
 m_iClip1: int32
 m_LightGroup: CUtlStringToken
 m_vCapsuleCenter2: Vector
 m_vecForce: Vector
 m_flRecoilIndex: float32
 m_nRenderMode: RenderMode_t
 m_bRenderToCubemaps: bool
 m_vecMins: Vector
 m_iItemIDHigh: uint32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
 m_iState: WeaponState_t
 m_nNextThinkTick: GameTick_t
 m_nSubclassID: CUtlStringToken
 m_nInteractsAs: uint64
 m_iGlowTeam: int32
 m_nFallbackPaintKit: int32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_nForceBone: int32
 m_bInitialized: bool
 m_flSimulationTime: float32
 m_iTeamNum: uint8
 m_bAnimatedEveryTick: bool
 m_nHierarchyId: uint16
 m_fadeMaxDist: float32
 m_flAnimTime: float32
 m_MoveType: MoveType_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_fLastShotTime: GameTime_t
 m_iNumEmptyAttacks: int32
 m_vecSpecifiedSurroundingMins: Vector
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vDecalPosition: Vector
 m_hOuter: CHandle< CBaseEntity >
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bInitiallyPopulateInterpHistory: bool
 m_iEntityQuality: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nInteractsExclude: uint64
 m_nCollisionGroup: uint8
 m_triggerBloat: uint8
 m_flGlowTime: float32
 m_nRenderFX: RenderFx_t
 m_nInteractsWith: uint64
 m_nSolidType: SolidType_t
 m_nGlowRange: int32
 m_nNextSecondaryAttackTick: GameTick_t
 m_nOwnerId: uint32
 m_iEntityLevel: uint32
 m_flCreateTime: GameTime_t
 m_nCollisionFunctionMask: uint8
 m_bShouldAnimateDuringGameplayPause: bool
 m_iRecoilIndex: int32
 m_flNextPrimaryAttackTickRatio: float32
 CBodyComponent: CBodyComponent
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
 m_bClientSideRagdoll: bool
 m_glowColorOverride: Color
 m_pReserveAmmo: int32[2]

42 CCSGO_TeamSelectTerroristPosition
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_bInitialized: bool
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
 m_flSimulationTime: float32
 CBodyComponent: CBodyComponent
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
 m_ubInterpolationFrame: uint8
 m_flElasticity: float32
 m_bSimulatedEveryTick: bool
 m_nSubclassID: CUtlStringToken
 m_nVariant: int32
 m_iItemIDHigh: uint32
 m_iInventoryPosition: uint32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveType: MoveType_t
 m_flCreateTime: GameTime_t
 m_iEntityQuality: int32
 m_szCustomName: char[161]
 m_flAnimTime: float32
 m_sWeaponName: CUtlString
 m_MoveCollide: MoveCollide_t
 m_iItemDefinitionIndex: uint16
 m_iItemIDLow: uint32
 m_bAnimatedEveryTick: bool
 m_nRandom: int32
 m_xuid: uint64
 m_iAccountID: uint32
 m_iTeamNum: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flNavIgnoreUntilTime: GameTime_t
 m_nOrdinal: int32
 m_iEntityLevel: uint32

90 CFootstepControl
 m_fEffects: uint32
 m_bRenderToCubemaps: bool
 m_bDisabled: bool
 CBodyComponent: CBodyComponent
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
 m_flNavIgnoreUntilTime: GameTime_t
 m_nCollisionGroup: uint8
 m_nGlowRangeMin: int32
 m_flFadeScale: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_vDecalForwardAxis: Vector
 m_nSubclassID: CUtlStringToken
 m_nEntityId: uint32
 m_nOwnerId: uint32
 m_flGlowStartTime: float32
 m_fadeMaxDist: float32
 CRenderComponent: CRenderComponent
 m_flCapsuleRadius: float32
 m_flDecalHealHeightRate: float32
 m_bClientSidePredicted: bool
 m_flSimulationTime: float32
 m_triggerBloat: uint8
 m_CollisionGroup: uint8
 m_vCapsuleCenter2: Vector
 m_fadeMinDist: float32
 m_flShadowStrength: float32
 m_flAnimTime: float32
 m_spawnflags: uint32
 m_nRenderMode: RenderMode_t
 m_flDecalHealBloodRate: float32
 m_source: CUtlSymbolLarge
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nHierarchyId: uint16
 m_flGlowBackfaceMult: float32
 m_flCreateTime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_bAnimatedEveryTick: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_bFlashing: bool
 m_bEligibleForScreenHighlight: bool
 m_destination: CUtlSymbolLarge
 m_vecMins: Vector
 m_nObjectCulling: uint8
 m_MoveCollide: MoveCollide_t
 m_MoveType: MoveType_t
 m_clrRender: Color
 m_nInteractsAs: uint64
 m_nCollisionFunctionMask: uint8
 m_vecMaxs: Vector
 m_usSolidFlags: uint8
 m_nGlowRange: int32
 m_flGlowTime: float32
 m_nAddDecal: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bSimulatedEveryTick: bool
 m_nRenderFX: RenderFx_t
 m_nInteractsWith: uint64
 m_nInteractsExclude: uint64
 m_vecSpecifiedSurroundingMins: Vector
 m_vCapsuleCenter1: Vector
 m_iGlowTeam: int32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_iGlowType: int32
 m_vDecalPosition: Vector
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSolidType: SolidType_t
 m_glowColorOverride: Color
 m_bvDisabledHitGroups: uint32[1]
 m_iTeamNum: uint8
 m_LightGroup: CUtlStringToken
 m_nSurroundType: SurroundingBoundsType_t
 m_flElasticity: float32
 m_nEnablePhysics: uint8

140 CPhysPropAmmoBox
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nEnablePhysics: uint8
 m_nAddDecal: int32
 m_bInitiallyPopulateInterpHistory: bool
 m_bAnimGraphUpdateEnabled: bool
 m_MoveType: MoveType_t
 m_nInteractsExclude: uint64
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nGlowRange: int32
 m_flGlowBackfaceMult: float32
 m_flFadeScale: float32
 m_spawnflags: uint32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nOwnerId: uint32
 m_bAnimatedEveryTick: bool
 m_nRenderMode: RenderMode_t
 m_vecSpecifiedSurroundingMins: Vector
 m_bFlashing: bool
 m_noGhostCollision: bool
 m_iTeamNum: uint8
 m_vDecalForwardAxis: Vector
 m_bClientRagdoll: bool
 m_usSolidFlags: uint8
 m_flCapsuleRadius: float32
 m_fadeMaxDist: float32
 m_flShadowStrength: float32
 m_flDecalHealHeightRate: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 CRenderComponent: CRenderComponent
 m_ubInterpolationFrame: uint8
 m_vCapsuleCenter2: Vector
 m_bSimulatedEveryTick: bool
 m_nCollisionFunctionMask: uint8
 m_vecMins: Vector
 m_nSolidType: SolidType_t
 m_iGlowTeam: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flCreateTime: GameTime_t
 m_nEntityId: uint32
 m_iGlowType: int32
 m_nGlowRangeMin: int32
 m_bAwake: bool
 m_MoveCollide: MoveCollide_t
 m_nSubclassID: CUtlStringToken
 m_bRenderToCubemaps: bool
 m_triggerBloat: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_CollisionGroup: uint8
 m_bShouldAnimateDuringGameplayPause: bool
 m_bClientSideRagdoll: bool
 m_nRenderFX: RenderFx_t
 m_nCollisionGroup: uint8
 m_flGlowStartTime: float32
 m_bEligibleForScreenHighlight: bool
 m_vDecalPosition: Vector
 m_flSimulationTime: float32
 m_clrRender: Color
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nHierarchyId: uint16
 m_vecMaxs: Vector
 m_bvDisabledHitGroups: uint32[1]
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_LightGroup: CUtlStringToken
 m_nInteractsWith: uint64
 m_flGlowTime: float32
 m_nObjectCulling: uint8
 m_vecForce: Vector
 m_flNavIgnoreUntilTime: GameTime_t
 m_nInteractsAs: uint64
 CBodyComponent: CBodyComponent
  m_nResetEventsParity: int32
  m_nOutsideWorld: uint16
  m_hierarchyAttachName: CUtlStringToken
  m_MeshGroupMask: uint64
  m_vecY: CNetworkedQuantizedFloat
  m_nNewSequenceParity: int32
  m_nRandomSeedOffset: int32
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_name: CUtlStringToken
  m_bUseParentRenderBounds: bool
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_flPrevCycle: float32
  m_nIdealMotionType: int8
  m_angRotation: QAngle
  m_flLastTeleportTime: float32
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_cellY: uint16
  m_flScale: float32
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bIsAnimationEnabled: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_materialGroup: CUtlStringToken
  m_nHitboxSet: uint8
  m_flWeight: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hParent: CGameSceneNodeHandle
  m_hSequence: HSequence
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nBoolVariablesCount: int32
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_vecX: CNetworkedQuantizedFloat
  m_bClientSideAnimation: bool
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_bClientClothCreationSuppressed: bool
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_flCycle: float32
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
 m_flElasticity: float32
 m_vCapsuleCenter1: Vector
 m_glowColorOverride: Color
 m_fadeMinDist: float32
 m_flDecalHealBloodRate: float32
 m_nForceBone: int32

157 CPostProcessingVolume
 m_nInteractsExclude: uint64
 m_nGlowRange: int32
 m_nObjectCulling: uint8
 m_MoveCollide: MoveCollide_t
 m_ubInterpolationFrame: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flGlowTime: float32
 m_vDecalPosition: Vector
 m_bMaster: bool
 m_flRate: float32
 m_bvDisabledHitGroups: uint32[1]
 m_flElasticity: float32
 m_nHierarchyId: uint16
 m_nRenderFX: RenderFx_t
 m_nGlowRangeMin: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bDisabled: bool
 m_flTonemapPercentTarget: float32
 m_flTonemapPercentBrightPixels: float32
 m_nSubclassID: CUtlStringToken
 m_bAnimatedEveryTick: bool
 m_flExposureCompensation: float32
 m_vCapsuleCenter2: Vector
 m_flGlowStartTime: float32
 m_vCapsuleCenter1: Vector
 m_flDecalHealBloodRate: float32
 m_flDecalHealHeightRate: float32
 m_flTonemapMinAvgLum: float32
 m_flSimulationTime: float32
 m_clrRender: Color
 m_flTonemapEVSmoothingRange: float32
 m_nEntityId: uint32
 m_nSurroundType: SurroundingBoundsType_t
 m_nCollisionFunctionMask: uint8
 m_CollisionGroup: uint8
 m_glowColorOverride: Color
 m_flFadeDuration: float32
 m_flAnimTime: float32
 m_nRenderMode: RenderMode_t
 m_nCollisionGroup: uint8
 m_usSolidFlags: uint8
 m_nSolidType: SolidType_t
 m_flShadowStrength: float32
 m_hPostSettings: CStrongHandle< InfoForResourceTypeCPostProcessingResource >
 m_MoveType: MoveType_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_flCapsuleRadius: float32
 m_vDecalForwardAxis: Vector
 CBodyComponent: CBodyComponent
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsAs: uint64
 m_iGlowType: int32
 m_bExposureControl: bool
 m_iTeamNum: uint8
 m_fEffects: uint32
 m_iGlowTeam: int32
 m_nAddDecal: int32
 m_flMaxLogExposure: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_spawnflags: uint32
 m_bFlashing: bool
 m_flMinExposure: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flCreateTime: GameTime_t
 m_triggerBloat: uint8
 m_flFadeScale: float32
 m_flExposureFadeSpeedDown: float32
 m_bRenderToCubemaps: bool
 m_vecMins: Vector
 m_bEligibleForScreenHighlight: bool
 m_fadeMinDist: float32
 m_bClientSidePredicted: bool
 m_nOwnerId: uint32
 m_nEnablePhysics: uint8
 m_fadeMaxDist: float32
 m_flMinLogExposure: float32
 m_LightGroup: CUtlStringToken
 m_flGlowBackfaceMult: float32
 m_vecMaxs: Vector
 m_vecSpecifiedSurroundingMins: Vector
 m_flMaxExposure: float32
 m_flExposureFadeSpeedUp: float32
 CRenderComponent: CRenderComponent
 m_bSimulatedEveryTick: bool
 m_nInteractsWith: uint64

60 CDecoyProjectile
 m_clrRender: Color
 m_bRenderToCubemaps: bool
 m_nSolidType: SolidType_t
 m_bClientSideRagdoll: bool
 m_ubInterpolationFrame: uint8
 m_iTeamNum: uint8
 m_bSimulatedEveryTick: bool
 m_nRenderMode: RenderMode_t
 m_vecSpecifiedSurroundingMaxs: Vector
 CRenderComponent: CRenderComponent
 m_nExplodeEffectTickBegin: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_vecMaxs: Vector
 m_CollisionGroup: uint8
 m_iGlowType: int32
 m_vDecalPosition: Vector
 m_nExplodeEffectIndex: CStrongHandle< InfoForResourceTypeIParticleSystemDefinition >
 m_nInteractsWith: uint64
 m_flDecalHealBloodRate: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_DmgRadius: float32
 m_bFlashing: bool
 m_flGlowTime: float32
 m_bEligibleForScreenHighlight: bool
 m_bvDisabledHitGroups: uint32[1]
 m_flNavIgnoreUntilTime: GameTime_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vCapsuleCenter1: Vector
 m_flFadeScale: float32
 m_nBounces: int32
 m_nCollisionGroup: uint8
 m_bAnimGraphUpdateEnabled: bool
 m_hThrower: CHandle< CBaseEntity >
 m_vecExplodeEffectOrigin: Vector
 m_vecZ: CNetworkedQuantizedFloat
 m_nInteractsExclude: uint64
 m_vecMins: Vector
 m_iGlowTeam: int32
 m_flGlowBackfaceMult: float32
 m_nSubclassID: CUtlStringToken
 m_fEffects: uint32
 m_nForceBone: int32
 m_vecForce: Vector
 m_bClientRagdoll: bool
 m_nRenderFX: RenderFx_t
 m_triggerBloat: uint8
 m_flCapsuleRadius: float32
 m_nAddDecal: int32
 m_vDecalForwardAxis: Vector
 m_nHierarchyId: uint16
 m_nEnablePhysics: uint8
 m_glowColorOverride: Color
 m_vInitialVelocity: Vector
 m_flCreateTime: GameTime_t
 m_vecSpecifiedSurroundingMins: Vector
 m_flDamage: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool
 m_nGlowRangeMin: int32
 m_vLookTargetPosition: Vector
 m_fadeMinDist: float32
 m_bIsLive: bool
 m_flSimulationTime: float32
 CBodyComponent: CBodyComponent
  m_nHitboxSet: uint8
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nBoolVariablesCount: int32
  m_name: CUtlStringToken
  m_MeshGroupMask: uint64
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_vecZ: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_flScale: float32
  m_bClientClothCreationSuppressed: bool
  m_cellY: uint16
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_nRandomSeedOffset: int32
  m_vecX: CNetworkedQuantizedFloat
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_bUseParentRenderBounds: bool
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_cellZ: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_hierarchyAttachName: CUtlStringToken
  m_materialGroup: CUtlStringToken
  m_bClientSideAnimation: bool
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flLastTeleportTime: float32
  m_angRotation: QAngle
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_cellX: uint16
  m_flWeight: CNetworkedQuantizedFloat
 m_MoveType: MoveType_t
 m_vCapsuleCenter2: Vector
 m_nGlowRange: int32
 m_fadeMaxDist: float32
 m_flShadowStrength: float32
 m_nObjectCulling: uint8
 m_fFlags: uint32
 m_flElasticity: float32
 m_nInteractsAs: uint64
 m_nSurroundType: SurroundingBoundsType_t
 m_flGlowStartTime: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_flDetonateTime: GameTime_t
 m_MoveCollide: MoveCollide_t
 m_nCollisionFunctionMask: uint8
 m_usSolidFlags: uint8
 m_vecX: CNetworkedQuantizedFloat
 m_vecY: CNetworkedQuantizedFloat
 m_LightGroup: CUtlStringToken
 m_nEntityId: uint32
 m_nOwnerId: uint32
 m_flDecalHealHeightRate: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >

55 CCSTeam
 m_numMapVictories: int32
 m_nGGLeaderSlot_T: CPlayerSlot
 m_bSurrendered: bool
 m_aPlayers: CNetworkUtlVectorBase< CHandle< CBasePlayerController > >
 m_iScore: int32
 m_scoreFirstHalf: int32
 m_scoreOvertime: int32
 m_iClanID: uint32
 m_iTeamNum: uint8
 m_szTeamname: char[129]
 m_szTeamMatchStat: char[512]
 m_scoreSecondHalf: int32
 m_aPawns: CNetworkUtlVectorBase< CHandle< CBasePlayerPawn > >
 m_szTeamFlagImage: char[8]
 m_szTeamLogoImage: char[8]
 m_nGGLeaderSlot_CT: CPlayerSlot
 m_szClanTeamname: char[129]

100 CGameRulesProxy
 

184 CSprite
 m_fadeMaxDist: float32
 m_flBrightnessDuration: float32
 m_flGlowProxySize: float32
 m_bAnimatedEveryTick: bool
 m_nInteractsWith: uint64
 m_flGlowStartTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_triggerBloat: uint8
 m_flFadeScale: float32
 m_nInteractsAs: uint64
 m_iGlowType: int32
 m_vDecalPosition: Vector
 m_flSpriteScale: float32
 m_bvDisabledHitGroups: uint32[1]
 m_MoveType: MoveType_t
 m_flElasticity: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_hAttachedToEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_nOwnerId: uint32
 m_flGlowBackfaceMult: float32
 m_flGlowTime: float32
 m_vDecalForwardAxis: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_vecSpecifiedSurroundingMins: Vector
 m_nGlowRange: int32
 m_glowColorOverride: Color
 m_MoveCollide: MoveCollide_t
 m_nEntityId: uint32
 m_nHierarchyId: uint16
 m_nCollisionFunctionMask: uint8
 m_iGlowTeam: int32
 m_bEligibleForScreenHighlight: bool
 m_flSimulationTime: float32
 m_flCreateTime: GameTime_t
 m_nRenderFX: RenderFx_t
 m_nSurroundType: SurroundingBoundsType_t
 m_nAttachment: AttachmentHandle_t
 m_iTeamNum: uint8
 m_bRenderToCubemaps: bool
 m_hSpriteMaterial: CStrongHandle< InfoForResourceTypeIMaterial2 >
 m_flHDRColorScale: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_bFlashing: bool
 m_bWorldSpaceScale: bool
 m_nEnablePhysics: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flCapsuleRadius: float32
 m_nBrightness: uint32
 m_bSimulatedEveryTick: bool
 m_nRenderMode: RenderMode_t
 m_CollisionGroup: uint8
 m_nSubclassID: CUtlStringToken
 m_vCapsuleCenter1: Vector
 m_fadeMinDist: float32
 m_nObjectCulling: uint8
 m_nAddDecal: int32
 m_flDecalHealBloodRate: float32
 m_flScaleDuration: float32
 m_flAnimTime: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nInteractsExclude: uint64
 CRenderComponent: CRenderComponent
 m_nSolidType: SolidType_t
 m_nGlowRangeMin: int32
 m_flShadowStrength: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_LightGroup: CUtlStringToken
 m_vecMins: Vector
 m_nCollisionGroup: uint8
 m_flSpriteFramerate: float32
 m_flFrame: float32
 m_vecMaxs: Vector
 m_usSolidFlags: uint8
 m_vCapsuleCenter2: Vector
 m_flDecalHealHeightRate: float32
 CBodyComponent: CBodyComponent
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
 m_ubInterpolationFrame: uint8
 m_clrRender: Color

21 CBombTarget
 m_flAnimTime: float32
 m_fEffects: uint32
 m_bDisabled: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nRenderFX: RenderFx_t
 m_vecMins: Vector
 m_vCapsuleCenter1: Vector
 m_MoveType: MoveType_t
 m_nRenderMode: RenderMode_t
 m_flCapsuleRadius: float32
 m_fadeMaxDist: float32
 m_flDecalHealBloodRate: float32
 m_clrRender: Color
 m_nInteractsAs: uint64
 m_nInteractsExclude: uint64
 m_vCapsuleCenter2: Vector
 m_iGlowTeam: int32
 m_flGlowStartTime: float32
 m_bvDisabledHitGroups: uint32[1]
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nEntityId: uint32
 m_vecSpecifiedSurroundingMins: Vector
 m_flGlowTime: float32
 m_nAddDecal: int32
 m_flSimulationTime: float32
 m_spawnflags: uint32
 m_nInteractsWith: uint64
 m_nHierarchyId: uint16
 m_CollisionGroup: uint8
 m_nSubclassID: CUtlStringToken
 m_bRenderToCubemaps: bool
 m_vecSpecifiedSurroundingMaxs: Vector
 m_iGlowType: int32
 m_nGlowRangeMin: int32
 CBodyComponent: CBodyComponent
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_name: CUtlStringToken
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
 m_nOwnerId: uint32
 m_flGlowBackfaceMult: float32
 m_flElasticity: float32
 m_vecMaxs: Vector
 m_glowColorOverride: Color
 m_flFadeScale: float32
 m_nCollisionFunctionMask: uint8
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 CRenderComponent: CRenderComponent
 m_flCreateTime: GameTime_t
 m_iTeamNum: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flNavIgnoreUntilTime: GameTime_t
 m_fadeMinDist: float32
 m_ubInterpolationFrame: uint8
 m_nCollisionGroup: uint8
 m_flShadowStrength: float32
 m_vDecalForwardAxis: Vector
 m_bClientSidePredicted: bool
 m_LightGroup: CUtlStringToken
 m_triggerBloat: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_bEligibleForScreenHighlight: bool
 m_nObjectCulling: uint8
 m_MoveCollide: MoveCollide_t
 m_bFlashing: bool
 m_flDecalHealHeightRate: float32
 m_bBombPlantedHere: bool
 m_vDecalPosition: Vector
 m_bSimulatedEveryTick: bool
 m_usSolidFlags: uint8
 m_nSolidType: SolidType_t
 m_nEnablePhysics: uint8
 m_nGlowRange: int32

163 CRagdollProp
 m_fEffects: uint32
 m_vCapsuleCenter2: Vector
 m_ragAngles: CNetworkUtlVectorBase< QAngle >
 m_nOwnerId: uint32
 m_flShadowStrength: float32
 CRenderComponent: CRenderComponent
 m_bRenderToCubemaps: bool
 m_nHierarchyId: uint16
 m_vecSpecifiedSurroundingMaxs: Vector
 m_glowColorOverride: Color
 m_bvDisabledHitGroups: uint32[1]
 m_iGlowType: int32
 m_nGlowRangeMin: int32
 m_bEligibleForScreenHighlight: bool
 m_flGlowBackfaceMult: float32
 m_bAnimGraphUpdateEnabled: bool
 m_flElasticity: float32
 m_nRenderFX: RenderFx_t
 m_nCollisionGroup: uint8
 m_triggerBloat: uint8
 m_flFadeScale: float32
 m_iGlowTeam: int32
 m_vecForce: Vector
 m_bSimulatedEveryTick: bool
 m_vecSpecifiedSurroundingMins: Vector
 m_fadeMinDist: float32
 m_nObjectCulling: uint8
 m_flDecalHealBloodRate: float32
 m_flBlendWeight: float32
 m_flAnimTime: float32
 m_MoveCollide: MoveCollide_t
 m_nInteractsAs: uint64
 m_nEnablePhysics: uint8
 m_nForceBone: int32
 m_flGlowTime: float32
 m_flSimulationTime: float32
 m_iTeamNum: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_LightGroup: CUtlStringToken
 m_nSolidType: SolidType_t
 m_CollisionGroup: uint8
 m_vCapsuleCenter1: Vector
 m_flGlowStartTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bAnimatedEveryTick: bool
 m_clrRender: Color
 m_nInteractsWith: uint64
 m_vecMaxs: Vector
 m_bClientRagdoll: bool
 m_ubInterpolationFrame: uint8
 m_flCapsuleRadius: float32
 m_fadeMaxDist: float32
 m_hRagdollSource: CHandle< CBaseEntity >
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flNavIgnoreUntilTime: GameTime_t
 m_nCollisionFunctionMask: uint8
 m_bInitiallyPopulateInterpHistory: bool
 m_nAddDecal: int32
 m_vDecalForwardAxis: Vector
 m_bShouldAnimateDuringGameplayPause: bool
 m_nSubclassID: CUtlStringToken
 m_flCreateTime: GameTime_t
 m_usSolidFlags: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_bFlashing: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_bClientSideRagdoll: bool
 m_nRenderMode: RenderMode_t
 m_nGlowRange: int32
 m_flDecalHealHeightRate: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 CBodyComponent: CBodyComponent
  m_nBoolVariablesCount: int32
  m_nRandomSeedOffset: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nOwnerOnlyBoolVariablesCount: int32
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
 m_nInteractsExclude: uint64
 m_ragPos: CNetworkUtlVectorBase< Vector >
 m_MoveType: MoveType_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nEntityId: uint32
 m_vecMins: Vector
 m_vDecalPosition: Vector

175 CSoundAreaEntityOrientedBox
 m_flAnimTime: float32
 m_flSimulationTime: float32
 m_iTeamNum: uint8
 m_flElasticity: float32
 m_bSimulatedEveryTick: bool
 m_vPos: Vector
 m_vMax: Vector
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_MoveType: MoveType_t
 m_flCreateTime: GameTime_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_bDisabled: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_ubInterpolationFrame: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vMin: Vector
 m_iszSoundAreaType: CUtlSymbolLarge
 CBodyComponent: CBodyComponent
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
 m_MoveCollide: MoveCollide_t
 m_nSubclassID: CUtlStringToken
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool

179 CSoundOpvarSetOBBWindEntity
 m_ubInterpolationFrame: uint8
 m_iTeamNum: uint8
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_iszOperatorName: CUtlSymbolLarge
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_MoveType: MoveType_t
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flElasticity: float32
 m_flAnimTime: float32
 CBodyComponent: CBodyComponent
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bSimulatedEveryTick: bool
 m_iszOpvarName: CUtlSymbolLarge
 m_bUseAutoCompare: bool
 m_flSimulationTime: float32
 m_nSubclassID: CUtlStringToken
 m_iszStackName: CUtlSymbolLarge
 m_iOpvarIndex: int32
 m_MoveCollide: MoveCollide_t
 m_flCreateTime: GameTime_t

189 CTeam
 m_iTeamNum: uint8
 m_aPlayers: CNetworkUtlVectorBase< CHandle< CBasePlayerController > >
 m_aPawns: CNetworkUtlVectorBase< CHandle< CBasePlayerPawn > >
 m_iScore: int32
 m_szTeamname: char[129]

142 CPhysPropRadarJammer
 m_nCollisionGroup: uint8
 m_vecMins: Vector
 m_iTeamNum: uint8
 m_LightGroup: CUtlStringToken
 m_vecSpecifiedSurroundingMaxs: Vector
 m_glowColorOverride: Color
 m_flGlowBackfaceMult: float32
 m_vDecalPosition: Vector
 m_nInteractsAs: uint64
 m_nInteractsWith: uint64
 m_flNavIgnoreUntilTime: GameTime_t
 m_nGlowRangeMin: int32
 CBodyComponent: CBodyComponent
  m_nRandomSeedOffset: int32
  m_name: CUtlStringToken
  m_bUseParentRenderBounds: bool
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_flPrevCycle: float32
  m_nIdealMotionType: int8
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_angRotation: QAngle
  m_flLastTeleportTime: float32
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_cellY: uint16
  m_flScale: float32
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_flWeight: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hParent: CGameSceneNodeHandle
  m_hSequence: HSequence
  m_materialGroup: CUtlStringToken
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nBoolVariablesCount: int32
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bClientSideAnimation: bool
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_flCycle: float32
  m_bClientClothCreationSuppressed: bool
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nResetEventsParity: int32
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_MeshGroupMask: uint64
  m_nNewSequenceParity: int32
 m_MoveCollide: MoveCollide_t
 m_usSolidFlags: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_hOwner: CHandle< CBaseEntity >
  m_Transforms: CNetworkUtlVectorBase< CTransform >
 m_flCreateTime: GameTime_t
 m_clrRender: Color
 m_vecMaxs: Vector
 m_flCapsuleRadius: float32
 m_bFlashing: bool
 m_bClientRagdoll: bool
 m_bvDisabledHitGroups: uint32[1]
 m_MoveType: MoveType_t
 m_nRenderFX: RenderFx_t
 m_triggerBloat: uint8
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_ubInterpolationFrame: uint8
 m_nGlowRange: int32
 m_vCapsuleCenter1: Vector
 m_iGlowTeam: int32
 m_nHierarchyId: uint16
 m_nSurroundType: SurroundingBoundsType_t
 m_nEnablePhysics: uint8
 m_flElasticity: float32
 m_nOwnerId: uint32
 m_bAwake: bool
 m_flFadeScale: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_nCollisionFunctionMask: uint8
 m_vCapsuleCenter2: Vector
 m_flGlowTime: float32
 m_bAnimGraphUpdateEnabled: bool
 m_nForceBone: int32
 m_noGhostCollision: bool
 m_flSimulationTime: float32
 m_nSubclassID: CUtlStringToken
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nObjectCulling: uint8
 m_nAddDecal: int32
 CRenderComponent: CRenderComponent
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nEntityId: uint32
 m_fEffects: uint32
 m_nRenderMode: RenderMode_t
 m_bRenderToCubemaps: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_spawnflags: uint32
 m_vecForce: Vector
 m_nInteractsExclude: uint64
 m_flGlowStartTime: float32
 m_bEligibleForScreenHighlight: bool
 m_flShadowStrength: float32
 m_flDecalHealBloodRate: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_bClientSideRagdoll: bool
 m_bAnimatedEveryTick: bool
 m_CollisionGroup: uint8
 m_iGlowType: int32
 m_fadeMinDist: float32
 m_fadeMaxDist: float32
 m_vDecalForwardAxis: Vector
 m_flDecalHealHeightRate: float32
 m_bSimulatedEveryTick: bool
 m_nSolidType: SolidType_t

230 CWeaponZoneRepulsor
 m_vCapsuleCenter2: Vector
 m_nGlowRangeMin: int32
 m_bInitiallyPopulateInterpHistory: bool
 CBodyComponent: CBodyComponent
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_angRotation: QAngle
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
 m_flFadeScale: float32
 m_ProviderType: attributeprovidertypes_t
 m_nFallbackStatTrak: int32
 m_bEligibleForScreenHighlight: bool
 m_flShadowStrength: float32
 m_flDecalHealBloodRate: float32
 m_nNextThinkTick: GameTick_t
 m_nCollisionGroup: uint8
 m_nEnablePhysics: uint8
 m_nGlowRange: int32
 m_iRecoilIndex: int32
 m_pReserveAmmo: int32[2]
 m_nViewModelIndex: uint32
 m_flElasticity: float32
 m_vLookTargetPosition: Vector
 m_flRecoilIndex: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_MoveCollide: MoveCollide_t
 m_ubInterpolationFrame: uint8
 m_bSimulatedEveryTick: bool
 m_clrRender: Color
 m_iState: WeaponState_t
 m_bInReload: bool
 m_nDropTick: GameTick_t
 m_nNextSecondaryAttackTick: GameTick_t
 m_iClip1: int32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_flAnimTime: float32
 m_weaponMode: CSWeaponMode
 m_iIronSightMode: int32
 m_flSimulationTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSubclassID: CUtlStringToken
 m_bClientRagdoll: bool
 m_iEntityLevel: uint32
 m_bSilencerOn: bool
 m_vecMaxs: Vector
 m_vCapsuleCenter1: Vector
 m_bFlashing: bool
 m_flGlowTime: float32
 CRenderComponent: CRenderComponent
 m_bNeedsBoltAction: bool
 m_nRenderMode: RenderMode_t
 m_nInteractsWith: uint64
 m_nCollisionFunctionMask: uint8
 m_nAddDecal: int32
 m_nForceBone: int32
 m_hOuter: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool
 m_LightGroup: CUtlStringToken
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_iItemIDLow: uint32
 m_bIsHauledBack: bool
 m_flGlowBackfaceMult: float32
 m_fadeMinDist: float32
 m_nObjectCulling: uint8
 m_flFallbackWear: float32
 m_bvDisabledHitGroups: uint32[1]
 m_flNextPrimaryAttackTickRatio: float32
 m_iInventoryPosition: uint32
 m_flDroppedAtTime: GameTime_t
 m_zoomLevel: int32
 m_flNextSecondaryAttackTickRatio: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_flGlowStartTime: float32
 m_bAnimGraphUpdateEnabled: bool
 m_bRenderToCubemaps: bool
 m_fadeMaxDist: float32
 m_iItemIDHigh: uint32
 m_OriginalOwnerXuidHigh: uint32
 m_nFireSequenceStartTimeChange: int32
 m_bReloadVisuallyComplete: bool
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_iTeamNum: uint8
 m_nEntityId: uint32
 m_usSolidFlags: uint8
 m_iReapplyProvisionParity: int32
 m_bInitialized: bool
 m_bBurstMode: bool
 m_iOriginalTeamNumber: int32
 m_iNumEmptyAttacks: int32
 m_vDecalForwardAxis: Vector
 m_bShouldAnimateDuringGameplayPause: bool
 m_iItemDefinitionIndex: uint16
 m_flNavIgnoreUntilTime: GameTime_t
 m_nFallbackSeed: int32
 m_fEffects: uint32
 m_nInteractsAs: uint64
 m_iGlowTeam: int32
 m_iEntityQuality: int32
 m_fLastShotTime: GameTime_t
 m_flCapsuleRadius: float32
 m_szCustomName: char[161]
 m_nOwnerId: uint32
 m_nSolidType: SolidType_t
 m_vDecalPosition: Vector
 m_vecForce: Vector
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
 m_fAccuracyPenalty: float32
 m_iClip2: int32
 m_MoveType: MoveType_t
 m_flCreateTime: GameTime_t
 m_nInteractsExclude: uint64
 m_CollisionGroup: uint8
 m_OriginalOwnerXuidLow: uint32
 m_nFallbackPaintKit: int32
 m_iBurstShotsRemaining: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nRenderFX: RenderFx_t
 m_nHierarchyId: uint16
 m_vecSpecifiedSurroundingMins: Vector
 m_iGlowType: int32
 m_iAccountID: uint32
 m_flFireSequenceStartTime: float32
 m_flPostponeFireReadyTime: GameTime_t
 m_nNextPrimaryAttackTick: GameTick_t
 m_nSurroundType: SurroundingBoundsType_t
 m_bPlayerFireEventIsPrimary: bool
 m_vecMins: Vector
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flDecalHealHeightRate: float32
 m_bClientSideRagdoll: bool
 m_triggerBloat: uint8
 m_glowColorOverride: Color

57 CDangerZoneController
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bSimulatedEveryTick: bool
 m_flWaveEndTimes: GameTime_t[5]
 m_hTheFinalZone: CHandle< CDangerZone >
 m_MoveCollide: MoveCollide_t
 m_flCreateTime: GameTime_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_flFinalExpansionTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nSubclassID: CUtlStringToken
 m_ubInterpolationFrame: uint8
 m_bDangerZoneControllerEnabled: bool
 m_bAnimatedEveryTick: bool
 m_flStartTime: GameTime_t
 m_flSimulationTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_iTeamNum: uint8
 m_flElasticity: float32
 CBodyComponent: CBodyComponent
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
 m_MoveType: MoveType_t
 m_vecEndGameCircleEnd: Vector
 m_DangerZones: CHandle< CDangerZone >[42]
 m_flAnimTime: float32
 m_fEffects: uint32
 m_bMissionControlledExplosions: bool
 m_vecEndGameCircleStart: Vector

193 CTriggerVolume
 m_nInteractsExclude: uint64
 m_bAnimatedEveryTick: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_LightGroup: CUtlStringToken
 m_nEntityId: uint32
 m_flGlowBackfaceMult: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 CRenderComponent: CRenderComponent
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nAddDecal: int32
 m_nGlowRangeMin: int32
 m_nOwnerId: uint32
 m_triggerBloat: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vCapsuleCenter1: Vector
 m_flCapsuleRadius: float32
 m_flDecalHealBloodRate: float32
 m_nInteractsAs: uint64
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_CollisionGroup: uint8
 m_vCapsuleCenter2: Vector
 m_flFadeScale: float32
 m_flCreateTime: GameTime_t
 m_nSubclassID: CUtlStringToken
 m_nCollisionGroup: uint8
 m_nCollisionFunctionMask: uint8
 m_nSolidType: SolidType_t
 m_glowColorOverride: Color
 CBodyComponent: CBodyComponent
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
 m_nHierarchyId: uint16
 m_flShadowStrength: float32
 m_bvDisabledHitGroups: uint32[1]
 m_flNavIgnoreUntilTime: GameTime_t
 m_flGlowStartTime: float32
 m_nObjectCulling: uint8
 m_usSolidFlags: uint8
 m_bSimulatedEveryTick: bool
 m_iGlowTeam: int32
 m_fadeMaxDist: float32
 m_flAnimTime: float32
 m_vecSpecifiedSurroundingMins: Vector
 m_fadeMinDist: float32
 m_nInteractsWith: uint64
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flGlowTime: float32
 m_vDecalPosition: Vector
 m_MoveType: MoveType_t
 m_bFlashing: bool
 m_vDecalForwardAxis: Vector
 m_ubInterpolationFrame: uint8
 m_flElasticity: float32
 m_nRenderMode: RenderMode_t
 m_bRenderToCubemaps: bool
 m_vecMins: Vector
 m_vecMaxs: Vector
 m_nEnablePhysics: uint8
 m_iGlowType: int32
 m_flSimulationTime: float32
 m_flDecalHealHeightRate: float32
 m_nGlowRange: int32
 m_nSurroundType: SurroundingBoundsType_t
 m_iTeamNum: uint8
 m_fEffects: uint32
 m_nRenderFX: RenderFx_t
 m_clrRender: Color
 m_bEligibleForScreenHighlight: bool
 m_MoveCollide: MoveCollide_t

231 CWorld
 m_iTeamNum: uint8
 m_nEntityId: uint32
 m_nCollisionFunctionMask: uint8
 m_usSolidFlags: uint8
 m_vCapsuleCenter1: Vector
 m_glowColorOverride: Color
 m_fadeMinDist: float32
 m_nGlowRange: int32
 m_flGlowTime: float32
 m_bRenderToCubemaps: bool
 m_nHierarchyId: uint16
 m_flCapsuleRadius: float32
 m_iGlowType: int32
 m_flGlowBackfaceMult: float32
 m_MoveCollide: MoveCollide_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecMaxs: Vector
 m_iGlowTeam: int32
 m_nAddDecal: int32
 m_flDecalHealBloodRate: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_nInteractsWith: uint64
 m_nCollisionGroup: uint8
 m_vecMins: Vector
 m_nGlowRangeMin: int32
 m_flFadeScale: float32
 m_vDecalPosition: Vector
 m_flCreateTime: GameTime_t
 m_clrRender: Color
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bAnimatedEveryTick: bool
 m_triggerBloat: uint8
 m_flAnimTime: float32
 m_flElasticity: float32
 m_nInteractsAs: uint64
 m_CollisionGroup: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vCapsuleCenter2: Vector
 m_flDecalHealHeightRate: float32
 m_flSimulationTime: float32
 m_MoveType: MoveType_t
 m_ubInterpolationFrame: uint8
 m_LightGroup: CUtlStringToken
 m_nRenderMode: RenderMode_t
 m_nObjectCulling: uint8
 m_vDecalForwardAxis: Vector
 CBodyComponent: CBodyComponent
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
 m_fEffects: uint32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 CRenderComponent: CRenderComponent
 m_fadeMaxDist: float32
 m_flShadowStrength: float32
 m_nSubclassID: CUtlStringToken
 m_nOwnerId: uint32
 m_bFlashing: bool
 m_bvDisabledHitGroups: uint32[1]
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nRenderFX: RenderFx_t
 m_nInteractsExclude: uint64
 m_nSolidType: SolidType_t
 m_nSurroundType: SurroundingBoundsType_t
 m_flGlowStartTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bSimulatedEveryTick: bool
 m_nEnablePhysics: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_bEligibleForScreenHighlight: bool

151 CPointClientUIWorldPanel
 m_nEnablePhysics: uint8
 m_iGlowTeam: int32
 m_bDisableMipGen: bool
 m_flAnimTime: float32
 m_clrRender: Color
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_hEffectEntity: CHandle< CBaseEntity >
 m_PanelID: CUtlSymbolLarge
 m_flHeight: float32
 m_vCapsuleCenter1: Vector
 m_bSimulatedEveryTick: bool
 m_bFlashing: bool
 m_bEnabled: bool
 m_bFollowPlayerAcrossTeleport: bool
 m_flWidth: float32
 m_flDepthOffset: float32
 m_bOpaque: bool
 m_ubInterpolationFrame: uint8
 m_bNoDepth: bool
 m_unVerticalAlign: uint32
 m_bRenderToCubemaps: bool
 m_nSolidType: SolidType_t
 m_vecSpecifiedSurroundingMins: Vector
 m_nGlowRange: int32
 m_unHorizontalAlign: uint32
 m_vecMins: Vector
 m_nRenderMode: RenderMode_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vCapsuleCenter2: Vector
 m_nAddDecal: int32
 m_unOwnerContext: uint32
 m_bExcludeFromSaveGames: bool
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_nRenderFX: RenderFx_t
 m_nInteractsAs: uint64
 m_bvDisabledHitGroups: uint32[1]
 m_iTeamNum: uint8
 m_flCreateTime: GameTime_t
 m_nInteractsWith: uint64
 m_nHierarchyId: uint16
 m_nSurroundType: SurroundingBoundsType_t
 m_nObjectCulling: uint8
 m_vecCSSClasses: CNetworkUtlVectorBase< CUtlSymbolLarge >
 m_bGrabbable: bool
 m_nSubclassID: CUtlStringToken
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_vecMaxs: Vector
 m_flGlowBackfaceMult: float32
 m_vDecalPosition: Vector
 m_bLit: bool
 m_bOnlyRenderToTexture: bool
 m_flSimulationTime: float32
 m_nInteractsExclude: uint64
 m_nCollisionGroup: uint8
 m_bEligibleForScreenHighlight: bool
 m_vDecalForwardAxis: Vector
 m_DialogXMLName: CUtlSymbolLarge
 m_flDPI: float32
 m_bRenderBackface: bool
 m_MoveType: MoveType_t
 m_LightGroup: CUtlStringToken
 m_triggerBloat: uint8
 m_flGlowTime: float32
 m_flDecalHealBloodRate: float32
 m_flInteractDistance: float32
 CBodyComponent: CBodyComponent
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
 m_flElasticity: float32
 m_iGlowType: int32
 m_nGlowRangeMin: int32
 m_fadeMinDist: float32
 m_fadeMaxDist: float32
 m_flShadowStrength: float32
 m_unOrientation: uint32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nCollisionFunctionMask: uint8
 m_usSolidFlags: uint8
 m_CollisionGroup: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_PanelClassName: CUtlSymbolLarge
 m_bAllowInteractionFromAllSceneWorlds: bool
 CRenderComponent: CRenderComponent
 m_nEntityId: uint32
 m_flNavIgnoreUntilTime: GameTime_t
 m_nOwnerId: uint32
 m_flCapsuleRadius: float32
 m_glowColorOverride: Color
 m_flGlowStartTime: float32
 m_flFadeScale: float32
 m_bIgnoreInput: bool
 m_MoveCollide: MoveCollide_t
 m_nExplicitImageLayout: int32
 m_bUseOffScreenIndicator: bool
 m_flDecalHealHeightRate: float32

210 CWeaponHKP2000
 m_nInteractsExclude: uint64
 m_iGlowTeam: int32
 m_nFallbackSeed: int32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_nViewModelIndex: uint32
 m_flElasticity: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_nFallbackStatTrak: int32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_flGlowBackfaceMult: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_iClip2: int32
 m_pReserveAmmo: int32[2]
 m_iClip1: int32
 m_usSolidFlags: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vCapsuleCenter1: Vector
 m_nInteractsAs: uint64
 m_vecMaxs: Vector
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
 m_flCreateTime: GameTime_t
 m_bClientSideRagdoll: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_flNextPrimaryAttackTickRatio: float32
 m_iBurstShotsRemaining: int32
 m_flPostponeFireReadyTime: GameTime_t
 m_MoveCollide: MoveCollide_t
 m_bAnimatedEveryTick: bool
 m_nAddDecal: int32
 m_ProviderType: attributeprovidertypes_t
 m_iAccountID: uint32
 m_bPlayerFireEventIsPrimary: bool
 m_flSimulationTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_bSimulatedEveryTick: bool
 m_vDecalPosition: Vector
 m_iNumEmptyAttacks: int32
 m_nOwnerId: uint32
 m_flCapsuleRadius: float32
 m_fadeMaxDist: float32
 m_hOuter: CHandle< CBaseEntity >
 m_nSurroundType: SurroundingBoundsType_t
 m_vecSpecifiedSurroundingMins: Vector
 m_iReapplyProvisionParity: int32
 m_bInitialized: bool
 m_nCollisionFunctionMask: uint8
 CRenderComponent: CRenderComponent
 m_iIronSightMode: int32
 m_nNextSecondaryAttackTick: GameTick_t
 m_nRenderMode: RenderMode_t
 m_nCollisionGroup: uint8
 m_vecMins: Vector
 m_bBurstMode: bool
 m_flFadeScale: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_bClientRagdoll: bool
 m_iItemIDLow: uint32
 m_nSubclassID: CUtlStringToken
 m_ubInterpolationFrame: uint8
 m_nRenderFX: RenderFx_t
 m_triggerBloat: uint8
 m_iInventoryPosition: uint32
 m_iRecoilIndex: int32
 m_nDropTick: GameTick_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nHierarchyId: uint16
 m_nNextPrimaryAttackTick: GameTick_t
 m_fadeMinDist: float32
 m_bAnimGraphUpdateEnabled: bool
 m_OriginalOwnerXuidHigh: uint32
 m_LightGroup: CUtlStringToken
 m_weaponMode: CSWeaponMode
 m_nEntityId: uint32
 m_nForceBone: int32
 m_iEntityLevel: uint32
 m_bNeedsBoltAction: bool
 m_bInReload: bool
 m_bIsHauledBack: bool
 m_bvDisabledHitGroups: uint32[1]
 CBodyComponent: CBodyComponent
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
 m_flDecalHealBloodRate: float32
 m_iItemDefinitionIndex: uint16
 m_iItemIDHigh: uint32
 m_MoveType: MoveType_t
 m_flFireSequenceStartTime: float32
 m_nInteractsWith: uint64
 m_OriginalOwnerXuidLow: uint32
 m_nFallbackPaintKit: int32
 m_fAccuracyPenalty: float32
 m_nNextThinkTick: GameTick_t
 m_flAnimTime: float32
 m_flDecalHealHeightRate: float32
 m_flRecoilIndex: float32
 m_zoomLevel: int32
 m_vecForce: Vector
 m_flNextSecondaryAttackTickRatio: float32
 m_fEffects: uint32
 m_vCapsuleCenter2: Vector
 m_flGlowStartTime: float32
 m_flShadowStrength: float32
 m_glowColorOverride: Color
 m_nFireSequenceStartTimeChange: int32
 m_flNavIgnoreUntilTime: GameTime_t
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nEnablePhysics: uint8
 m_vLookTargetPosition: Vector
 m_szCustomName: char[161]
 m_iGlowType: int32
 m_iEntityQuality: int32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_iOriginalTeamNumber: int32
 m_clrRender: Color
 m_bRenderToCubemaps: bool
 m_CollisionGroup: uint8
 m_flGlowTime: float32
 m_bReloadVisuallyComplete: bool
 m_nGlowRange: int32
 m_nGlowRangeMin: int32
 m_bFlashing: bool
 m_vDecalForwardAxis: Vector
 m_bEligibleForScreenHighlight: bool
 m_nObjectCulling: uint8
 m_bSilencerOn: bool
 m_iTeamNum: uint8
 m_nSolidType: SolidType_t
 m_flFallbackWear: float32
 m_fLastShotTime: GameTime_t
 m_iState: WeaponState_t
 m_flDroppedAtTime: GameTime_t

211 CWeaponM249
 m_fAccuracyPenalty: float32
 m_nInteractsAs: uint64
 m_nGlowRange: int32
 m_vecForce: Vector
 m_nFallbackPaintKit: int32
 m_iReapplyProvisionParity: int32
 m_iItemIDHigh: uint32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_flSimulationTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_iTeamNum: uint8
 m_CollisionGroup: uint8
 m_bBurstMode: bool
 m_bNeedsBoltAction: bool
 m_nSolidType: SolidType_t
 m_flShadowStrength: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flRecoilIndex: float32
 CBodyComponent: CBodyComponent
  m_angRotation: QAngle
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
 m_usSolidFlags: uint8
 m_vCapsuleCenter1: Vector
 m_flGlowStartTime: float32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_bvDisabledHitGroups: uint32[1]
 m_nInteractsWith: uint64
 m_nSurroundType: SurroundingBoundsType_t
 m_bFlashing: bool
 m_vDecalForwardAxis: Vector
 m_flDroppedAtTime: GameTime_t
 m_iNumEmptyAttacks: int32
 m_nNextSecondaryAttackTick: GameTick_t
 m_iClip1: int32
 m_flCapsuleRadius: float32
 m_bClientRagdoll: bool
 m_bInitialized: bool
 m_vecMins: Vector
 m_iGlowTeam: int32
 m_iEntityQuality: int32
 m_OriginalOwnerXuidHigh: uint32
 m_flPostponeFireReadyTime: GameTime_t
 m_bClientSideRagdoll: bool
 m_nCollisionFunctionMask: uint8
 m_nObjectCulling: uint8
 m_ProviderType: attributeprovidertypes_t
 m_ubInterpolationFrame: uint8
 m_iInventoryPosition: uint32
 m_nNextThinkTick: GameTick_t
 m_nAddDecal: int32
 m_hOuter: CHandle< CBaseEntity >
 m_zoomLevel: int32
 m_iBurstShotsRemaining: int32
 m_bSimulatedEveryTick: bool
 m_nRenderFX: RenderFx_t
 m_vecSpecifiedSurroundingMins: Vector
 m_fadeMaxDist: float32
 m_flNextPrimaryAttackTickRatio: float32
 m_bRenderToCubemaps: bool
 m_bShouldAnimateDuringGameplayPause: bool
 m_fEffects: uint32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_MoveCollide: MoveCollide_t
 m_nFallbackSeed: int32
 m_flElasticity: float32
 m_nEnablePhysics: uint8
 m_glowColorOverride: Color
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_nHierarchyId: uint16
 m_iOriginalTeamNumber: int32
 m_iIronSightMode: int32
 m_vLookTargetPosition: Vector
 m_flFallbackWear: float32
 m_bInReload: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_flGlowTime: float32
 m_bAnimGraphUpdateEnabled: bool
 m_fadeMinDist: float32
 m_vDecalPosition: Vector
 m_bPlayerFireEventIsPrimary: bool
 m_flAnimTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nEntityId: uint32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_LightGroup: CUtlStringToken
 m_nInteractsExclude: uint64
 m_triggerBloat: uint8
 m_flFadeScale: float32
 m_bAnimatedEveryTick: bool
 m_vecSpecifiedSurroundingMaxs: Vector
 m_szCustomName: char[161]
 m_flNextSecondaryAttackTickRatio: float32
 m_vecMaxs: Vector
 m_iState: WeaponState_t
 CRenderComponent: CRenderComponent
 m_MoveType: MoveType_t
 m_iRecoilIndex: int32
 m_flDecalHealBloodRate: float32
 m_flDecalHealHeightRate: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_fLastShotTime: GameTime_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_iEntityLevel: uint32
 m_iItemIDLow: uint32
 m_nFallbackStatTrak: int32
 m_iAccountID: uint32
 m_flFireSequenceStartTime: float32
 m_flCreateTime: GameTime_t
 m_clrRender: Color
 m_vCapsuleCenter2: Vector
 m_iItemDefinitionIndex: uint16
 m_bSilencerOn: bool
 m_nDropTick: GameTick_t
 m_nOwnerId: uint32
 m_bEligibleForScreenHighlight: bool
 m_nForceBone: int32
 m_bIsHauledBack: bool
 m_nSubclassID: CUtlStringToken
 m_bReloadVisuallyComplete: bool
 m_OriginalOwnerXuidLow: uint32
 m_weaponMode: CSWeaponMode
 m_nNextPrimaryAttackTick: GameTick_t
 m_nViewModelIndex: uint32
 m_pReserveAmmo: int32[2]
 m_nCollisionGroup: uint8
 m_iGlowType: int32
 m_nGlowRangeMin: int32
 m_nFireSequenceStartTimeChange: int32
 m_flGlowBackfaceMult: float32
 m_iClip2: int32

68 CEntityFlame
 m_MoveType: MoveType_t
 m_ubInterpolationFrame: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 CBodyComponent: CBodyComponent
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flElasticity: float32
 m_hEntAttached: CHandle< CBaseEntity >
 m_flAnimTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flCreateTime: GameTime_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_fEffects: uint32
 m_bSimulatedEveryTick: bool
 m_bAnimatedEveryTick: bool
 m_bCheapEffect: bool
 m_flSimulationTime: float32
 m_MoveCollide: MoveCollide_t
 m_nSubclassID: CUtlStringToken
 m_iTeamNum: uint8

71 CEnvCubemapBox
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flCreateTime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_LightGroups: CUtlSymbolLarge
 m_nPriority: int32
 m_MoveType: MoveType_t
 m_iTeamNum: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bCustomCubemapTexture: bool
 m_flInfluenceRadius: float32
 m_nHandshake: int32
 m_flDiffuseScale: float32
 CBodyComponent: CBodyComponent
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
 m_flElasticity: float32
 m_hCubemapTexture: CStrongHandle< InfoForResourceTypeCTextureBase >
 m_bMoveable: bool
 m_flEdgeFadeDist: float32
 m_bDefaultEnvMap: bool
 m_bDefaultSpecEnvMap: bool
 m_flAnimTime: float32
 m_flSimulationTime: float32
 m_bSimulatedEveryTick: bool
 m_bAnimatedEveryTick: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_MoveCollide: MoveCollide_t
 m_vBoxProjectMaxs: Vector
 m_bIndoorCubeMap: bool
 m_nSubclassID: CUtlStringToken
 m_flNavIgnoreUntilTime: GameTime_t
 m_vBoxProjectMins: Vector
 m_bStartDisabled: bool
 m_bEnabled: bool
 m_fEffects: uint32
 m_nEnvCubeMapArrayIndex: int32
 m_vEdgeFadeDists: Vector
 m_bCopyDiffuseFromDefaultCubemap: bool

129 CModelPointEntity
 m_flCreateTime: GameTime_t
 m_nRenderFX: RenderFx_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flGlowTime: float32
 m_bvDisabledHitGroups: uint32[1]
 m_nHierarchyId: uint16
 m_vecMins: Vector
 m_vDecalPosition: Vector
 CRenderComponent: CRenderComponent
 m_MoveType: MoveType_t
 m_nGlowRangeMin: int32
 m_nInteractsExclude: uint64
 m_nSurroundType: SurroundingBoundsType_t
 m_bSimulatedEveryTick: bool
 m_nEntityId: uint32
 m_nOwnerId: uint32
 m_CollisionGroup: uint8
 m_flFadeScale: float32
 m_nCollisionFunctionMask: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_flCapsuleRadius: float32
 m_fadeMinDist: float32
 m_nAddDecal: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_iTeamNum: uint8
 m_bEligibleForScreenHighlight: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_LightGroup: CUtlStringToken
 m_nInteractsAs: uint64
 m_iGlowTeam: int32
 m_nGlowRange: int32
 m_bFlashing: bool
 m_flAnimTime: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool
 m_nInteractsWith: uint64
 m_MoveCollide: MoveCollide_t
 m_nSolidType: SolidType_t
 m_vDecalForwardAxis: Vector
 CBodyComponent: CBodyComponent
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
 m_glowColorOverride: Color
 m_fadeMaxDist: float32
 m_flShadowStrength: float32
 m_nObjectCulling: uint8
 m_fEffects: uint32
 m_clrRender: Color
 m_nCollisionGroup: uint8
 m_nEnablePhysics: uint8
 m_flGlowStartTime: float32
 m_flDecalHealHeightRate: float32
 m_flSimulationTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nSubclassID: CUtlStringToken
 m_vecMaxs: Vector
 m_vCapsuleCenter2: Vector
 m_flGlowBackfaceMult: float32
 m_iGlowType: int32
 m_flDecalHealBloodRate: float32
 m_flElasticity: float32
 m_bRenderToCubemaps: bool
 m_usSolidFlags: uint8
 m_vCapsuleCenter1: Vector
 m_ubInterpolationFrame: uint8
 m_triggerBloat: uint8
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >

136 CPhysBox
 m_flShadowStrength: float32
 m_MoveCollide: MoveCollide_t
 m_flElasticity: float32
 m_flDecalHealHeightRate: float32
 m_nOwnerId: uint32
 m_vDecalForwardAxis: Vector
 m_nInteractsWith: uint64
 m_nEnablePhysics: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_bSimulatedEveryTick: bool
 m_nRenderFX: RenderFx_t
 m_bAnimatedEveryTick: bool
 m_nSolidType: SolidType_t
 m_nSurroundType: SurroundingBoundsType_t
 m_iGlowTeam: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_MoveType: MoveType_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nInteractsExclude: uint64
 m_nCollisionFunctionMask: uint8
 m_flCreateTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_triggerBloat: uint8
 m_flCapsuleRadius: float32
 m_glowColorOverride: Color
 m_bFlashing: bool
 m_fEffects: uint32
 m_LightGroup: CUtlStringToken
 m_ubInterpolationFrame: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flGlowBackfaceMult: float32
 m_nInteractsAs: uint64
 m_vCapsuleCenter2: Vector
 m_flGlowTime: float32
 m_nAddDecal: int32
 CRenderComponent: CRenderComponent
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flNavIgnoreUntilTime: GameTime_t
 m_fadeMinDist: float32
 m_flFadeScale: float32
 m_nObjectCulling: uint8
 CBodyComponent: CBodyComponent
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
 m_bRenderToCubemaps: bool
 m_flGlowStartTime: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vecMins: Vector
 m_flDecalHealBloodRate: float32
 m_iTeamNum: uint8
 m_nHierarchyId: uint16
 m_vCapsuleCenter1: Vector
 m_nGlowRange: int32
 m_fadeMaxDist: float32
 m_flAnimTime: float32
 m_nCollisionGroup: uint8
 m_vecMaxs: Vector
 m_bEligibleForScreenHighlight: bool
 m_flSimulationTime: float32
 m_nSubclassID: CUtlStringToken
 m_usSolidFlags: uint8
 m_CollisionGroup: uint8
 m_nEntityId: uint32
 m_iGlowType: int32
 m_nGlowRangeMin: int32
 m_vDecalPosition: Vector
 m_bvDisabledHitGroups: uint32[1]
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_clrRender: Color

0 CAK47
 m_bInitiallyPopulateInterpHistory: bool
 m_vLookTargetPosition: Vector
 m_flPostponeFireReadyTime: GameTime_t
 m_pReserveAmmo: int32[2]
 m_vCapsuleCenter2: Vector
 m_flDecalHealHeightRate: float32
 m_OriginalOwnerXuidHigh: uint32
 m_iClip2: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_iEntityLevel: uint32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_zoomLevel: int32
 m_iBurstShotsRemaining: int32
 CBodyComponent: CBodyComponent
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_angRotation: QAngle
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
 m_bBurstMode: bool
 m_weaponMode: CSWeaponMode
 m_iIronSightMode: int32
 m_nViewModelIndex: uint32
 m_iTeamNum: uint8
 m_fEffects: uint32
 m_bEligibleForScreenHighlight: bool
 m_nHierarchyId: uint16
 m_triggerBloat: uint8
 m_iOriginalTeamNumber: int32
 m_bNeedsBoltAction: bool
 m_usSolidFlags: uint8
 m_iState: WeaponState_t
 m_nFallbackStatTrak: int32
 m_fLastShotTime: GameTime_t
 m_iRecoilIndex: int32
 m_iItemDefinitionIndex: uint16
 m_iAccountID: uint32
 m_iItemIDHigh: uint32
 m_flFallbackWear: float32
 m_flCapsuleRadius: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_iClip1: int32
 m_iReapplyProvisionParity: int32
 m_vCapsuleCenter1: Vector
 m_nFallbackPaintKit: int32
 m_bSilencerOn: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vDecalForwardAxis: Vector
 m_iGlowTeam: int32
 m_szCustomName: char[161]
 m_iNumEmptyAttacks: int32
 m_hOuter: CHandle< CBaseEntity >
 m_nFireSequenceStartTimeChange: int32
 m_flNavIgnoreUntilTime: GameTime_t
 m_flGlowTime: float32
 m_bFlashing: bool
 m_fadeMinDist: float32
 m_iInventoryPosition: uint32
 m_flFireSequenceStartTime: float32
 m_flRecoilIndex: float32
 m_bReloadVisuallyComplete: bool
 m_nCollisionGroup: uint8
 m_nGlowRange: int32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
 m_bInReload: bool
 m_nNextPrimaryAttackTick: GameTick_t
 m_nInteractsExclude: uint64
 m_vecForce: Vector
 m_nNextSecondaryAttackTick: GameTick_t
 m_flGlowStartTime: float32
 m_bAnimGraphUpdateEnabled: bool
 m_MoveType: MoveType_t
 m_nSolidType: SolidType_t
 m_nEnablePhysics: uint8
 m_iGlowType: int32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_hOwner: CHandle< CBaseEntity >
  m_Transforms: CNetworkUtlVectorBase< CTransform >
 m_bClientRagdoll: bool
 m_flAnimTime: float32
 m_flSimulationTime: float32
 m_flNextSecondaryAttackTickRatio: float32
 m_flCreateTime: GameTime_t
 m_glowColorOverride: Color
 m_nDropTick: GameTick_t
 m_vecSpecifiedSurroundingMins: Vector
 m_fAccuracyPenalty: float32
 m_CollisionGroup: uint8
 m_nGlowRangeMin: int32
 m_fadeMaxDist: float32
 m_nFallbackSeed: int32
 m_bAnimatedEveryTick: bool
 m_vecMaxs: Vector
 m_vecMins: Vector
 m_nSurroundType: SurroundingBoundsType_t
 m_flDecalHealBloodRate: float32
 m_iItemIDLow: uint32
 m_bIsHauledBack: bool
 CRenderComponent: CRenderComponent
 m_bClientSideRagdoll: bool
 m_bSimulatedEveryTick: bool
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flGlowBackfaceMult: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bvDisabledHitGroups: uint32[1]
 m_nRenderFX: RenderFx_t
 m_nInteractsAs: uint64
 m_bInitialized: bool
 m_nNextThinkTick: GameTick_t
 m_flNextPrimaryAttackTickRatio: float32
 m_nOwnerId: uint32
 m_nCollisionFunctionMask: uint8
 m_nInteractsWith: uint64
 m_ProviderType: attributeprovidertypes_t
 m_bPlayerFireEventIsPrimary: bool
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_clrRender: Color
 m_iEntityQuality: int32
 m_nObjectCulling: uint8
 m_OriginalOwnerXuidLow: uint32
 m_bRenderToCubemaps: bool
 m_nEntityId: uint32
 m_nAddDecal: int32
 m_flDroppedAtTime: GameTime_t
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flElasticity: float32
 m_ubInterpolationFrame: uint8
 m_flFadeScale: float32
 m_MoveCollide: MoveCollide_t
 m_nSubclassID: CUtlStringToken
 m_flShadowStrength: float32
 m_vDecalPosition: Vector
 m_nForceBone: int32
 m_nRenderMode: RenderMode_t
 m_LightGroup: CUtlStringToken

32 CColorCorrection
 m_bClientSide: bool
 m_flCurWeight: float32
 m_netlookupFilename: char[512]
 m_flFadeInDuration: float32
 m_flFadeOutDuration: float32
 m_flMaxWeight: float32
 m_bEnabled: bool
 m_bMaster: bool
 m_bExclusive: bool
 m_MinFalloff: float32
 m_MaxFalloff: float32
 CBodyComponent: CBodyComponent
  m_cellX: uint16
  m_cellY: uint16
  m_cellZ: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_nOutsideWorld: uint16

174 CSoundAreaEntityBase
 m_flAnimTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveCollide: MoveCollide_t
 m_flCreateTime: GameTime_t
 m_flElasticity: float32
 m_bSimulatedEveryTick: bool
 m_iszSoundAreaType: CUtlSymbolLarge
 CBodyComponent: CBodyComponent
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
 m_iTeamNum: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_bDisabled: bool
 m_nSubclassID: CUtlStringToken
 m_ubInterpolationFrame: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_flSimulationTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_MoveType: MoveType_t
 m_vPos: Vector

185 CSpriteOriented
 m_vecSpecifiedSurroundingMins: Vector
 m_flGlowStartTime: float32
 m_flAnimTime: float32
 m_clrRender: Color
 m_nInteractsWith: uint64
 m_nInteractsExclude: uint64
 m_nOwnerId: uint32
 CBodyComponent: CBodyComponent
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_flBrightnessDuration: float32
 m_bvDisabledHitGroups: uint32[1]
 m_hAttachedToEntity: CHandle< CBaseEntity >
 m_nBrightness: uint32
 m_flHDRColorScale: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nSolidType: SolidType_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flCapsuleRadius: float32
 m_fadeMinDist: float32
 m_flDecalHealHeightRate: float32
 m_nSubclassID: CUtlStringToken
 m_nRenderMode: RenderMode_t
 m_bRenderToCubemaps: bool
 m_glowColorOverride: Color
 m_flDecalHealBloodRate: float32
 m_ubInterpolationFrame: uint8
 m_vecMins: Vector
 CRenderComponent: CRenderComponent
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bWorldSpaceScale: bool
 m_flCreateTime: GameTime_t
 m_LightGroup: CUtlStringToken
 m_nInteractsAs: uint64
 m_nCollisionGroup: uint8
 m_nEnablePhysics: uint8
 m_MoveCollide: MoveCollide_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_flGlowBackfaceMult: float32
 m_flGlowProxySize: float32
 m_nCollisionFunctionMask: uint8
 m_bEligibleForScreenHighlight: bool
 m_nObjectCulling: uint8
 m_vDecalPosition: Vector
 m_nGlowRangeMin: int32
 m_flFrame: float32
 m_MoveType: MoveType_t
 m_iTeamNum: uint8
 m_nRenderFX: RenderFx_t
 m_nEntityId: uint32
 m_iGlowTeam: int32
 m_flGlowTime: float32
 m_iGlowType: int32
 m_bFlashing: bool
 m_nAddDecal: int32
 m_flSimulationTime: float32
 m_nHierarchyId: uint16
 m_triggerBloat: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_vCapsuleCenter2: Vector
 m_nAttachment: AttachmentHandle_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flElasticity: float32
 m_CollisionGroup: uint8
 m_hSpriteMaterial: CStrongHandle< InfoForResourceTypeIMaterial2 >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flSpriteFramerate: float32
 m_bSimulatedEveryTick: bool
 m_flFadeScale: float32
 m_vCapsuleCenter1: Vector
 m_flSpriteScale: float32
 m_flScaleDuration: float32
 m_flShadowStrength: float32
 m_vDecalForwardAxis: Vector
 m_fEffects: uint32
 m_vecMaxs: Vector
 m_usSolidFlags: uint8
 m_nGlowRange: int32
 m_fadeMaxDist: float32

65 CEconEntity
 m_iAccountID: uint32
 m_nObjectCulling: uint8
 m_flDecalHealHeightRate: float32
 m_nForceBone: int32
 m_bClientRagdoll: bool
 m_OriginalOwnerXuidHigh: uint32
 m_vecForce: Vector
 m_nSurroundType: SurroundingBoundsType_t
 m_flGlowBackfaceMult: float32
 m_flFallbackWear: float32
 m_bSimulatedEveryTick: bool
 m_nRenderMode: RenderMode_t
 m_triggerBloat: uint8
 m_bClientSideRagdoll: bool
 m_bAnimatedEveryTick: bool
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_ProviderType: attributeprovidertypes_t
 m_nFallbackPaintKit: int32
 m_nRenderFX: RenderFx_t
 m_bRenderToCubemaps: bool
 m_fadeMinDist: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_blinktoggle: bool
 m_iReapplyProvisionParity: int32
 m_iItemIDHigh: uint32
 m_nCollisionFunctionMask: uint8
 m_nSolidType: SolidType_t
 m_vecSpecifiedSurroundingMins: Vector
 m_bInitialized: bool
 m_bvDisabledHitGroups: uint32[1]
 m_nOwnerId: uint32
 m_vecMaxs: Vector
 m_flGlowStartTime: float32
 m_nFallbackStatTrak: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveType: MoveType_t
 m_nEnablePhysics: uint8
 m_ubInterpolationFrame: uint8
 m_iEntityLevel: uint32
 m_clrRender: Color
 m_nEntityId: uint32
 m_fEffects: uint32
 m_CollisionGroup: uint8
 m_flDecalHealBloodRate: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_iInventoryPosition: uint32
 m_flAnimTime: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_fadeMaxDist: float32
 m_glowColorOverride: Color
 m_vDecalPosition: Vector
 m_flexWeight: CNetworkUtlVectorBase< float32 >
 m_nGlowRange: int32
 m_flFadeScale: float32
 m_MoveCollide: MoveCollide_t
 m_nCollisionGroup: uint8
 m_nFallbackSeed: int32
 m_nSubclassID: CUtlStringToken
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flNavIgnoreUntilTime: GameTime_t
 m_bInitiallyPopulateInterpHistory: bool
 m_LightGroup: CUtlStringToken
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flShadowStrength: float32
 m_vCapsuleCenter1: Vector
 m_nAddDecal: int32
 m_iItemIDLow: uint32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flElasticity: float32
 m_usSolidFlags: uint8
 m_flSimulationTime: float32
 m_hOuter: CHandle< CBaseEntity >
 m_szCustomName: char[161]
 CRenderComponent: CRenderComponent
 m_nInteractsExclude: uint64
 m_bAnimGraphUpdateEnabled: bool
 m_vLookTargetPosition: Vector
 m_nInteractsWith: uint64
 m_nHierarchyId: uint16
 m_flGlowTime: float32
 m_flCapsuleRadius: float32
 m_iGlowTeam: int32
 m_bEligibleForScreenHighlight: bool
 CBodyComponent: CBodyComponent
  m_nBoolVariablesCount: int32
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_nOutsideWorld: uint16
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
 m_iGlowType: int32
 m_iItemDefinitionIndex: uint16
 m_flCreateTime: GameTime_t
 m_iTeamNum: uint8
 m_nGlowRangeMin: int32
 m_vDecalForwardAxis: Vector
 m_vecMins: Vector
 m_iEntityQuality: int32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_OriginalOwnerXuidLow: uint32
 m_nInteractsAs: uint64
 m_vCapsuleCenter2: Vector
 m_bFlashing: bool

194 CTripWireFire
 m_nForceBone: int32
 m_bPinPulled: bool
 m_bClientRagdoll: bool
 m_iEntityQuality: int32
 m_nNextThinkTick: GameTick_t
 m_bClientSideRagdoll: bool
 m_LightGroup: CUtlStringToken
 m_bShouldAnimateDuringGameplayPause: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_ubInterpolationFrame: uint8
 m_nRenderFX: RenderFx_t
 m_fThrowTime: GameTime_t
 m_nNextSecondaryAttackTick: GameTick_t
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_bIsHeldByPlayer: bool
 m_iClip2: int32
 m_flDecalHealHeightRate: float32
 m_OriginalOwnerXuidHigh: uint32
 m_flPostponeFireReadyTime: GameTime_t
 m_iOriginalTeamNumber: int32
 m_iInventoryPosition: uint32
 m_szCustomName: char[161]
 m_iState: WeaponState_t
 m_flRecoilIndex: float32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vCapsuleCenter1: Vector
 m_bJumpThrow: bool
 m_nInteractsWith: uint64
 m_vecMins: Vector
 m_iGlowType: int32
 m_iItemIDHigh: uint32
 m_nFallbackSeed: int32
 m_iNumEmptyAttacks: int32
 m_flNextSecondaryAttackTickRatio: float32
 m_nNextPrimaryAttackTick: GameTick_t
 m_flShadowStrength: float32
 m_nFireSequenceStartTimeChange: int32
 m_bIsHauledBack: bool
 m_fLastShotTime: GameTime_t
 m_triggerBloat: uint8
 m_nEntityId: uint32
 m_nSolidType: SolidType_t
 m_flDroppedAtTime: GameTime_t
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_vecMaxs: Vector
 m_CollisionGroup: uint8
 m_vDecalForwardAxis: Vector
 m_vLookTargetPosition: Vector
 m_MoveCollide: MoveCollide_t
 m_flCreateTime: GameTime_t
 m_fadeMinDist: float32
 m_bSilencerOn: bool
 m_nSubclassID: CUtlStringToken
 m_iItemIDLow: uint32
 m_nDropTick: GameTick_t
 m_iIronSightMode: int32
 m_nViewModelIndex: uint32
 m_fEffects: uint32
 m_nRenderMode: RenderMode_t
 m_usSolidFlags: uint8
 m_nGlowRange: int32
 m_glowColorOverride: Color
 m_bSimulatedEveryTick: bool
 m_vCapsuleCenter2: Vector
 m_bEligibleForScreenHighlight: bool
 m_flGlowBackfaceMult: float32
 m_vDecalPosition: Vector
 m_iItemDefinitionIndex: uint16
 m_nObjectCulling: uint8
 m_iAccountID: uint32
 m_MoveType: MoveType_t
 m_bRenderToCubemaps: bool
 m_nInteractsAs: uint64
 m_iGlowTeam: int32
 m_bRedraw: bool
 CRenderComponent: CRenderComponent
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nGlowRangeMin: int32
 m_iReapplyProvisionParity: int32
 m_flFireSequenceStartTime: float32
 m_bAnimatedEveryTick: bool
 m_iEntityLevel: uint32
 m_OriginalOwnerXuidLow: uint32
 m_flAnimTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nHierarchyId: uint16
 m_bFlashing: bool
 m_fAccuracyPenalty: float32
 m_bBurstMode: bool
 m_eThrowStatus: EGrenadeThrowState
 m_flThrowStrengthApproach: float32
 m_nEnablePhysics: uint8
 m_flCapsuleRadius: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_bInitialized: bool
 m_pReserveAmmo: int32[2]
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flDecalHealBloodRate: float32
 m_vecForce: Vector
 m_weaponMode: CSWeaponMode
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_flGlowStartTime: float32
 m_bInReload: bool
 m_flNextPrimaryAttackTickRatio: float32
 m_vecSpecifiedSurroundingMins: Vector
 m_bPlayerFireEventIsPrimary: bool
 m_flThrowStrength: float32
 m_clrRender: Color
 m_nCollisionGroup: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_flFadeScale: float32
 m_iClip1: int32
 m_iTeamNum: uint8
 m_flElasticity: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_ProviderType: attributeprovidertypes_t
 m_flFallbackWear: float32
 m_bvDisabledHitGroups: uint32[1]
 m_flSimulationTime: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nFallbackPaintKit: int32
 CBodyComponent: CBodyComponent
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_nBoolVariablesCount: int32
  m_cellZ: uint16
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_nNewSequenceParity: int32
  m_bClientSideAnimation: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_hParent: CGameSceneNodeHandle
  m_nRandomSeedOffset: int32
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_angRotation: QAngle
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
 m_nFallbackStatTrak: int32
 m_fDropTime: GameTime_t
 m_bReloadVisuallyComplete: bool
 m_nAddDecal: int32
 m_bAnimGraphUpdateEnabled: bool
 m_hOuter: CHandle< CBaseEntity >
 m_iRecoilIndex: int32
 m_fadeMaxDist: float32
 m_nInteractsExclude: uint64
 m_nOwnerId: uint32
 m_nCollisionFunctionMask: uint8
 m_flGlowTime: float32

101 CGradientFog
 m_flNavIgnoreUntilTime: GameTime_t
 m_flFogStartHeight: float32
 m_flFarZ: float32
 m_flFogStrength: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool
 m_bStartDisabled: bool
 CBodyComponent: CBodyComponent
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
 m_flFogEndHeight: float32
 m_flElasticity: float32
 m_hGradientFogTexture: CStrongHandle< InfoForResourceTypeCTextureBase >
 m_flFogMaxOpacity: float32
 m_nSubclassID: CUtlStringToken
 m_flCreateTime: GameTime_t
 m_bSimulatedEveryTick: bool
 m_MoveCollide: MoveCollide_t
 m_iTeamNum: uint8
 m_bHeightFogEnabled: bool
 m_flAnimTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_MoveType: MoveType_t
 m_ubInterpolationFrame: uint8
 m_fEffects: uint32
 m_flFogEndDistance: float32
 m_flFogFalloffExponent: float32
 m_bIsEnabled: bool
 m_flSimulationTime: float32
 m_flFogStartDistance: float32
 m_flFogVerticalExponent: float32
 m_fogColor: Color
 m_flFadeTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32

198 CWeaponAug
 m_nHierarchyId: uint16
 m_glowColorOverride: Color
 m_pReserveAmmo: int32[2]
 m_MoveType: MoveType_t
 m_flGlowTime: float32
 m_flShadowStrength: float32
 m_nObjectCulling: uint8
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
 m_iIronSightMode: int32
 m_nSubclassID: CUtlStringToken
 m_iInventoryPosition: uint32
 m_bInReload: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flCreateTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_iEntityLevel: uint32
 m_flRecoilIndex: float32
 m_bSilencerOn: bool
 m_clrRender: Color
 m_flGlowBackfaceMult: float32
 m_flDecalHealBloodRate: float32
 m_ubInterpolationFrame: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_nInteractsExclude: uint64
 m_iState: WeaponState_t
 m_flGlowStartTime: float32
 m_OriginalOwnerXuidLow: uint32
 m_iBurstShotsRemaining: int32
 m_bvDisabledHitGroups: uint32[1]
 m_vLookTargetPosition: Vector
 m_iAccountID: uint32
 m_nFireSequenceStartTimeChange: int32
 m_flSimulationTime: float32
 m_flCapsuleRadius: float32
 m_iItemIDHigh: uint32
 m_iItemIDLow: uint32
 m_nFallbackPaintKit: int32
 m_fAccuracyPenalty: float32
 m_nInteractsWith: uint64
 m_vecSpecifiedSurroundingMins: Vector
 m_vCapsuleCenter1: Vector
 m_bShouldAnimateDuringGameplayPause: bool
 m_zoomLevel: int32
 CBodyComponent: CBodyComponent
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_hParent: CGameSceneNodeHandle
  m_nRandomSeedOffset: int32
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_nNewSequenceParity: int32
  m_bClientSideAnimation: bool
 m_flElasticity: float32
 m_iGlowType: int32
 m_ProviderType: attributeprovidertypes_t
 m_szCustomName: char[161]
 m_flPostponeFireReadyTime: GameTime_t
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_iOriginalTeamNumber: int32
 m_nViewModelIndex: uint32
 m_bClientSideRagdoll: bool
 m_flFireSequenceStartTime: float32
 m_weaponMode: CSWeaponMode
 m_fLastShotTime: GameTime_t
 m_flNextPrimaryAttackTickRatio: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_MoveCollide: MoveCollide_t
 m_iGlowTeam: int32
 m_flDecalHealHeightRate: float32
 m_bInitialized: bool
 m_bReloadVisuallyComplete: bool
 m_bInitiallyPopulateInterpHistory: bool
 m_bBurstMode: bool
 m_nDropTick: GameTick_t
 m_nGlowRange: int32
 m_iItemDefinitionIndex: uint16
 m_flAnimTime: float32
 m_fEffects: uint32
 m_hOuter: CHandle< CBaseEntity >
 m_nFallbackSeed: int32
 m_nFallbackStatTrak: int32
 m_LightGroup: CUtlStringToken
 m_nInteractsAs: uint64
 m_vecForce: Vector
 m_iRecoilIndex: int32
 m_triggerBloat: uint8
 m_nAddDecal: int32
 m_bAnimGraphUpdateEnabled: bool
 m_iEntityQuality: int32
 m_bPlayerFireEventIsPrimary: bool
 m_nOwnerId: uint32
 m_fadeMaxDist: float32
 m_nNextSecondaryAttackTick: GameTick_t
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_iClip1: int32
 m_iTeamNum: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool
 m_nEntityId: uint32
 m_nSurroundType: SurroundingBoundsType_t
 m_flDroppedAtTime: GameTime_t
 m_nNextThinkTick: GameTick_t
 m_nRenderFX: RenderFx_t
 m_vCapsuleCenter2: Vector
 m_nGlowRangeMin: int32
 m_fadeMinDist: float32
 m_flFadeScale: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nNextPrimaryAttackTick: GameTick_t
 m_vecMaxs: Vector
 m_CollisionGroup: uint8
 m_nEnablePhysics: uint8
 m_nForceBone: int32
 m_bClientRagdoll: bool
 m_flFallbackWear: float32
 m_bNeedsBoltAction: bool
 m_nCollisionFunctionMask: uint8
 m_vDecalPosition: Vector
 m_iReapplyProvisionParity: int32
 m_bEligibleForScreenHighlight: bool
 m_OriginalOwnerXuidHigh: uint32
 CRenderComponent: CRenderComponent
 m_flNextSecondaryAttackTickRatio: float32
 m_iClip2: int32
 m_bSimulatedEveryTick: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_usSolidFlags: uint8
 m_nSolidType: SolidType_t
 m_bFlashing: bool
 m_iNumEmptyAttacks: int32
 m_bRenderToCubemaps: bool
 m_nCollisionGroup: uint8
 m_vecMins: Vector
 m_bIsHauledBack: bool
 m_vDecalForwardAxis: Vector

205 CWeaponFamas
 m_bSimulatedEveryTick: bool
 m_usSolidFlags: uint8
 m_CollisionGroup: uint8
 m_flRecoilIndex: float32
 m_bIsHauledBack: bool
 m_iNumEmptyAttacks: int32
 CBodyComponent: CBodyComponent
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
 m_nViewModelIndex: uint32
 m_flDecalHealBloodRate: float32
 m_nSolidType: SolidType_t
 m_fadeMaxDist: float32
 m_nAddDecal: int32
 m_ProviderType: attributeprovidertypes_t
 m_MoveCollide: MoveCollide_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nCollisionFunctionMask: uint8
 m_nEnablePhysics: uint8
 m_flFadeScale: float32
 m_iAccountID: uint32
 m_bSilencerOn: bool
 m_pReserveAmmo: int32[2]
 m_nRenderMode: RenderMode_t
 m_triggerBloat: uint8
 m_fadeMinDist: float32
 m_fEffects: uint32
 m_flDroppedAtTime: GameTime_t
 m_nObjectCulling: uint8
 m_flElasticity: float32
 m_LightGroup: CUtlStringToken
 m_vecSpecifiedSurroundingMins: Vector
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_vDecalForwardAxis: Vector
 m_bInitialized: bool
 m_OriginalOwnerXuidLow: uint32
 m_iState: WeaponState_t
 m_vecMaxs: Vector
 m_flNavIgnoreUntilTime: GameTime_t
 m_bAnimGraphUpdateEnabled: bool
 m_iInventoryPosition: uint32
 m_szCustomName: char[161]
 m_flAnimTime: float32
 m_nSubclassID: CUtlStringToken
 m_flShadowStrength: float32
 m_iItemIDLow: uint32
 m_zoomLevel: int32
 m_iClip1: int32
 m_iItemDefinitionIndex: uint16
 m_clrRender: Color
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nFireSequenceStartTimeChange: int32
 m_nInteractsAs: uint64
 m_iReapplyProvisionParity: int32
 m_hOuter: CHandle< CBaseEntity >
 m_weaponMode: CSWeaponMode
 m_iRecoilIndex: int32
 m_fLastShotTime: GameTime_t
 m_bInitiallyPopulateInterpHistory: bool
 m_bClientRagdoll: bool
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nInteractsWith: uint64
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_nDropTick: GameTick_t
 m_flCreateTime: GameTime_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vCapsuleCenter2: Vector
 m_bShouldAnimateDuringGameplayPause: bool
 m_iBurstShotsRemaining: int32
 m_flSimulationTime: float32
 m_nInteractsExclude: uint64
 m_nCollisionGroup: uint8
 m_glowColorOverride: Color
 m_nForceBone: int32
 m_nNextThinkTick: GameTick_t
 m_flNextPrimaryAttackTickRatio: float32
 m_ubInterpolationFrame: uint8
 m_iIronSightMode: int32
 m_bReloadVisuallyComplete: bool
 m_MoveType: MoveType_t
 m_vecMins: Vector
 m_vCapsuleCenter1: Vector
 m_iGlowTeam: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_OriginalOwnerXuidHigh: uint32
 m_nFallbackStatTrak: int32
 m_bInReload: bool
 m_nNextPrimaryAttackTick: GameTick_t
 m_vecForce: Vector
 m_flFireSequenceStartTime: float32
 m_bPlayerFireEventIsPrimary: bool
 m_flPostponeFireReadyTime: GameTime_t
 m_vLookTargetPosition: Vector
 m_flGlowStartTime: float32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_nNextSecondaryAttackTick: GameTick_t
 m_nSurroundType: SurroundingBoundsType_t
 m_iOriginalTeamNumber: int32
 m_bNeedsBoltAction: bool
 CRenderComponent: CRenderComponent
 m_nGlowRange: int32
 m_flGlowTime: float32
 m_fAccuracyPenalty: float32
 m_iGlowType: int32
 m_nFallbackPaintKit: int32
 m_bBurstMode: bool
 m_bvDisabledHitGroups: uint32[1]
 m_bEligibleForScreenHighlight: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_nFallbackSeed: int32
 m_flFallbackWear: float32
 m_iClip2: int32
 m_bFlashing: bool
 m_bAnimatedEveryTick: bool
 m_nRenderFX: RenderFx_t
 m_nEntityId: uint32
 m_nHierarchyId: uint16
 m_flCapsuleRadius: float32
 m_vDecalPosition: Vector
 m_bClientSideRagdoll: bool
 m_flNextSecondaryAttackTickRatio: float32
 m_iTeamNum: uint8
 m_iEntityQuality: int32
 m_iItemIDHigh: uint32
 m_flDecalHealHeightRate: float32
 m_nOwnerId: uint32
 m_nGlowRangeMin: int32
 m_flGlowBackfaceMult: float32
 m_iEntityLevel: uint32
 m_bRenderToCubemaps: bool

227 CWeaponTec9
 m_bSimulatedEveryTick: bool
 m_bAnimatedEveryTick: bool
 m_vecMaxs: Vector
 m_flFadeScale: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_flShadowStrength: float32
 m_hOuter: CHandle< CBaseEntity >
 m_bInitialized: bool
 m_iOriginalTeamNumber: int32
 m_nNextPrimaryAttackTick: GameTick_t
 m_flDecalHealHeightRate: float32
 m_triggerBloat: uint8
 m_iGlowType: int32
 m_flRecoilIndex: float32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_flNextSecondaryAttackTickRatio: float32
 m_flElasticity: float32
 m_nRenderMode: RenderMode_t
 m_vCapsuleCenter2: Vector
 m_nForceBone: int32
 m_bPlayerFireEventIsPrimary: bool
 m_flGlowStartTime: float32
 m_bClientRagdoll: bool
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_LightGroup: CUtlStringToken
 m_flAnimTime: float32
 m_nInteractsAs: uint64
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flCreateTime: GameTime_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nFallbackStatTrak: int32
 m_bReloadVisuallyComplete: bool
 m_nHierarchyId: uint16
 m_CollisionGroup: uint8
 m_weaponMode: CSWeaponMode
 m_flNavIgnoreUntilTime: GameTime_t
 m_nRenderFX: RenderFx_t
 m_nInteractsWith: uint64
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_OriginalOwnerXuidLow: uint32
 m_nNextSecondaryAttackTick: GameTick_t
 m_zoomLevel: int32
 m_pReserveAmmo: int32[2]
 m_iClip1: int32
 m_nInteractsExclude: uint64
 m_bAnimGraphUpdateEnabled: bool
 m_bShouldAnimateDuringGameplayPause: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nCollisionGroup: uint8
 m_flGlowBackfaceMult: float32
 m_nObjectCulling: uint8
 m_OriginalOwnerXuidHigh: uint32
 CRenderComponent: CRenderComponent
 m_usSolidFlags: uint8
 m_fadeMinDist: float32
 m_iNumEmptyAttacks: int32
 m_bNeedsBoltAction: bool
 m_vCapsuleCenter1: Vector
 m_nGlowRange: int32
 m_bEligibleForScreenHighlight: bool
 m_bInitiallyPopulateInterpHistory: bool
 m_iInventoryPosition: uint32
 m_nFallbackSeed: int32
 m_MoveCollide: MoveCollide_t
 m_flCapsuleRadius: float32
 m_nFireSequenceStartTimeChange: int32
 m_nDropTick: GameTick_t
 m_iClip2: int32
 m_fEffects: uint32
 m_nEnablePhysics: uint8
 m_iTeamNum: uint8
 m_nEntityId: uint32
 m_nNextThinkTick: GameTick_t
 m_nSurroundType: SurroundingBoundsType_t
 m_vDecalPosition: Vector
 m_iRecoilIndex: int32
 m_iItemDefinitionIndex: uint16
 m_nAddDecal: int32
 m_vLookTargetPosition: Vector
 m_flFireSequenceStartTime: float32
 m_bvDisabledHitGroups: uint32[1]
 m_nSubclassID: CUtlStringToken
 m_nOwnerId: uint32
 m_iReapplyProvisionParity: int32
 m_flNextPrimaryAttackTickRatio: float32
 m_flDroppedAtTime: GameTime_t
 m_bIsHauledBack: bool
 m_nCollisionFunctionMask: uint8
 m_iGlowTeam: int32
 m_flGlowTime: float32
 m_iItemIDHigh: uint32
 m_iItemIDLow: uint32
 m_flPostponeFireReadyTime: GameTime_t
 m_flSimulationTime: float32
 m_bFlashing: bool
 m_bRenderToCubemaps: bool
 m_fadeMaxDist: float32
 m_flDecalHealBloodRate: float32
 m_bBurstMode: bool
 m_bSilencerOn: bool
 m_MoveType: MoveType_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_szCustomName: char[161]
 m_nFallbackPaintKit: int32
 m_bInReload: bool
 m_nViewModelIndex: uint32
 m_bClientSideRagdoll: bool
 m_nGlowRangeMin: int32
 m_ProviderType: attributeprovidertypes_t
 m_iEntityLevel: uint32
 CBodyComponent: CBodyComponent
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
 m_ubInterpolationFrame: uint8
 m_flFallbackWear: float32
 m_fAccuracyPenalty: float32
 m_iIronSightMode: int32
 m_iBurstShotsRemaining: int32
 m_fLastShotTime: GameTime_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecMins: Vector
 m_nSolidType: SolidType_t
 m_glowColorOverride: Color
 m_vecForce: Vector
 m_iEntityQuality: int32
 m_clrRender: Color
 m_vecSpecifiedSurroundingMins: Vector
 m_vDecalForwardAxis: Vector
 m_iAccountID: uint32
 m_iState: WeaponState_t

54 CCSPropExplodingBarrelTop
 m_nHierarchyId: uint16
 m_flCapsuleRadius: float32
 m_iTeamNum: uint8
 m_nInteractsExclude: uint64
 m_nGlowRangeMin: int32
 m_bFlashing: bool
 CRenderComponent: CRenderComponent
 m_flNavIgnoreUntilTime: GameTime_t
 m_nCollisionFunctionMask: uint8
 m_nForceBone: int32
 CBodyComponent: CBodyComponent
  m_flPrevCycle: float32
  m_nIdealMotionType: int8
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_flLastTeleportTime: float32
  m_angRotation: QAngle
  m_flScale: float32
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_cellY: uint16
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_hSequence: HSequence
  m_materialGroup: CUtlStringToken
  m_nHitboxSet: uint8
  m_flWeight: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_nBoolVariablesCount: int32
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_vecX: CNetworkedQuantizedFloat
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_bClientSideAnimation: bool
  m_flCycle: float32
  m_bClientClothCreationSuppressed: bool
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nOutsideWorld: uint16
  m_nResetEventsParity: int32
  m_vecY: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_MeshGroupMask: uint64
  m_nNewSequenceParity: int32
  m_nRandomSeedOffset: int32
  m_bUseParentRenderBounds: bool
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_name: CUtlStringToken
 m_nEnablePhysics: uint8
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nRenderFX: RenderFx_t
 m_iGlowType: int32
 m_flShadowStrength: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_hOwner: CHandle< CBaseEntity >
  m_Transforms: CNetworkUtlVectorBase< CTransform >
 m_flSimulationTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nSolidType: SolidType_t
 m_iGlowTeam: int32
 m_nGlowRange: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nInteractsAs: uint64
 m_spawnflags: uint32
 m_flGlowTime: float32
 m_nCollisionGroup: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_glowColorOverride: Color
 m_nAddDecal: int32
 m_fEffects: uint32
 m_nInteractsWith: uint64
 m_fadeMinDist: float32
 m_flFadeScale: float32
 m_vecForce: Vector
 m_flOverrideAlpha: float32
 m_bvDisabledHitGroups: uint32[1]
 m_nOwnerId: uint32
 m_vecSpecifiedSurroundingMins: Vector
 m_nEntityId: uint32
 m_vecMins: Vector
 m_vCapsuleCenter2: Vector
 m_bEligibleForScreenHighlight: bool
 m_vDecalPosition: Vector
 m_bShouldAnimateDuringGameplayPause: bool
 m_MoveType: MoveType_t
 m_bSimulatedEveryTick: bool
 m_flCreateTime: GameTime_t
 m_vecMaxs: Vector
 m_vCapsuleCenter1: Vector
 m_flDecalHealHeightRate: float32
 m_bClientRagdoll: bool
 m_bAwake: bool
 m_triggerBloat: uint8
 m_CollisionGroup: uint8
 m_bRenderToCubemaps: bool
 m_nSurroundType: SurroundingBoundsType_t
 m_bInitiallyPopulateInterpHistory: bool
 m_flElasticity: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nRenderMode: RenderMode_t
 m_clrRender: Color
 m_usSolidFlags: uint8
 m_flGlowStartTime: float32
 m_flDecalHealBloodRate: float32
 m_nSubclassID: CUtlStringToken
 m_bAnimatedEveryTick: bool
 m_bClientSideRagdoll: bool
 m_ubInterpolationFrame: uint8
 m_flGlowBackfaceMult: float32
 m_fadeMaxDist: float32
 m_vDecalForwardAxis: Vector
 m_bAnimGraphUpdateEnabled: bool
 m_noGhostCollision: bool
 m_MoveCollide: MoveCollide_t
 m_LightGroup: CUtlStringToken
 m_nObjectCulling: uint8
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >

125 CLightOrthoEntity
 m_nCollisionFunctionMask: uint8
 m_nSolidType: SolidType_t
 m_CollisionGroup: uint8
 m_vCapsuleCenter2: Vector
 m_nEntityId: uint32
 CRenderComponent: CRenderComponent
 m_triggerBloat: uint8
 m_LightGroup: CUtlStringToken
 m_nSurroundType: SurroundingBoundsType_t
 m_vCapsuleCenter1: Vector
 m_fadeMaxDist: float32
 m_bSimulatedEveryTick: bool
 CLightComponent: CLightComponent
  m_flFalloff: float32
  m_nRenderSpecular: int32
  m_nCascadeRenderStaticObjects: int32
  m_nIndirectLight: int32
  m_flFadeMinDist: float32
  m_vPrecomputedOBBExtent: Vector
  m_flPhi: float32
  m_bRenderTransmissive: bool
  m_bEnabled: bool
  m_nShadowWidth: int32
  m_nShadowCascadeResolution0: int32
  m_flNearClipPlane: float32
  m_bPrecomputedFieldsValid: bool
  m_vPrecomputedBoundsMins: Vector
  m_SkyColor: Color
  m_flTheta: float32
  m_flShadowCascadeCrossFade: float32
  m_nDirectLight: int32
  m_vPrecomputedBoundsMaxs: Vector
  m_flRange: float32
  m_flCapsuleLength: float32
  m_flAttenuation0: float32
  m_bFlicker: bool
  m_vPrecomputedOBBAngles: QAngle
  m_bUseSecondaryColor: bool
  m_flOrthoLightHeight: float32
  m_flShadowCascadeDistance2: float32
  m_nShadowPriority: int32
  m_nBakedShadowIndex: int32
  m_Color: Color
  m_flOrthoLightWidth: float32
  m_flShadowCascadeDistance1: float32
  m_nShadowCascadeResolution3: int32
  m_flFadeMaxDist: float32
  m_flShadowFadeMinDist: float32
  m_flShadowFadeMaxDist: float32
  m_flFogContributionStength: float32
  m_flBrightnessScale: float32
  m_nShadowHeight: int32
  m_nStyle: int32
  m_flShadowCascadeDistance3: float32
  m_bUsesIndexedBakedLighting: bool
  m_bMixedShadows: bool
  m_Pattern: CUtlString
  m_flShadowCascadeDistanceFade: float32
  m_flAttenuation2: float32
  m_nCastShadows: int32
  m_bRenderDiffuse: bool
  m_flShadowCascadeDistance0: float32
  m_nShadowCascadeResolution1: int32
  m_flPrecomputedMaxRange: float32
  m_nFogLightingMode: int32
  m_flSkyIntensity: float32
  m_flMinRoughness: float32
  m_hLightCookie: CStrongHandle< InfoForResourceTypeCTextureBase >
  m_nShadowCascadeResolution2: int32
  m_nCascades: int32
  m_vPrecomputedOBBOrigin: Vector
  m_SkyAmbientBounce: Color
  m_flBrightness: float32
  m_flAttenuation1: float32
  m_bRenderToCubemaps: bool
  m_LightGroups: CUtlSymbolLarge
  m_SecondaryColor: Color
  m_flBrightnessMult: float32
  m_flLightStyleStartTime: GameTime_t
 m_nAddDecal: int32
 m_nOwnerId: uint32
 m_nObjectCulling: uint8
 m_clrRender: Color
 m_glowColorOverride: Color
 m_flGlowBackfaceMult: float32
 m_vecMaxs: Vector
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_iGlowTeam: int32
 m_vDecalForwardAxis: Vector
 m_flAnimTime: float32
 m_bFlashing: bool
 m_usSolidFlags: uint8
 m_nRenderFX: RenderFx_t
 m_nInteractsWith: uint64
 m_nInteractsExclude: uint64
 m_nHierarchyId: uint16
 m_vecMins: Vector
 m_bEligibleForScreenHighlight: bool
 m_fadeMinDist: float32
 m_nRenderMode: RenderMode_t
 m_bRenderToCubemaps: bool
 m_nGlowRangeMin: int32
 m_flShadowStrength: float32
 m_bvDisabledHitGroups: uint32[1]
 m_flElasticity: float32
 m_nSubclassID: CUtlStringToken
 m_flCreateTime: GameTime_t
 m_fEffects: uint32
 m_nCollisionGroup: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flGlowTime: float32
 m_flFadeScale: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_ubInterpolationFrame: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nEnablePhysics: uint8
 m_vDecalPosition: Vector
 m_flDecalHealHeightRate: float32
 CBodyComponent: CBodyComponent
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
 m_MoveCollide: MoveCollide_t
 m_MoveType: MoveType_t
 m_iTeamNum: uint8
 m_flCapsuleRadius: float32
 m_iGlowType: int32
 m_flSimulationTime: float32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nGlowRange: int32
 m_flGlowStartTime: float32
 m_flDecalHealBloodRate: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nInteractsAs: uint64
 m_hOwnerEntity: CHandle< CBaseEntity >

190 CTextureBasedAnimatable
 m_nRenderMode: RenderMode_t
 m_iGlowTeam: int32
 m_glowColorOverride: Color
 m_flGlowStartTime: float32
 m_flFPS: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nRenderFX: RenderFx_t
 m_clrRender: Color
 m_nOwnerId: uint32
 m_flDecalHealHeightRate: float32
 m_vAnimationBoundsMax: Vector
 m_vAnimationBoundsMin: Vector
 m_nSubclassID: CUtlStringToken
 m_vDecalPosition: Vector
 m_hPositionKeys: CStrongHandle< InfoForResourceTypeCTextureBase >
 m_flStartTime: float32
 m_nCollisionFunctionMask: uint8
 m_vecMaxs: Vector
 m_flAnimTime: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vCapsuleCenter2: Vector
 m_fadeMinDist: float32
 CRenderComponent: CRenderComponent
 m_flCreateTime: GameTime_t
 m_nGlowRange: int32
 m_bFlashing: bool
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_hRotationKeys: CStrongHandle< InfoForResourceTypeCTextureBase >
 CBodyComponent: CBodyComponent
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
 m_MoveType: MoveType_t
 m_bSimulatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_nHierarchyId: uint16
 m_vecMins: Vector
 m_nSolidType: SolidType_t
 m_bEligibleForScreenHighlight: bool
 m_CollisionGroup: uint8
 m_vCapsuleCenter1: Vector
 m_nGlowRangeMin: int32
 m_nObjectCulling: uint8
 m_flElasticity: float32
 m_nInteractsExclude: uint64
 m_fEffects: uint32
 m_nEntityId: uint32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_ubInterpolationFrame: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_usSolidFlags: uint8
 m_flGlowTime: float32
 m_fadeMaxDist: float32
 m_flFadeScale: float32
 m_nAddDecal: int32
 m_bLoop: bool
 m_flSimulationTime: float32
 m_iTeamNum: uint8
 m_bAnimatedEveryTick: bool
 m_LightGroup: CUtlStringToken
 m_flShadowStrength: float32
 m_bvDisabledHitGroups: uint32[1]
 m_MoveCollide: MoveCollide_t
 m_nInteractsWith: uint64
 m_triggerBloat: uint8
 m_nEnablePhysics: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_flDecalHealBloodRate: float32
 m_flStartFrame: float32
 m_nInteractsAs: uint64
 m_flCapsuleRadius: float32
 m_iGlowType: int32
 m_vDecalForwardAxis: Vector
 m_bRenderToCubemaps: bool
 m_nCollisionGroup: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_flGlowBackfaceMult: float32

10 CBaseFlex
 m_vecMaxs: Vector
 m_nSolidType: SolidType_t
 m_fadeMinDist: float32
 m_flAnimTime: float32
 CBodyComponent: CBodyComponent
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_nOutsideWorld: uint16
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nBoolVariablesCount: int32
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
 m_bClientSideRagdoll: bool
 m_vecSpecifiedSurroundingMins: Vector
 m_vDecalPosition: Vector
 m_nHierarchyId: uint16
 m_usSolidFlags: uint8
 m_bFlashing: bool
 m_nInteractsAs: uint64
 m_flGlowBackfaceMult: float32
 m_fadeMaxDist: float32
 m_flFadeScale: float32
 m_nAddDecal: int32
 m_iTeamNum: uint8
 m_CollisionGroup: uint8
 m_nGlowRangeMin: int32
 m_nObjectCulling: uint8
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_nInteractsExclude: uint64
 m_nOwnerId: uint32
 m_iGlowType: int32
 m_flDecalHealBloodRate: float32
 m_vLookTargetPosition: Vector
 m_blinktoggle: bool
 m_ubInterpolationFrame: uint8
 m_bRenderToCubemaps: bool
 m_nCollisionGroup: uint8
 m_iGlowTeam: int32
 m_nForceBone: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nSubclassID: CUtlStringToken
 m_nEnablePhysics: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flCreateTime: GameTime_t
 m_glowColorOverride: Color
 m_flElasticity: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_nEntityId: uint32
 m_vecMins: Vector
 m_nSurroundType: SurroundingBoundsType_t
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_nRenderFX: RenderFx_t
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flShadowStrength: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_vecForce: Vector
 m_flSimulationTime: float32
 m_MoveCollide: MoveCollide_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_triggerBloat: uint8
 m_flGlowTime: float32
 m_bClientRagdoll: bool
 CRenderComponent: CRenderComponent
 m_clrRender: Color
 m_flCapsuleRadius: float32
 m_bEligibleForScreenHighlight: bool
 m_vCapsuleCenter1: Vector
 m_vCapsuleCenter2: Vector
 m_nGlowRange: int32
 m_MoveType: MoveType_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nRenderMode: RenderMode_t
 m_LightGroup: CUtlStringToken
 m_nCollisionFunctionMask: uint8
 m_bvDisabledHitGroups: uint32[1]
 m_flGlowStartTime: float32
 m_flDecalHealHeightRate: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_bAnimGraphUpdateEnabled: bool
 m_flexWeight: CNetworkUtlVectorBase< float32 >
 m_bSimulatedEveryTick: bool
 m_nInteractsWith: uint64
 m_vDecalForwardAxis: Vector

43 CCSGO_WingmanIntroCharacterPosition
 m_MoveType: MoveType_t
 m_bSimulatedEveryTick: bool
 m_nRandom: int32
 m_iEntityLevel: uint32
 m_iAccountID: uint32
 m_MoveCollide: MoveCollide_t
 m_iEntityQuality: int32
 m_iItemIDLow: uint32
 m_nSubclassID: CUtlStringToken
 m_iTeamNum: uint8
 m_nOrdinal: int32
 CBodyComponent: CBodyComponent
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flCreateTime: GameTime_t
 m_iItemDefinitionIndex: uint16
 m_iInventoryPosition: uint32
 m_flAnimTime: float32
 m_sWeaponName: CUtlString
 m_xuid: uint64
 m_iItemIDHigh: uint32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
 m_fEffects: uint32
 m_flElasticity: float32
 m_ubInterpolationFrame: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_szCustomName: char[161]
 m_flSimulationTime: float32
 m_nVariant: int32
 m_bInitialized: bool
 m_hOwnerEntity: CHandle< CBaseEntity >

45 CCSGO_WingmanIntroTerroristPosition
 m_MoveType: MoveType_t
 m_nSubclassID: CUtlStringToken
 m_flElasticity: float32
 m_iItemDefinitionIndex: uint16
 m_iItemIDHigh: uint32
 m_flNavIgnoreUntilTime: GameTime_t
 m_sWeaponName: CUtlString
 m_iEntityLevel: uint32
 m_iAccountID: uint32
 m_szCustomName: char[161]
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bSimulatedEveryTick: bool
 m_iEntityQuality: int32
 m_flAnimTime: float32
 m_iTeamNum: uint8
 m_nRandom: int32
 m_bInitialized: bool
 CBodyComponent: CBodyComponent
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
 m_flCreateTime: GameTime_t
 m_xuid: uint64
 m_iInventoryPosition: uint32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nOrdinal: int32
 m_ubInterpolationFrame: uint8
 m_bAnimatedEveryTick: bool
 m_iItemIDLow: uint32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
 m_flSimulationTime: float32
 m_MoveCollide: MoveCollide_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_nVariant: int32

50 CCSPlayerPawn
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_glowColorOverride: Color
 m_bRagdollDamageHeadshot: bool
 colorPrimaryLerpTo: Color
 enable: bool
 m_lifeState: uint8
 m_flFieldOfView: float32
 m_nHeavyAssaultSuitCooldownRemaining: int32
 m_nLastKillerIndex: CEntityIndex
 start: float32
 m_iGunGameProgressiveWeaponIndex: int32
 m_aimPunchTickBase: int32
 m_iHealth: int32
 m_nHierarchyId: uint16
 m_flGlowStartTime: float32
 m_flTimeOfLastInjury: float32
 m_vecZ: CNetworkedQuantizedFloat
 flClip3DSkyBoxNearToWorldFarOffset: float32
 m_bFlashing: bool
 m_bHud_MiniScoreHidden: bool
 m_iEntityLevel: uint32
 scale: int16
 m_vDecalForwardAxis: Vector
 m_bInNoDefuseArea: bool
 m_nHitBodyPart: int32
 m_iProgressBarDuration: int32
 m_iAccountID: uint32
 endLerpTo: float32
 m_flGravityScale: float32
 m_thirdPersonHeading: QAngle
 m_bClientSideRagdoll: bool
 m_nOwnerId: uint32
 m_vecMaxs: Vector
 m_pCameraServices: CPlayer_CameraServices*
  m_hTonemapController: CHandle< CTonemapController2 >
  localSound: Vector[8]
  soundscapeEntityListIndex: int32
  m_flFOVRate: float32
  m_hCtrl: CHandle< CFogController >
  m_vecCsViewPunchAngle: QAngle
  m_flCsViewPunchAngleTickRatio: float32
  m_hColorCorrectionCtrl: CHandle< CColorCorrection >
  m_nCsViewPunchAngleTick: GameTick_t
  m_hZoomOwner: CHandle< CBaseEntity >
  localBits: uint8
  soundEventHash: uint32
  m_PostProcessingVolumes: CNetworkUtlVectorBase< CHandle< CPostProcessingVolume > >
  m_iFOVStart: uint32
  m_flFOVTime: GameTime_t
  soundscapeIndex: int32
  m_hViewEntity: CHandle< CBaseEntity >
  m_iFOV: uint32
 skyboxFogFactor: float32
 m_nNextThinkTick: GameTick_t
 m_nRagdollDamageBone: int32
 m_iItemDefinitionIndex: uint16
 m_iGlowTeam: int32
 m_vLookTargetPosition: Vector
 m_iNumGunGameKillsWithCurrentWeapon: int32
 m_fMolotovDamageTime: float32
 m_iRetakesOffering: int32
 m_bNoReflectionFog: bool
 m_flShadowStrength: float32
 m_bAnimGraphUpdateEnabled: bool
 m_aimPunchAngle: QAngle
 colorSecondaryLerpTo: Color
 m_pViewModelServices: CCSPlayer_ViewModelServices*
  m_hViewModel: CHandle< CBaseViewModel >[3]
 m_bRetakesHasDefuseKit: bool
 m_bInitialized: bool
 maxdensity: float32
 m_vCapsuleCenter2: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flVelocityModifier: float32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_pWeaponServices: CPlayer_WeaponServices*
  m_bIsLookingAtWeapon: bool
  m_bIsHoldingLookAtWeapon: bool
  m_hLastWeapon: CHandle< CBasePlayerWeapon >
  m_flNextAttack: GameTime_t
  m_hMyWeapons: CNetworkUtlVectorBase< CHandle< CBasePlayerWeapon > >
  m_hActiveWeapon: CHandle< CBasePlayerWeapon >
  m_iAmmo: uint16[32]
 m_pWaterServices: CPlayer_WaterServices*
 m_flLowerBodyYawTarget: float32
 m_iSecondaryAddon: int32
 m_nInteractsWith: uint64
 m_pItemServices: CPlayer_ItemServices*
  m_bHasDefuser: bool
  m_bHasHelmet: bool
  m_bHasHeavyArmor: bool
 m_vecSpawnRappellingRopeOrigin: Vector
 end: float32
 m_nSurvivalTeam: int32
 m_bWaitForNoAttack: bool
 m_unTotalRoundDamageDealt: uint32
 m_pBulletServices: CCSPlayer_BulletServices*
  m_totalHitsOnServer: int32
 exponent: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_iGlowType: int32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_hOwner: CHandle< CBaseEntity >
  m_Transforms: CNetworkUtlVectorBase< CTransform >
 m_iBlockingUseActionInProgress: CSPlayerBlockingUseAction_t
 m_aimPunchTickFraction: float32
 m_vecBaseVelocity: Vector
 m_nEntityId: uint32
 bClip3DSkyBoxNearToWorldFar: bool
 m_flFriction: float32
 m_bAnimatedEveryTick: bool
 m_triggerBloat: uint8
 m_flCapsuleRadius: float32
 m_flDecalHealHeightRate: float32
 m_bCanMoveDuringFreezePeriod: bool
 m_hSurvivalAssassinationTarget: CHandle< CCSPlayerPawnBase >
 m_szCustomName: char[161]
 m_ServerViewAngleChanges: CUtlVectorEmbeddedNetworkVar< ViewAngleServerChange_t >
  nType: FixAngleSet_t
  qAngle: QAngle
  nIndex: uint32
 m_iTeamNum: uint8
 m_bEligibleForScreenHighlight: bool
 m_bInitiallyPopulateInterpHistory: bool
 m_bSpotted: bool
 m_unCurrentEquipmentValue: uint16
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 blend: bool
 m_bIsDefusing: bool
 m_flFlashDuration: float32
 startLerpTo: float32
 m_vecX: CNetworkedQuantizedFloat
 m_RetakesMVPBoostExtraUtility: loadout_slot_t
 dirPrimary: Vector
 m_flFadeScale: float32
 m_pMovementServices: CPlayer_MovementServices*
  m_bDuckOverride: bool
  m_nToggleButtonDownMask: uint64
  m_arrForceSubtickMoveWhen: float32[4]
  m_nButtonDownMaskPrev: uint64
  m_bDucking: bool
  m_bInDuckJump: bool
  m_flLastDuckTime: float32
  m_vecLadderNormal: Vector
  m_flJumpVel: float32
  m_bOldJumpPressed: bool
  m_fStashGrenadeParameterWhen: GameTime_t
  m_nLadderSurfacePropIndex: int32
  m_flJumpUntil: float32
  m_flOffsetTickStashedSpeed: float32
  m_flCrouchTransitionStartTime: GameTime_t
  m_bDesiresDuck: bool
  m_flFallVelocity: float32
  m_nDuckJumpTimeMsecs: uint32
  m_nDuckTimeMsecs: uint32
  m_flMaxFallVelocity: float32
  m_nCrouchState: uint32
  m_bDucked: bool
  m_nJumpTimeMsecs: uint32
  m_flMaxspeed: float32
  m_flDuckAmount: float32
  m_flDuckSpeed: float32
  m_flOffsetTickCompleteTime: float32
  m_bInCrouch: bool
 m_iNumGunGameTRKillPoints: int32
 m_flDetectedByEnemySensorTime: GameTime_t
 m_qDeathEyeAngles: QAngle
 m_fadeMaxDist: float32
 m_bInHostageRescueZone: bool
 m_iPrimaryAddon: int32
 m_MoveCollide: MoveCollide_t
 m_flDeathTime: GameTime_t
 m_hOriginalController: CHandle< CCSPlayerController >
 m_szRagdollDamageWeaponName: char[64]
 scattering: float32
 m_bIsGrabbingHostage: bool
 m_bIsSpawnRappelling: bool
 HDRColorScale: float32
 maxdensityLerpTo: float32
 colorSecondary: Color
 m_blinktoggle: bool
 m_fImmuneToGunGameDamageTime: GameTime_t
 m_flStamina: float32
 m_vecSpecifiedSurroundingMins: Vector
 m_vDecalPosition: Vector
 m_flHitHeading: float32
 m_iRetakesMVPBoostItem: int32
 m_iItemIDHigh: uint32
 m_iMaxHealth: int32
 m_isCurrentGunGameTeamLeader: bool
 m_fEffects: uint32
 m_fadeMinDist: float32
 m_nObjectCulling: uint8
 m_bSpottedByMask: uint32[2]
 m_pUseServices: CPlayer_UseServices*
 m_bNightVisionOn: bool
 m_iShotsFired: int32
 m_szLastPlaceName: char[18]
 m_ubInterpolationFrame: uint8
 m_CollisionGroup: uint8
 m_bHasMovedSinceSpawn: bool
 m_hGroundEntity: CHandle< CBaseEntity >
 m_flElasticity: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_bResumeZoom: bool
 m_iEntityQuality: int32
 m_bvDisabledHitGroups: uint32[1]
 m_pBuyServices: CCSPlayer_BuyServices*
  m_vecSellbackPurchaseEntries: CUtlVectorEmbeddedNetworkVar< SellbackPurchaseEntry_t >
   m_nPrevArmor: int32
   m_bPrevHelmet: bool
   m_hItem: CEntityHandle
   m_unDefIdx: uint16
   m_nCost: int32
 lerptime: GameTime_t
 m_flGlowBackfaceMult: float32
 m_pPingServices: CCSPlayer_PingServices*
  m_hPlayerPing: CHandle< CBaseEntity >
 m_fFlags: uint32
 m_bRenderToCubemaps: bool
 m_bClientRagdoll: bool
 m_nRelativeDirectionOfLastInjury: RelativeDamagedDirection_t
 m_bHideTargetID: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bShouldAnimateDuringGameplayPause: bool
 m_nLastConcurrentKilled: int32
 m_aimPunchAngleVel: QAngle
 skyboxFogFactorLerpTo: float32
 m_vHeadConstraintOffset: Vector
 m_hMyWearables: CNetworkUtlVectorBase< CHandle< CEconWearable > >
 m_iMoveState: int32
 m_LightGroup: CUtlStringToken
 m_bGunGameImmunity: bool
 duration: float32
 m_nGlowRangeMin: int32
 m_flGlowTime: float32
 m_bIsScoped: bool
 m_bMadeFinalGunGameProgressiveKill: bool
 m_bStrafing: bool
 m_bKilledByHeadshot: bool
 m_nForceBone: int32
 m_bIsWalking: bool
 locallightscale: float32
 m_nRenderMode: RenderMode_t
 m_nGlowRange: int32
 m_flHealthShotBoostExpirationTime: float32
 m_flProgressBarStartTime: float32
 m_nWorldGroupID: WorldGroupId_t
 m_nRenderFX: RenderFx_t
 m_nSurroundType: SurroundingBoundsType_t
 m_iAddonBits: int32
 m_vRagdollDamageForce: Vector
 m_flSimulationTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nCollisionFunctionMask: uint8
 m_pActionTrackingServices: CCSPlayer_ActionTrackingServices*
  m_bIsRescuing: bool
  m_weaponPurchases: CUtlVectorEmbeddedNetworkVar< WeaponPurchaseCount_t >
   m_nItemDefIndex: uint16
   m_nCount: uint16
 m_iPlayerState: CSPlayerState
 m_flGuardianTooFarDistFrac: float32
 m_nWhichBombZone: int32
 m_flSlopeDropHeight: float32
 m_flDecalHealBloodRate: float32
 m_vecForce: Vector
 m_flEmitSoundTime: GameTime_t
 m_ArmorValue: int32
 colorPrimary: Color
 m_nSurvivalTeamNumber: int32
 CRenderComponent: CRenderComponent
 m_vecY: CNetworkedQuantizedFloat
 m_vRagdollDamagePosition: Vector
 m_iItemIDLow: uint32
 m_nCollisionGroup: uint8
 m_iInventoryPosition: uint32
 m_flTimeScale: float32
 CBodyComponent: CBodyComponent
  m_bUseParentRenderBounds: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_hParent: CGameSceneNodeHandle
  m_nResetEventsParity: int32
  m_flPrevCycle: float32
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_vecY: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nRandomSeedOffset: int32
  m_cellY: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_bClientClothCreationSuppressed: bool
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hierarchyAttachName: CUtlStringToken
  m_flWeight: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nOutsideWorld: uint16
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_nNewSequenceParity: int32
  m_flCycle: float32
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_cellX: uint16
  m_flScale: float32
  m_nIdealMotionType: int8
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nBoolVariablesCount: int32
  m_cellZ: uint16
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecX: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_flLastTeleportTime: float32
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_name: CUtlStringToken
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
 m_nInteractsExclude: uint64
 m_usSolidFlags: uint8
 m_bHasFemaleVoice: bool
 m_passiveItems: bool[4]
 m_bHasNightVision: bool
 m_pHostageServices: CCSPlayer_HostageServices*
  m_hCarriedHostageProp: CHandle< CBaseEntity >
  m_hCarriedHostage: CHandle< CBaseEntity >
 m_flNextSprayDecalTime: float32
 m_isCurrentGunGameLeader: bool
 m_bKilledByTaser: bool
 m_iRetakesOfferingCard: int32
 blendtobackground: float32
 m_nSolidType: SolidType_t
 m_nEnablePhysics: uint8
 origin: Vector
 m_flWaterLevel: float32
 m_clrRender: Color
 m_unFreezetimeEndEquipmentValue: uint16
 m_bRetakesMVPLastRound: bool
 m_MoveType: MoveType_t
 m_bSimulatedEveryTick: bool
 m_pObserverServices: CPlayer_ObserverServices*
  m_iObserverMode: uint8
  m_hObserverTarget: CHandle< CBaseEntity >
 m_bInBombZone: bool
 m_vecPlayerPatchEconIndices: uint32[5]
 m_bIsBuyMenuOpen: bool
 m_flFlashMaxAlpha: float32
 m_nSubclassID: CUtlStringToken
 m_vCapsuleCenter1: Vector
 m_unRoundStartEquipmentValue: uint16
 m_iHideHUD: uint32
 m_iDirection: int32
 m_nDeathCamMusic: int32
 farz: float32
 m_angEyeAngles: QAngle
 m_flCreateTime: GameTime_t
 m_nInteractsAs: uint64
 m_nAddDecal: int32
 m_bInBuyZone: bool
 m_flSlopeDropOffset: float32
 m_vecMins: Vector
 m_hController: CHandle< CBasePlayerController >
 m_bHud_RadarHidden: bool

114 CInfoVisibilityBox
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flElasticity: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_nSubclassID: CUtlStringToken
 m_iTeamNum: uint8
 m_vBoxSize: Vector
 m_bEnabled: bool
 m_MoveCollide: MoveCollide_t
 m_flCreateTime: GameTime_t
 CBodyComponent: CBodyComponent
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bSimulatedEveryTick: bool
 m_flSimulationTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_ubInterpolationFrame: uint8
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_nMode: int32
 m_flAnimTime: float32
 m_MoveType: MoveType_t

164 CRagdollPropAttached
 m_nGlowRangeMin: int32
 CRenderComponent: CRenderComponent
 m_nRenderFX: RenderFx_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nSurroundType: SurroundingBoundsType_t
 m_vCapsuleCenter1: Vector
 m_MoveType: MoveType_t
 m_flCreateTime: GameTime_t
 m_vDecalForwardAxis: Vector
 m_attachmentPointBoneSpace: Vector
 m_triggerBloat: uint8
 m_CollisionGroup: uint8
 m_vCapsuleCenter2: Vector
 m_flCapsuleRadius: float32
 m_flSimulationTime: float32
 m_fEffects: uint32
 m_nInteractsAs: uint64
 m_nCollisionFunctionMask: uint8
 m_flGlowBackfaceMult: float32
 m_bAnimGraphUpdateEnabled: bool
 m_bvDisabledHitGroups: uint32[1]
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nGlowRange: int32
 m_attachmentPointRagdollSpace: Vector
 m_iGlowType: int32
 m_fadeMinDist: float32
 m_vDecalPosition: Vector
 m_iTeamNum: uint8
 m_bSimulatedEveryTick: bool
 m_nInteractsWith: uint64
 m_usSolidFlags: uint8
 m_flShadowStrength: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_flBlendWeight: float32
 CBodyComponent: CBodyComponent
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nBoolVariablesCount: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
 m_nRenderMode: RenderMode_t
 m_vecMins: Vector
 m_flGlowTime: float32
 m_boneIndexAttached: uint32
 m_bClientSideRagdoll: bool
 m_bEligibleForScreenHighlight: bool
 m_bShouldAnimateDuringGameplayPause: bool
 m_vecForce: Vector
 m_hRagdollSource: CHandle< CBaseEntity >
 m_flElasticity: float32
 m_nOwnerId: uint32
 m_vecSpecifiedSurroundingMins: Vector
 m_flFadeScale: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_bRenderToCubemaps: bool
 m_glowColorOverride: Color
 m_flGlowStartTime: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool
 m_clrRender: Color
 m_nForceBone: int32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flDecalHealBloodRate: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nHierarchyId: uint16
 m_fadeMaxDist: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_ubInterpolationFrame: uint8
 m_LightGroup: CUtlStringToken
 m_nInteractsExclude: uint64
 m_nEntityId: uint32
 m_nEnablePhysics: uint8
 m_bFlashing: bool
 m_nObjectCulling: uint8
 m_bClientRagdoll: bool
 m_MoveCollide: MoveCollide_t
 m_nCollisionGroup: uint8
 m_nAddDecal: int32
 m_flDecalHealHeightRate: float32
 m_nSolidType: SolidType_t
 m_iGlowTeam: int32
 m_ragAngles: CNetworkUtlVectorBase< QAngle >
 m_flAnimTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSubclassID: CUtlStringToken
 m_vecMaxs: Vector
 m_ragPos: CNetworkUtlVectorBase< Vector >
 m_ragdollAttachedObjectIndex: uint32

82 CEnvVolumetricFogVolume
 m_iTeamNum: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flAnimTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveCollide: MoveCollide_t
 m_flCreateTime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_vBoxMins: Vector
 m_vBoxMaxs: Vector
 m_nSubclassID: CUtlStringToken
 m_nFalloffShape: int32
 CBodyComponent: CBodyComponent
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
 m_fEffects: uint32
 m_bActive: bool
 m_flStrength: float32
 m_flFalloffExponent: float32
 m_bStartDisabled: bool
 m_flSimulationTime: float32
 m_MoveType: MoveType_t
 m_flElasticity: float32
 m_bSimulatedEveryTick: bool
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t

94 CFuncLadder
 CRenderComponent: CRenderComponent
 m_nInteractsWith: uint64
 m_nEnablePhysics: uint8
 m_CollisionGroup: uint8
 m_ubInterpolationFrame: uint8
 m_nInteractsAs: uint64
 m_bFlashing: bool
 m_iTeamNum: uint8
 m_flCapsuleRadius: float32
 m_glowColorOverride: Color
 m_bvDisabledHitGroups: uint32[1]
 m_flNavIgnoreUntilTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_vecMaxs: Vector
 m_usSolidFlags: uint8
 m_nSolidType: SolidType_t
 m_nSurroundType: SurroundingBoundsType_t
 m_iGlowTeam: int32
 m_flGlowTime: float32
 m_bSimulatedEveryTick: bool
 m_nHierarchyId: uint16
 m_flShadowStrength: float32
 m_flAnimTime: float32
 m_flGlowBackfaceMult: float32
 m_bRenderToCubemaps: bool
 m_iGlowType: int32
 m_fadeMinDist: float32
 m_nAddDecal: int32
 m_MoveType: MoveType_t
 m_flCreateTime: GameTime_t
 m_vecMins: Vector
 m_vDecalPosition: Vector
 m_bFakeLadder: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nSubclassID: CUtlStringToken
 m_LightGroup: CUtlStringToken
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vecPlayerMountPositionTop: Vector
 m_nRenderFX: RenderFx_t
 m_clrRender: Color
 m_vecSpecifiedSurroundingMins: Vector
 m_vCapsuleCenter1: Vector
 m_bEligibleForScreenHighlight: bool
 m_flDecalHealBloodRate: float32
 CBodyComponent: CBodyComponent
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
 m_triggerBloat: uint8
 m_vCapsuleCenter2: Vector
 m_nCollisionFunctionMask: uint8
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flAutoRideSpeed: float32
 m_fEffects: uint32
 m_nInteractsExclude: uint64
 m_nGlowRange: int32
 m_nObjectCulling: uint8
 m_flDecalHealHeightRate: float32
 m_flElasticity: float32
 m_nCollisionGroup: uint8
 m_nOwnerId: uint32
 m_vDecalForwardAxis: Vector
 m_MoveCollide: MoveCollide_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nEntityId: uint32
 m_nGlowRangeMin: int32
 m_flSimulationTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_vecLadderDir: Vector
 m_vecPlayerMountPositionBottom: Vector
 m_flFadeScale: float32
 m_flGlowStartTime: float32
 m_fadeMaxDist: float32

107 CHostageRescueZone
 m_clrRender: Color
 m_flGlowBackfaceMult: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nOwnerId: uint32
 m_nHierarchyId: uint16
 m_nSurroundType: SurroundingBoundsType_t
 m_flCapsuleRadius: float32
 m_nGlowRangeMin: int32
 m_flGlowTime: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bAnimatedEveryTick: bool
 m_MoveCollide: MoveCollide_t
 m_nCollisionFunctionMask: uint8
 m_vCapsuleCenter1: Vector
 m_flGlowStartTime: float32
 CRenderComponent: CRenderComponent
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nRenderMode: RenderMode_t
 m_vecMaxs: Vector
 m_fadeMaxDist: float32
 m_flShadowStrength: float32
 m_nAddDecal: int32
 CBodyComponent: CBodyComponent
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
 m_iGlowType: int32
 m_LightGroup: CUtlStringToken
 m_iGlowTeam: int32
 m_fadeMinDist: float32
 m_bvDisabledHitGroups: uint32[1]
 m_flSimulationTime: float32
 m_nInteractsExclude: uint64
 m_nObjectCulling: uint8
 m_ubInterpolationFrame: uint8
 m_nRenderFX: RenderFx_t
 m_nEnablePhysics: uint8
 m_nGlowRange: int32
 m_flAnimTime: float32
 m_triggerBloat: uint8
 m_nSubclassID: CUtlStringToken
 m_flFadeScale: float32
 m_bDisabled: bool
 m_usSolidFlags: uint8
 m_bSimulatedEveryTick: bool
 m_nInteractsAs: uint64
 m_nSolidType: SolidType_t
 m_glowColorOverride: Color
 m_MoveType: MoveType_t
 m_nInteractsWith: uint64
 m_nEntityId: uint32
 m_vecMins: Vector
 m_bFlashing: bool
 m_bEligibleForScreenHighlight: bool
 m_fEffects: uint32
 m_flElasticity: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vDecalForwardAxis: Vector
 m_flDecalHealBloodRate: float32
 m_iTeamNum: uint8
 m_bRenderToCubemaps: bool
 m_nCollisionGroup: uint8
 m_CollisionGroup: uint8
 m_spawnflags: uint32
 m_vecSpecifiedSurroundingMins: Vector
 m_vCapsuleCenter2: Vector
 m_bClientSidePredicted: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vDecalPosition: Vector
 m_flDecalHealHeightRate: float32
 m_flCreateTime: GameTime_t

113 CInfoOffscreenPanoramaTexture
 m_RenderAttrName: CUtlSymbolLarge
 m_flAnimTime: float32
 m_nSubclassID: CUtlStringToken
 m_nResolutionX: int32
 m_nResolutionY: int32
 m_bSimulatedEveryTick: bool
 m_flCreateTime: GameTime_t
 m_vecCSSClasses: CNetworkUtlVectorBase< CUtlSymbolLarge >
 CBodyComponent: CBodyComponent
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool
 m_bDisabled: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveCollide: MoveCollide_t
 m_MoveType: MoveType_t
 m_iTeamNum: uint8
 m_TargetEntities: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_fEffects: uint32
 m_flElasticity: float32
 m_flSimulationTime: float32
 m_ubInterpolationFrame: uint8
 m_szLayoutFileName: CUtlSymbolLarge
 m_nTargetChangeCount: int32

124 CLightGlow
 CBodyComponent: CBodyComponent
  m_vecX: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_cellX: uint16
  m_cellY: uint16
  m_cellZ: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_nOutsideWorld: uint16
 m_clrRender: Color
 m_nHorizontalSize: uint32
 m_nVerticalSize: uint32
 m_nMinDist: uint32
 m_flGlowProxySize: float32
 m_spawnflags: uint32
 m_nMaxDist: uint32
 m_nOuterMaxDist: uint32
 m_flHDRColorScale: float32

3 CBaseButton
 m_nGlowRange: int32
 m_flFadeScale: float32
 m_vDecalPosition: Vector
 CBodyComponent: CBodyComponent
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
 m_nRenderFX: RenderFx_t
 m_nEntityId: uint32
 m_triggerBloat: uint8
 m_MoveType: MoveType_t
 m_bEligibleForScreenHighlight: bool
 m_flGlowBackfaceMult: float32
 m_nObjectCulling: uint8
 m_nSolidType: SolidType_t
 m_bFlashing: bool
 m_nAddDecal: int32
 m_flAnimTime: float32
 m_MoveCollide: MoveCollide_t
 m_nCollisionGroup: uint8
 m_usSolidFlags: uint8
 m_flDecalHealBloodRate: float32
 m_nRenderMode: RenderMode_t
 m_nInteractsWith: uint64
 m_nInteractsExclude: uint64
 m_iGlowTeam: int32
 m_flDecalHealHeightRate: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nInteractsAs: uint64
 m_glowColorOverride: Color
 m_flGlowTime: float32
 m_bAnimatedEveryTick: bool
 m_CollisionGroup: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_nGlowRangeMin: int32
 m_flGlowStartTime: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bRenderToCubemaps: bool
 m_nCollisionFunctionMask: uint8
 m_vCapsuleCenter2: Vector
 m_iGlowType: int32
 m_vCapsuleCenter1: Vector
 m_vDecalForwardAxis: Vector
 m_szDisplayText: CUtlSymbolLarge
 m_LightGroup: CUtlStringToken
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bvDisabledHitGroups: uint32[1]
 m_flCreateTime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_nOwnerId: uint32
 m_usable: bool
 m_iTeamNum: uint8
 m_clrRender: Color
 m_fadeMinDist: float32
 CRenderComponent: CRenderComponent
 m_vecMaxs: Vector
 m_nSubclassID: CUtlStringToken
 m_fEffects: uint32
 m_nHierarchyId: uint16
 m_vecMins: Vector
 m_flNavIgnoreUntilTime: GameTime_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nSurroundType: SurroundingBoundsType_t
 m_nEnablePhysics: uint8
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flElasticity: float32
 m_bSimulatedEveryTick: bool
 m_glowEntity: CHandle< CBaseModelEntity >
 m_flSimulationTime: float32
 m_fadeMaxDist: float32
 m_flCapsuleRadius: float32
 m_flShadowStrength: float32

25 CBreachChargeProjectile
 m_bClientSideRagdoll: bool
 m_nObjectCulling: uint8
 m_bAnimGraphUpdateEnabled: bool
 CRenderComponent: CRenderComponent
 m_iTeamNum: uint8
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_bIsLive: bool
 m_DmgRadius: float32
 m_flDetonateTime: GameTime_t
 m_fFlags: uint32
 m_nRenderMode: RenderMode_t
 m_nRenderFX: RenderFx_t
 m_nEnablePhysics: uint8
 m_bClientRagdoll: bool
 m_fEffects: uint32
 m_vecMaxs: Vector
 m_iGlowTeam: int32
 m_fadeMaxDist: float32
 m_nAddDecal: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_vecMins: Vector
 m_triggerBloat: uint8
 m_nGlowRange: int32
 m_vecZ: CNetworkedQuantizedFloat
 m_clrRender: Color
 m_flCapsuleRadius: float32
 m_bEligibleForScreenHighlight: bool
 m_vecX: CNetworkedQuantizedFloat
 m_hThrower: CHandle< CBaseEntity >
 m_nCollisionFunctionMask: uint8
 m_bFlashing: bool
 m_flGlowBackfaceMult: float32
 m_vDecalForwardAxis: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nForceBone: int32
 m_vecY: CNetworkedQuantizedFloat
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flElasticity: float32
 m_nInteractsAs: uint64
 m_nGlowRangeMin: int32
 m_glowColorOverride: Color
 m_nSubclassID: CUtlStringToken
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_flDecalHealBloodRate: float32
 m_vecParentBonePos: Vector
 m_bvDisabledHitGroups: uint32[1]
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vCapsuleCenter1: Vector
 m_bInitiallyPopulateInterpHistory: bool
 m_ubInterpolationFrame: uint8
 m_LightGroup: CUtlStringToken
 m_nInteractsExclude: uint64
 m_nHierarchyId: uint16
 m_nSolidType: SolidType_t
 m_bShouldAnimateDuringGameplayPause: bool
 m_vecSpecifiedSurroundingMins: Vector
 m_flGlowTime: float32
 m_flGlowStartTime: float32
 m_flDamage: float32
 m_flFadeScale: float32
 m_flShadowStrength: float32
 m_vDecalPosition: Vector
 m_bShouldExplode: bool
 m_nParentBoneIndex: int32
 m_CollisionGroup: uint8
 m_flSimulationTime: float32
 m_MoveType: MoveType_t
 m_bAnimatedEveryTick: bool
 m_bRenderToCubemaps: bool
 m_nSurroundType: SurroundingBoundsType_t
 m_flCreateTime: GameTime_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_nCollisionGroup: uint8
 m_fadeMinDist: float32
 m_weaponThatThrewMe: CHandle< CBaseEntity >
 m_flDecalHealHeightRate: float32
 m_vLookTargetPosition: Vector
 CBodyComponent: CBodyComponent
  m_nIdealMotionType: int8
  m_bClientSideAnimation: bool
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flLastTeleportTime: float32
  m_angRotation: QAngle
  m_nOutsideWorld: uint16
  m_flWeight: CNetworkedQuantizedFloat
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_cellX: uint16
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nBoolVariablesCount: int32
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flScale: float32
  m_bClientClothCreationSuppressed: bool
  m_cellY: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_nRandomSeedOffset: int32
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_bUseParentRenderBounds: bool
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_cellZ: uint16
  m_materialGroup: CUtlStringToken
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_hierarchyAttachName: CUtlStringToken
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bSimulatedEveryTick: bool
 m_nInteractsWith: uint64
 m_usSolidFlags: uint8
 m_vecForce: Vector
 m_MoveCollide: MoveCollide_t
 m_nEntityId: uint32
 m_nOwnerId: uint32
 m_vCapsuleCenter2: Vector
 m_iGlowType: int32

36 CCSGO_TeamIntroCharacterPosition
 m_nVariant: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_iEntityQuality: int32
 m_iItemIDHigh: uint32
 m_szCustomName: char[161]
 m_xuid: uint64
 m_iEntityLevel: uint32
 m_iInventoryPosition: uint32
 m_bInitialized: bool
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flSimulationTime: float32
 m_iItemIDLow: uint32
 m_flAnimTime: float32
 m_fEffects: uint32
 m_bSimulatedEveryTick: bool
 m_sWeaponName: CUtlString
 m_iTeamNum: uint8
 m_flCreateTime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_bAnimatedEveryTick: bool
 m_nOrdinal: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nRandom: int32
 m_iItemDefinitionIndex: uint16
 m_iAccountID: uint32
 m_MoveCollide: MoveCollide_t
 m_MoveType: MoveType_t
 m_nSubclassID: CUtlStringToken
 m_flElasticity: float32
 m_flNavIgnoreUntilTime: GameTime_t
 CBodyComponent: CBodyComponent
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32

105 CHostage
 m_fFlags: uint32
 m_nInteractsExclude: uint64
 m_nCollisionFunctionMask: uint8
 m_vDecalForwardAxis: Vector
 m_flexWeight: CNetworkUtlVectorBase< float32 >
 m_vecY: CNetworkedQuantizedFloat
 m_bClientSideRagdoll: bool
 m_ubInterpolationFrame: uint8
 m_flFieldOfView: float32
 m_nEnablePhysics: uint8
 m_timescale: float32
 m_lifeState: uint8
 m_timestamp: GameTime_t
 m_flCreateTime: GameTime_t
 m_vDecalPosition: Vector
 m_bAnimGraphUpdateEnabled: bool
 m_isRescued: bool
 CBodyComponent: CBodyComponent
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_name: CUtlStringToken
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_nHitboxSet: uint8
  m_MeshGroupMask: uint64
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nBoolVariablesCount: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bInitiallyPopulateInterpHistory: bool
 m_CollisionGroup: uint8
 m_nGlowRangeMin: int32
 m_flElasticity: float32
 m_flDropStartTime: GameTime_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_vecSpecifiedSurroundingMins: Vector
 m_flGlowTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flRescueStartTime: GameTime_t
 m_vecForce: Vector
 m_blinktoggle: bool
 m_clrRender: Color
 m_flGlowBackfaceMult: float32
 m_fadeMaxDist: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_nSurroundType: SurroundingBoundsType_t
 m_MoveType: MoveType_t
 m_nRenderFX: RenderFx_t
 m_nOwnerId: uint32
 m_triggerBloat: uint8
 m_flDecalHealBloodRate: float32
 m_iMaxHealth: int32
 m_leader: CHandle< CBaseEntity >
 m_iGlowType: int32
 m_flSimulationTime: float32
 m_nInteractsWith: uint64
 m_glowColorOverride: Color
 m_bFlashing: bool
 m_fadeMinDist: float32
 m_nRenderMode: RenderMode_t
 m_jumpedThisFrame: bool
 m_duration: float32
 m_nAddDecal: int32
 m_iGlowTeam: int32
 m_iTeamNum: uint8
 m_usSolidFlags: uint8
 m_nEntityId: uint32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flAnimTime: float32
 m_bAnimatedEveryTick: bool
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_hMyWearables: CNetworkUtlVectorBase< CHandle< CEconWearable > >
 m_bSpottedByMask: uint32[2]
 m_flNavIgnoreUntilTime: GameTime_t
 m_nHierarchyId: uint16
 m_vecX: CNetworkedQuantizedFloat
 m_nCollisionGroup: uint8
 m_nHostageState: int32
 m_vCapsuleCenter2: Vector
 m_bEligibleForScreenHighlight: bool
 m_hHostageGrabber: CHandle< CCSPlayerPawn >
 m_iHealth: int32
 m_fEffects: uint32
 m_flDecalHealHeightRate: float32
 m_nForceBone: int32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_vecZ: CNetworkedQuantizedFloat
 m_vCapsuleCenter1: Vector
 m_flFadeScale: float32
 m_MoveCollide: MoveCollide_t
 m_vel: Vector
 m_flShadowStrength: float32
 m_vLookTargetPosition: Vector
 m_nWorldGroupId: WorldGroupId_t
 m_nSolidType: SolidType_t
 m_nGlowRange: int32
 m_vecMaxs: Vector
 m_nObjectCulling: uint8
 m_bSimulatedEveryTick: bool
 m_LightGroup: CUtlStringToken
 m_bRenderToCubemaps: bool
 m_nSubclassID: CUtlStringToken
 m_bClientRagdoll: bool
 m_flGlowStartTime: float32
 m_flGrabSuccessTime: GameTime_t
 CRenderComponent: CRenderComponent
 m_nInteractsAs: uint64
 m_vecMins: Vector
 m_bHandsHaveBeenCut: bool
 m_bvDisabledHitGroups: uint32[1]
 m_flCapsuleRadius: float32
 m_bSpotted: bool

97 CFuncRotating
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_ubInterpolationFrame: uint8
 m_flElasticity: float32
 m_nInteractsWith: uint64
 m_nObjectCulling: uint8
 m_iTeamNum: uint8
 m_nHierarchyId: uint16
 m_nEnablePhysics: uint8
 m_flFadeScale: float32
 m_bAnimatedEveryTick: bool
 m_nGlowRange: int32
 m_fadeMaxDist: float32
 m_MoveType: MoveType_t
 m_fEffects: uint32
 m_nSurroundType: SurroundingBoundsType_t
 m_nGlowRangeMin: int32
 m_vDecalForwardAxis: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 CRenderComponent: CRenderComponent
 m_nRenderFX: RenderFx_t
 m_usSolidFlags: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nCollisionGroup: uint8
 m_nSolidType: SolidType_t
 m_triggerBloat: uint8
 m_bRenderToCubemaps: bool
 m_nCollisionFunctionMask: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_fadeMinDist: float32
 m_flShadowStrength: float32
 m_flCreateTime: GameTime_t
 m_nInteractsExclude: uint64
 m_flCapsuleRadius: float32
 m_iGlowType: int32
 m_flDecalHealBloodRate: float32
 m_flAnimTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_CollisionGroup: uint8
 m_bFlashing: bool
 m_nRenderMode: RenderMode_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vecMaxs: Vector
 m_bvDisabledHitGroups: uint32[1]
 m_flSimulationTime: float32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flGlowTime: float32
 m_nAddDecal: int32
 CBodyComponent: CBodyComponent
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
 m_clrRender: Color
 m_nOwnerId: uint32
 m_vecMins: Vector
 m_bEligibleForScreenHighlight: bool
 m_MoveCollide: MoveCollide_t
 m_nSubclassID: CUtlStringToken
 m_vCapsuleCenter2: Vector
 m_vDecalPosition: Vector
 m_nInteractsAs: uint64
 m_iGlowTeam: int32
 m_glowColorOverride: Color
 m_flGlowStartTime: float32
 m_flGlowBackfaceMult: float32
 m_flDecalHealHeightRate: float32
 m_bSimulatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_LightGroup: CUtlStringToken
 m_nEntityId: uint32
 m_vCapsuleCenter1: Vector

37 CCSGO_TeamIntroCounterTerroristPosition
 m_nSubclassID: CUtlStringToken
 m_MoveCollide: MoveCollide_t
 m_flElasticity: float32
 m_iItemDefinitionIndex: uint16
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_iAccountID: uint32
 m_flCreateTime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_iItemIDLow: uint32
 m_szCustomName: char[161]
 m_iEntityLevel: uint32
 m_iItemIDHigh: uint32
 m_xuid: uint64
 m_iInventoryPosition: uint32
 m_iTeamNum: uint8
 m_bSimulatedEveryTick: bool
 m_nRandom: int32
 m_nOrdinal: int32
 m_iEntityQuality: int32
 m_flAnimTime: float32
 m_fEffects: uint32
 m_flSimulationTime: float32
 m_bInitialized: bool
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_nVariant: int32
 m_sWeaponName: CUtlString
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
 CBodyComponent: CBodyComponent
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
 m_MoveType: MoveType_t

41 CCSGO_TeamSelectCounterTerroristPosition
 m_iEntityQuality: int32
 m_iInventoryPosition: uint32
 m_flCreateTime: GameTime_t
 m_nVariant: int32
 m_sWeaponName: CUtlString
 m_bAnimatedEveryTick: bool
 m_nOrdinal: int32
 m_iEntityLevel: uint32
 m_iAccountID: uint32
 m_MoveCollide: MoveCollide_t
 m_iTeamNum: uint8
 m_bSimulatedEveryTick: bool
 m_MoveType: MoveType_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_flAnimTime: float32
 CBodyComponent: CBodyComponent
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flElasticity: float32
 m_iItemIDHigh: uint32
 m_bInitialized: bool
 m_xuid: uint64
 m_szCustomName: char[161]
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nRandom: int32
 m_fEffects: uint32
 m_iItemIDLow: uint32
 m_iItemDefinitionIndex: uint16
 m_flSimulationTime: float32
 m_nSubclassID: CUtlStringToken
 m_ubInterpolationFrame: uint8

79 CEnvScreenOverlay
 m_iDesiredOverlay: int32
 m_MoveCollide: MoveCollide_t
 m_flElasticity: float32
 m_ubInterpolationFrame: uint8
 CBodyComponent: CBodyComponent
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
 m_flCreateTime: GameTime_t
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSubclassID: CUtlStringToken
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_bIsActive: bool
 m_flAnimTime: float32
 m_flSimulationTime: float32
 m_iTeamNum: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bSimulatedEveryTick: bool
 m_iszOverlayNames: CUtlSymbolLarge[10]
 m_flOverlayTimes: float32[10]
 m_flStartTime: GameTime_t
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_MoveType: MoveType_t

87 CFists
 m_nSolidType: SolidType_t
 m_bFlashing: bool
 m_szCustomName: char[161]
 m_fEffects: uint32
 m_nInteractsWith: uint64
 m_nGlowRangeMin: int32
 m_flDecalHealHeightRate: float32
 m_iNumEmptyAttacks: int32
 m_flElasticity: float32
 m_iItemIDHigh: uint32
 m_flPostponeFireReadyTime: GameTime_t
 m_nInteractsExclude: uint64
 m_vecSpecifiedSurroundingMins: Vector
 m_glowColorOverride: Color
 m_flFallbackWear: float32
 m_bInReload: bool
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_triggerBloat: uint8
 m_nObjectCulling: uint8
 m_bShouldAnimateDuringGameplayPause: bool
 m_ubInterpolationFrame: uint8
 m_iState: WeaponState_t
 m_fAccuracyPenalty: float32
 m_bBurstMode: bool
 m_nUninterruptableActivity: PlayerAnimEvent_t
 m_nEntityId: uint32
 m_flGlowBackfaceMult: float32
 m_nNextThinkTick: GameTick_t
 m_flAnimTime: float32
 m_flFadeScale: float32
 m_nFallbackStatTrak: int32
 m_bPlayingUninterruptableAct: bool
 m_nRenderFX: RenderFx_t
 m_vecMins: Vector
 m_nSurroundType: SurroundingBoundsType_t
 m_vLookTargetPosition: Vector
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
 m_flNextSecondaryAttackTickRatio: float32
 m_bClientSideRagdoll: bool
 m_nOwnerId: uint32
 m_iItemIDLow: uint32
 m_flFireSequenceStartTime: float32
 m_iClip1: int32
 m_vCapsuleCenter1: Vector
 m_OriginalOwnerXuidLow: uint32
 m_bReloadVisuallyComplete: bool
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nCollisionFunctionMask: uint8
 m_nFireSequenceStartTimeChange: int32
 m_bPlayerFireEventIsPrimary: bool
 m_bIsHauledBack: bool
 m_iClip2: int32
 m_flShadowStrength: float32
 m_iIronSightMode: int32
 CRenderComponent: CRenderComponent
 m_flGlowTime: float32
 m_iReapplyProvisionParity: int32
 m_hOuter: CHandle< CBaseEntity >
 m_iEntityLevel: uint32
 m_flRecoilIndex: float32
 m_flDroppedAtTime: GameTime_t
 m_fLastShotTime: GameTime_t
 m_iGlowType: int32
 m_flDecalHealBloodRate: float32
 m_nNextPrimaryAttackTick: GameTick_t
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nAddDecal: int32
 m_iRecoilIndex: int32
 m_nEnablePhysics: uint8
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_vecForce: Vector
 m_pReserveAmmo: int32[2]
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_flSimulationTime: float32
 m_nRenderMode: RenderMode_t
 m_nInteractsAs: uint64
 m_CollisionGroup: uint8
 m_vCapsuleCenter2: Vector
 m_flCapsuleRadius: float32
 m_OriginalOwnerXuidHigh: uint32
 CBodyComponent: CBodyComponent
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
 m_fadeMinDist: float32
 m_bClientRagdoll: bool
 m_iAccountID: uint32
 m_nCollisionGroup: uint8
 m_ProviderType: attributeprovidertypes_t
 m_iEntityQuality: int32
 m_nFallbackSeed: int32
 m_iTeamNum: uint8
 m_bSimulatedEveryTick: bool
 m_bInitiallyPopulateInterpHistory: bool
 m_bAnimGraphUpdateEnabled: bool
 m_nDropTick: GameTick_t
 m_LightGroup: CUtlStringToken
 m_iGlowTeam: int32
 m_nForceBone: int32
 m_nSubclassID: CUtlStringToken
 m_vecMaxs: Vector
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_iItemDefinitionIndex: uint16
 m_weaponMode: CSWeaponMode
 m_vDecalPosition: Vector
 m_flCreateTime: GameTime_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vecSpecifiedSurroundingMaxs: Vector
 m_fadeMaxDist: float32
 m_vDecalForwardAxis: Vector
 m_bSilencerOn: bool
 m_nViewModelIndex: uint32
 m_iOriginalTeamNumber: int32
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_bRenderToCubemaps: bool
 m_nHierarchyId: uint16
 m_usSolidFlags: uint8
 m_bEligibleForScreenHighlight: bool
 m_nFallbackPaintKit: int32
 m_bvDisabledHitGroups: uint32[1]
 m_flNextPrimaryAttackTickRatio: float32
 m_nNextSecondaryAttackTick: GameTick_t
 m_MoveCollide: MoveCollide_t
 m_MoveType: MoveType_t
 m_flGlowStartTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_iInventoryPosition: uint32
 m_clrRender: Color
 m_nGlowRange: int32
 m_bInitialized: bool

95 CFuncMonitor
 m_flAnimTime: float32
 CBodyComponent: CBodyComponent
  m_MeshGroupMask: uint64
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_materialGroup: CUtlStringToken
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
 m_nOwnerId: uint32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_glowColorOverride: Color
 m_flSimulationTime: float32
 m_MoveType: MoveType_t
 m_nRenderFX: RenderFx_t
 m_nEnablePhysics: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bSimulatedEveryTick: bool
 m_nGlowRange: int32
 m_bDraw3DSkybox: bool
 m_nSurroundType: SurroundingBoundsType_t
 m_fadeMinDist: float32
 m_nInteractsExclude: uint64
 m_usSolidFlags: uint8
 m_targetCamera: CUtlString
 m_fEffects: uint32
 m_clrRender: Color
 m_vCapsuleCenter1: Vector
 m_flGlowTime: float32
 m_iGlowType: int32
 m_bFlashing: bool
 m_bEligibleForScreenHighlight: bool
 m_flFadeScale: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nInteractsAs: uint64
 m_nInteractsWith: uint64
 m_nHierarchyId: uint16
 m_vDecalPosition: Vector
 m_brushModelName: CUtlString
 m_hTargetCamera: CHandle< CBaseEntity >
 CRenderComponent: CRenderComponent
 m_flGlowBackfaceMult: float32
 m_vDecalForwardAxis: Vector
 m_flDecalHealBloodRate: float32
 m_bEnabled: bool
 m_nRenderMode: RenderMode_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vecMins: Vector
 m_nSolidType: SolidType_t
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flCreateTime: GameTime_t
 m_nAddDecal: int32
 m_nResolutionEnum: int32
 m_nObjectCulling: uint8
 m_bAnimatedEveryTick: bool
 m_bRenderToCubemaps: bool
 m_nEntityId: uint32
 m_fadeMaxDist: float32
 m_flShadowStrength: float32
 m_bRenderShadows: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_nCollisionFunctionMask: uint8
 m_vecMaxs: Vector
 m_CollisionGroup: uint8
 m_flDecalHealHeightRate: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nCollisionGroup: uint8
 m_triggerBloat: uint8
 m_flGlowStartTime: float32
 m_bUseUniqueColorTarget: bool
 m_bvDisabledHitGroups: uint32[1]
 m_vCapsuleCenter2: Vector
 m_flCapsuleRadius: float32
 m_nGlowRangeMin: int32
 m_ubInterpolationFrame: uint8
 m_iTeamNum: uint8
 m_flElasticity: float32
 m_LightGroup: CUtlStringToken
 m_MoveCollide: MoveCollide_t
 m_nSubclassID: CUtlStringToken
 m_vecSpecifiedSurroundingMins: Vector
 m_iGlowTeam: int32

116 CItem_Healthshot
 m_flSimulationTime: float32
 m_MoveCollide: MoveCollide_t
 m_nRenderMode: RenderMode_t
 m_nRenderFX: RenderFx_t
 m_nSurroundType: SurroundingBoundsType_t
 m_bClientRagdoll: bool
 m_iEntityLevel: uint32
 m_iIronSightMode: int32
 m_nForceBone: int32
 m_bClientSideRagdoll: bool
 m_nFireSequenceStartTimeChange: int32
 m_bPlayerFireEventIsPrimary: bool
 m_fAccuracyPenalty: float32
 m_nNextSecondaryAttackTick: GameTick_t
 m_flGlowTime: float32
 m_nSolidType: SolidType_t
 m_nGlowRangeMin: int32
 m_flDecalHealHeightRate: float32
 m_bReloadVisuallyComplete: bool
 m_bSilencerOn: bool
 m_nWorldGroupId: WorldGroupId_t
 m_flAnimTime: float32
 m_nCollisionFunctionMask: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_hOwner: CHandle< CBaseEntity >
  m_Transforms: CNetworkUtlVectorBase< CTransform >
 m_bInitialized: bool
 m_weaponMode: CSWeaponMode
 m_flDroppedAtTime: GameTime_t
 m_duration: float32
 m_nViewModelIndex: uint32
 m_flFallbackWear: float32
 m_timescale: float32
 m_nNextPrimaryAttackTick: GameTick_t
 m_flCapsuleRadius: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bRenderToCubemaps: bool
 m_hOuter: CHandle< CBaseEntity >
 m_iItemIDLow: uint32
 m_flFireSequenceStartTime: float32
 m_iNumEmptyAttacks: int32
 m_MoveType: MoveType_t
 m_nObjectCulling: uint8
 m_nFallbackPaintKit: int32
 m_nEnablePhysics: uint8
 m_iGlowType: int32
 m_flGlowStartTime: float32
 m_bAnimGraphUpdateEnabled: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_LightGroup: CUtlStringToken
 m_nInteractsWith: uint64
 m_nCollisionGroup: uint8
 m_ProviderType: attributeprovidertypes_t
 m_vecMins: Vector
 m_vecForce: Vector
 m_nFallbackSeed: int32
 m_iState: WeaponState_t
 m_flPostponeFireReadyTime: GameTime_t
 m_bSimulatedEveryTick: bool
 m_bAnimatedEveryTick: bool
 m_usSolidFlags: uint8
 m_flGlowBackfaceMult: float32
 m_OriginalOwnerXuidHigh: uint32
 m_iClip1: int32
 m_iEntityQuality: int32
 m_nFallbackStatTrak: int32
 m_flTimeSilencerSwitchComplete: GameTime_t
 CBodyComponent: CBodyComponent
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_nNewSequenceParity: int32
  m_bClientSideAnimation: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_hParent: CGameSceneNodeHandle
  m_nRandomSeedOffset: int32
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_angRotation: QAngle
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_nBoolVariablesCount: int32
  m_cellZ: uint16
 m_flCreateTime: GameTime_t
 m_nHierarchyId: uint16
 m_vCapsuleCenter1: Vector
 m_flFadeScale: float32
 m_nAddDecal: int32
 m_vDecalForwardAxis: Vector
 m_iOriginalTeamNumber: int32
 m_pReserveAmmo: int32[2]
 m_iItemIDHigh: uint32
 m_iInventoryPosition: uint32
 m_szCustomName: char[161]
 m_timestamp: GameTime_t
 m_flNextPrimaryAttackTickRatio: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_bBurstMode: bool
 m_flNextSecondaryAttackTickRatio: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flElasticity: float32
 m_nOwnerId: uint32
 m_vecMaxs: Vector
 m_iReapplyProvisionParity: int32
 m_iItemDefinitionIndex: uint16
 m_iAccountID: uint32
 m_iClip2: int32
 m_bEligibleForScreenHighlight: bool
 m_bIsHauledBack: bool
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_nEntityId: uint32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bFlashing: bool
 m_bShouldAnimateDuringGameplayPause: bool
 m_vLookTargetPosition: Vector
 m_iRecoilIndex: int32
 m_flNavIgnoreUntilTime: GameTime_t
 m_clrRender: Color
 m_nInteractsAs: uint64
 m_iGlowTeam: int32
 CRenderComponent: CRenderComponent
 m_iTeamNum: uint8
 m_glowColorOverride: Color
 m_fadeMinDist: float32
 m_flShadowStrength: float32
 m_triggerBloat: uint8
 m_nGlowRange: int32
 m_fadeMaxDist: float32
 m_flDecalHealBloodRate: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bInReload: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsExclude: uint64
 m_vCapsuleCenter2: Vector
 m_fEffects: uint32
 m_bRedraw: bool
 m_bvDisabledHitGroups: uint32[1]
 m_nSubclassID: CUtlStringToken
 m_ubInterpolationFrame: uint8
 m_CollisionGroup: uint8
 m_vDecalPosition: Vector
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
 m_flRecoilIndex: float32
 m_fLastShotTime: GameTime_t
 m_OriginalOwnerXuidLow: uint32
 m_nDropTick: GameTick_t
 m_nNextThinkTick: GameTick_t

200 CWeaponBaseItem
 m_iInventoryPosition: uint32
 m_duration: float32
 m_iClip1: int32
 CBodyComponent: CBodyComponent
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
 m_MoveCollide: MoveCollide_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_nRenderFX: RenderFx_t
 m_OriginalOwnerXuidLow: uint32
 m_fEffects: uint32
 m_iEntityLevel: uint32
 m_iState: WeaponState_t
 m_bInReload: bool
 m_pReserveAmmo: int32[2]
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_vecForce: Vector
 CRenderComponent: CRenderComponent
 m_nInteractsWith: uint64
 m_nAddDecal: int32
 m_flPostponeFireReadyTime: GameTime_t
 m_timescale: float32
 m_nWorldGroupId: WorldGroupId_t
 m_nSubclassID: CUtlStringToken
 m_bEligibleForScreenHighlight: bool
 m_nSurroundType: SurroundingBoundsType_t
 m_vDecalPosition: Vector
 m_iItemIDHigh: uint32
 m_flDroppedAtTime: GameTime_t
 m_clrRender: Color
 m_bInitiallyPopulateInterpHistory: bool
 m_bClientRagdoll: bool
 m_bRedraw: bool
 m_vecSpecifiedSurroundingMins: Vector
 m_hOuter: CHandle< CBaseEntity >
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_fadeMaxDist: float32
 m_vDecalForwardAxis: Vector
 m_nFireSequenceStartTimeChange: int32
 m_nRenderMode: RenderMode_t
 m_LightGroup: CUtlStringToken
 m_usSolidFlags: uint8
 m_flFadeScale: float32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_iIronSightMode: int32
 m_nEntityId: uint32
 m_nHierarchyId: uint16
 m_iGlowType: int32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_iAccountID: uint32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_nSolidType: SolidType_t
 m_iGlowTeam: int32
 m_nGlowRange: int32
 m_flGlowBackfaceMult: float32
 m_bBurstMode: bool
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flDecalHealHeightRate: float32
 m_nForceBone: int32
 m_nFallbackStatTrak: int32
 m_timestamp: GameTime_t
 m_nNextPrimaryAttackTick: GameTick_t
 m_iTeamNum: uint8
 m_bFlashing: bool
 m_nObjectCulling: uint8
 m_iEntityQuality: int32
 m_flRecoilIndex: float32
 m_flNextPrimaryAttackTickRatio: float32
 m_nDropTick: GameTick_t
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveType: MoveType_t
 m_nEnablePhysics: uint8
 m_flCapsuleRadius: float32
 m_weaponMode: CSWeaponMode
 m_bIsHauledBack: bool
 m_flAnimTime: float32
 m_vCapsuleCenter2: Vector
 m_flGlowTime: float32
 m_fAccuracyPenalty: float32
 m_nNextThinkTick: GameTick_t
 m_bSimulatedEveryTick: bool
 m_nViewModelIndex: uint32
 m_nFallbackSeed: int32
 m_bPlayerFireEventIsPrimary: bool
 m_bReloadVisuallyComplete: bool
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_bvDisabledHitGroups: uint32[1]
 m_bAnimGraphUpdateEnabled: bool
 m_flNextSecondaryAttackTickRatio: float32
 m_ubInterpolationFrame: uint8
 m_bAnimatedEveryTick: bool
 m_flElasticity: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsExclude: uint64
 m_nNextSecondaryAttackTick: GameTick_t
 m_fLastShotTime: GameTime_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bRenderToCubemaps: bool
 m_vecMins: Vector
 m_ProviderType: attributeprovidertypes_t
 m_iItemDefinitionIndex: uint16
 m_flFallbackWear: float32
 m_nCollisionGroup: uint8
 m_vLookTargetPosition: Vector
 m_iItemIDLow: uint32
 m_nInteractsAs: uint64
 m_nCollisionFunctionMask: uint8
 m_vCapsuleCenter1: Vector
 m_iReapplyProvisionParity: int32
 m_iNumEmptyAttacks: int32
 m_flSimulationTime: float32
 m_nGlowRangeMin: int32
 m_glowColorOverride: Color
 m_fadeMinDist: float32
 m_nFallbackPaintKit: int32
 m_iRecoilIndex: int32
 m_bClientSideRagdoll: bool
 m_iClip2: int32
 m_vecMaxs: Vector
 m_szCustomName: char[161]
 m_flFireSequenceStartTime: float32
 m_iOriginalTeamNumber: int32
 m_triggerBloat: uint8
 m_flGlowStartTime: float32
 m_bInitialized: bool
 m_OriginalOwnerXuidHigh: uint32
 m_bShouldAnimateDuringGameplayPause: bool
 m_bSilencerOn: bool
 m_flCreateTime: GameTime_t
 m_nOwnerId: uint32
 m_CollisionGroup: uint8
 m_flShadowStrength: float32
 m_flDecalHealBloodRate: float32

209 CWeaponGlock
 m_iEntityQuality: int32
 m_ubInterpolationFrame: uint8
 m_CollisionGroup: uint8
 m_nEnablePhysics: uint8
 m_flGlowStartTime: float32
 m_flDecalHealHeightRate: float32
 m_nEntityId: uint32
 m_flFadeScale: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_bClientRagdoll: bool
 m_nFallbackStatTrak: int32
 m_MoveCollide: MoveCollide_t
 m_iTeamNum: uint8
 m_iGlowType: int32
 m_nAddDecal: int32
 m_bBurstMode: bool
 m_nInteractsAs: uint64
 m_vLookTargetPosition: Vector
 m_iItemDefinitionIndex: uint16
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_flNextPrimaryAttackTickRatio: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nOwnerId: uint32
 m_flGlowTime: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_iInventoryPosition: uint32
 m_bClientSideRagdoll: bool
 m_fadeMaxDist: float32
 m_flDecalHealBloodRate: float32
 m_bInitialized: bool
 m_OriginalOwnerXuidLow: uint32
 m_nFallbackSeed: int32
 m_iBurstShotsRemaining: int32
 m_vecSpecifiedSurroundingMins: Vector
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nObjectCulling: uint8
 m_bvDisabledHitGroups: uint32[1]
 m_nNextPrimaryAttackTick: GameTick_t
 m_flAnimTime: float32
 m_flSimulationTime: float32
 m_vCapsuleCenter2: Vector
 m_bFlashing: bool
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flNavIgnoreUntilTime: GameTime_t
 m_glowColorOverride: Color
 m_flCreateTime: GameTime_t
 m_nGlowRange: int32
 m_nGlowRangeMin: int32
 m_bAnimGraphUpdateEnabled: bool
 m_vecForce: Vector
 m_nDropTick: GameTick_t
 m_iAccountID: uint32
 m_flFallbackWear: float32
 m_iReapplyProvisionParity: int32
 m_OriginalOwnerXuidHigh: uint32
 m_iNumEmptyAttacks: int32
 CRenderComponent: CRenderComponent
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vCapsuleCenter1: Vector
 m_nFallbackPaintKit: int32
 m_bSilencerOn: bool
 m_ProviderType: attributeprovidertypes_t
 m_bSimulatedEveryTick: bool
 m_nRenderFX: RenderFx_t
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_fEffects: uint32
 m_nCollisionFunctionMask: uint8
 m_triggerBloat: uint8
 m_fadeMinDist: float32
 m_iItemIDHigh: uint32
 m_iClip1: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecMaxs: Vector
 m_bInitiallyPopulateInterpHistory: bool
 m_fAccuracyPenalty: float32
 m_bReloadVisuallyComplete: bool
 m_flNextSecondaryAttackTickRatio: float32
 m_nHierarchyId: uint16
 m_flGlowBackfaceMult: float32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
 m_szCustomName: char[161]
 m_zoomLevel: int32
 m_bNeedsBoltAction: bool
 m_usSolidFlags: uint8
 m_nSolidType: SolidType_t
 m_iGlowTeam: int32
 m_flShadowStrength: float32
 m_nForceBone: int32
 m_flFireSequenceStartTime: float32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_iClip2: int32
 m_vDecalForwardAxis: Vector
 m_nNextThinkTick: GameTick_t
 m_iOriginalTeamNumber: int32
 m_nInteractsWith: uint64
 m_vecMins: Vector
 m_hOuter: CHandle< CBaseEntity >
 m_bPlayerFireEventIsPrimary: bool
 m_flDroppedAtTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_nViewModelIndex: uint32
 m_clrRender: Color
 m_nInteractsExclude: uint64
 m_nCollisionGroup: uint8
 m_iRecoilIndex: int32
 m_iIronSightMode: int32
 m_bAnimatedEveryTick: bool
 m_iEntityLevel: uint32
 m_iItemIDLow: uint32
 m_pReserveAmmo: int32[2]
 CBodyComponent: CBodyComponent
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_nNewSequenceParity: int32
  m_bClientSideAnimation: bool
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_hParent: CGameSceneNodeHandle
  m_nRandomSeedOffset: int32
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
 m_nSurroundType: SurroundingBoundsType_t
 m_bEligibleForScreenHighlight: bool
 m_vDecalPosition: Vector
 m_MoveType: MoveType_t
 m_weaponMode: CSWeaponMode
 m_bInReload: bool
 m_nNextSecondaryAttackTick: GameTick_t
 m_nSubclassID: CUtlStringToken
 m_flElasticity: float32
 m_LightGroup: CUtlStringToken
 m_bRenderToCubemaps: bool
 m_flRecoilIndex: float32
 m_flPostponeFireReadyTime: GameTime_t
 m_flCapsuleRadius: float32
 m_iState: WeaponState_t
 m_nFireSequenceStartTimeChange: int32
 m_bIsHauledBack: bool
 m_fLastShotTime: GameTime_t

132 COmniLight
 m_clrRender: Color
 m_bEligibleForScreenHighlight: bool
 m_flFadeScale: float32
 m_flBrightness: float32
 m_flBrightnessScale: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsExclude: uint64
 m_nHierarchyId: uint16
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bPrecomputedFieldsValid: bool
 m_nCollisionFunctionMask: uint8
 m_flDecalHealHeightRate: float32
 m_nSolidType: SolidType_t
 m_bEnabled: bool
 m_nShadowPriority: int32
 m_bvDisabledHitGroups: uint32[1]
 m_flSkirt: float32
 m_flFogScale: float32
 m_flAnimTime: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_triggerBloat: uint8
 m_iGlowTeam: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flSoftY: float32
 m_bContactShadow: bool
 m_flShadowFadeSizeStart: float32
 m_MoveCollide: MoveCollide_t
 m_flElasticity: float32
 m_nEnablePhysics: uint8
 m_Color: Color
 m_vSizeParams: Vector
 m_vecMaxs: Vector
 m_flGlowStartTime: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flSoftX: float32
 m_vPrecomputedOBBAngles: QAngle
 m_flSimulationTime: float32
 m_vDecalPosition: Vector
 m_flColorTemperature: float32
 m_vBakeSpecularToCubemapsSize: Vector
 m_nSubclassID: CUtlStringToken
 m_nLuminaireShape: int32
 m_nFogShadows: int32
 m_flGlowBackfaceMult: float32
 m_flLuminaireAnisotropy: float32
 m_nRenderMode: RenderMode_t
 m_flFogStrength: float32
 m_flLuminaireSize: float32
 CRenderComponent: CRenderComponent
 m_MoveType: MoveType_t
 m_hLightCookie: CStrongHandle< InfoForResourceTypeCTextureBase >
 m_fAlternateColorBrightness: float32
 m_flMinRoughness: float32
 m_fEffects: uint32
 m_flNavIgnoreUntilTime: GameTime_t
 m_nCollisionGroup: uint8
 m_iGlowType: int32
 CBodyComponent: CBodyComponent
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
 m_flGlowTime: float32
 m_fadeMaxDist: float32
 m_flShadowFadeSizeEnd: float32
 m_vCapsuleCenter2: Vector
 m_flDecalHealBloodRate: float32
 m_flSkirtNear: float32
 m_nCastShadows: int32
 m_flOuterAngle: float32
 m_vPrecomputedBoundsMaxs: Vector
 m_nRenderFX: RenderFx_t
 m_nInteractsWith: uint64
 m_nObjectCulling: uint8
 m_nFog: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bAnimatedEveryTick: bool
 m_CollisionGroup: uint8
 m_nAddDecal: int32
 m_flShadowStrength: float32
 m_nColorMode: int32
 m_flLightStyleStartTime: GameTime_t
 m_vPrecomputedBoundsMins: Vector
 m_vPrecomputedOBBOrigin: Vector
 m_bShowLight: bool
 m_vCapsuleCenter1: Vector
 m_flCapsuleRadius: float32
 m_LightStyleTargets: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_flInnerAngle: float32
 m_vecSpecifiedSurroundingMins: Vector
 m_nBakeSpecularToCubemaps: int32
 m_nBounceLight: int32
 m_flBounceScale: float32
 m_nDirectLight: int32
 m_flRange: float32
 m_nShadowMapSize: int32
 m_flCreateTime: GameTime_t
 m_nInteractsAs: uint64
 m_usSolidFlags: uint8
 m_glowColorOverride: Color
 m_bRenderToCubemaps: bool
 m_flFadeSizeStart: float32
 m_flFadeSizeEnd: float32
 m_ubInterpolationFrame: uint8
 m_nOwnerId: uint32
 m_flShape: float32
 m_vAlternateColor: Vector
 m_nSurroundType: SurroundingBoundsType_t
 m_nBakedShadowIndex: int32
 m_QueuedLightStyleStrings: CNetworkUtlVectorBase< CUtlString >
 m_vShear: Vector
 m_bSimulatedEveryTick: bool
 m_nGlowRange: int32
 m_LightStyleEvents: CNetworkUtlVectorBase< CUtlString >
 m_vecMins: Vector
 m_nGlowRangeMin: int32
 m_LightStyleString: CUtlString
 m_vPrecomputedOBBExtent: Vector
 m_nEntityId: uint32
 m_bFlashing: bool
 m_fadeMinDist: float32
 m_vDecalForwardAxis: Vector
 m_iTeamNum: uint8
 m_LightGroup: CUtlStringToken

152 CPointClientUIWorldTextPanel
 m_flAnimTime: float32
 m_flGlowStartTime: float32
 m_vDecalForwardAxis: Vector
 m_bEnabled: bool
 m_unOrientation: uint32
 m_nRenderFX: RenderFx_t
 m_vecMins: Vector
 m_flDecalHealHeightRate: float32
 m_nExplicitImageLayout: int32
 m_MoveCollide: MoveCollide_t
 m_nInteractsAs: uint64
 m_nEnablePhysics: uint8
 m_unOwnerContext: uint32
 CBodyComponent: CBodyComponent
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
 m_nSubclassID: CUtlStringToken
 m_fEffects: uint32
 m_nRenderMode: RenderMode_t
 m_vCapsuleCenter1: Vector
 m_flInteractDistance: float32
 m_flElasticity: float32
 m_CollisionGroup: uint8
 m_vCapsuleCenter2: Vector
 m_fadeMaxDist: float32
 m_bFollowPlayerAcrossTeleport: bool
 m_flHeight: float32
 m_bvDisabledHitGroups: uint32[1]
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nHierarchyId: uint16
 m_usSolidFlags: uint8
 m_iGlowType: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bSimulatedEveryTick: bool
 m_nInteractsExclude: uint64
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bLit: bool
 m_unVerticalAlign: uint32
 m_bNoDepth: bool
 m_bExcludeFromSaveGames: bool
 m_MoveType: MoveType_t
 m_iTeamNum: uint8
 m_glowColorOverride: Color
 m_flShadowStrength: float32
 m_flDecalHealBloodRate: float32
 m_flWidth: float32
 m_messageText: char[512]
 m_flNavIgnoreUntilTime: GameTime_t
 m_LightGroup: CUtlStringToken
 m_nCollisionGroup: uint8
 m_bEligibleForScreenHighlight: bool
 m_PanelID: CUtlSymbolLarge
 m_flDPI: float32
 m_vecCSSClasses: CNetworkUtlVectorBase< CUtlSymbolLarge >
 m_nInteractsWith: uint64
 m_nCollisionFunctionMask: uint8
 m_triggerBloat: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_vecSpecifiedSurroundingMins: Vector
 m_flCapsuleRadius: float32
 CRenderComponent: CRenderComponent
 m_nOwnerId: uint32
 m_vecMaxs: Vector
 m_flGlowTime: float32
 m_nAddDecal: int32
 m_clrRender: Color
 m_bUseOffScreenIndicator: bool
 m_bAllowInteractionFromAllSceneWorlds: bool
 m_bRenderBackface: bool
 m_ubInterpolationFrame: uint8
 m_bAnimatedEveryTick: bool
 m_iGlowTeam: int32
 m_nObjectCulling: uint8
 m_bIgnoreInput: bool
 m_flDepthOffset: float32
 m_nSolidType: SolidType_t
 m_nGlowRange: int32
 m_flGlowBackfaceMult: float32
 m_fadeMinDist: float32
 m_bGrabbable: bool
 m_bDisableMipGen: bool
 m_vDecalPosition: Vector
 m_unHorizontalAlign: uint32
 m_flSimulationTime: float32
 m_flCreateTime: GameTime_t
 m_bRenderToCubemaps: bool
 m_nEntityId: uint32
 m_nGlowRangeMin: int32
 m_bFlashing: bool
 m_bOpaque: bool
 m_bOnlyRenderToTexture: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flFadeScale: float32
 m_DialogXMLName: CUtlSymbolLarge
 m_PanelClassName: CUtlSymbolLarge

192 CTriggerBuoyancy
 m_MoveType: MoveType_t
 m_flElasticity: float32
 m_flDecalHealBloodRate: float32
 m_nGlowRangeMin: int32
 m_flGlowStartTime: float32
 m_nSubclassID: CUtlStringToken
 m_spawnflags: uint32
 m_bAnimatedEveryTick: bool
 m_nInteractsExclude: uint64
 m_fadeMaxDist: float32
 m_flFadeScale: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nInteractsAs: uint64
 m_vecMaxs: Vector
 m_nSolidType: SolidType_t
 m_vDecalForwardAxis: Vector
 CBodyComponent: CBodyComponent
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
 m_bSimulatedEveryTick: bool
 m_nCollisionGroup: uint8
 m_vCapsuleCenter1: Vector
 m_iTeamNum: uint8
 m_flDecalHealHeightRate: float32
 m_bClientSidePredicted: bool
 m_MoveCollide: MoveCollide_t
 m_nInteractsWith: uint64
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nObjectCulling: uint8
 m_nCollisionFunctionMask: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_iGlowTeam: int32
 m_bFlashing: bool
 m_clrRender: Color
 m_nHierarchyId: uint16
 m_flCapsuleRadius: float32
 m_bDisabled: bool
 m_flFluidDensity: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_CollisionGroup: uint8
 m_bEligibleForScreenHighlight: bool
 m_nEntityId: uint32
 m_glowColorOverride: Color
 CRenderComponent: CRenderComponent
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flSimulationTime: float32
 m_flCreateTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_usSolidFlags: uint8
 m_bRenderToCubemaps: bool
 m_flGlowBackfaceMult: float32
 m_fadeMinDist: float32
 m_flAnimTime: float32
 m_flGlowTime: float32
 m_nAddDecal: int32
 m_bvDisabledHitGroups: uint32[1]
 m_triggerBloat: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_vDecalPosition: Vector
 m_fEffects: uint32
 m_flNavIgnoreUntilTime: GameTime_t
 m_nRenderFX: RenderFx_t
 m_nOwnerId: uint32
 m_ubInterpolationFrame: uint8
 m_vCapsuleCenter2: Vector
 m_nGlowRange: int32
 m_flShadowStrength: float32
 m_iGlowType: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_LightGroup: CUtlStringToken
 m_vecMins: Vector
 m_nEnablePhysics: uint8

225 CWeaponSSG08
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nInteractsExclude: uint64
 m_flDecalHealHeightRate: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nRenderFX: RenderFx_t
 m_glowColorOverride: Color
 m_flGlowTime: float32
 m_vDecalForwardAxis: Vector
 m_bInitiallyPopulateInterpHistory: bool
 m_flDroppedAtTime: GameTime_t
 m_iTeamNum: uint8
 m_iGlowTeam: int32
 m_flShadowStrength: float32
 m_iItemDefinitionIndex: uint16
 m_bReloadVisuallyComplete: bool
 m_nCollisionFunctionMask: uint8
 m_triggerBloat: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_flPostponeFireReadyTime: GameTime_t
 m_bSilencerOn: bool
 m_flNextSecondaryAttackTickRatio: float32
 m_flAnimTime: float32
 m_iOriginalTeamNumber: int32
 m_iBurstShotsRemaining: int32
 m_iClip2: int32
 m_nFallbackStatTrak: int32
 m_bEligibleForScreenHighlight: bool
 m_fadeMaxDist: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_vLookTargetPosition: Vector
 m_flSimulationTime: float32
 m_flCapsuleRadius: float32
 m_clrRender: Color
 m_vecForce: Vector
 m_bInReload: bool
 m_fLastShotTime: GameTime_t
 m_pReserveAmmo: int32[2]
 m_nRenderMode: RenderMode_t
 m_flGlowStartTime: float32
 m_iEntityLevel: uint32
 m_flRecoilIndex: float32
 m_nViewModelIndex: uint32
 m_flElasticity: float32
 m_iGlowType: int32
 m_flFadeScale: float32
 m_iIronSightMode: int32
 m_nSolidType: SolidType_t
 m_nNextPrimaryAttackTick: GameTick_t
 m_bvDisabledHitGroups: uint32[1]
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_iEntityQuality: int32
 m_szCustomName: char[161]
 m_bBurstMode: bool
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_nHierarchyId: uint16
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_LightGroup: CUtlStringToken
 m_iAccountID: uint32
 m_zoomLevel: int32
 CBodyComponent: CBodyComponent
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
 m_nEntityId: uint32
 m_nSurroundType: SurroundingBoundsType_t
 m_CollisionGroup: uint8
 m_bFlashing: bool
 CRenderComponent: CRenderComponent
 m_nNextThinkTick: GameTick_t
 m_bRenderToCubemaps: bool
 m_nFallbackSeed: int32
 m_weaponMode: CSWeaponMode
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_vecMins: Vector
 m_nObjectCulling: uint8
 m_ProviderType: attributeprovidertypes_t
 m_iState: WeaponState_t
 m_nCollisionGroup: uint8
 m_nEnablePhysics: uint8
 m_nInteractsWith: uint64
 m_nOwnerId: uint32
 m_bIsHauledBack: bool
 m_MoveType: MoveType_t
 m_usSolidFlags: uint8
 m_nDropTick: GameTick_t
 m_bClientSideRagdoll: bool
 m_fadeMinDist: float32
 m_nAddDecal: int32
 m_bAnimGraphUpdateEnabled: bool
 m_nSubclassID: CUtlStringToken
 m_flGlowBackfaceMult: float32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_flNextPrimaryAttackTickRatio: float32
 m_nInteractsAs: uint64
 m_bSimulatedEveryTick: bool
 m_flFallbackWear: float32
 m_bNeedsBoltAction: bool
 m_OriginalOwnerXuidLow: uint32
 m_vCapsuleCenter1: Vector
 m_flDecalHealBloodRate: float32
 m_hOuter: CHandle< CBaseEntity >
 m_iItemIDLow: uint32
 m_OriginalOwnerXuidHigh: uint32
 m_nFireSequenceStartTimeChange: int32
 m_iNumEmptyAttacks: int32
 m_vecMaxs: Vector
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nForceBone: int32
 m_iRecoilIndex: int32
 m_ubInterpolationFrame: uint8
 m_iReapplyProvisionParity: int32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
 m_fAccuracyPenalty: float32
 m_flCreateTime: GameTime_t
 m_bShouldAnimateDuringGameplayPause: bool
 m_nNextSecondaryAttackTick: GameTick_t
 m_nGlowRange: int32
 m_vCapsuleCenter2: Vector
 m_vDecalPosition: Vector
 m_iInventoryPosition: uint32
 m_bInitialized: bool
 m_flFireSequenceStartTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flNavIgnoreUntilTime: GameTime_t
 m_iClip1: int32
 m_iItemIDHigh: uint32
 m_nGlowRangeMin: int32
 m_bClientRagdoll: bool
 m_nFallbackPaintKit: int32
 m_bPlayerFireEventIsPrimary: bool
 m_MoveCollide: MoveCollide_t

20 CBeam
 m_nAttachIndex: AttachmentHandle_t[10]
 m_fHaloScale: float32
 m_nClipStyle: BeamClipStyle_t
 m_bTurnedOff: bool
 m_flHDRColorScale: float32
 m_nNumBeamEnts: uint8
 m_nHaloIndex: CStrongHandle< InfoForResourceTypeIMaterial2 >
 m_nBeamFlags: uint32
 m_flFrameRate: float32
 m_fWidth: float32
 m_fStartFrame: float32
 m_nBeamType: BeamType_t
 m_fEndWidth: float32
 m_fAmplitude: float32
 m_vecEndPos: Vector
 m_clrRender: Color
 CBodyComponent: CBodyComponent
  m_cellY: uint16
  m_cellZ: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_nOutsideWorld: uint16
  m_cellX: uint16
 m_nRenderMode: RenderMode_t
 m_nRenderFX: RenderFx_t
 m_fSpeed: float32
 m_flFrame: float32
 m_hBaseMaterial: CStrongHandle< InfoForResourceTypeIMaterial2 >
 m_hAttachEntity: CHandle< CBaseEntity >[10]
 m_fFadeLength: float32

39 CCSGO_TeamPreviewCharacterPosition
 m_MoveCollide: MoveCollide_t
 m_ubInterpolationFrame: uint8
 m_nOrdinal: int32
 m_iAccountID: uint32
 m_iInventoryPosition: uint32
 m_MoveType: MoveType_t
 m_iTeamNum: uint8
 m_bSimulatedEveryTick: bool
 m_iItemIDLow: uint32
 m_iItemIDHigh: uint32
 m_flSimulationTime: float32
 CBodyComponent: CBodyComponent
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
 m_flElasticity: float32
 m_sWeaponName: CUtlString
 m_flNavIgnoreUntilTime: GameTime_t
 m_nVariant: int32
 m_iEntityLevel: uint32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSubclassID: CUtlStringToken
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_szCustomName: char[161]
 m_xuid: uint64
 m_iItemDefinitionIndex: uint16
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nRandom: int32
 m_bInitialized: bool
 m_flAnimTime: float32
 m_flCreateTime: GameTime_t
 m_bAnimatedEveryTick: bool
 m_iEntityQuality: int32

103 CHandleTest
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_bSimulatedEveryTick: bool
 m_bAnimatedEveryTick: bool
 m_Handle: CHandle< CBaseEntity >
 m_flSimulationTime: float32
 m_MoveCollide: MoveCollide_t
 m_ubInterpolationFrame: uint8
 m_flCreateTime: GameTime_t
 m_iTeamNum: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flAnimTime: float32
 CBodyComponent: CBodyComponent
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveType: MoveType_t
 m_nSubclassID: CUtlStringToken
 m_fEffects: uint32
 m_flElasticity: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_bSendHandle: bool

117 CItemCash
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bRenderToCubemaps: bool
 m_bClientRagdoll: bool
 m_nEnablePhysics: uint8
 m_flGlowTime: float32
 m_fadeMinDist: float32
 m_flShadowStrength: float32
 m_flAnimTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_bClientSideRagdoll: bool
 m_nEntityId: uint32
 CBodyComponent: CBodyComponent
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_name: CUtlStringToken
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_nHitboxSet: uint8
  m_MeshGroupMask: uint64
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nBoolVariablesCount: int32
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
 m_nSubclassID: CUtlStringToken
 m_vDecalForwardAxis: Vector
 m_flCreateTime: GameTime_t
 m_iGlowTeam: int32
 m_vecSpecifiedSurroundingMins: Vector
 m_vCapsuleCenter1: Vector
 m_flCapsuleRadius: float32
 m_iGlowType: int32
 m_flElasticity: float32
 m_nRenderMode: RenderMode_t
 m_nRenderFX: RenderFx_t
 m_nCollisionGroup: uint8
 m_flFadeScale: float32
 m_vDecalPosition: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nForceBone: int32
 m_MoveCollide: MoveCollide_t
 m_iTeamNum: uint8
 m_bSimulatedEveryTick: bool
 m_nInteractsAs: uint64
 m_flDecalHealHeightRate: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_hOwner: CHandle< CBaseEntity >
  m_Transforms: CNetworkUtlVectorBase< CTransform >
 m_nOwnerId: uint32
 m_usSolidFlags: uint8
 m_bFlashing: bool
 m_nObjectCulling: uint8
 m_MoveType: MoveType_t
 m_triggerBloat: uint8
 m_flGlowBackfaceMult: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_nInteractsWith: uint64
 m_bEligibleForScreenHighlight: bool
 CRenderComponent: CRenderComponent
 m_fadeMaxDist: float32
 m_ubInterpolationFrame: uint8
 m_bAnimatedEveryTick: bool
 m_CollisionGroup: uint8
 m_nGlowRangeMin: int32
 m_bAnimGraphUpdateEnabled: bool
 m_bvDisabledHitGroups: uint32[1]
 m_nHierarchyId: uint16
 m_vecMaxs: Vector
 m_nSolidType: SolidType_t
 m_flGlowStartTime: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bShouldAnimateDuringGameplayPause: bool
 m_vecForce: Vector
 m_nAddDecal: int32
 m_flSimulationTime: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_LightGroup: CUtlStringToken
 m_nSurroundType: SurroundingBoundsType_t
 m_clrRender: Color
 m_nInteractsExclude: uint64
 m_nCollisionFunctionMask: uint8
 m_glowColorOverride: Color
 m_flDecalHealBloodRate: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_fEffects: uint32
 m_vecMins: Vector
 m_vCapsuleCenter2: Vector
 m_nGlowRange: int32

80 CEnvSky
 m_vecMins: Vector
 m_flGlowTime: float32
 m_flGlowBackfaceMult: float32
 m_vTintColor: Color
 m_flSimulationTime: float32
 m_bEligibleForScreenHighlight: bool
 m_flBrightnessScale: float32
 m_bFlashing: bool
 m_MoveCollide: MoveCollide_t
 m_clrRender: Color
 m_LightGroup: CUtlStringToken
 m_usSolidFlags: uint8
 m_iGlowTeam: int32
 m_nRenderFX: RenderFx_t
 m_flDecalHealHeightRate: float32
 m_nFogType: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flFadeScale: float32
 m_MoveType: MoveType_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flNavIgnoreUntilTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_CollisionGroup: uint8
 m_flCreateTime: GameTime_t
 m_nEntityId: uint32
 m_nCollisionFunctionMask: uint8
 m_vCapsuleCenter2: Vector
 m_flFogMinEnd: float32
 m_bEnabled: bool
 CRenderComponent: CRenderComponent
 m_bSimulatedEveryTick: bool
 m_nInteractsWith: uint64
 m_iGlowType: int32
 m_nAddDecal: int32
 m_flFogMinStart: float32
 m_vTintColorLightingOnly: Color
 m_nCollisionGroup: uint8
 m_triggerBloat: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_flDecalHealBloodRate: float32
 m_bStartDisabled: bool
 m_flFogMaxStart: float32
 m_flAnimTime: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vecMaxs: Vector
 m_vecSpecifiedSurroundingMins: Vector
 m_vDecalPosition: Vector
 m_bRenderToCubemaps: bool
 m_flGlowStartTime: float32
 m_hSkyMaterialLightingOnly: CStrongHandle< InfoForResourceTypeIMaterial2 >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSubclassID: CUtlStringToken
 m_iTeamNum: uint8
 m_vCapsuleCenter1: Vector
 m_flFogMaxEnd: float32
 m_fEffects: uint32
 m_nGlowRange: int32
 m_glowColorOverride: Color
 m_fadeMinDist: float32
 m_flShadowStrength: float32
 m_bvDisabledHitGroups: uint32[1]
 m_ubInterpolationFrame: uint8
 m_nInteractsAs: uint64
 m_nSolidType: SolidType_t
 m_nGlowRangeMin: int32
 m_fadeMaxDist: float32
 CBodyComponent: CBodyComponent
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
 m_flElasticity: float32
 m_nInteractsExclude: uint64
 m_flCapsuleRadius: float32
 m_vDecalForwardAxis: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_hSkyMaterial: CStrongHandle< InfoForResourceTypeIMaterial2 >
 m_bAnimatedEveryTick: bool
 m_nOwnerId: uint32
 m_nHierarchyId: uint16
 m_nEnablePhysics: uint8
 m_nObjectCulling: uint8

121 CLightDirectionalEntity
 m_MoveType: MoveType_t
 m_iGlowTeam: int32
 m_flDecalHealHeightRate: float32
 m_flElasticity: float32
 m_bAnimatedEveryTick: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nHierarchyId: uint16
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flGlowStartTime: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_nInteractsExclude: uint64
 m_nCollisionGroup: uint8
 m_flCapsuleRadius: float32
 m_nGlowRangeMin: int32
 m_fadeMaxDist: float32
 m_flFadeScale: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_ubInterpolationFrame: uint8
 m_iTeamNum: uint8
 m_glowColorOverride: Color
 m_MoveCollide: MoveCollide_t
 m_nSurroundType: SurroundingBoundsType_t
 m_CollisionGroup: uint8
 CLightComponent: CLightComponent
  m_flOrthoLightWidth: float32
  m_flShadowCascadeDistance1: float32
  m_nShadowCascadeResolution3: int32
  m_flFadeMaxDist: float32
  m_flShadowFadeMinDist: float32
  m_flShadowFadeMaxDist: float32
  m_flFogContributionStength: float32
  m_Color: Color
  m_nShadowHeight: int32
  m_nStyle: int32
  m_flShadowCascadeDistance3: float32
  m_bUsesIndexedBakedLighting: bool
  m_bMixedShadows: bool
  m_flBrightnessScale: float32
  m_flShadowCascadeDistanceFade: float32
  m_Pattern: CUtlString
  m_nCastShadows: int32
  m_bRenderDiffuse: bool
  m_flShadowCascadeDistance0: float32
  m_nShadowCascadeResolution1: int32
  m_flPrecomputedMaxRange: float32
  m_nFogLightingMode: int32
  m_flSkyIntensity: float32
  m_flAttenuation2: float32
  m_flMinRoughness: float32
  m_nShadowCascadeResolution2: int32
  m_hLightCookie: CStrongHandle< InfoForResourceTypeCTextureBase >
  m_vPrecomputedOBBOrigin: Vector
  m_SkyAmbientBounce: Color
  m_nCascades: int32
  m_flAttenuation1: float32
  m_bRenderToCubemaps: bool
  m_LightGroups: CUtlSymbolLarge
  m_flBrightness: float32
  m_flBrightnessMult: float32
  m_flLightStyleStartTime: GameTime_t
  m_SecondaryColor: Color
  m_nRenderSpecular: int32
  m_nCascadeRenderStaticObjects: int32
  m_nIndirectLight: int32
  m_flFadeMinDist: float32
  m_vPrecomputedOBBExtent: Vector
  m_flFalloff: float32
  m_bRenderTransmissive: bool
  m_bEnabled: bool
  m_flPhi: float32
  m_nShadowCascadeResolution0: int32
  m_flNearClipPlane: float32
  m_nShadowWidth: int32
  m_vPrecomputedBoundsMins: Vector
  m_SkyColor: Color
  m_bPrecomputedFieldsValid: bool
  m_flShadowCascadeCrossFade: float32
  m_nDirectLight: int32
  m_vPrecomputedBoundsMaxs: Vector
  m_flTheta: float32
  m_flCapsuleLength: float32
  m_flRange: float32
  m_bFlicker: bool
  m_vPrecomputedOBBAngles: QAngle
  m_bUseSecondaryColor: bool
  m_flAttenuation0: float32
  m_flShadowCascadeDistance2: float32
  m_nShadowPriority: int32
  m_nBakedShadowIndex: int32
  m_flOrthoLightHeight: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bRenderToCubemaps: bool
 m_nInteractsAs: uint64
 m_nInteractsWith: uint64
 m_nEntityId: uint32
 m_vecMins: Vector
 m_hOwnerEntity: CHandle< CBaseEntity >
 CBodyComponent: CBodyComponent
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
 m_nCollisionFunctionMask: uint8
 m_fEffects: uint32
 m_nSolidType: SolidType_t
 m_bFlashing: bool
 m_bvDisabledHitGroups: uint32[1]
 m_flGlowBackfaceMult: float32
 m_flDecalHealBloodRate: float32
 m_flAnimTime: float32
 m_nRenderFX: RenderFx_t
 m_nOwnerId: uint32
 m_nEnablePhysics: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_vCapsuleCenter1: Vector
 m_nGlowRange: int32
 m_LightGroup: CUtlStringToken
 m_vecMaxs: Vector
 m_flShadowStrength: float32
 m_vDecalPosition: Vector
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSubclassID: CUtlStringToken
 m_vCapsuleCenter2: Vector
 m_flGlowTime: float32
 m_nAddDecal: int32
 m_flCreateTime: GameTime_t
 m_bSimulatedEveryTick: bool
 m_usSolidFlags: uint8
 m_iGlowType: int32
 CRenderComponent: CRenderComponent
 m_clrRender: Color
 m_flSimulationTime: float32
 m_triggerBloat: uint8
 m_bEligibleForScreenHighlight: bool
 m_fadeMinDist: float32
 m_nObjectCulling: uint8
 m_vDecalForwardAxis: Vector

172 CSmokeGrenade
 m_bInitiallyPopulateInterpHistory: bool
 m_hOuter: CHandle< CBaseEntity >
 m_szCustomName: char[161]
 m_flFallbackWear: float32
 CBodyComponent: CBodyComponent
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
 m_nOwnerId: uint32
 m_iGlowTeam: int32
 m_nGlowRangeMin: int32
 m_flGlowBackfaceMult: float32
 m_iInventoryPosition: uint32
 m_flFireSequenceStartTime: float32
 CRenderComponent: CRenderComponent
 m_vecForce: Vector
 m_bRedraw: bool
 m_flAnimTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_bClientSideRagdoll: bool
 m_nSolidType: SolidType_t
 m_bSimulatedEveryTick: bool
 m_flRecoilIndex: float32
 m_vecMins: Vector
 m_nObjectCulling: uint8
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_hOwner: CHandle< CBaseEntity >
  m_Transforms: CNetworkUtlVectorBase< CTransform >
 m_bSilencerOn: bool
 m_bIsHeldByPlayer: bool
 m_LightGroup: CUtlStringToken
 m_flGlowStartTime: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bAnimGraphUpdateEnabled: bool
 m_OriginalOwnerXuidLow: uint32
 m_iState: WeaponState_t
 m_clrRender: Color
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nHierarchyId: uint16
 m_flGlowTime: float32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
 m_nSubclassID: CUtlStringToken
 m_nNextThinkTick: GameTick_t
 m_iEntityLevel: uint32
 m_nFireSequenceStartTimeChange: int32
 m_flThrowStrengthApproach: float32
 m_flElasticity: float32
 m_vecSpecifiedSurroundingMins: Vector
 m_glowColorOverride: Color
 m_nAddDecal: int32
 m_nForceBone: int32
 m_nDropTick: GameTick_t
 m_flNextSecondaryAttackTickRatio: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_bRenderToCubemaps: bool
 m_vecMaxs: Vector
 m_fadeMinDist: float32
 m_bReloadVisuallyComplete: bool
 m_nInteractsExclude: uint64
 m_nCollisionGroup: uint8
 m_flShadowStrength: float32
 m_iItemDefinitionIndex: uint16
 m_nEntityId: uint32
 m_nCollisionFunctionMask: uint8
 m_bBurstMode: bool
 m_bJumpThrow: bool
 m_fDropTime: GameTime_t
 m_bInitialized: bool
 m_flPostponeFireReadyTime: GameTime_t
 m_iNumEmptyAttacks: int32
 m_fThrowTime: GameTime_t
 m_nSurroundType: SurroundingBoundsType_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nFallbackSeed: int32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_nRenderFX: RenderFx_t
 m_usSolidFlags: uint8
 m_nGlowRange: int32
 m_bShouldAnimateDuringGameplayPause: bool
 m_nNextSecondaryAttackTick: GameTick_t
 m_pReserveAmmo: int32[2]
 m_MoveType: MoveType_t
 m_triggerBloat: uint8
 m_bClientRagdoll: bool
 m_iIronSightMode: int32
 m_eThrowStatus: EGrenadeThrowState
 m_iOriginalTeamNumber: int32
 m_bvDisabledHitGroups: uint32[1]
 m_flNextPrimaryAttackTickRatio: float32
 m_nInteractsWith: uint64
 m_flCapsuleRadius: float32
 m_iReapplyProvisionParity: int32
 m_fAccuracyPenalty: float32
 m_bIsHauledBack: bool
 m_nNextPrimaryAttackTick: GameTick_t
 m_nViewModelIndex: uint32
 m_ubInterpolationFrame: uint8
 m_nRenderMode: RenderMode_t
 m_vDecalPosition: Vector
 m_iItemIDLow: uint32
 m_flDroppedAtTime: GameTime_t
 m_ProviderType: attributeprovidertypes_t
 m_fLastShotTime: GameTime_t
 m_iClip1: int32
 m_iGlowType: int32
 m_bEligibleForScreenHighlight: bool
 m_fadeMaxDist: float32
 m_vLookTargetPosition: Vector
 m_flCreateTime: GameTime_t
 m_CollisionGroup: uint8
 m_iItemIDHigh: uint32
 m_iAccountID: uint32
 m_iEntityQuality: int32
 m_iClip2: int32
 m_flFadeScale: float32
 m_vDecalForwardAxis: Vector
 m_flDecalHealBloodRate: float32
 m_flDecalHealHeightRate: float32
 m_bPlayerFireEventIsPrimary: bool
 m_iTeamNum: uint8
 m_nFallbackPaintKit: int32
 m_weaponMode: CSWeaponMode
 m_iRecoilIndex: int32
 m_nInteractsAs: uint64
 m_vCapsuleCenter2: Vector
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_flSimulationTime: float32
 m_bAnimatedEveryTick: bool
 m_nEnablePhysics: uint8
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_vCapsuleCenter1: Vector
 m_bFlashing: bool
 m_OriginalOwnerXuidHigh: uint32
 m_bPinPulled: bool
 m_flThrowStrength: float32
 m_MoveCollide: MoveCollide_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_nFallbackStatTrak: int32
 m_bInReload: bool

1 CBarnLight
 m_iGlowType: int32
 m_iGlowTeam: int32
 m_nDirectLight: int32
 m_vShear: Vector
 m_flFogStrength: float32
 CRenderComponent: CRenderComponent
 m_bSimulatedEveryTick: bool
 m_flShadowStrength: float32
 m_LightGroup: CUtlStringToken
 m_glowColorOverride: Color
 m_vDecalPosition: Vector
 m_flAnimTime: float32
 m_nCastShadows: int32
 m_nLuminaireShape: int32
 m_nBakedShadowIndex: int32
 m_flRange: float32
 m_nBounceLight: int32
 m_nGlowRangeMin: int32
 m_bFlashing: bool
 m_flBrightnessScale: float32
 m_fAlternateColorBrightness: float32
 m_bvDisabledHitGroups: uint32[1]
 m_bRenderToCubemaps: bool
 m_nAddDecal: int32
 m_triggerBloat: uint8
 m_vecMins: Vector
 m_flDecalHealBloodRate: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_vCapsuleCenter1: Vector
 m_nBakeSpecularToCubemaps: int32
 m_nShadowPriority: int32
 m_nInteractsExclude: uint64
 m_flDecalHealHeightRate: float32
 m_usSolidFlags: uint8
 m_CollisionGroup: uint8
 m_flSkirt: float32
 m_vBakeSpecularToCubemapsSize: Vector
 m_nFog: int32
 m_vPrecomputedBoundsMins: Vector
 m_vPrecomputedOBBOrigin: Vector
 m_ubInterpolationFrame: uint8
 CBodyComponent: CBodyComponent
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
 m_hLightCookie: CStrongHandle< InfoForResourceTypeCTextureBase >
 m_flLightStyleStartTime: GameTime_t
 m_nSurroundType: SurroundingBoundsType_t
 m_nEnablePhysics: uint8
 m_flGlowTime: float32
 m_Color: Color
 m_flColorTemperature: float32
 m_flShape: float32
 m_flFadeSizeEnd: float32
 m_nOwnerId: uint32
 m_nRenderMode: RenderMode_t
 m_nSolidType: SolidType_t
 m_fadeMaxDist: float32
 m_flFadeScale: float32
 m_flBrightness: float32
 m_LightStyleTargets: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_iTeamNum: uint8
 m_nGlowRange: int32
 m_nSubclassID: CUtlStringToken
 m_flNavIgnoreUntilTime: GameTime_t
 m_nHierarchyId: uint16
 m_nCollisionGroup: uint8
 m_MoveType: MoveType_t
 m_clrRender: Color
 m_vecMaxs: Vector
 m_flCapsuleRadius: float32
 m_flSkirtNear: float32
 m_bContactShadow: bool
 m_flMinRoughness: float32
 m_flCreateTime: GameTime_t
 m_vCapsuleCenter2: Vector
 m_flLuminaireSize: float32
 m_flFadeSizeStart: float32
 m_flShadowFadeSizeEnd: float32
 m_bPrecomputedFieldsValid: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_flGlowBackfaceMult: float32
 m_vAlternateColor: Vector
 m_vPrecomputedOBBAngles: QAngle
 m_nRenderFX: RenderFx_t
 m_nInteractsAs: uint64
 m_vSizeParams: Vector
 m_vPrecomputedBoundsMaxs: Vector
 m_vDecalForwardAxis: Vector
 m_MoveCollide: MoveCollide_t
 m_nEntityId: uint32
 m_flGlowStartTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flElasticity: float32
 m_nObjectCulling: uint8
 m_nColorMode: int32
 m_flSoftY: float32
 m_flFogScale: float32
 m_flSimulationTime: float32
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_nInteractsWith: uint64
 m_LightStyleEvents: CNetworkUtlVectorBase< CUtlString >
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bEnabled: bool
 m_LightStyleString: CUtlString
 m_flShadowFadeSizeStart: float32
 m_nCollisionFunctionMask: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flLuminaireAnisotropy: float32
 m_flSoftX: float32
 m_vecSpecifiedSurroundingMins: Vector
 m_fadeMinDist: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_QueuedLightStyleStrings: CNetworkUtlVectorBase< CUtlString >
 m_bEligibleForScreenHighlight: bool
 m_flBounceScale: float32
 m_nFogShadows: int32
 m_vPrecomputedOBBExtent: Vector
 m_nShadowMapSize: int32

19 CBaseViewModel
 CBodyComponent: CBodyComponent
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nResetEventsParity: int32
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_flLastTeleportTime: float32
  m_nRandomSeedOffset: int32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_hSequence: HSequence
  m_MeshGroupMask: uint64
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nBoolVariablesCount: int32
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
 m_clrRender: Color
 m_nViewModelIndex: uint32
 m_flAnimationStartTime: float32
 m_hControlPanel: CHandle< CBaseEntity >
 m_flAnimTime: float32
 m_flSimulationTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_nAnimationParity: uint32
 m_hWeapon: CHandle< CBasePlayerWeapon >

29 CBumpMineProjectile
 m_MoveType: MoveType_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_DmgRadius: float32
 m_MoveCollide: MoveCollide_t
 m_bSimulatedEveryTick: bool
 m_flCapsuleRadius: float32
 m_nAddDecal: int32
 m_nForceBone: int32
 m_vLookTargetPosition: Vector
 m_nParentBoneIndex: int32
 CBodyComponent: CBodyComponent
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_cellZ: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_bUseParentRenderBounds: bool
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_hierarchyAttachName: CUtlStringToken
  m_materialGroup: CUtlStringToken
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flLastTeleportTime: float32
  m_angRotation: QAngle
  m_nIdealMotionType: int8
  m_bClientSideAnimation: bool
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_cellX: uint16
  m_flWeight: CNetworkedQuantizedFloat
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nBoolVariablesCount: int32
  m_name: CUtlStringToken
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_vecZ: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_cellY: uint16
  m_hParent: CGameSceneNodeHandle
  m_flScale: float32
  m_bClientClothCreationSuppressed: bool
  m_nRandomSeedOffset: int32
  m_vecX: CNetworkedQuantizedFloat
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
 m_nRenderMode: RenderMode_t
 m_nEntityId: uint32
 m_vCapsuleCenter2: Vector
 m_flDecalHealHeightRate: float32
 m_bAnimGraphUpdateEnabled: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nSolidType: SolidType_t
 m_flDecalHealBloodRate: float32
 m_vecForce: Vector
 m_bClientRagdoll: bool
 m_fEffects: uint32
 m_nEnablePhysics: uint8
 m_iGlowTeam: int32
 m_flFadeScale: float32
 m_vecY: CNetworkedQuantizedFloat
 m_nSubclassID: CUtlStringToken
 m_hEffectEntity: CHandle< CBaseEntity >
 m_glowColorOverride: Color
 m_flGlowStartTime: float32
 m_bEligibleForScreenHighlight: bool
 m_bIsLive: bool
 m_bArmed: bool
 m_bvDisabledHitGroups: uint32[1]
 m_ubInterpolationFrame: uint8
 m_nHierarchyId: uint16
 m_vecMins: Vector
 m_usSolidFlags: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_flShadowStrength: float32
 m_vecZ: CNetworkedQuantizedFloat
 m_flElasticity: float32
 m_LightGroup: CUtlStringToken
 m_nOwnerId: uint32
 m_vecSpecifiedSurroundingMins: Vector
 m_iGlowType: int32
 CRenderComponent: CRenderComponent
 m_vecMaxs: Vector
 m_vDecalPosition: Vector
 m_hThrower: CHandle< CBaseEntity >
 m_vecParentBonePos: Vector
 m_flSimulationTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nInteractsWith: uint64
 m_triggerBloat: uint8
 m_nObjectCulling: uint8
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_fFlags: uint32
 m_nCollisionFunctionMask: uint8
 m_nGlowRangeMin: int32
 m_flGlowTime: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_vecX: CNetworkedQuantizedFloat
 m_fadeMaxDist: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_flCreateTime: GameTime_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_nInteractsAs: uint64
 m_CollisionGroup: uint8
 m_vCapsuleCenter1: Vector
 m_nGlowRange: int32
 m_bClientSideRagdoll: bool
 m_bAnimatedEveryTick: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_vDecalForwardAxis: Vector
 m_flDetonateTime: GameTime_t
 m_nRenderFX: RenderFx_t
 m_bRenderToCubemaps: bool
 m_bFlashing: bool
 m_flDamage: float32
 m_iTeamNum: uint8
 m_clrRender: Color
 m_nInteractsExclude: uint64
 m_nCollisionGroup: uint8
 m_flGlowBackfaceMult: float32
 m_fadeMinDist: float32

47 CCSMinimapBoundary
 m_ubInterpolationFrame: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_flAnimTime: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nSubclassID: CUtlStringToken
 m_flCreateTime: GameTime_t
 m_iTeamNum: uint8
 m_fEffects: uint32
 m_flElasticity: float32
 m_flSimulationTime: float32
 CBodyComponent: CBodyComponent
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveCollide: MoveCollide_t
 m_MoveType: MoveType_t
 m_bSimulatedEveryTick: bool
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool

5 CBaseCombatCharacter
 m_clrRender: Color
 m_nSurroundType: SurroundingBoundsType_t
 m_flShadowStrength: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_nInteractsAs: uint64
 m_vecMaxs: Vector
 m_flGlowStartTime: float32
 m_vDecalForwardAxis: Vector
 m_nGlowRangeMin: int32
 m_bFlashing: bool
 m_vLookTargetPosition: Vector
 m_blinktoggle: bool
 m_flAnimTime: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_vecSpecifiedSurroundingMins: Vector
 m_vecSpecifiedSurroundingMaxs: Vector
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nCollisionFunctionMask: uint8
 m_flFadeScale: float32
 m_nForceBone: int32
 m_vecMins: Vector
 m_usSolidFlags: uint8
 m_bShouldAnimateDuringGameplayPause: bool
 m_LightGroup: CUtlStringToken
 m_nInteractsExclude: uint64
 m_bEligibleForScreenHighlight: bool
 m_fadeMinDist: float32
 m_flElasticity: float32
 m_iGlowTeam: int32
 m_flGlowBackfaceMult: float32
 m_nObjectCulling: uint8
 CBodyComponent: CBodyComponent
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nBoolVariablesCount: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_flWeight: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
 m_nSubclassID: CUtlStringToken
 m_flCreateTime: GameTime_t
 m_fEffects: uint32
 m_flDecalHealBloodRate: float32
 m_bvDisabledHitGroups: uint32[1]
 m_nOwnerId: uint32
 m_triggerBloat: uint8
 m_vCapsuleCenter1: Vector
 m_vDecalPosition: Vector
 m_bAnimGraphUpdateEnabled: bool
 m_flSimulationTime: float32
 m_MoveType: MoveType_t
 m_vCapsuleCenter2: Vector
 m_fadeMaxDist: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecForce: Vector
 m_hMyWearables: CNetworkUtlVectorBase< CHandle< CEconWearable > >
 m_flFieldOfView: float32
 m_iTeamNum: uint8
 m_nRenderMode: RenderMode_t
 m_nGlowRange: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_ubInterpolationFrame: uint8
 m_nRenderFX: RenderFx_t
 m_flexWeight: CNetworkUtlVectorBase< float32 >
 m_nInteractsWith: uint64
 m_nEntityId: uint32
 m_flCapsuleRadius: float32
 m_glowColorOverride: Color
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_bClientRagdoll: bool
 m_nHierarchyId: uint16
 m_nCollisionGroup: uint8
 m_nAddDecal: int32
 m_flDecalHealHeightRate: float32
 m_CollisionGroup: uint8
 m_iGlowType: int32
 m_flGlowTime: float32
 CRenderComponent: CRenderComponent
 m_MoveCollide: MoveCollide_t
 m_bAnimatedEveryTick: bool
 m_bRenderToCubemaps: bool
 m_nSolidType: SolidType_t
 m_bClientSideRagdoll: bool
 m_bSimulatedEveryTick: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nEnablePhysics: uint8

171 CSkyCamera
 m_bNoReflectionFog: bool
 CBodyComponent: CBodyComponent
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
 m_bAnimatedEveryTick: bool
 flClip3DSkyBoxNearToWorldFarOffset: float32
 start: float32
 blend: bool
 colorSecondaryLerpTo: Color
 skyboxFogFactor: float32
 m_flSimulationTime: float32
 lerptime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 colorPrimary: Color
 exponent: float32
 HDRColorScale: float32
 m_skyboxSlotToken: CUtlStringToken
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_flElasticity: float32
 end: float32
 m_nWorldGroupID: WorldGroupId_t
 colorPrimaryLerpTo: Color
 endLerpTo: float32
 duration: float32
 locallightscale: float32
 m_iTeamNum: uint8
 m_flCreateTime: GameTime_t
 origin: Vector
 dirPrimary: Vector
 enable: bool
 blendtobackground: float32
 scattering: float32
 m_flAnimTime: float32
 m_MoveType: MoveType_t
 m_nSubclassID: CUtlStringToken
 m_hEffectEntity: CHandle< CBaseEntity >
 colorSecondary: Color
 maxdensity: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bSimulatedEveryTick: bool
 bClip3DSkyBoxNearToWorldFar: bool
 farz: float32
 maxdensityLerpTo: float32
 m_fEffects: uint32
 scale: int16
 skyboxFogFactorLerpTo: float32
 startLerpTo: float32
 m_MoveCollide: MoveCollide_t

46 CCSGOViewModel
 CBodyComponent: CBodyComponent
  m_nRandomSeedOffset: int32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_hSequence: HSequence
  m_MeshGroupMask: uint64
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nBoolVariablesCount: int32
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_flLastTeleportTime: float32
  m_nResetEventsParity: int32
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
 m_nAnimationParity: uint32
 m_flAnimationStartTime: float32
 m_hWeapon: CHandle< CBasePlayerWeapon >
 m_hControlPanel: CHandle< CBaseEntity >
 m_bShouldIgnoreOffsetAndAccuracy: bool
 m_nWeaponParity: uint32
 m_flSimulationTime: float32
 m_flAnimTime: float32
 m_fEffects: uint32
 m_clrRender: Color
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nViewModelIndex: uint32

92 CFuncConveyor
 m_nInteractsExclude: uint64
 m_nEntityId: uint32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveType: MoveType_t
 m_bSimulatedEveryTick: bool
 m_nRenderFX: RenderFx_t
 m_CollisionGroup: uint8
 m_fadeMaxDist: float32
 m_MoveCollide: MoveCollide_t
 m_nInteractsAs: uint64
 m_nSolidType: SolidType_t
 m_nSurroundType: SurroundingBoundsType_t
 m_nGlowRangeMin: int32
 m_glowColorOverride: Color
 m_bvDisabledHitGroups: uint32[1]
 m_nGlowRange: int32
 m_bFlashing: bool
 m_flCreateTime: GameTime_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_flGlowStartTime: float32
 m_fadeMinDist: float32
 CBodyComponent: CBodyComponent
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
 m_nEnablePhysics: uint8
 m_flGlowBackfaceMult: float32
 m_vDecalPosition: Vector
 m_ubInterpolationFrame: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nOwnerId: uint32
 m_iGlowType: int32
 m_nObjectCulling: uint8
 m_iTeamNum: uint8
 m_fFlags: uint32
 m_vecMins: Vector
 m_triggerBloat: uint8
 m_nAddDecal: int32
 m_nTransitionDurationTicks: int32
 m_flSimulationTime: float32
 m_bAnimatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_vCapsuleCenter1: Vector
 m_flFadeScale: float32
 m_nTransitionStartTick: GameTick_t
 m_flDecalHealBloodRate: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_fEffects: uint32
 m_flElasticity: float32
 m_LightGroup: CUtlStringToken
 m_nCollisionGroup: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vCapsuleCenter2: Vector
 m_hConveyorModels: CNetworkUtlVectorBase< CHandle< CBaseEntity > >
 m_bEligibleForScreenHighlight: bool
 m_vecMoveDirEntitySpace: Vector
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nInteractsWith: uint64
 m_nCollisionFunctionMask: uint8
 m_usSolidFlags: uint8
 m_flCapsuleRadius: float32
 m_vDecalForwardAxis: Vector
 m_flDecalHealHeightRate: float32
 m_flTargetSpeed: float32
 m_clrRender: Color
 m_nHierarchyId: uint16
 m_vecMaxs: Vector
 m_vecSpecifiedSurroundingMins: Vector
 m_flShadowStrength: float32
 m_flTransitionStartSpeed: float32
 CRenderComponent: CRenderComponent
 m_flAnimTime: float32
 m_nSubclassID: CUtlStringToken
 m_bRenderToCubemaps: bool
 m_iGlowTeam: int32
 m_flGlowTime: float32

26 CBreakable
 m_bAnimatedEveryTick: bool
 m_glowColorOverride: Color
 m_fadeMinDist: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveCollide: MoveCollide_t
 m_flCapsuleRadius: float32
 m_flCreateTime: GameTime_t
 m_nSolidType: SolidType_t
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nRenderFX: RenderFx_t
 m_nHierarchyId: uint16
 m_vecSpecifiedSurroundingMins: Vector
 m_iGlowTeam: int32
 m_bFlashing: bool
 m_flGlowTime: float32
 m_fEffects: uint32
 m_nRenderMode: RenderMode_t
 m_triggerBloat: uint8
 m_vCapsuleCenter1: Vector
 m_flDecalHealBloodRate: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_iTeamNum: uint8
 m_nEnablePhysics: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_CollisionGroup: uint8
 m_fadeMaxDist: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_LightGroup: CUtlStringToken
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vDecalForwardAxis: Vector
 m_nInteractsWith: uint64
 m_nInteractsExclude: uint64
 m_flElasticity: float32
 m_flDecalHealHeightRate: float32
 m_bvDisabledHitGroups: uint32[1]
 CBodyComponent: CBodyComponent
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
 m_nSubclassID: CUtlStringToken
 m_vecMaxs: Vector
 m_vCapsuleCenter2: Vector
 m_nGlowRange: int32
 m_bEligibleForScreenHighlight: bool
 m_flShadowStrength: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsAs: uint64
 m_flGlowStartTime: float32
 m_flAnimTime: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_clrRender: Color
 m_nCollisionFunctionMask: uint8
 m_vecMins: Vector
 CRenderComponent: CRenderComponent
 m_flSimulationTime: float32
 m_MoveType: MoveType_t
 m_flFadeScale: float32
 m_usSolidFlags: uint8
 m_nGlowRangeMin: int32
 m_flGlowBackfaceMult: float32
 m_vDecalPosition: Vector
 m_bRenderToCubemaps: bool
 m_nOwnerId: uint32
 m_nCollisionGroup: uint8
 m_iGlowType: int32
 m_nObjectCulling: uint8
 m_bSimulatedEveryTick: bool
 m_nEntityId: uint32
 m_nAddDecal: int32

131 CMolotovProjectile
 m_flDetonateTime: GameTime_t
 m_nSubclassID: CUtlStringToken
 m_nEntityId: uint32
 m_nCollisionFunctionMask: uint8
 m_bIsLive: bool
 m_CollisionGroup: uint8
 m_vCapsuleCenter2: Vector
 m_vDecalPosition: Vector
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_flCreateTime: GameTime_t
 m_iTeamNum: uint8
 m_clrRender: Color
 m_nSolidType: SolidType_t
 m_vLookTargetPosition: Vector
 m_vInitialVelocity: Vector
 m_bIsIncGrenade: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_fFlags: uint32
 m_glowColorOverride: Color
 m_bFlashing: bool
 m_triggerBloat: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_fadeMinDist: float32
 m_DmgRadius: float32
 m_bClientSideRagdoll: bool
 m_nRenderMode: RenderMode_t
 m_nRenderFX: RenderFx_t
 m_LightGroup: CUtlStringToken
 m_vecZ: CNetworkedQuantizedFloat
 m_vecMaxs: Vector
 m_vCapsuleCenter1: Vector
 m_nForceBone: int32
 m_flSimulationTime: float32
 m_fEffects: uint32
 m_bSimulatedEveryTick: bool
 m_nHierarchyId: uint16
 m_flCapsuleRadius: float32
 m_flDamage: float32
 m_hThrower: CHandle< CBaseEntity >
 m_bvDisabledHitGroups: uint32[1]
 m_nSurroundType: SurroundingBoundsType_t
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nExplodeEffectIndex: CStrongHandle< InfoForResourceTypeIParticleSystemDefinition >
 m_ubInterpolationFrame: uint8
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nEnablePhysics: uint8
 m_nGlowRange: int32
 m_iGlowTeam: int32
 m_flGlowStartTime: float32
 m_bEligibleForScreenHighlight: bool
 m_fadeMaxDist: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nInteractsAs: uint64
 m_nInteractsExclude: uint64
 m_vecMins: Vector
 m_bAnimGraphUpdateEnabled: bool
 m_nBounces: int32
 m_MoveType: MoveType_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_bRenderToCubemaps: bool
 m_bInitiallyPopulateInterpHistory: bool
 m_vecX: CNetworkedQuantizedFloat
 m_bAnimatedEveryTick: bool
 m_nOwnerId: uint32
 m_iGlowType: int32
 m_nExplodeEffectTickBegin: int32
 m_vecForce: Vector
 CRenderComponent: CRenderComponent
 m_nInteractsWith: uint64
 m_nCollisionGroup: uint8
 m_flShadowStrength: float32
 m_nAddDecal: int32
 m_flDecalHealBloodRate: float32
 m_vecY: CNetworkedQuantizedFloat
 m_MoveCollide: MoveCollide_t
 m_flElasticity: float32
 m_usSolidFlags: uint8
 m_nObjectCulling: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flGlowBackfaceMult: float32
 m_bClientRagdoll: bool
 m_nGlowRangeMin: int32
 m_flFadeScale: float32
 m_flDecalHealHeightRate: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_vecExplodeEffectOrigin: Vector
 CBodyComponent: CBodyComponent
  m_vecZ: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_cellY: uint16
  m_hParent: CGameSceneNodeHandle
  m_flScale: float32
  m_bClientClothCreationSuppressed: bool
  m_nRandomSeedOffset: int32
  m_vecX: CNetworkedQuantizedFloat
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_cellZ: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_bUseParentRenderBounds: bool
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_hierarchyAttachName: CUtlStringToken
  m_materialGroup: CUtlStringToken
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flLastTeleportTime: float32
  m_angRotation: QAngle
  m_nIdealMotionType: int8
  m_bClientSideAnimation: bool
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_cellX: uint16
  m_flWeight: CNetworkedQuantizedFloat
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nBoolVariablesCount: int32
  m_name: CUtlStringToken
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flGlowTime: float32
 m_vDecalForwardAxis: Vector

145 CPlantedC4Survival
 m_nForceBone: int32
 m_flAnimTime: float32
 m_MoveCollide: MoveCollide_t
 m_bRenderToCubemaps: bool
 m_usSolidFlags: uint8
 m_iGlowTeam: int32
 m_flGlowStartTime: float32
 m_MoveType: MoveType_t
 m_LightGroup: CUtlStringToken
 m_nGlowRange: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bHasExploded: bool
 m_flGlowTime: float32
 m_nObjectCulling: uint8
 m_flDefuseCountDown: GameTime_t
 m_flCapsuleRadius: float32
 m_bEligibleForScreenHighlight: bool
 m_vDecalPosition: Vector
 m_vDecalForwardAxis: Vector
 m_bInitiallyPopulateInterpHistory: bool
 m_flTimerLength: float32
 m_nSolidType: SolidType_t
 m_CollisionGroup: uint8
 m_flDecalHealHeightRate: float32
 m_flSimulationTime: float32
 m_flElasticity: float32
 m_nRenderFX: RenderFx_t
 m_nInteractsWith: uint64
 m_vecMins: Vector
 m_hControlPanel: CHandle< CBaseEntity >
 m_iTeamNum: uint8
 m_nHierarchyId: uint16
 m_fadeMaxDist: float32
 m_flFadeScale: float32
 m_flShadowStrength: float32
 m_bSpotted: bool
 m_nCollisionFunctionMask: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_bClientSideRagdoll: bool
 m_ubInterpolationFrame: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsExclude: uint64
 m_iGlowType: int32
 m_flC4Blow: GameTime_t
 m_bBeingDefused: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_bCannotBeDefused: bool
 m_bvDisabledHitGroups: uint32[1]
 CRenderComponent: CRenderComponent
 m_nRenderMode: RenderMode_t
 m_vCapsuleCenter2: Vector
 m_nGlowRangeMin: int32
 m_fadeMinDist: float32
 m_flDecalHealBloodRate: float32
 m_bClientRagdoll: bool
 m_bBombDefused: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nSubclassID: CUtlStringToken
 m_flCreateTime: GameTime_t
 m_nCollisionGroup: uint8
 m_triggerBloat: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 CBodyComponent: CBodyComponent
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nBoolVariablesCount: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_flWeight: CNetworkedQuantizedFloat
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_clrRender: Color
 m_nInteractsAs: uint64
 m_flNavIgnoreUntilTime: GameTime_t
 m_vCapsuleCenter1: Vector
 m_bShouldAnimateDuringGameplayPause: bool
 m_bAnimGraphUpdateEnabled: bool
 m_hBombDefuser: CHandle< CCSPlayerPawnBase >
 m_bSimulatedEveryTick: bool
 m_vecMaxs: Vector
 m_nEnablePhysics: uint8
 m_glowColorOverride: Color
 m_bBombTicking: bool
 m_bSpottedByMask: uint32[2]
 m_nEntityId: uint32
 m_bFlashing: bool
 m_flDefuseLength: float32
 m_nSourceSoundscapeHash: int32
 m_nOwnerId: uint32
 m_vecSpecifiedSurroundingMins: Vector
 m_flGlowBackfaceMult: float32
 m_nAddDecal: int32
 m_vecForce: Vector
 m_nBombSite: int32

7 CBaseCSGrenadeProjectile
 m_bRenderToCubemaps: bool
 m_triggerBloat: uint8
 m_nAddDecal: int32
 m_vDecalPosition: Vector
 m_fFlags: uint32
 m_nCollisionFunctionMask: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_nEnablePhysics: uint8
 m_vDecalForwardAxis: Vector
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bFlashing: bool
 m_nExplodeEffectIndex: CStrongHandle< InfoForResourceTypeIParticleSystemDefinition >
 CBodyComponent: CBodyComponent
  m_name: CUtlStringToken
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nBoolVariablesCount: int32
  m_vecZ: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_cellY: uint16
  m_hParent: CGameSceneNodeHandle
  m_flScale: float32
  m_bClientClothCreationSuppressed: bool
  m_vecX: CNetworkedQuantizedFloat
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_nRandomSeedOffset: int32
  m_cellZ: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_bUseParentRenderBounds: bool
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_hierarchyAttachName: CUtlStringToken
  m_materialGroup: CUtlStringToken
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_flLastTeleportTime: float32
  m_angRotation: QAngle
  m_nIdealMotionType: int8
  m_bClientSideAnimation: bool
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_flWeight: CNetworkedQuantizedFloat
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
 m_flCapsuleRadius: float32
 m_flGlowStartTime: float32
 m_bEligibleForScreenHighlight: bool
 m_fadeMaxDist: float32
 m_iTeamNum: uint8
 m_nInteractsAs: uint64
 m_nInteractsExclude: uint64
 m_nEntityId: uint32
 m_flGlowTime: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nForceBone: int32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_MoveCollide: MoveCollide_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_flShadowStrength: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_vLookTargetPosition: Vector
 m_nExplodeEffectTickBegin: int32
 m_MoveType: MoveType_t
 m_fEffects: uint32
 m_vecSpecifiedSurroundingMins: Vector
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_ubInterpolationFrame: uint8
 m_flElasticity: float32
 m_DmgRadius: float32
 m_vecY: CNetworkedQuantizedFloat
 m_bIsLive: bool
 m_flSimulationTime: float32
 m_usSolidFlags: uint8
 m_vCapsuleCenter1: Vector
 m_flGlowBackfaceMult: float32
 m_flFadeScale: float32
 m_flDecalHealHeightRate: float32
 m_vecForce: Vector
 m_hThrower: CHandle< CBaseEntity >
 m_vecX: CNetworkedQuantizedFloat
 m_bAnimGraphUpdateEnabled: bool
 m_bClientSideRagdoll: bool
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nOwnerId: uint32
 m_vecMins: Vector
 m_nGlowRangeMin: int32
 m_flDecalHealBloodRate: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_bClientRagdoll: bool
 m_flDetonateTime: GameTime_t
 m_nRenderFX: RenderFx_t
 m_nHierarchyId: uint16
 m_iGlowType: int32
 m_vecExplodeEffectOrigin: Vector
 m_bvDisabledHitGroups: uint32[1]
 m_vecZ: CNetworkedQuantizedFloat
 m_clrRender: Color
 m_nSolidType: SolidType_t
 m_iGlowTeam: int32
 m_nBounces: int32
 m_bAnimatedEveryTick: bool
 m_vCapsuleCenter2: Vector
 m_nObjectCulling: uint8
 m_flDamage: float32
 CRenderComponent: CRenderComponent
 m_flCreateTime: GameTime_t
 m_CollisionGroup: uint8
 m_nGlowRange: int32
 m_glowColorOverride: Color
 m_fadeMinDist: float32
 m_vInitialVelocity: Vector
 m_vecMaxs: Vector
 m_nSubclassID: CUtlStringToken
 m_bSimulatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_nRenderMode: RenderMode_t
 m_LightGroup: CUtlStringToken
 m_nInteractsWith: uint64
 m_nCollisionGroup: uint8
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_hOwner: CHandle< CBaseEntity >
  m_Transforms: CNetworkUtlVectorBase< CTransform >

24 CBreachCharge
 m_nEntityId: uint32
 m_triggerBloat: uint8
 m_fAccuracyPenalty: float32
 m_fLastShotTime: GameTime_t
 m_iIronSightMode: int32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flDecalHealHeightRate: float32
 m_iAccountID: uint32
 m_MoveType: MoveType_t
 m_flGlowStartTime: float32
 m_flGlowBackfaceMult: float32
 m_nDropTick: GameTick_t
 m_nEnablePhysics: uint8
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_szCustomName: char[161]
 m_nInteractsWith: uint64
 m_flGlowTime: float32
 m_nFallbackSeed: int32
 m_nNextPrimaryAttackTick: GameTick_t
 m_flAnimTime: float32
 m_nSubclassID: CUtlStringToken
 m_iGlowTeam: int32
 m_flDecalHealBloodRate: float32
 m_OriginalOwnerXuidHigh: uint32
 m_flNavIgnoreUntilTime: GameTime_t
 m_LightGroup: CUtlStringToken
 m_bInitialized: bool
 m_flRecoilIndex: float32
 m_bInReload: bool
 m_MoveCollide: MoveCollide_t
 m_OriginalOwnerXuidLow: uint32
 m_nRenderMode: RenderMode_t
 m_vecSpecifiedSurroundingMins: Vector
 m_vCapsuleCenter1: Vector
 m_nNextThinkTick: GameTick_t
 m_nNextSecondaryAttackTick: GameTick_t
 m_flSimulationTime: float32
 m_nCollisionFunctionMask: uint8
 m_CollisionGroup: uint8
 m_fadeMinDist: float32
 m_iInventoryPosition: uint32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 CBodyComponent: CBodyComponent
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_nNewSequenceParity: int32
  m_bClientSideAnimation: bool
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_hParent: CGameSceneNodeHandle
  m_nRandomSeedOffset: int32
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
 m_ubInterpolationFrame: uint8
 m_iItemIDHigh: uint32
 m_iRecoilIndex: int32
 m_flDroppedAtTime: GameTime_t
 m_iClip2: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nHierarchyId: uint16
 m_nForceBone: int32
 m_iEntityQuality: int32
 m_vecMaxs: Vector
 m_vCapsuleCenter2: Vector
 m_fEffects: uint32
 m_nGlowRange: int32
 m_bFlashing: bool
 m_bInitiallyPopulateInterpHistory: bool
 m_nViewModelIndex: uint32
 m_nInteractsExclude: uint64
 m_flCapsuleRadius: float32
 m_ProviderType: attributeprovidertypes_t
 m_nFallbackPaintKit: int32
 m_flPostponeFireReadyTime: GameTime_t
 m_bSilencerOn: bool
 m_iTeamNum: uint8
 m_bEligibleForScreenHighlight: bool
 m_iNumEmptyAttacks: int32
 m_flFadeScale: float32
 m_bReloadVisuallyComplete: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_bAnimatedEveryTick: bool
 m_glowColorOverride: Color
 m_nObjectCulling: uint8
 m_bClientRagdoll: bool
 m_vLookTargetPosition: Vector
 m_iItemDefinitionIndex: uint16
 m_flNextPrimaryAttackTickRatio: float32
 m_pReserveAmmo: int32[2]
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_vecMins: Vector
 m_vDecalPosition: Vector
 m_bPlayerFireEventIsPrimary: bool
 m_clrRender: Color
 m_nGlowRangeMin: int32
 m_bAnimGraphUpdateEnabled: bool
 m_nFireSequenceStartTimeChange: int32
 m_flCreateTime: GameTime_t
 m_nSurroundType: SurroundingBoundsType_t
 m_usSolidFlags: uint8
 m_iGlowType: int32
 m_iOriginalTeamNumber: int32
 m_bvDisabledHitGroups: uint32[1]
 m_fadeMaxDist: float32
 m_bIsHauledBack: bool
 m_nSolidType: SolidType_t
 m_iItemIDLow: uint32
 m_bSimulatedEveryTick: bool
 m_bRenderToCubemaps: bool
 m_nInteractsAs: uint64
 m_nAddDecal: int32
 m_vDecalForwardAxis: Vector
 m_vecForce: Vector
 m_bBurstMode: bool
 m_iReapplyProvisionParity: int32
 m_iEntityLevel: uint32
 m_flFallbackWear: float32
 m_bClientSideRagdoll: bool
 m_flShadowStrength: float32
 m_iState: WeaponState_t
 m_nOwnerId: uint32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nRenderFX: RenderFx_t
 m_hOuter: CHandle< CBaseEntity >
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_flFireSequenceStartTime: float32
 CRenderComponent: CRenderComponent
 m_flNextSecondaryAttackTickRatio: float32
 m_iClip1: int32
 m_flElasticity: float32
 m_nCollisionGroup: uint8
 m_bShouldAnimateDuringGameplayPause: bool
 m_nFallbackStatTrak: int32
 m_weaponMode: CSWeaponMode
 m_flTimeSilencerSwitchComplete: GameTime_t

127 CMapVetoPickController
 m_nMapId3: int32[64]
 m_nMapId4: int32[64]
 m_flAnimTime: float32
 m_flCreateTime: GameTime_t
 m_bSimulatedEveryTick: bool
 m_nMapId2: int32[64]
 m_flElasticity: float32
 m_nMapId1: int32[64]
 m_nPhaseStartTick: int32
 m_iTeamNum: uint8
 m_nVoteMapIdsList: int32[7]
 m_MoveType: MoveType_t
 m_nDraftType: int32
 m_nTeamWithFirstChoice: int32[64]
 m_nMapId5: int32[64]
 m_nAccountIDs: int32[64]
 m_nCurrentPhase: int32
 m_nSubclassID: CUtlStringToken
 m_fEffects: uint32
 m_bAnimatedEveryTick: bool
 m_nTeamWinningCoinToss: int32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_hEffectEntity: CHandle< CBaseEntity >
 m_nMapId0: int32[64]
 m_flSimulationTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flNavIgnoreUntilTime: GameTime_t
 m_nPhaseDurationTicks: int32
 CBodyComponent: CBodyComponent
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_cellZ: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
 m_MoveCollide: MoveCollide_t
 m_ubInterpolationFrame: uint8
 m_nStartingSide0: int32[64]

141 CPhysPropLootCrate
 m_bvDisabledHitGroups: uint32[1]
 m_ubInterpolationFrame: uint8
 m_nRenderMode: RenderMode_t
 m_CollisionGroup: uint8
 m_fadeMinDist: float32
 m_vDecalPosition: Vector
 m_vDecalForwardAxis: Vector
 m_bClientRagdoll: bool
 m_nAddDecal: int32
 m_flDecalHealBloodRate: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_nForceBone: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSubclassID: CUtlStringToken
 m_bClientSideRagdoll: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_iGlowType: int32
 m_nGlowRangeMin: int32
 m_glowColorOverride: Color
 m_nInteractsWith: uint64
 m_bInitiallyPopulateInterpHistory: bool
 m_iHealth: int32
 m_nCollisionFunctionMask: uint8
 m_flCapsuleRadius: float32
 m_bRenderInPSPM: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_bRenderToCubemaps: bool
 m_nInteractsAs: uint64
 m_vCapsuleCenter1: Vector
 m_iGlowTeam: int32
 m_nGlowRange: int32
 m_vecMins: Vector
 m_bEligibleForScreenHighlight: bool
 m_nObjectCulling: uint8
 m_MoveType: MoveType_t
 m_bAnimGraphUpdateEnabled: bool
 m_noGhostCollision: bool
 m_flSimulationTime: float32
 m_MoveCollide: MoveCollide_t
 m_bAnimatedEveryTick: bool
 m_nEntityId: uint32
 m_bRenderInTablet: bool
 m_iMaxHealth: int32
 m_bSimulatedEveryTick: bool
 m_nRenderFX: RenderFx_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_bFlashing: bool
 m_flShadowStrength: float32
 m_flDecalHealHeightRate: float32
 m_iTeamNum: uint8
 m_spawnflags: uint32
 m_vecMaxs: Vector
 m_usSolidFlags: uint8
 m_triggerBloat: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_flCreateTime: GameTime_t
 m_nEnablePhysics: uint8
 m_vCapsuleCenter2: Vector
 m_flFadeScale: float32
 m_bAwake: bool
 CBodyComponent: CBodyComponent
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_vecX: CNetworkedQuantizedFloat
  m_bClientSideAnimation: bool
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_flCycle: float32
  m_bClientClothCreationSuppressed: bool
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nResetEventsParity: int32
  m_nOutsideWorld: uint16
  m_hierarchyAttachName: CUtlStringToken
  m_MeshGroupMask: uint64
  m_vecY: CNetworkedQuantizedFloat
  m_nNewSequenceParity: int32
  m_nRandomSeedOffset: int32
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_name: CUtlStringToken
  m_bUseParentRenderBounds: bool
  m_nIdealMotionType: int8
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_flPrevCycle: float32
  m_angRotation: QAngle
  m_flLastTeleportTime: float32
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_cellY: uint16
  m_flScale: float32
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_bIsAnimationEnabled: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_cellZ: uint16
  m_hSequence: HSequence
  m_materialGroup: CUtlStringToken
  m_nHitboxSet: uint8
  m_flWeight: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_nBoolVariablesCount: int32
 m_flElasticity: float32
 m_clrRender: Color
 m_LightGroup: CUtlStringToken
 m_nHierarchyId: uint16
 m_vecSpecifiedSurroundingMins: Vector
 m_flGlowBackfaceMult: float32
 m_nInteractsExclude: uint64
 m_nOwnerId: uint32
 m_nCollisionGroup: uint8
 m_nSolidType: SolidType_t
 m_flGlowStartTime: float32
 m_fadeMaxDist: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_vecSpecifiedSurroundingMaxs: Vector
 m_flGlowTime: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_vecForce: Vector
 CRenderComponent: CRenderComponent

214 CWeaponMag7
 m_bFlashing: bool
 m_fAccuracyPenalty: float32
 CRenderComponent: CRenderComponent
 m_bClientRagdoll: bool
 m_weaponMode: CSWeaponMode
 m_nInteractsWith: uint64
 m_vCapsuleCenter1: Vector
 m_flGlowTime: float32
 m_hOuter: CHandle< CBaseEntity >
 m_iInventoryPosition: uint32
 m_nDropTick: GameTick_t
 m_usSolidFlags: uint8
 m_flCapsuleRadius: float32
 m_nSolidType: SolidType_t
 m_nForceBone: int32
 m_bPlayerFireEventIsPrimary: bool
 m_bSimulatedEveryTick: bool
 m_LightGroup: CUtlStringToken
 m_bRenderToCubemaps: bool
 m_nOwnerId: uint32
 m_iItemDefinitionIndex: uint16
 m_flNavIgnoreUntilTime: GameTime_t
 m_iGlowTeam: int32
 m_iIronSightMode: int32
 m_nFallbackSeed: int32
 m_bInReload: bool
 m_iTeamNum: uint8
 m_iEntityQuality: int32
 m_nInteractsExclude: uint64
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nFireSequenceStartTimeChange: int32
 m_iOriginalTeamNumber: int32
 m_nNextThinkTick: GameTick_t
 m_iClip2: int32
 m_flAnimTime: float32
 CBodyComponent: CBodyComponent
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_angRotation: QAngle
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
 m_flPostponeFireReadyTime: GameTime_t
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_nNextPrimaryAttackTick: GameTick_t
 m_clrRender: Color
 m_flDecalHealHeightRate: float32
 m_nInteractsAs: uint64
 m_vLookTargetPosition: Vector
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_nEnablePhysics: uint8
 m_bEligibleForScreenHighlight: bool
 m_ProviderType: attributeprovidertypes_t
 m_bReloadVisuallyComplete: bool
 m_nEntityId: uint32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_iGlowType: int32
 m_flGlowStartTime: float32
 m_nAddDecal: int32
 m_bInitiallyPopulateInterpHistory: bool
 m_ubInterpolationFrame: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_bSilencerOn: bool
 m_bAnimatedEveryTick: bool
 m_flFadeScale: float32
 m_glowColorOverride: Color
 m_flDecalHealBloodRate: float32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
 m_fEffects: uint32
 m_nCollisionFunctionMask: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_fadeMaxDist: float32
 m_nNextSecondaryAttackTick: GameTick_t
 m_MoveType: MoveType_t
 m_flElasticity: float32
 m_fLastShotTime: GameTime_t
 m_iNumEmptyAttacks: int32
 m_vCapsuleCenter2: Vector
 m_flGlowBackfaceMult: float32
 m_iItemIDHigh: uint32
 m_szCustomName: char[161]
 m_OriginalOwnerXuidHigh: uint32
 m_iRecoilIndex: int32
 m_zoomLevel: int32
 m_bClientSideRagdoll: bool
 m_nCollisionGroup: uint8
 m_bInitialized: bool
 m_flFallbackWear: float32
 m_nFallbackStatTrak: int32
 m_bvDisabledHitGroups: uint32[1]
 m_nViewModelIndex: uint32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_nHierarchyId: uint16
 m_nSubclassID: CUtlStringToken
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_bShouldAnimateDuringGameplayPause: bool
 m_OriginalOwnerXuidLow: uint32
 m_flRecoilIndex: float32
 m_nGlowRange: int32
 m_vDecalPosition: Vector
 m_bIsHauledBack: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_vDecalForwardAxis: Vector
 m_flFireSequenceStartTime: float32
 m_flCreateTime: GameTime_t
 m_fadeMinDist: float32
 m_iItemIDLow: uint32
 m_bNeedsBoltAction: bool
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nObjectCulling: uint8
 m_iReapplyProvisionParity: int32
 m_nFallbackPaintKit: int32
 m_pReserveAmmo: int32[2]
 m_triggerBloat: uint8
 m_bAnimGraphUpdateEnabled: bool
 m_flShadowStrength: float32
 m_iState: WeaponState_t
 m_bBurstMode: bool
 m_iBurstShotsRemaining: int32
 m_flNextSecondaryAttackTickRatio: float32
 m_iClip1: int32
 m_MoveCollide: MoveCollide_t
 m_nRenderFX: RenderFx_t
 m_vecMaxs: Vector
 m_CollisionGroup: uint8
 m_vecForce: Vector
 m_flSimulationTime: float32
 m_nRenderMode: RenderMode_t
 m_nGlowRangeMin: int32
 m_iEntityLevel: uint32
 m_iAccountID: uint32
 m_flDroppedAtTime: GameTime_t
 m_flNextPrimaryAttackTickRatio: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecMins: Vector

118 CItemDogtags
 m_OwningPlayer: CHandle< CCSPlayerPawnBase >
 m_flAnimTime: float32
 m_flCreateTime: GameTime_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_nInteractsAs: uint64
 m_nEnablePhysics: uint8
 m_flGlowBackfaceMult: float32
 m_flShadowStrength: float32
 m_flSimulationTime: float32
 m_LightGroup: CUtlStringToken
 m_nInteractsExclude: uint64
 m_nHierarchyId: uint16
 m_nSolidType: SolidType_t
 m_nAddDecal: int32
 CBodyComponent: CBodyComponent
  m_vecY: CNetworkedQuantizedFloat
  m_flScale: float32
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellZ: uint16
  m_nNewSequenceParity: int32
  m_flWeight: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_bUseParentRenderBounds: bool
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_name: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nIdealMotionType: int8
  m_vecX: CNetworkedQuantizedFloat
  m_hSequence: HSequence
  m_nResetEventsParity: int32
  m_materialGroup: CUtlStringToken
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_bIsAnimationEnabled: bool
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_flCycle: float32
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flPrevCycle: float32
  m_hierarchyAttachName: CUtlStringToken
  m_nOutsideWorld: uint16
  m_MeshGroupMask: uint64
  m_nHitboxSet: uint8
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_vecZ: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_flLastTeleportTime: float32
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nBoolVariablesCount: int32
  m_cellY: uint16
  m_angRotation: QAngle
  m_bClientSideAnimation: bool
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_nRandomSeedOffset: int32
 m_bClientSideRagdoll: bool
 m_bRenderToCubemaps: bool
 m_flCapsuleRadius: float32
 m_bAnimGraphUpdateEnabled: bool
 m_bClientRagdoll: bool
 m_KillingPlayer: CHandle< CCSPlayerPawnBase >
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_fEffects: uint32
 m_nCollisionFunctionMask: uint8
 m_CollisionGroup: uint8
 m_vCapsuleCenter2: Vector
 m_bEligibleForScreenHighlight: bool
 m_triggerBloat: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_nGlowRangeMin: int32
 m_fadeMinDist: float32
 m_vDecalPosition: Vector
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_Values: Vector4D
  m_ID: CUtlStringToken
 m_vecSpecifiedSurroundingMaxs: Vector
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_iTeamNum: uint8
 m_nInteractsWith: uint64
 m_flGlowTime: float32
 m_vDecalForwardAxis: Vector
 m_vecForce: Vector
 m_MoveType: MoveType_t
 m_nEntityId: uint32
 m_vecMaxs: Vector
 m_usSolidFlags: uint8
 m_flFadeScale: float32
 m_nObjectCulling: uint8
 m_bShouldAnimateDuringGameplayPause: bool
 m_bInitiallyPopulateInterpHistory: bool
 m_nForceBone: int32
 m_flElasticity: float32
 m_vCapsuleCenter1: Vector
 m_clrRender: Color
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_fadeMaxDist: float32
 m_MoveCollide: MoveCollide_t
 m_bSimulatedEveryTick: bool
 m_bAnimatedEveryTick: bool
 m_nRenderFX: RenderFx_t
 m_nCollisionGroup: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_flGlowStartTime: float32
 m_ubInterpolationFrame: uint8
 m_nRenderMode: RenderMode_t
 m_nOwnerId: uint32
 m_iGlowTeam: int32
 m_iGlowType: int32
 m_glowColorOverride: Color
 m_bFlashing: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_bvDisabledHitGroups: uint32[1]
 CRenderComponent: CRenderComponent
 m_nSubclassID: CUtlStringToken
 m_vecMins: Vector
 m_nGlowRange: int32
 m_flDecalHealBloodRate: float32
 m_flDecalHealHeightRate: float32

120 CKnifeGG
 m_flPostponeFireReadyTime: GameTime_t
 m_nNextSecondaryAttackTick: GameTick_t
 m_bEligibleForScreenHighlight: bool
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_bShouldAnimateDuringGameplayPause: bool
 m_iItemIDHigh: uint32
 m_nFallbackStatTrak: int32
 m_nSubclassID: CUtlStringToken
 m_clrRender: Color
 m_usSolidFlags: uint8
 m_nGlowRange: int32
 m_CollisionGroup: uint8
 m_flGlowStartTime: float32
 m_nObjectCulling: uint8
 m_vecForce: Vector
 m_szCustomName: char[161]
 m_flFallbackWear: float32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_hOwner: CHandle< CBaseEntity >
  m_Transforms: CNetworkUtlVectorBase< CTransform >
 m_iEntityLevel: uint32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flNavIgnoreUntilTime: GameTime_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nGlowRangeMin: int32
 m_LightGroup: CUtlStringToken
 m_nEnablePhysics: uint8
 CRenderComponent: CRenderComponent
 m_nViewModelIndex: uint32
 m_vecMins: Vector
 m_bInitialized: bool
 m_iState: WeaponState_t
 m_nInteractsAs: uint64
 m_iGlowType: int32
 m_iGlowTeam: int32
 m_fadeMinDist: float32
 m_flShadowStrength: float32
 m_iClip1: int32
 m_flElasticity: float32
 m_nSurroundType: SurroundingBoundsType_t
 m_vCapsuleCenter2: Vector
 m_iClip2: int32
 m_flCapsuleRadius: float32
 m_fAccuracyPenalty: float32
 m_bIsHauledBack: bool
 m_vecSpecifiedSurroundingMins: Vector
 m_vDecalPosition: Vector
 m_flDecalHealBloodRate: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_nNextThinkTick: GameTick_t
 m_pReserveAmmo: int32[2]
 m_flAnimTime: float32
 m_flDroppedAtTime: GameTime_t
 m_bvDisabledHitGroups: uint32[1]
 m_weaponMode: CSWeaponMode
 m_nNextPrimaryAttackTick: GameTick_t
 m_nEntityId: uint32
 m_flGlowTime: float32
 m_iReapplyProvisionParity: int32
 m_iItemIDLow: uint32
 m_flCreateTime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_nInteractsWith: uint64
 m_vDecalForwardAxis: Vector
 m_flSimulationTime: float32
 m_vecMaxs: Vector
 m_nSolidType: SolidType_t
 m_flGlowBackfaceMult: float32
 m_flNextPrimaryAttackTickRatio: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_bRenderToCubemaps: bool
 m_nOwnerId: uint32
 m_flRecoilIndex: float32
 m_nCollisionGroup: uint8
 m_vCapsuleCenter1: Vector
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_bClientSideRagdoll: bool
 m_vecSpecifiedSurroundingMaxs: Vector
 m_hOuter: CHandle< CBaseEntity >
 m_bReloadVisuallyComplete: bool
 m_nRenderMode: RenderMode_t
 m_nCollisionFunctionMask: uint8
 m_bClientRagdoll: bool
 m_iInventoryPosition: uint32
 m_MoveType: MoveType_t
 m_iTeamNum: uint8
 m_bSimulatedEveryTick: bool
 m_triggerBloat: uint8
 m_flDecalHealHeightRate: float32
 m_OriginalOwnerXuidHigh: uint32
 m_iNumEmptyAttacks: int32
 m_iIronSightMode: int32
 m_nRenderFX: RenderFx_t
 m_glowColorOverride: Color
 m_fadeMaxDist: float32
 m_nDropTick: GameTick_t
 m_nHierarchyId: uint16
 m_bFlashing: bool
 m_bBurstMode: bool
 m_OriginalOwnerXuidLow: uint32
 m_nFireSequenceStartTimeChange: int32
 m_iRecoilIndex: int32
 m_fLastShotTime: GameTime_t
 CBodyComponent: CBodyComponent
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
 m_flFadeScale: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_nForceBone: int32
 m_bPlayerFireEventIsPrimary: bool
 m_iOriginalTeamNumber: int32
 m_nAddDecal: int32
 m_bAnimGraphUpdateEnabled: bool
 m_vLookTargetPosition: Vector
 m_flFireSequenceStartTime: float32
 m_MoveCollide: MoveCollide_t
 m_bAnimatedEveryTick: bool
 m_nFallbackSeed: int32
 m_bInReload: bool
 m_flNextSecondaryAttackTickRatio: float32
 m_fEffects: uint32
 m_nInteractsExclude: uint64
 m_iItemDefinitionIndex: uint16
 m_nFallbackPaintKit: int32
 m_ProviderType: attributeprovidertypes_t
 m_iEntityQuality: int32
 m_iAccountID: uint32
 m_bSilencerOn: bool

150 CPointClientUIDialog
 m_fadeMaxDist: float32
 m_nAddDecal: int32
 m_bEnabled: bool
 m_fEffects: uint32
 m_nRenderMode: RenderMode_t
 m_clrRender: Color
 m_vCapsuleCenter2: Vector
 m_nSolidType: SolidType_t
 m_flGlowBackfaceMult: float32
 m_hActivator: CHandle< CBaseEntity >
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flElasticity: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vecMins: Vector
 m_MoveType: MoveType_t
 m_nEntityId: uint32
 m_nOwnerId: uint32
 m_nGlowRangeMin: int32
 m_nObjectCulling: uint8
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nInteractsAs: uint64
 m_nSurroundType: SurroundingBoundsType_t
 m_CollisionGroup: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_vCapsuleCenter1: Vector
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_PanelID: CUtlSymbolLarge
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_ubInterpolationFrame: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_nInteractsWith: uint64
 m_MoveCollide: MoveCollide_t
 m_triggerBloat: uint8
 m_flCapsuleRadius: float32
 m_iGlowTeam: int32
 m_nGlowRange: int32
 m_usSolidFlags: uint8
 m_iGlowType: int32
 m_flGlowTime: float32
 m_vDecalForwardAxis: Vector
 m_flSimulationTime: float32
 CBodyComponent: CBodyComponent
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
 m_nRenderFX: RenderFx_t
 m_nCollisionGroup: uint8
 m_flShadowStrength: float32
 m_DialogXMLName: CUtlSymbolLarge
 m_nSubclassID: CUtlStringToken
 m_LightGroup: CUtlStringToken
 m_vecMaxs: Vector
 m_bEligibleForScreenHighlight: bool
 m_flDecalHealBloodRate: float32
 m_PanelClassName: CUtlSymbolLarge
 m_flAnimTime: float32
 m_nHierarchyId: uint16
 m_fadeMinDist: float32
 m_vDecalPosition: Vector
 m_flFadeScale: float32
 m_flCreateTime: GameTime_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bFlashing: bool
 m_flGlowStartTime: float32
 m_flDecalHealHeightRate: float32
 m_glowColorOverride: Color
 m_nEnablePhysics: uint8
 m_iTeamNum: uint8
 m_bAnimatedEveryTick: bool
 m_bRenderToCubemaps: bool
 m_nInteractsExclude: uint64
 m_bSimulatedEveryTick: bool
 m_nCollisionFunctionMask: uint8
 m_bvDisabledHitGroups: uint32[1]
 CRenderComponent: CRenderComponent

168 CSensorGrenade
 m_nInteractsWith: uint64
 m_vecMins: Vector
 m_flGlowStartTime: float32
 m_fThrowTime: GameTime_t
 m_flDecalHealBloodRate: float32
 m_iEntityLevel: uint32
 m_bInitialized: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_usSolidFlags: uint8
 m_vCapsuleCenter2: Vector
 m_nGlowRangeMin: int32
 m_vLookTargetPosition: Vector
 m_nNextSecondaryAttackTick: GameTick_t
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSurroundType: SurroundingBoundsType_t
 m_iReapplyProvisionParity: int32
 m_bBurstMode: bool
 m_MoveCollide: MoveCollide_t
 m_nCollisionFunctionMask: uint8
 m_flFadeScale: float32
 m_iRecoilIndex: int32
 m_bPinPulled: bool
 m_flThrowStrengthApproach: float32
 m_nNextThinkTick: GameTick_t
 m_flElasticity: float32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_iOriginalTeamNumber: int32
 m_eThrowStatus: EGrenadeThrowState
 m_pReserveAmmo: int32[2]
 m_nRenderMode: RenderMode_t
 m_nForceBone: int32
 m_iItemDefinitionIndex: uint16
 m_flCreateTime: GameTime_t
 m_bShouldAnimateDuringGameplayPause: bool
 m_bClientRagdoll: bool
 m_hOuter: CHandle< CBaseEntity >
 m_nNextPrimaryAttackTick: GameTick_t
 m_flSimulationTime: float32
 m_bAnimGraphUpdateEnabled: bool
 m_iInventoryPosition: uint32
 m_flFallbackWear: float32
 m_nViewModelIndex: uint32
 m_bSimulatedEveryTick: bool
 m_CollisionGroup: uint8
 m_iGlowTeam: int32
 m_fadeMaxDist: float32
 m_weaponMode: CSWeaponMode
 m_fDropTime: GameTime_t
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_iAccountID: uint32
 m_nFallbackPaintKit: int32
 m_nFallbackSeed: int32
 m_nGlowRange: int32
 m_flShadowStrength: float32
 m_OriginalOwnerXuidHigh: uint32
 m_nFallbackStatTrak: int32
 m_nEnablePhysics: uint8
 m_nFireSequenceStartTimeChange: int32
 m_fLastShotTime: GameTime_t
 m_fAccuracyPenalty: float32
 m_bIsHeldByPlayer: bool
 m_nRenderFX: RenderFx_t
 m_vecSpecifiedSurroundingMins: Vector
 m_iTeamNum: uint8
 m_nHierarchyId: uint16
 m_bEligibleForScreenHighlight: bool
 m_nOwnerId: uint32
 m_nSolidType: SolidType_t
 m_iItemIDHigh: uint32
 m_OriginalOwnerXuidLow: uint32
 m_bIsHauledBack: bool
 m_nDropTick: GameTick_t
 m_iIronSightMode: int32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vDecalForwardAxis: Vector
 m_flFireSequenceStartTime: float32
 m_ubInterpolationFrame: uint8
 m_flGlowTime: float32
 m_flGlowBackfaceMult: float32
 m_nAddDecal: int32
 m_vecForce: Vector
 m_flDroppedAtTime: GameTime_t
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nInteractsAs: uint64
 m_nInteractsExclude: uint64
 m_bInReload: bool
 m_LightGroup: CUtlStringToken
 m_vCapsuleCenter1: Vector
 m_bRedraw: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_clrRender: Color
 m_bRenderToCubemaps: bool
 m_iGlowType: int32
 m_iEntityQuality: int32
 m_flThrowStrength: float32
 m_bClientSideRagdoll: bool
 m_bAnimatedEveryTick: bool
 m_nCollisionGroup: uint8
 m_flCapsuleRadius: float32
 m_fadeMinDist: float32
 m_MoveType: MoveType_t
 m_nEntityId: uint32
 m_iClip2: int32
 m_iClip1: int32
 m_nSubclassID: CUtlStringToken
 m_iItemIDLow: uint32
 m_iState: WeaponState_t
 m_fEffects: uint32
 m_vDecalPosition: Vector
 m_flPostponeFireReadyTime: GameTime_t
 m_bReloadVisuallyComplete: bool
 m_iNumEmptyAttacks: int32
 m_bJumpThrow: bool
 m_bvDisabledHitGroups: uint32[1]
 m_flDecalHealHeightRate: float32
 m_ProviderType: attributeprovidertypes_t
 m_bSilencerOn: bool
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_flNextPrimaryAttackTickRatio: float32
 m_flNextSecondaryAttackTickRatio: float32
 m_flAnimTime: float32
 m_triggerBloat: uint8
 m_glowColorOverride: Color
 m_bInitiallyPopulateInterpHistory: bool
 m_szCustomName: char[161]
 m_flRecoilIndex: float32
 CRenderComponent: CRenderComponent
 CBodyComponent: CBodyComponent
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
 m_bFlashing: bool
 m_nObjectCulling: uint8
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_vecMaxs: Vector
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bPlayerFireEventIsPrimary: bool

8 CBaseDoor
 m_nEntityId: uint32
 m_nCollisionGroup: uint8
 m_iGlowType: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_MoveCollide: MoveCollide_t
 m_nInteractsAs: uint64
 m_nSurroundType: SurroundingBoundsType_t
 m_vCapsuleCenter1: Vector
 m_nObjectCulling: uint8
 m_vDecalPosition: Vector
 m_nRenderMode: RenderMode_t
 m_usSolidFlags: uint8
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_iGlowTeam: int32
 m_vecMins: Vector
 m_nSolidType: SolidType_t
 m_triggerBloat: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_nAddDecal: int32
 CRenderComponent: CRenderComponent
 m_flSimulationTime: float32
 m_flCreateTime: GameTime_t
 m_clrRender: Color
 m_bRenderToCubemaps: bool
 m_flCapsuleRadius: float32
 m_flDecalHealHeightRate: float32
 m_nSubclassID: CUtlStringToken
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vDecalForwardAxis: Vector
 m_nInteractsWith: uint64
 m_nGlowRange: int32
 m_LightGroup: CUtlStringToken
 m_CollisionGroup: uint8
 m_glowColorOverride: Color
 m_flGlowStartTime: float32
 m_bSimulatedEveryTick: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_bFlashing: bool
 m_fadeMinDist: float32
 m_flDecalHealBloodRate: float32
 m_iTeamNum: uint8
 m_vecMaxs: Vector
 m_ubInterpolationFrame: uint8
 m_flGlowTime: float32
 m_flGlowBackfaceMult: float32
 m_fadeMaxDist: float32
 m_flShadowStrength: float32
 m_bIsUsable: bool
 CBodyComponent: CBodyComponent
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
 m_MoveType: MoveType_t
 m_nEnablePhysics: uint8
 m_bEligibleForScreenHighlight: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_nHierarchyId: uint16
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vCapsuleCenter2: Vector
 m_flFadeScale: float32
 m_flAnimTime: float32
 m_fEffects: uint32
 m_nGlowRangeMin: int32
 m_bvDisabledHitGroups: uint32[1]
 m_flElasticity: float32
 m_nRenderFX: RenderFx_t
 m_bAnimatedEveryTick: bool
 m_nOwnerId: uint32
 m_nCollisionFunctionMask: uint8
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nInteractsExclude: uint64

63 CDynamicLight
 m_fadeMaxDist: float32
 m_flShadowStrength: float32
 m_flSimulationTime: float32
 CBodyComponent: CBodyComponent
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
 m_nRenderMode: RenderMode_t
 m_nSolidType: SolidType_t
 m_vecSpecifiedSurroundingMaxs: Vector
 m_iGlowTeam: int32
 m_Flags: uint8
 CRenderComponent: CRenderComponent
 m_nInteractsWith: uint64
 m_vDecalForwardAxis: Vector
 m_Exponent: int32
 m_flAnimTime: float32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flCapsuleRadius: float32
 m_flFadeScale: float32
 m_nHierarchyId: uint16
 m_triggerBloat: uint8
 m_nEnablePhysics: uint8
 m_SpotRadius: float32
 m_nInteractsAs: uint64
 m_vecMins: Vector
 m_vecMaxs: Vector
 m_iTeamNum: uint8
 m_clrRender: Color
 m_vecSpecifiedSurroundingMins: Vector
 m_nGlowRangeMin: int32
 m_flGlowTime: float32
 m_OuterAngle: float32
 m_MoveType: MoveType_t
 m_nInteractsExclude: uint64
 m_usSolidFlags: uint8
 m_bEligibleForScreenHighlight: bool
 m_flDecalHealBloodRate: float32
 m_flNavIgnoreUntilTime: GameTime_t
 m_bvDisabledHitGroups: uint32[1]
 m_ubInterpolationFrame: uint8
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_bRenderToCubemaps: bool
 m_CollisionGroup: uint8
 m_flGlowBackfaceMult: float32
 m_LightStyle: uint8
 m_bFlashing: bool
 m_flGlowStartTime: float32
 m_MoveCollide: MoveCollide_t
 m_nSubclassID: CUtlStringToken
 m_flCreateTime: GameTime_t
 m_fEffects: uint32
 m_nSurroundType: SurroundingBoundsType_t
 m_nGlowRange: int32
 m_nAddDecal: int32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_LightGroup: CUtlStringToken
 m_nOwnerId: uint32
 m_nCollisionFunctionMask: uint8
 m_Radius: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nEntityId: uint32
 m_InnerAngle: float32
 m_flDecalHealHeightRate: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flElasticity: float32
 m_bAnimatedEveryTick: bool
 m_iGlowType: int32
 m_glowColorOverride: Color
 m_bSimulatedEveryTick: bool
 m_vCapsuleCenter1: Vector
 m_fadeMinDist: float32
 m_vDecalPosition: Vector
 m_nRenderFX: RenderFx_t
 m_nCollisionGroup: uint8
 m_vCapsuleCenter2: Vector
 m_nObjectCulling: uint8

216 CWeaponMP9
 m_fadeMinDist: float32
 m_nAddDecal: int32
 m_bClientRagdoll: bool
 m_OriginalOwnerXuidLow: uint32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_nNextThinkTick: GameTick_t
 m_LightGroup: CUtlStringToken
 m_vDecalForwardAxis: Vector
 m_pReserveAmmo: int32[2]
 m_clrRender: Color
 m_nCollisionGroup: uint8
 m_nGlowRange: int32
 m_nGlowRangeMin: int32
 m_hOuter: CHandle< CBaseEntity >
 m_iItemIDHigh: uint32
 m_OriginalOwnerXuidHigh: uint32
 m_bReloadVisuallyComplete: bool
 m_iTeamNum: uint8
 m_bClientSideRagdoll: bool
 m_iItemIDLow: uint32
 CRenderComponent: CRenderComponent
 m_nNextSecondaryAttackTick: GameTick_t
 m_flNavIgnoreUntilTime: GameTime_t
 m_bNeedsBoltAction: bool
 m_ubInterpolationFrame: uint8
 m_vCapsuleCenter1: Vector
 m_bvDisabledHitGroups: uint32[1]
 CBodyComponent: CBodyComponent
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_angRotation: QAngle
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
 m_vecMaxs: Vector
 m_flGlowTime: float32
 m_nInteractsAs: uint64
 m_MoveType: MoveType_t
 m_nCollisionFunctionMask: uint8
 m_nEnablePhysics: uint8
 m_flCapsuleRadius: float32
 m_bInReload: bool
 m_flNextSecondaryAttackTickRatio: float32
 m_nViewModelIndex: uint32
 m_flAnimTime: float32
 m_nEntityId: uint32
 m_nFallbackPaintKit: int32
 m_iRecoilIndex: int32
 m_flCreateTime: GameTime_t
 m_bSimulatedEveryTick: bool
 m_vecSpecifiedSurroundingMaxs: Vector
 m_iItemDefinitionIndex: uint16
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_nForceBone: int32
 m_flElasticity: float32
 m_bIsHauledBack: bool
 m_bEligibleForScreenHighlight: bool
 m_triggerBloat: uint8
 m_bInitiallyPopulateInterpHistory: bool
 m_nInteractsExclude: uint64
 m_iReapplyProvisionParity: int32
 m_iState: WeaponState_t
 m_bPlayerFireEventIsPrimary: bool
 m_flDroppedAtTime: GameTime_t
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_vecForce: Vector
 m_flDecalHealHeightRate: float32
 m_nRenderMode: RenderMode_t
 m_vCapsuleCenter2: Vector
 m_bFlashing: bool
 m_flGlowStartTime: float32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_iBurstShotsRemaining: int32
 m_bAnimatedEveryTick: bool
 m_vecMins: Vector
 m_flDecalHealBloodRate: float32
 m_bAnimGraphUpdateEnabled: bool
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
 m_iIronSightMode: int32
 m_iClip1: int32
 m_iGlowType: int32
 m_glowColorOverride: Color
 m_fadeMaxDist: float32
 m_szCustomName: char[161]
 m_zoomLevel: int32
 m_iClip2: int32
 m_nRenderFX: RenderFx_t
 m_nDropTick: GameTick_t
 m_iEntityLevel: uint32
 m_MoveCollide: MoveCollide_t
 m_bRenderToCubemaps: bool
 m_nOwnerId: uint32
 m_nHierarchyId: uint16
 m_nSolidType: SolidType_t
 m_CollisionGroup: uint8
 m_iGlowTeam: int32
 m_vDecalPosition: Vector
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fAccuracyPenalty: float32
 m_nInteractsWith: uint64
 m_vecSpecifiedSurroundingMins: Vector
 m_iEntityQuality: int32
 m_flPostponeFireReadyTime: GameTime_t
 m_fEffects: uint32
 m_nFireSequenceStartTimeChange: int32
 m_flFallbackWear: float32
 m_nSubclassID: CUtlStringToken
 m_nObjectCulling: uint8
 m_iInventoryPosition: uint32
 m_weaponMode: CSWeaponMode
 m_fLastShotTime: GameTime_t
 m_flSimulationTime: float32
 m_nSurroundType: SurroundingBoundsType_t
 m_flShadowStrength: float32
 m_bBurstMode: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_flGlowBackfaceMult: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_iAccountID: uint32
 m_nFallbackSeed: int32
 m_flRecoilIndex: float32
 m_bSilencerOn: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_flFadeScale: float32
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_ProviderType: attributeprovidertypes_t
 m_bInitialized: bool
 m_iOriginalTeamNumber: int32
 m_usSolidFlags: uint8
 m_nFallbackStatTrak: int32
 m_flFireSequenceStartTime: float32
 m_iNumEmptyAttacks: int32
 m_nNextPrimaryAttackTick: GameTick_t
 m_flNextPrimaryAttackTickRatio: float32
 m_vLookTargetPosition: Vector

222 CWeaponSCAR20
 m_fLastShotTime: GameTime_t
 m_nInteractsWith: uint64
 m_bEligibleForScreenHighlight: bool
 m_hOuter: CHandle< CBaseEntity >
 m_nFallbackPaintKit: int32
 m_bReloadVisuallyComplete: bool
 m_iClip1: int32
 m_LightGroup: CUtlStringToken
 m_nGlowRange: int32
 m_flRecoilIndex: float32
 m_fEffects: uint32
 m_szCustomName: char[161]
 m_weaponMode: CSWeaponMode
 m_nInteractsExclude: uint64
 m_nSolidType: SolidType_t
 m_flFallbackWear: float32
 m_iNumEmptyAttacks: int32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_clrRender: Color
 m_vCapsuleCenter2: Vector
 m_flFadeScale: float32
 m_ProviderType: attributeprovidertypes_t
 m_nFireSequenceStartTimeChange: int32
 m_flCapsuleRadius: float32
 m_nNextPrimaryAttackTick: GameTick_t
 m_MoveType: MoveType_t
 m_vecMins: Vector
 m_bInitiallyPopulateInterpHistory: bool
 m_iItemIDHigh: uint32
 m_nCollisionFunctionMask: uint8
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_vDecalPosition: Vector
 m_fAccuracyPenalty: float32
 m_flDroppedAtTime: GameTime_t
 m_iInventoryPosition: uint32
 m_bBurstMode: bool
 m_iClip2: int32
 m_flElasticity: float32
 m_nAddDecal: int32
 m_flFireSequenceStartTime: float32
 m_bSilencerOn: bool
 m_zoomLevel: int32
 m_triggerBloat: uint8
 m_CollisionGroup: uint8
 m_nGlowRangeMin: int32
 m_iEntityLevel: uint32
 m_flNextSecondaryAttackTickRatio: float32
 m_iState: WeaponState_t
 m_bInReload: bool
 m_flSimulationTime: float32
 m_bAnimatedEveryTick: bool
 m_vecSpecifiedSurroundingMins: Vector
 m_iItemIDLow: uint32
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
 m_vCapsuleCenter1: Vector
 m_bvDisabledHitGroups: uint32[1]
 CRenderComponent: CRenderComponent
 m_nRenderFX: RenderFx_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_bRenderToCubemaps: bool
 m_flDecalHealHeightRate: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_MoveCollide: MoveCollide_t
 m_nObjectCulling: uint8
 m_nHierarchyId: uint16
 m_usSolidFlags: uint8
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_bInitialized: bool
 m_flGlowTime: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_vDecalForwardAxis: Vector
 m_flPostponeFireReadyTime: GameTime_t
 m_nDropTick: GameTick_t
 m_iIronSightMode: int32
 m_vecMaxs: Vector
 m_iItemDefinitionIndex: uint16
 m_bClientRagdoll: bool
 m_nFallbackSeed: int32
 m_bIsHauledBack: bool
 m_nInteractsAs: uint64
 m_nEntityId: uint32
 m_flGlowBackfaceMult: float32
 m_iAccountID: uint32
 CBodyComponent: CBodyComponent
  m_angRotation: QAngle
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_hSequence: HSequence
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nOutsideWorld: uint16
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
 m_ubInterpolationFrame: uint8
 m_nOwnerId: uint32
 m_glowColorOverride: Color
 m_iRecoilIndex: int32
 m_nSubclassID: CUtlStringToken
 m_nRenderMode: RenderMode_t
 m_iGlowType: int32
 m_flShadowStrength: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_fadeMinDist: float32
 m_bPlayerFireEventIsPrimary: bool
 m_iGlowTeam: int32
 m_iOriginalTeamNumber: int32
 m_iBurstShotsRemaining: int32
 m_flAnimTime: float32
 m_flCreateTime: GameTime_t
 m_bClientSideRagdoll: bool
 m_nEnablePhysics: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_bFlashing: bool
 m_vLookTargetPosition: Vector
 m_iReapplyProvisionParity: int32
 m_nFallbackStatTrak: int32
 m_nCollisionGroup: uint8
 m_vecForce: Vector
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_bNeedsBoltAction: bool
 m_flNextPrimaryAttackTickRatio: float32
 m_bSimulatedEveryTick: bool
 m_nForceBone: int32
 m_OriginalOwnerXuidLow: uint32
 m_OriginalOwnerXuidHigh: uint32
 m_nNextSecondaryAttackTick: GameTick_t
 m_fadeMaxDist: float32
 m_bAnimGraphUpdateEnabled: bool
 m_iEntityQuality: int32
 m_nNextThinkTick: GameTick_t
 m_iTeamNum: uint8
 m_flNavIgnoreUntilTime: GameTime_t
 m_nSurroundType: SurroundingBoundsType_t
 m_pReserveAmmo: int32[2]
 m_hEffectEntity: CHandle< CBaseEntity >
 m_flGlowStartTime: float32
 m_flDecalHealBloodRate: float32
 m_nViewModelIndex: uint32

15 CBasePlayerWeapon
 m_nSubclassID: CUtlStringToken
 m_flElasticity: float32
 m_vecSpecifiedSurroundingMaxs: Vector
 m_nForceBone: int32
 m_ubInterpolationFrame: uint8
 m_vecMaxs: Vector
 m_flGlowBackfaceMult: float32
 m_hOuter: CHandle< CBaseEntity >
 m_vCapsuleCenter1: Vector
 m_vecForce: Vector
 m_bClientRagdoll: bool
 m_iItemIDLow: uint32
 m_bvDisabledHitGroups: uint32[1]
 m_MoveCollide: MoveCollide_t
 m_nInteractsWith: uint64
 m_nInteractsAs: uint64
 m_nEntityId: uint32
 m_triggerBloat: uint8
 m_vecSpecifiedSurroundingMins: Vector
 m_flCapsuleRadius: float32
 m_nGlowRange: int32
 m_vDecalPosition: Vector
 m_nNextPrimaryAttackTick: GameTick_t
 m_hEffectEntity: CHandle< CBaseEntity >
 m_clrRender: Color
 m_bClientSideRagdoll: bool
 m_vecMins: Vector
 m_nEnablePhysics: uint8
 m_flGlowStartTime: float32
 m_iAccountID: uint32
 m_iTeamNum: uint8
 m_nRenderMode: RenderMode_t
 m_nCollisionGroup: uint8
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_CollisionGroup: uint8
 m_vDecalForwardAxis: Vector
 m_bAnimGraphUpdateEnabled: bool
 m_OriginalOwnerXuidLow: uint32
 m_nNextSecondaryAttackTick: GameTick_t
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_MoveType: MoveType_t
 m_nSurroundType: SurroundingBoundsType_t
 m_bRenderToCubemaps: bool
 m_usSolidFlags: uint8
 m_fadeMaxDist: float32
 m_ProviderType: attributeprovidertypes_t
 m_iItemIDHigh: uint32
 m_fEffects: uint32
 m_flNavIgnoreUntilTime: GameTime_t
 m_iReapplyProvisionParity: int32
 m_iClip1: int32
 m_flCreateTime: GameTime_t
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsExclude: uint64
 m_nHierarchyId: uint16
 m_OriginalOwnerXuidHigh: uint32
 m_nFallbackStatTrak: int32
 m_iState: WeaponState_t
 m_bSimulatedEveryTick: bool
 m_iInventoryPosition: uint32
 m_bAnimatedEveryTick: bool
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iAttributeDefinitionIndex: uint16
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_hOwner: CHandle< CBaseEntity >
  m_Transforms: CNetworkUtlVectorBase< CTransform >
 m_flNextSecondaryAttackTickRatio: float32
 m_nGlowRangeMin: int32
 m_flDecalHealHeightRate: float32
 m_bInitiallyPopulateInterpHistory: bool
 m_vLookTargetPosition: Vector
 m_iEntityQuality: int32
 m_pReserveAmmo: int32[2]
 m_nObjectCulling: uint8
 m_flNextPrimaryAttackTickRatio: float32
 m_iClip2: int32
 CBodyComponent: CBodyComponent
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_angRotation: QAngle
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
 m_bShouldAnimateDuringGameplayPause: bool
 CRenderComponent: CRenderComponent
 m_iGlowType: int32
 m_flGlowTime: float32
 m_nAddDecal: int32
 m_flAnimTime: float32
 m_nRenderFX: RenderFx_t
 m_nCollisionFunctionMask: uint8
 m_vCapsuleCenter2: Vector
 m_fadeMinDist: float32
 m_nNextThinkTick: GameTick_t
 m_flSimulationTime: float32
 m_glowColorOverride: Color
 m_bFlashing: bool
 m_iEntityLevel: uint32
 m_iGlowTeam: int32
 m_flShadowStrength: float32
 m_flFallbackWear: float32
 m_LightGroup: CUtlStringToken
 m_nOwnerId: uint32
 m_bEligibleForScreenHighlight: bool
 m_flFadeScale: float32
 m_nFallbackPaintKit: int32
 m_flDecalHealBloodRate: float32
 m_szCustomName: char[161]
 m_nSolidType: SolidType_t
 m_iItemDefinitionIndex: uint16
 m_bInitialized: bool
 m_nFallbackSeed: int32

135 CPathParticleRope
 m_iEffectIndex: CStrongHandle< InfoForResourceTypeIParticleSystemDefinition >
 m_MoveCollide: MoveCollide_t
 m_bSimulatedEveryTick: bool
 m_flNavIgnoreUntilTime: GameTime_t
 m_PathNodes_Position: CNetworkUtlVectorBase< Vector >
 m_PathNodes_PinEnabled: CNetworkUtlVectorBase< bool >
 m_MoveType: MoveType_t
 m_flSlack: float32
 m_ColorTint: Color
 m_bAnimatedEveryTick: bool
 m_PathNodes_Color: CNetworkUtlVectorBase< Vector >
 m_flElasticity: float32
 m_nEffectState: int32
 m_PathNodes_TangentIn: CNetworkUtlVectorBase< Vector >
 m_PathNodes_TangentOut: CNetworkUtlVectorBase< Vector >
 m_PathNodes_RadiusScale: CNetworkUtlVectorBase< float32 >
 m_flAnimTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_ubInterpolationFrame: uint8
 m_flParticleSpacing: float32
 m_flRadius: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_hEffectEntity: CHandle< CBaseEntity >
 m_fEffects: uint32
 m_flSimulationTime: float32
 m_nSubclassID: CUtlStringToken
 m_flCreateTime: GameTime_t
 CBodyComponent: CBodyComponent
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
 m_iTeamNum: uint8

203 CWeaponCSBaseGun
 m_nGlowRangeMin: int32
 m_flFadeScale: float32
 m_bClientRagdoll: bool
 m_nNextSecondaryAttackTick: GameTick_t
 m_flElasticity: float32
 m_bIsHauledBack: bool
 m_bSilencerOn: bool
 m_zoomLevel: int32
 m_iBurstShotsRemaining: int32
 m_iAccountID: uint32
 m_nEnablePhysics: uint8
 m_flDecalHealHeightRate: float32
 m_clrRender: Color
 m_ubInterpolationFrame: uint8
 m_nObjectCulling: uint8
 m_bInitiallyPopulateInterpHistory: bool
 m_bPlayerFireEventIsPrimary: bool
 CBodyComponent: CBodyComponent
  m_nRandomSeedOffset: int32
  m_hParent: CGameSceneNodeHandle
  m_OwnerOnlyPredNetVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_OwnerOnlyPredNetFloatVariables: CNetworkUtlVectorBase< float32 >
  m_angRotation: QAngle
  m_PredUInt16Variables: CNetworkUtlVectorBase< uint16 >
  m_PredVectorVariables: CNetworkUtlVectorBase< Vector >
  m_OwnerOnlyPredNetQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_flWeight: CNetworkedQuantizedFloat
  m_nAnimLoopMode: AnimLoopMode_t
  m_OwnerOnlyPredNetIntVariables: CNetworkUtlVectorBase< int32 >
  m_hSequence: HSequence
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_nIdealMotionType: int8
  m_nHitboxSet: uint8
  m_flLastTeleportTime: float32
  m_flPlaybackRate: CNetworkedQuantizedFloat
  m_PredQuaternionVariables: CNetworkUtlVectorBase< Quaternion >
  m_PredBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_flScale: float32
  m_MeshGroupMask: uint64
  m_bIsAnimationEnabled: bool
  m_materialGroup: CUtlStringToken
  m_vecZ: CNetworkedQuantizedFloat
  m_cellZ: uint16
  m_nBoolVariablesCount: int32
  m_nOutsideWorld: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_OwnerOnlyPredNetUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_nResetEventsParity: int32
  m_bUseParentRenderBounds: bool
  m_PredByteVariables: CNetworkUtlVectorBase< uint8 >
  m_PredUInt32Variables: CNetworkUtlVectorBase< uint32 >
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_PredUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_OwnerOnlyPredNetBoolVariables: CNetworkUtlVectorBase< uint32 >
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_PredFloatVariables: CNetworkUtlVectorBase< float32 >
  m_nOwnerOnlyBoolVariablesCount: int32
  m_cellX: uint16
  m_bClientSideAnimation: bool
  m_nNewSequenceParity: int32
  m_PredIntVariables: CNetworkUtlVectorBase< int32 >
  m_OwnerOnlyPredNetByteVariables: CNetworkUtlVectorBase< uint8 >
  m_OwnerOnlyPredNetUInt64Variables: CNetworkUtlVectorBase< uint64 >
  m_bClientClothCreationSuppressed: bool
 m_nNextPrimaryAttackTick: GameTick_t
 m_nNextThinkTick: GameTick_t
 m_nSolidType: SolidType_t
 m_nAddDecal: int32
 m_vecMins: Vector
 m_vCapsuleCenter2: Vector
 m_flCapsuleRadius: float32
 m_nFallbackPaintKit: int32
 m_flDroppedAtTime: GameTime_t
 m_vecSpecifiedSurroundingMins: Vector
 m_Attributes: CUtlVector< CEconItemAttribute >
  m_iRawValue32: float32
  m_flInitialValue: float32
  m_nRefundableCurrency: int32
  m_bSetBonus: bool
  m_iAttributeDefinitionIndex: uint16
 m_pRagdollPose: PhysicsRagdollPose_t*
  m_Transforms: CNetworkUtlVectorBase< CTransform >
  m_hOwner: CHandle< CBaseEntity >
 m_iInventoryPosition: uint32
 m_nFallbackSeed: int32
 m_nFallbackStatTrak: int32
 m_flTimeSilencerSwitchComplete: GameTime_t
 m_nDropTick: GameTick_t
 m_MoveCollide: MoveCollide_t
 m_nInteractsAs: uint64
 m_flDecalHealBloodRate: float32
 m_bShouldAnimateDuringGameplayPause: bool
 m_nForceBone: int32
 m_weaponMode: CSWeaponMode
 m_fAccuracyPenalty: float32
 m_nRenderFX: RenderFx_t
 m_iTeamNum: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_vecForce: Vector
 m_iClip1: int32
 m_iIronSightMode: int32
 m_iNumEmptyAttacks: int32
 m_flNavIgnoreUntilTime: GameTime_t
 m_iGlowTeam: int32
 m_nGlowRange: int32
 m_flGlowTime: float32
 m_vLookTargetPosition: Vector
 m_bInitialized: bool
 m_flNextSecondaryAttackTickRatio: float32
 m_nOwnerId: uint32
 m_MoveType: MoveType_t
 m_bRenderToCubemaps: bool
 m_vCapsuleCenter1: Vector
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_iEntityQuality: int32
 m_iRecoilIndex: int32
 m_bReloadVisuallyComplete: bool
 m_flShadowStrength: float32
 m_fEffects: uint32
 m_flRecoilIndex: float32
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_hOuter: CHandle< CBaseEntity >
 m_vDecalForwardAxis: Vector
 m_bvDisabledHitGroups: uint32[1]
 m_pReserveAmmo: int32[2]
 m_LightGroup: CUtlStringToken
 m_bEligibleForScreenHighlight: bool
 m_vDecalPosition: Vector
 m_OriginalOwnerXuidHigh: uint32
 m_bBurstMode: bool
 m_bNeedsBoltAction: bool
 m_nEntityId: uint32
 m_flFallbackWear: float32
 m_flCreateTime: GameTime_t
 m_nFireSequenceStartTimeChange: int32
 m_bInReload: bool
 m_iItemDefinitionIndex: uint16
 m_fadeMinDist: float32
 m_flFireSequenceStartTime: float32
 CRenderComponent: CRenderComponent
 m_usSolidFlags: uint8
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_CollisionGroup: uint8
 m_iState: WeaponState_t
 m_nViewModelIndex: uint32
 m_bSimulatedEveryTick: bool
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_vecMaxs: Vector
 m_OriginalOwnerXuidLow: uint32
 m_nSubclassID: CUtlStringToken
 m_nCollisionFunctionMask: uint8
 m_bAnimGraphUpdateEnabled: bool
 m_iReapplyProvisionParity: int32
 m_hPrevOwner: CHandle< CCSPlayerPawnBase >
 m_bAnimatedEveryTick: bool
 m_iOriginalTeamNumber: int32
 m_fLastShotTime: GameTime_t
 m_bClientSideRagdoll: bool
 m_ProviderType: attributeprovidertypes_t
 m_iEntityLevel: uint32
 m_iItemIDHigh: uint32
 m_iItemIDLow: uint32
 m_hEffectEntity: CHandle< CBaseEntity >
 m_bFlashing: bool
 m_flGlowBackfaceMult: float32
 m_fadeMaxDist: float32
 m_flPostponeFireReadyTime: GameTime_t
 m_flAnimTime: float32
 m_nRenderMode: RenderMode_t
 m_iClip2: int32
 m_flSimulationTime: float32
 m_nCollisionGroup: uint8
 m_vecSpecifiedSurroundingMaxs: Vector
 m_iGlowType: int32
 m_glowColorOverride: Color
 m_szCustomName: char[161]
 m_flNextPrimaryAttackTickRatio: float32
 m_nInteractsWith: uint64
 m_nHierarchyId: uint16
 m_triggerBloat: uint8
 m_flGlowStartTime: float32
 m_nInteractsExclude: uint64

18 CBaseTrigger
 m_spawnflags: uint32
 m_vDecalForwardAxis: Vector
 m_iTeamNum: uint8
 m_flElasticity: float32
 m_nHierarchyId: uint16
 m_flGlowTime: float32
 m_bEligibleForScreenHighlight: bool
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSubclassID: CUtlStringToken
 m_vecMins: Vector
 m_vecMaxs: Vector
 m_bClientSidePredicted: bool
 m_vCapsuleCenter2: Vector
 m_fadeMinDist: float32
 m_nAddDecal: int32
 CRenderComponent: CRenderComponent
 m_flAnimTime: float32
 m_vCapsuleCenter1: Vector
 m_flCapsuleRadius: float32
 m_bDisabled: bool
 m_bvDisabledHitGroups: uint32[1]
 m_flNavIgnoreUntilTime: GameTime_t
 m_glowColorOverride: Color
 m_flShadowStrength: float32
 m_vecRenderAttributes: CUtlVectorEmbeddedNetworkVar< EntityRenderAttribute_t >
  m_ID: CUtlStringToken
  m_Values: Vector4D
 m_nInteractsExclude: uint64
 m_nSolidType: SolidType_t
 m_triggerBloat: uint8
 CBodyComponent: CBodyComponent
  m_nIdealMotionType: int8
  m_nOutsideWorld: uint16
  m_cellY: uint16
  m_vecY: CNetworkedQuantizedFloat
  m_angRotation: QAngle
  m_flScale: float32
  m_name: CUtlStringToken
  m_bIsAnimationEnabled: bool
  m_bUseParentRenderBounds: bool
  m_materialGroup: CUtlStringToken
  m_bClientClothCreationSuppressed: bool
  m_nHitboxSet: uint8
  m_cellX: uint16
  m_cellZ: uint16
  m_vecZ: CNetworkedQuantizedFloat
  m_hierarchyAttachName: CUtlStringToken
  m_vecX: CNetworkedQuantizedFloat
  m_hParent: CGameSceneNodeHandle
  m_hModel: CStrongHandle< InfoForResourceTypeCModel >
  m_MeshGroupMask: uint64
 m_fEffects: uint32
 m_nRenderMode: RenderMode_t
 m_nInteractsAs: uint64
 m_iGlowTeam: int32
 m_nCollisionGroup: uint8
 m_usSolidFlags: uint8
 m_nSurroundType: SurroundingBoundsType_t
 m_iGlowType: int32
 m_nRenderFX: RenderFx_t
 m_bRenderToCubemaps: bool
 m_nCollisionFunctionMask: uint8
 m_CollisionGroup: uint8
 m_nEnablePhysics: uint8
 m_bSimulatedEveryTick: bool
 m_bAnimatedEveryTick: bool
 m_bFlashing: bool
 m_flGlowBackfaceMult: float32
 m_ConfigEntitiesToPropagateMaterialDecalsTo: CNetworkUtlVectorBase< CHandle< CBaseModelEntity > >
 m_nInteractsWith: uint64
 m_nOwnerId: uint32
 m_flGlowStartTime: float32
 m_MoveCollide: MoveCollide_t
 m_nEntityId: uint32
 m_nObjectCulling: uint8
 m_hOwnerEntity: CHandle< CBaseEntity >
 m_clrRender: Color
 m_vecSpecifiedSurroundingMaxs: Vector
 m_vDecalPosition: Vector
 m_flDecalHealBloodRate: float32
 m_flDecalHealHeightRate: float32
 m_MoveType: MoveType_t
 m_flCreateTime: GameTime_t
 m_ubInterpolationFrame: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_vecSpecifiedSurroundingMins: Vector
 m_nGlowRangeMin: int32
 m_fadeMaxDist: float32
 m_flSimulationTime: float32
 m_LightGroup: CUtlStringToken
 m_nGlowRange: int32
 m_flFadeScale: float32

34 CCSEnvGunfire
 m_MoveCollide: MoveCollide_t
 m_flNavIgnoreUntilTime: GameTime_t
 CBodyComponent: CBodyComponent
  m_hParent: CGameSceneNodeHandle
  m_angRotation: QAngle
  m_name: CUtlStringToken
  m_hierarchyAttachName: CUtlStringToken
  m_cellZ: uint16
  m_cellY: uint16
  m_vecX: CNetworkedQuantizedFloat
  m_vecY: CNetworkedQuantizedFloat
  m_vecZ: CNetworkedQuantizedFloat
  m_flScale: float32
  m_nOutsideWorld: uint16
  m_cellX: uint16
 m_ubInterpolationFrame: uint8
 m_bAnimatedEveryTick: bool
 m_flSimulationTime: float32
 m_pEntity: CEntityIdentity*
  m_nameStringableIndex: int32
 m_nSubclassID: CUtlStringToken
 m_flCreateTime: GameTime_t
 m_fEffects: uint32
 m_flElasticity: float32
 m_bSimulatedEveryTick: bool
 m_flAnimTime: float32
 m_MoveType: MoveType_t
 m_iTeamNum: uint8
 m_hEffectEntity: CHandle< CBaseEntity >
 m_hOwnerEntity: CHandle< CBaseEntity >
